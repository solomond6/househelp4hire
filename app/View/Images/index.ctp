<script>
function selectURL(url) {
	if (url == '') return false;

	url = '<?php echo Helper::url('/uploads/')?>' + url;

	field = window.top.opener.browserWin.document.forms[0].elements[window.top.opener.browserField];
	field.value = url;
	if (field.onchange != null) field.onchange();
	window.top.close();
	window.top.opener.browserWin.focus();
}
</script>
<div class="editer-uploadWrap">
<?php
    echo $this->Form->create(
        null,
        array(
            'type' => 'file',
            'url' => array(
                'action' => 'upload'
            )
        )
    );
    echo $this->Form->label(
        'Image.image',
        'Upload image'
    );
    echo $this->Form->file(
        'Image.image'
    );    
	echo $this->Form->submit('Upload', array('class' => 'btn-large btn-primary'));
    echo $this->Form->end();
?>
</div>
<div class="row-fluid">
<div class="span12"  style=" border: 1px solid #94C8B6; padding:10px;">
<?php if(isset($images[0])) {  
$i=0;
    foreach($images As $the_image) { 
	
?> 
       <div class="span2" <?php echo $i++%6==0?'style="margin-left:0;"':''?>>
<?php
echo $this->Html->link($this->Html->image('../uploads/'.$the_image['basename'], array('style' => 'height:60px; margin-bottom:10px;', 'onclick' => 'selectURL("'.$the_image['basename'].'");')), 'javascript:void(0);', array('onclick' => 'selectURL("'.$the_image['basename'].'");', 'escape' => false));
?>
       </div>
       
<?php } } ?> 
</div>
</div>
