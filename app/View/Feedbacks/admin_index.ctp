

  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Feedback Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i>Manage Feedback</h4>
                                </div>
                                <div class="widget-body">
                                <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                <thead>
									<tr>
										<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('user_id'); ?></th>
										<th><?php echo $this->Paginator->sort('comment'); ?></th>
										<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
								</thead>
								<tbody>
								<?php
									foreach ($feedbacks as $feedback): ?>
									<tr>
										<td><?php echo h($feedback['Feedback']['id']); ?>&nbsp;</td>
										<td>
											<?php echo $this->Html->link($feedback['User']['name'], array('controller' => 'users', 'action' => 'view', $feedback['User']['id'])); ?>
										</td>
										<td><?php echo h($feedback['Feedback']['comment']); ?>&nbsp;</td>
										<td class="actions">
											<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $feedback['Feedback']['id']), null, __('Are you sure you want to delete # %s?', $feedback['Feedback']['id'])); ?>
										</td>
									</tr>
								<?php endforeach; ?>
									</tbody>
								</table>
                         		<p>
								<?php
								echo $this->Paginator->counter(array('format' => __("Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%")));
								?>	
								</p>

								<div class="paging">
									<?php echo $this->Paginator->prev('Previous', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
									<?php echo $this->Paginator->numbers(array('separator'=>'')); ?>
									<?php echo $this->Paginator->next('Next', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
								</div>	
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->