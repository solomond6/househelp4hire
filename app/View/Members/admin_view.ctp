<div >
<h2><?php  __('Member');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Email Address'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['email_address']; ?>
			&nbsp;
		</dd>
		<!-- <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Password'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['password']; ?>
			&nbsp;
		</dd> -->
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('First Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['first_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['last_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Full Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Company Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['company_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Featured Company'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php if($member['Member']['featured_company']=='Y'){
				echo 'Yes';
			}
			else{
				echo 'No';
			} ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Company Logo'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['company_logo']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Industry'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($member['Industry']['name'], array('controller' => 'industries', 'action' => 'view', $member['Industry']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Address1'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['address1']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Address2'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['address2']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('City'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['city']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Country'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($member['Country']['name'], array('controller' => 'countries', 'action' => 'view', $member['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Zip Code'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['zip_code']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Telephone'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['telephone']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('News Letter'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['news_letter']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Email Format'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['email_format']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Subscription Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php if($member['Member']['subscription_status']=='Y'){
				echo 'Subscribed';
			}
			else{
				echo 'Unsubscribed';
			} ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php if($member['Member']['status']=='Y'){
				echo 'Active';
			}
			else{
				echo 'Inctive';
			} ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date Of Joining'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['date_of_joining']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date Of Expiry'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $member['Member']['date_of_expiry']; ?>
			&nbsp;
		</dd>
		<dt>&nbsp;</dt>
		<dd class="actions"><?php echo $this->Html->link(__('Back', true), array('action' => 'index')); ?> &nbsp;</dd>
	</dl>
</div>
<!-- <div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Member', true), array('action' => 'edit', $member['Member']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Member', true), array('action' => 'delete', $member['Member']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $member['Member']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Members', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Industries', true), array('controller' => 'industries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Industry', true), array('controller' => 'industries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries', true), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country', true), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Jobs', true), array('controller' => 'jobs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Job', true), array('controller' => 'jobs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resume Expertise Areas', true), array('controller' => 'resume_expertise_areas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resume Expertise Area', true), array('controller' => 'resume_expertise_areas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Upresumes', true), array('controller' => 'upresumes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upresume', true), array('controller' => 'upresumes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Jobs');?></h3>
	<?php if (!empty($member['Job'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Member Id'); ?></th>
		<th><?php __('Website'); ?></th>
		<th><?php __('Job Title'); ?></th>
		<th><?php __('Job Description'); ?></th>
		<th><?php __('Salary'); ?></th>
		<th><?php __('Employment Type Id'); ?></th>
		<th><?php __('Reference Code'); ?></th>
		<th><?php __('Job Category Id'); ?></th>
		<th><?php __('Desired Skill'); ?></th>
		<th><?php __('Other Skill'); ?></th>
		<th><?php __('Industry Experience Id'); ?></th>
		<th><?php __('Education Level Id'); ?></th>
		<th><?php __('Job Location'); ?></th>
		<th><?php __('Country Id'); ?></th>
		<th><?php __('Application Type'); ?></th>
		<th><?php __('World Region Id'); ?></th>
		<th><?php __('Date Of Post'); ?></th>
		<th><?php __('Date Of Expire'); ?></th>
		<th><?php __('Featured'); ?></th>
		<th><?php __('Fax'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($member['Job'] as $job):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $job['id'];?></td>
			<td><?php echo $job['member_id'];?></td>
			<td><?php echo $job['website'];?></td>
			<td><?php echo $job['job_title'];?></td>
			<td><?php echo $job['job_description'];?></td>
			<td><?php echo $job['salary'];?></td>
			<td><?php echo $job['employment_type_id'];?></td>
			<td><?php echo $job['reference_code'];?></td>
			<td><?php echo $job['job_category_id'];?></td>
			<td><?php echo $job['desired_skill'];?></td>
			<td><?php echo $job['other_skill'];?></td>
			<td><?php echo $job['industry_experience_id'];?></td>
			<td><?php echo $job['education_level_id'];?></td>
			<td><?php echo $job['job_location'];?></td>
			<td><?php echo $job['country_id'];?></td>
			<td><?php echo $job['application_type'];?></td>
			<td><?php echo $job['world_region_id'];?></td>
			<td><?php echo $job['date_of_post'];?></td>
			<td><?php echo $job['date_of_expire'];?></td>
			<td><?php echo $job['featured'];?></td>
			<td><?php echo $job['fax'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'jobs', 'action' => 'view', $job['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'jobs', 'action' => 'edit', $job['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'jobs', 'action' => 'delete', $job['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $job['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Job', true), array('controller' => 'jobs', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Resume Expertise Areas');?></h3>
	<?php if (!empty($member['ResumeExpertiseArea'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Member Id'); ?></th>
		<th><?php __('Job Category Id'); ?></th>
		<th><?php __('Job Subcategory Id'); ?></th>
		<th><?php __('Industry Experience Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($member['ResumeExpertiseArea'] as $resumeExpertiseArea):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $resumeExpertiseArea['id'];?></td>
			<td><?php echo $resumeExpertiseArea['member_id'];?></td>
			<td><?php echo $resumeExpertiseArea['job_category_id'];?></td>
			<td><?php echo $resumeExpertiseArea['job_subcategory_id'];?></td>
			<td><?php echo $resumeExpertiseArea['industry_experience_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'resume_expertise_areas', 'action' => 'view', $resumeExpertiseArea['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'resume_expertise_areas', 'action' => 'edit', $resumeExpertiseArea['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'resume_expertise_areas', 'action' => 'delete', $resumeExpertiseArea['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $resumeExpertiseArea['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Resume Expertise Area', true), array('controller' => 'resume_expertise_areas', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Upresumes');?></h3>
	<?php if (!empty($member['Upresume'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Member Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Post Date'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($member['Upresume'] as $upresume):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $upresume['id'];?></td>
			<td><?php echo $upresume['member_id'];?></td>
			<td><?php echo $upresume['name'];?></td>
			<td><?php echo $upresume['status'];?></td>
			<td><?php echo $upresume['post_date'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'upresumes', 'action' => 'view', $upresume['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'upresumes', 'action' => 'edit', $upresume['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'upresumes', 'action' => 'delete', $upresume['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $upresume['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Upresume', true), array('controller' => 'upresumes', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div> -->
