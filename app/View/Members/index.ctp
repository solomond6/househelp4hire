<div class="main subpageWrap">
           <div class="col-main f-right">
               <ul class="breadcrumbs">
                  <li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a> ></li>
                  <li><strong>Retail Outlets</strong></li>
               </ul>
               <h1>Retail Outlets</h1>
               <table width="100%" border="0" cellpadding="0" cellspacing="0" class="retail-outlate-table">
                   <thead>
                          <tr>
                                <th width="25%">Customer</th>
                                <th width="13%">Main Phone</th>
                                <th width="26%">Ship To Street1</th>
                                <th width="12%">Ship To City</th>
                                <th width="13%">Ship To State</th>
                                <th width="11%">Ship To Zip</th>
                          </tr>
                      </thead>
                   <tbody>
				   <?php foreach ($members as $member):?>
                      <tr>
                        <td><?php echo $member['Member']['customer']; ?></td>
						<td><?php echo $member['Member']['main_phone']; ?></td>
						<td><?php echo $member['Member']['ship-to_street1']; ?></td>
						<td><?php echo $member['Member']['ship-to_city']; ?></td>
						<td><?php echo $member['Member']['ship-to_state']; ?></td>
						<td><?php echo $member['Member']['ship-to_zip']; ?></td>
                      </tr>
					  <?php endforeach; ?>
                    </tbody>
                </table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
        </div>
           <div class="clearfix"></div>
      </div>