<div>
<?php echo $this->Form->create('Member');?>
	<fieldset>
		<legend><?php echo('Add Outlet'); ?></legend>
	<?php
		echo $this->Form->input('customer');
		echo $this->Form->input('main_phone');
		echo $this->Form->input('ship-to_street1');
		echo $this->Form->input('ship-to_city');
		echo $this->Form->input('ship-to_state');
		echo $this->Form->input('ship-to_zip');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>