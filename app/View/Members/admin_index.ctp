<div >
	<div style="float: left"><h2><?php __('Manage Retail outlets');?></h2></div>
	<div style="float: right; padding-bottom:5px;"><?php echo $this->Html->link(__('(Add New Retail outlet)', true), array('action' => 'add'), array('style'=>'color:#003D4C;')); ?></div> 
	<div style="clear: both"></div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('customer');?></th>
			<th><?php echo $this->Paginator->sort('main_phone');?></th>
			<th><?php echo $this->Paginator->sort('ship-to_state');?></th>
			<th class="actions" style="text-align:center;"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($members as $member):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $member['Member']['id']; ?>&nbsp;</td>
		<td><?php echo $member['Member']['customer']; ?>&nbsp;</td>
		<td><?php echo $member['Member']['main_phone']; ?>&nbsp;</td>
		<td><?php echo $member['Member']['ship-to_state']; ?>&nbsp;</td>
		<td class="actions" style="text-align:center;">
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $member['Member']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $member['Member']['id']), array('class'=>'ask'), null, sprintf(__('Are you sure you want to delete # %s?', true), $member['Member']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>