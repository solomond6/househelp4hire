	  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Newsletter Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-forward"></i> send Newsletter</h4>
                                </div>
                                <div class="widget-body form">
                                    <div class="row-fluid">
                                        <div class="span8">
                                            <div class="form-horizontal">
                                                <div class="news-b">
                                                <div class="control-group">
                                                    <h5 style="">Send Newsletters by uploading xls file</h5>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Select any Newsletter<span class="red">*</span></label>
                                                    <div class="controls">
                                                       <?php 
														echo $this->Form->create('send', array('enctype' => 'multipart/form-data'));
														echo $this->Form->input('newsletters_id', array('type'=>'select', 'options'=>$newsletters, 'label'=>'Select any newsletter'));
														echo $this->Form->input('file', array('type'=>'file', 'label'=>'Upload .csv file only'));
														echo $this->Form->hidden('mode', array('value'=>1));
														echo $this->Form->end(__('Send', true));
		?>
                                                    </div>
                                                </div>
                                               
                                                </div>
                                            </div>
                                            <div style="clear: both; margin-top: 50px;"></div>
                                             <div class="form-horizontal">
                                                <div class="news-b">
                                                <div class="control-group">
                                                    <h5 style="">Send Newsletters to all newsletter subscriber</h5>
                                                </div>
                                               <?php 
		echo $this->Form->create('send', array('enctype' => 'multipart/form-data'));
		echo $this->Form->input('newsletters_id', array('type'=>'select', 'options'=>$newsletters, 'label'=>'Select any newsletter'));
		echo $this->Form->hidden('mode', array('value'=>6));
		echo $this->Form->end(__('Send', true));
		?>
                                                </div>
                                            </div>
                                            <div style="clear: both; margin-top: 50px;"></div>
                                             <div class="form-horizontal">
                                                <div class="news-b">
                                                <div class="control-group">
                                                    <h5 style="">Send Newsletters to some admin selected newsletter subscribers</h5>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Select any Newsletter<span class="red">*</span></label>
                                                    <div class="controls">
                                                        <select class="span6">
                                                            <option>Product Newsletter</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="control-group">
                                                    <label class="control-label">Select How many users whome you want to send the newsletters<span class="red">*</span></label>
                                                    <div class="controls">
                                                   <?php 
		echo $this->Form->create('send', array('enctype' => 'multipart/form-data'));
		echo $this->Form->input('newsletters_id', array('type'=>'select', 'options'=>$newsletters, 'label'=>'Select any newsletter'));
		echo $this->Form->input('email_id', array('type'=>'select', 'multiple' => true, 'options'=>$emails, 'label'=>'Select how many users whom you want to send the newsletters', 'style'=>'height:200px;'));
		echo $this->Form->hidden('mode', array('value'=>7));
		echo $this->Form->end(__('Send', true));
		?>
                                                    </div>
                                                </div>
                                              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
</div>
