 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Admin User Manager</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>-->
                            <!--error msg end-->
                            <div class="widget">
                            	<?php echo $this->Form->create('User');?>
                                <div class="widget-title">
                                    <h4><i class="fa fa-user-plus"></i> Add New Admin User</h4>
                                </div>
                                <div class="widget-body form">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                    <label class="control-label">First Name<span class="red">*</span></label>
                                                    <div class="controls">
                                                        <?php echo $this->Form->input('first_name',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Last Name</label>
                                                    <div class="controls">
                                                        <?php echo $this->Form->input('last_name',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Password<span class="red">*</span></label>
                                                    <div class="controls">
                                                         <?php echo $this->Form->input('password',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Email<span class="red">*</span></label>
                                                    <div class="controls">
                                                         <?php echo $this->Form->input('email',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group" style="display:none">
                                                    <label class="control-label">Address<span class="red">*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="span12" />
                                                        <?php echo $this->Form->input('address',array('style'=>'display:none','value'=>'N/A')); ?>
                                                    </div>
                                                </div> 
                                                <div class="control-group">
                                                    <label class="control-label">Location<span class="red">*</span></label>
                                                    <div class="controls">
                                                        <?php echo $this->Form->input('location_id', array('options'=>$locations,'class'=>'span5')); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Phone<span class="red">*</span></label>
                                                    <div class="controls">
                                                         <?php echo $this->Form->input('phone',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Status<span class="red">*</span></label>
                                                    <div class="controls">
                                                      <?php echo $this->Form->input('status', array('class'=>'span5','type' => 'select', 'options' => array('Y' => 'Active', 'N' => 'Inactive')));?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label">Choose Access Level</label>
                                                <div class="controls">
                                                    <div class="row-fluid">
                                                        <div class="span4">
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Settings"  /> Manage Settings
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Agents"  /> Manage Agents
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Employers"  /> Manage Subscribers
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Staffs" /> Manage Staffs
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id=""  value="Payments" /> Manage Payments
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Bookings"  /> Manage Bookings
                                                            </label>
                                                             <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Admins"  /> Manage Admin User
                                                            </label>
                                                        </div>
                                                        <div class="span4">
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Categories"  /> Manage Categories
                                                            </label>
                                                            <label class="checkbox">
                                                               <input type="checkbox" name="data[User][access][]" id="" value="Locations"  /> Manage Locations
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Languages"  /> Manage Languages
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Religions"  /> Manage Religions
                                                            </label>
                                                            <label class="checkbox">
                                                               <input type="checkbox" name="data[User][access][]" id="" value="Educations"  /> Manage Educations
                                                            </label>
                                                            <label class="checkbox">
                                                               <input type="checkbox" name="data[User][access][]" id="" value="Pages"  /> Manage Pages
                                                            </label>
                                                        </div>
                                                        <div class="span4">
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Services"  /> Manage Services
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Contacts"  /> Manage Contacts
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="FAQs"  /> Manage FAQs
                                                            </label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Newsletters"  /> Manage Newsletter
                                                            </label>
														    <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Coupons"  /> Manage Coupons
                                                            </label>
                                                             <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="ImportExport"  /> Manage Import & Export
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
								<?php echo $this->Form->end();?>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->