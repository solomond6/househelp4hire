<?php
	echo $this->Html->css(array('style', 'validationEngine.jquery'));
	echo $this->Html->script(array('jquery.min', 'jquery.colorbox'));
	echo $this->Html->script(array('jquery.validationEngine', 'jquery.validationEngine-en', 'jconfirmaction.jquery'));
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#flashMessage").fadeOut(10000);
	$("#authMessage").fadeOut(10000);
	$('#overlay').click(function(){
		$('#overlay').fadeOut('fast');
	});
});
</script>
<script type="text/javascript">
   $(document).ready(function() {
	$("#UserChangeAddressInfoForm").validationEngine({scroll:true, promptPosition: "topLeft"})
   });
</script>
<h6>Update Address</h6>
<?php 
echo $this->Form->create('User', array('action'=>'changeAddressInfo'));
?>
<table class="pop" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td>
		<?php echo $this->Form->input('id', array('label'=>false, 'div'=>false)); ?>
		<p><span style="color:red;">*</span> Address Line 1</p>
		<p><?php echo $this->Form->text('address_1', array('label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
    <td>
		<p>Address Line 2 (Optional)</p>
		<p><?php echo $this->Form->text('address_2', array('label'=>false, 'div'=>false)); ?></p>
	</td>
  </tr>
  <tr>
	<td>
		<p><span style="color:red;">*</span> Country</p>
		<p><?php echo $this->Form->input('country_id', array('options'=>$cuntries, 'label'=>false, 'div'=>false, 'class'=>'validate[required')); ?></p>
	</td>
    <td>
		<p><span style="color:red;">*</span> State</p>
		<p><?php echo $this->Form->input('state', array('label'=>false, 'div'=>false, 'class'=>'validate[required')); ?></p>
	</td>
  </tr>
  <tr>
	<td>
		<p><span style="color:red;">*</span> City / Province</p>
		<p><?php echo $this->Form->input('city', array('label'=>false, 'div'=>false, 'class'=>'validate[required')); ?></p>
	</td>
	<td>
		<p><span style="color:red;">*</span> Zip Code</p>
		<p><?php echo $this->Form->input('zip_code', array('label'=>false, 'div'=>false, 'class'=>'validate[required')); ?></p>
	</td>
  </tr>
  <tr>
    <td><span style="color:red;font-weight:bold;">** Required fields.</span></td>
	<td align="right" class="btn">
	<?php echo $this->Form->submit(__('Save', true), array('div'=>false, 'class'=>'', 'style'=>'height:34px; font-weight:bold;font-size:12px; color:#fff;width:100px;margin-right:20px;border:0')); ?>
	</td>
  </tr>
</table>
<?php echo $this->Form->end(); ?>