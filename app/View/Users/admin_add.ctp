<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Subscriber Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-user-plus"></i> Add Subscriber</h4>
                                </div>
                                <?php echo $this->Form->create('User');?>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">

                                            <div class="row-fluid">

                                                <div class="span6">
                                                    <div class="control-group">
                                                        <label>First Name</label>
                                                        <?php echo $this->Form->input('first_name',array('class'=>'span12','label'=>false)); ?>
                                                        <label>Last Name</label>
                                                        <?php echo $this->Form->input('last_name',array('class'=>'span12','label'=>false)); ?>
                                                        <label>Email<span class="red">*</span></label>
                                                        <?php echo $this->Form->input('email',array('class'=>'span12','label'=>false)); ?>
                                                        <label>Phone<span class="red">*</span></label>
                                                        <?php echo $this->Form->input('phone',array('class'=>'span12','label'=>false)); ?>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="control-group">
                                                      
                                                      <?php echo $this->Form->input('status', array('type' => 'select', 'options' => array('Y' => 'Active', 'N' => 'Inactive')),array('class'=>'span5','label'=>false));?>
                                                      
                                                        <div class="controls">
                                                          <?php echo $this->Form->input('location_id', array('options'=>$locations),array('class'=>'span12','label'=>false)); ?>
                                                        </div>
                                                        <label>Address<span class="red">*</span></label>
                                                        <?php echo $this->Form->input('address',array('class'=>'span12','label'=>false)); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-lg">Submit</button>
                                    </div>
                                </div>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->