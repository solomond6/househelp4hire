   <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">New Vetting Company</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> New Vetting Company</h4>
                                </div>
                                <div class="widget-body form">
                            <?php echo $this->Form->create('User', array('enctype' => 'multipart/form-data'));?>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="control-group">
                                                        <label>Company Name</label>
                                                        <?php 
                                                            echo $this->Form->input('company_name',array('required'=>'required',  'class'=>'form-control', 'label'=>false));
                                                        ?>

                                                        <label>Company Phone Number</label>
                                                        <?php echo $this->Form->input('user_mobile',array('maxlength'=>'11','class'=>'span12', 'label'=>false));?>

                                                        <label>Company Email</label>
                                                        <?php echo $this->Form->input('company_email',array('class'=>'span12', 'label'=>false));?>
                                                        
                                                        <div class="controls">
                                                        <?php 
                                                            echo $this->Form->input('location_id', array('options'=>$locations)); 
                                                        ?>
                                                        </div> 
                                                        
                                                        <?php 
                                                            echo $this->Form->input('address',array('class'=>'input-medium span12'));
                                                        ?>

                                                        <?php 
                                                            echo $this->Form->input('vetting_category_id', array('required'=>'required',  'class'=>'form-control')); 
                                                        ?>
                                                        <?php 
                                                            echo $this->Form->input('price',array('required'=>'required',  'class'=>'form-control'));
                                                        ?>

                                                        
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="control-group">
                                                        
                                                        <label>Contact Firstname</label>
                                                        <?php echo $this->Form->input('first_name',array('class'=>'span12', 'label'=>false)); ?>
                                                        
                                                        <label>Contact Lastname</label>
                                                        <?php echo $this->Form->input('last_name',array('class'=>'span12', 'label'=>false)); ?>
                                                        
                                                        <label>Contact Email</label>
                                                        <?php echo $this->Form->input('email',array('class'=>'span12', 'label'=>false)); ?>
                                                        
                                                        <label>Contact Phone Number</label>
                                                        <?php echo $this->Form->input('phone',array('class'=>'span12', 'label'=>false)); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-lg">Submit</button>
                                    </div>
                                </div>
                            <?php echo $this->Form->end();?>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
