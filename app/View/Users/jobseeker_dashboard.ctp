
<script type="text/javascript">
	var hsi = 1;
	function tiggerFire(value, formIdToSubmit, linkIdToClick){
		if(value == 'Cancelled' || value == 'Released'){
			if(value == 'Released'){
				$('#confTxt').html('Are you sure to release this staff?');
			}
			if(value == 'Cancelled'){
				$('#confTxt').html('Are you sure to cancel this booking?');
			}
			if(hsi == '1'){
				$( "#"+linkIdToClick ).trigger( "click" );
			} 
			hsi++;
		} else {
			document.getElementById(formIdToSubmit).submit();
			//$( "#" + formIdToSubmit ).submit();
		}
		return false;
	}
</script>

<!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content bg-white pad-btm-z" style="background: #e9eaed!important;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-big" style="margin-bottom: 25px;">
                            <h3><span style="color:#666;">My Account </span></h3>
                            <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-md-4" style="margin-bottom:10px">
                        <div class="profile-big min-h-500">
                            <h4 class="text-uppercase"><i class="fa fa-user"></i> Profile Information<a href="#" data-toggle="modal" data-target="#editinfo"><i class="fa fa-edit"></i></a></h4>
                            <p><span class="text-bold">Name </span><?php if($profileInfo['User']['name']){echo $profileInfo['User']['name'];} else {echo '';} ?></p>
                            <p><span class="text-bold">Unique ID </span><?php if($profileInfo['User']['emp-uid']){echo $profileInfo['User']['emp-uid'];} else {echo '<a class="hk-link" href="#myModal" role="button" data-toggle="modal" style="color: #B94A48;">Get Your UID</a>';} ?></p>
                            <p><span class="text-bold">Email </span> <?php if($profileInfo['User']['email']){echo $profileInfo['User']['email'];} else {echo '';} ?></p>
                            <p><span class="text-bold">Phone no </span> <?php if($profileInfo['User']['phone']){echo $profileInfo['User']['phone'];} else {echo '';} ?></p>
                            <p><span class="text-bold">Joined On </span> <?php echo date('F j, Y', strtotime($profileInfo['User']['registration_date'])); ?></p>
                            <p><span class="text-bold">Location </span> <?php if($profileInfo['Location']['name']){echo $profileInfo['Location']['name'];} else {echo '';} ?></p>
                            <h4>Profile Completeness</h4>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage;?>%">
                                  <span class=""><?php echo $percentage;?>% Complete</span>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <h4 class="text-uppercase"><i class="fa fa-lock"></i> Update Profile 
                                <a href="<?php echo(DOMAIN_NAME_PATH);?>jobseeker/users/profile/<?php echo $jobseeker_id?>"><i class="fa fa-edit">
                                </i></a>
                            </h4>
                            <p>Kindly update you profile clicking on the Edit icon. </p>
                            <br/>
                        </div>
                    </div>
                    <div class="col-md-4 " style="margin-bottom:10px">
                        <div class="profile-big min-h-500">
                            <h4 class="text-uppercase"><i class="fa fa-lock"></i> Verification Status
                                <i class="fa fa-setting"></i></h4>
                                <table class="table table-striped">
                                <?php if(!empty($vetstatus)){ ?>
                                    <?php foreach($vetstatus as $vetstat){ ?>
                                        <tr>
                                            <td><b><?php echo $vetstat['a']['name']; ?></b></td>
                                            <td>
                                                <?php 
                                                    if($vetstat['b']['status'] =='0'){
                                                        echo "<button class='btn btn-sm btn-danger'>Pending</button>";
                                                    }else{
                                                        echo "<button class='btn btn-sm btn-success'>Verified</button>";
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="2">No Verification Yet</td>
                                    </tr>
                                    <tr>
                                        <td>Get Hired Faster! <a href="#learn-more" data-toggle="modal">Learn More.</a></td></td>
                                    </tr>
                                <?php } ?>
                                </table>
                                <br/>
                                <br/>
                                <h4>Choose A Verification Type</h4>
                                <table class="table">
                                <tr>
                                    <td>
                                        <select name="vatcat" class="form-control vatcat">
                                            <?php foreach($vetCategories as $vetCategory){ ?>
                                                <option value="<?php echo $vetCategory['hh_vetting_categories']['id']; ?>"><?php echo $vetCategory['hh_vetting_categories']['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>        
                                        <td>
                                            <a class="btn btn-danger vetCatId" data-toggle="modal">Apply</a>
                                        </td>
                                    </tr>
                                </table>
                            <br/>
                            <br/>
                            <div class="clear-fix"></div>
                            <h4 class="text-uppercase"><i class="fa fa-lock"></i> Change Password<a href="#" data-toggle="modal" data-target="#changepass"><i class="fa fa-edit"></i></a></h4>
                            <p>Change your password clicking on the Edit icon. </p>

                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom:10px">
                    <?php if(count($newsletter) == 0){ ?>
                    <?php echo $this->Form->create('Email', array('action' => 'add', 'novalidate'=>'')); ?>
                        <div class="profile-big min-h-500">
                            <h4 class="text-uppercase"><i class="fa fa-envelope"></i> Subscribe to our newsletter</h4>
                            <div class="form-group">
                                <p>Signup now ! Stay informed on the latest from HouseHelp4Hire!</p>
                            <label>Your Email Address :</label>
                            <?php echo $this->Form->input('email', array('type'=>'email', 'label'=>false, 'div'=>false, 'placeholder'=>'Your email address', 'class'=>'form-control', 'value'=>$profileInfo['User']['email'], 'required'=>'required')); ?>
                            </div>
                            <div class="form-group"><input type="submit"  class="btn btn-danger" value="Subscribe"></div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                    <?php } else { ?>
                        <div class="profile-big min-h-300">
                            <h4 class="text-uppercase"><i class="fa fa-envelope"></i> Subscribe to our newsletter</h4>
                            <div class="form-group">
                                <?php 
                            $string = $this->Session->read('Auth.User.email');
                            $convertedString = bin2hex(Security::cipher(serialize($string),Configure::read('Security.salt')));
                            echo $this->Html->link('Unsubscribe', array('controller'=>'Emails', 'action'=>'delete', $convertedString, 1), array('class'=>'btn btn-danger btn-danger btn-lg')); 
                            ?>                                
                            </div>

                        </div>
                    <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-big" style="margin-bottom: 100px;">
                            <h4 class="text-uppercase">Payment History</h4>
                            <?php if(count($payments) > 0){ ?>
                             <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                <thead style="background: #eee; color:#333;">
                                <tr>
                                    <th>Option</th>
                                    <th>Status</th>
                                    <th>Transaction ID</th>
                                    <th>Paymnet Date</th>
                                    <!--<th>Expiry Date</th>-->
                                    <th>Access Limit</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
	                           </thead>
                                <tbody class="pay-bor">
                                <?php foreach($payments as $payment){ ?>
            		<tr>
            		  <td><?php echo $payment['PaymentOption']['name']; ?></td>
            		  <td><?php if($payment['Payment']['gtpay_tranx_state'] == 'completed'){echo 'Subscribed';}else if($payment['Payment']['gtpay_tranx_state'] == 'pending'){echo 'Pending';} else { echo 'Expired';} ?></td>
            		  <td><?php echo $payment['Payment']['gtpay_tranx_id']; ?></td>
            		  <td><?php echo date('F j, Y', strtotime($payment['Payment']['created_at'])); ?></td>
            		  <!--<td><?php echo date('F j, Y', strtotime($payment['Payment']['expiry_date'])); ?></td>-->
            		  <td><?php echo $payment['Payment']['quantity']; ?> Profile</td>
            		  <td><?php echo $payment['Payment']['paid_amount']; ?> &#8358;</td>
            		  <td><a href="javascript:void(0);" role="button" data-toggle="modal" data-target="#access" class="btn btn-danger btn-sm" onclick="accessList('<?php echo $payment['Payment']['id']; ?>');">Contacted Profile</a>
                      <a href="javascript:void(0);" role="button" data-toggle="modal" data-target="#access" class="btn btn-info btn-sm" onclick="shortList('<?php echo $payment['Payment']['id']; ?>');">Shortlisted Profile</a>
                      </td>
            		</tr>
            		<?php } ?>
			                     </tbody>
                            </table>
                            
                            <?php } else { ?>
								<p>No payment history yet</p>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!--/portfolio-->
       
<!-- Confirm Profile model popup -->
<div id="learn-more" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Get Hired Faster!</h4>
            </div>
            <div class="modal-body">
                Get verified, complete the background checks and get hired much faster!
            </div>
        </div>
    </div>
</div>

<div id="vetModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="selectVetCom">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select a Company</h4>
            </div>
            <div class="modal-body">
            <!--<select id="vetComapny" class="form-control"></select>-->
                <table class="table vetComapny">
                    <tr>
                        <th class="name">Name</th>
                        <th class="price">Price</th>
                        <th class="action">Action</th>
                    </tr>
                </table>
            </div>
        </div>
        <div class="cardDetails">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter Your Card Information</h4>
            </div>
            <div class="modal-body">
                <form id="paymentForm" class="inline-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="card_no">Card Number:</label>
                                <input type="text" name="card_no" class="form-control card_no" placeholder="Enter Your Card No" required="required">
                                  <input type="hidden" name="amount" class="form-control amount" required="required">
                                  <input type="hidden" name="comId" class="form-control comId" required="required">
                                  <input type="hidden" name="catComId" class="form-control catComId" required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="expiry_month">Expiry Month:</label>
                                <select name="expiry_month" class="form-control expiry_month" required="required">
                                    <option value="">Expiry Month On Card</option>
                                    <option value="01">January</option>
                                    <option value="02">Febraury</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="expiry_year">Expiry Year:</label>
                                <select name="expiry_year" class="form-control expiry_year" required="required">
                                    <option value="">Expiry Year On Card</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                    <option value="25">2025</option>
                                    <option value="26">2026</option>
                                    <option value="27">2027</option>
                                    <option value="28">2028</option>
                                    <option value="29">2029</option>
                                    <option value="30">2030</option>
                                    <option value="31">2031</option>
                                    <option value="32">2032</option>
                                    <option value="33">2033</option>
                                    <option value="34">2034</option>
                                    <option value="35">2035</option>
                                    <option value="36">2036</option>
                                    <option value="37">2037</option>
                                    <option value="38">2038</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="cvv">Card CVV:</label>
                                <input type="text" name="cvv" class="form-control cvv" placeholder="Enter your card CVV" required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="pin">Pin:</label>
                                <input type="text" name="pin" class="form-control pin" placeholder="Expiry Year" required="required">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="processing" id="processing">
                    <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')) ?>
                    Processing Payment
                </div>
                <button type="button" class="btn btn-warning paymentbutton">Pay</button>
            </div>
        </div>
        <div class="otpDetails">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter OTP Sent to your phone</h4>
            </div>
            <div class="modal-body">
                <form id="otpForm" class="inline-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="card_no">OTP:</label>
                                <input type="text" name="otp" class="form-control otp" placeholder="Enter OTP Sent to your phone" required="required">
                                <input type="hidden" name="ref" class="form-control ref" required="required">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="processing1" id="processing1">
                    <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')); ?>
                    Processing Payment
                </div>
                <button type="button" class="btn btn-warning otpValidate">Validate OTP</button>
            </div>
        </div>
        <div class="failedDetails">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Failed Transaction</h4>
            </div>
            <div class="modal-body">
                <h3>Your Payment Failed, Please try again</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <div class="successDetails">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Successful Transaction</h4>
            </div>
            <div class="modal-body">
                <h3>Your Payment was successfull</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

  </div>
</div>

<div class="modal fade" id="editinfo" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <div class="modal-body pad-tpz">
                        <div class="row">
                            <div class="col-sm-12">
                            <?php echo $this->Form->create('User', array('action'=>'changeProfileInfo', 'novalidate'=>'')); ?>
						<?php echo $this->Form->input('id', array('div'=>false, 'type'=>'hidden', 'value'=>$profileInfo['User']['id'])); ?>
                                <div class="form-group mrgn-btm">
                                    <h3 class="mrgn-tpz">Update Profile Information</h3>
                                </div>
                                <fieldset style="width: 100%;">
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('first_name', array('div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['first_name'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('last_name', array('div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['last_name'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('email', array('type'=>'email', 'div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['email'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('phone', array('label'=>'Mobile Number', 'div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['phone'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                           <?php echo $this->Form->input('location_id', array('div'=>false, 'class'=>'form-control', 'empty'=>'Select Location', 'onchange'=>'locationChecking(this.value, "locMsg2")', 'selected'=>$profileInfo['User']['location_id'], 'required'=>'required')); ?>
										<br/>
										<div id="locMsg2" class="span5"></div>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <!-- <span style="color:red;" class="span5">* Required fields.</span> -->
                                </fieldset>
                                <div class="form-group">
                                    <input type="submit" value="Save" class="btn btn-danger">
                                </div>
                         <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
<!-- Change Password model popup -->
<div class="modal fade" id="changepass" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <div class="modal-body pad-tpz">
                        <div class="row">
                            <div class="col-sm-12">
                            <?php echo $this->Form->create('User', array('action'=>'changePassword', 'novalidate'=>'')); ?>
                                <div class="form-group mrgn-btm">
                                    <h3 class="mrgn-tpz">Change Password</h3>
                                </div>
                                <fieldset style="width: 100%;">
                                    <div class="form-group">
                                        <div class="controls">
                                           
                                            <?php echo $this->Form->input('old_password', array('type'=>'password', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required', 'onblur'=>'chek_old_password(this.value);')); ?>
										<span id="oldPass"></span>								
                                        <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                           <?php echo $this->Form->input('password', array('type'=>'password', 'label'=>'New Password', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required')); ?>						
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                           	<?php echo $this->Form->input('confirmpassword', array('type'=>'password', 'label'=>'Confirm New Password', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required', 'data-validation-matches-message'=>'Must match new password entered above', 'data-validation-matches-match'=>'data[User][password]')); ?>
                                           	<div class="help-block"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <input type="submit" value="Save" class="btn btn-danger">
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
<!-- Access History model popup -->
<div class="modal fade" id="access" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group mrgn-btm">
                            <h3 class="mrgn-tpz">Access History</h3>
                        </div>
                        <p>No profile contact has been viewed by you!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/additional-methods.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.selectVetCom').show();
    jQuery('.cardDetails').hide();
    jQuery("#processing").hide();
    jQuery('.otpDetails').hide();
    jQuery(".failedDetails").hide();
    jQuery(".otpDetails").hide();
    jQuery(".successDetails").hide();
    jQuery(".vetCatId").click(function(){ // Click to only happen on announce links
        jQuery('#vetModal').modal('show');
        var vetCatId = jQuery('.vatcat').val();
        jQuery('.vetComapny').find('tr').empty();
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Users/vettinCompanies/'; ?>"+vetCatId,
            success: function(data){
                if(data){
                    var opts = jQuery.parseJSON(data);
                    jQuery.each(opts, function(i, d) {        
                        tr = jQuery('<tr/>');
                        tr.append(jQuery('<td/>').html(d.hh_vetting_companies.name));
                        tr.append(jQuery('<td/>').html('NGN' +d.hh_vetting_companies.price));
                        tr.append(jQuery('<td/>').html('<button class="btn btn-danger selectedVetCom" data-price="'+d.hh_vetting_companies.price+'" data-id="'+d.hh_vetting_companies.id+'" data-cat="'+d.hh_vetting_companies.vetting_category_id+'" >Select & Pay</button>'));
                        jQuery('.vetComapny tr:last').after(tr);
                    });
                }else{
                    jQuery("#access").html(jQuery('Nothing found!').fadeIn('slow'));
                }
            }
        });
    });
    jQuery(document).on('click', '.selectedVetCom', function(){ 
        var selectedVetComId = jQuery(this).data('id');
        var selectedVetComPrice = jQuery(this).data('price');
        var selectedVetCat = jQuery(this).data('cat');
        jQuery('.amount').val(selectedVetComPrice);
        jQuery('.comId').val(selectedVetComId);
        jQuery('.catComId').val(selectedVetCat);
        jQuery('.selectVetCom').hide();
        jQuery('.cardDetails').show();
        jQuery(".failedDetails").hide();
        jQuery(".otpDetails").hide();
    });
    jQuery("#paymentForm").validate({
        rules: {
            card_no: {
              required: true,
              number: true,
              minlength: 16,
              maxlength: 16
            },
            cvv: {
              required: true,
              number: true,
              minlength: 3,
              maxlength: 3
            },
            expiry_month: {
              required: true,
              number: true,
            },
            expiry_year: {
              required: true,
              number: true,
            },
            pin: {
              required: true,
              number: true,
              minlength: 4,
              maxlength: 4
            }
        },
        messages: {
            card_no: {
              required: "Card Number Required",
              minlength: "At least 16 characters required!"
            },
            cvv: {
              required: "Card CVV Required",
              minlength: "At least 3 characters required!"
            },
            expiry_month: {
              required: "Expiry Month Required",
              minlength: "At least 2 characters required!"
            },
            expiry_year: {
              required: "Expiry Year Required",
              minlength: "At least 2 characters required!"
            },
            pin: {
              required: "Pin Required",
              minlength: "At least 4 characters required!"
            }
        },
        highlight: function(element){
            jQuery(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element){
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
    });
    jQuery("#otpForm").validate({
        rules: {
            otp: {
              required: true,
              number: true,
              minlength: 4,
              maxlength: 16
            }
        },
        messages: {
            otp: {
              required: "OTP Required",
              minlength: "At least 4 characters required!"
            }
        },
        highlight: function(element){
            jQuery(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element){
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
    });
    jQuery("#vetModal").on("hidden.bs.modal", function() {
        jQuery('.selectVetCom').show();
        jQuery('.cardDetails').hide();
        jQuery("#processing").hide();
        jQuery('.otpDetails').hide();
        jQuery(".failedDetails").hide();
    });
    jQuery(document).on('click', '.paymentbutton', function(e){
        var amount = jQuery('.amount').val();
        var comId = jQuery('.comId').val();
        var card_no = jQuery('.card_no').val();
        var cvv = jQuery('.cvv').val();
        var expiry_month = jQuery('.expiry_month').val();
        var expiry_year = jQuery('.expiry_year').val();
        if(jQuery("#paymentForm").valid()){
            jQuery("#processing").show();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo DOMAIN_NAME_PATH.'Users/vetPayment/'; ?>",
                data: jQuery("#paymentForm").serialize(),
                complete: function(){
                    jQuery("#processing").hide();
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);
                    if(obj.status = 'success'){
                        jQuery('.selectVetCom').hide();
                        jQuery('.cardDetails').hide();
                        jQuery(".otpDetails").show();
                        jQuery(".failedDetails").hide();
                        jQuery("#processing1").hide();
                        jQuery(".successDetails").hide();
                        jQuery('.ref').val(obj.data.transactionreference);
                    }else{
                        jQuery('.selectVetCom').hide();
                        jQuery('.cardDetails').hide();
                        jQuery(".otpDetails").hide();
                       jQuery(".failedDetails").show();
                       jQuery("#processing1").hide();
                       jQuery(".successDetails").hide();
                    }
                }
            });  
        }else{
            return false;
        }
        e.preventDefault();
    });

    jQuery(document).on('click', '.otpValidate', function(e){
        var amount = jQuery('.otp').val();
        var ref = jQuery('.ref').val();
        if(jQuery("#otpForm").valid()){
            jQuery("#processing1").show();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo DOMAIN_NAME_PATH.'Users/validateOtp/'; ?>",
                data: jQuery("#otpForm").serialize(),
                complete: function(){
                    jQuery("#processing1").hide();
                },
                success: function(data){
                    if(data == 'Failed'){
                        jQuery('.selectVetCom').hide();
                        jQuery('.cardDetails').hide();
                        jQuery(".otpDetails").hide();
                        jQuery(".failedDetails").show();
                        jQuery(".successDetails").hide();
                        jQuery("#processing1").hide();
                    }else if(data == 'Successful'){
                        jQuery('.selectVetCom').hide();
                        jQuery('.cardDetails').hide();
                        jQuery(".otpDetails").hide();
                        jQuery(".failedDetails").hide();
                        jQuery("#processing1").hide();
                        jQuery(".successDetails").show();
                    }
                }
            });  
        }else{
            return false;
        }
        e.preventDefault();
    });
});
    function accessList(optionId){
        jQuery("#access").html(jQuery('').fadeIn('slow'));
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Users/accesslist/'; ?>"+optionId,
            success: function(data){
                if(data){
                    jQuery("#access").html(jQuery(data).fadeIn('slow'));
                }else{
                    jQuery("#access").html(jQuery('Nothing found!').fadeIn('slow'));
                }
            }
        });
    }
    function shortList(optionId){
        jQuery("#access").html(jQuery('').fadeIn('slow'));
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Users/shortlist/'; ?>"+optionId,
            success: function(data){
                if(data){
                    jQuery("#access").html(jQuery(data).fadeIn('slow'));
                }else{
                    jQuery("#access").html(jQuery('Nothing found!').fadeIn('slow'));
                }
            }
        });
    }
	function chek_old_password(password){
		//alert(password);
		inputId = 'oldPass';
		if(password != ''){
			var dataString = "password=" + password;
			$.ajax({
				type: "POST",
				url: "<?php echo DOMAIN_NAME_PATH.'Users/chek_old_password/'; ?>",
				data: dataString,
				success: function(html){
					if(html){
						/*$("#passOldMatching").fadeOut("slow", function() {
							$(this).remove();
						});*/
						$("#passOldMatching").remove();
						$("#"+inputId).append($(html).fadeIn('slow'));
					}else{
						$("#passOldMatching").fadeOut("slow", function() {
							$(this).remove();
						});
					}
				}
			});
		} else {
			$("#passOldMatching").fadeOut("slow", function() {
				$(this).remove();
			});
		}
	}
</script>

