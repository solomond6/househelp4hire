<!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-forward"></i>Users - Import/Export</h4>
                                </div>
                                <div class="widget-body form">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="form-horizontal">
                                                <div class="news-b"  style="padding-bottom: 20px;">
                                                    <div class="control-group">
                                                        <h5>Export Excel Sheet of Projects</h5>
                                                    </div>
                                                    <div class="control-group">
                                                        <?php echo $this->Html->link('Export .xls File', array('controller' => $this->params['controller'], 'action' => $this->params['action'], 'export'), array('style'=>'color:#000;','div'=>false));?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
              