<div id="content-wrapper">
    <section id="section-hero" style="background-position: center center; z-index: 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="cta-bottom-float-border bkgd-quinary-100 text-center" style="background: none; border: none;">
                        <h1 class="promo-text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </h1>
                            <a id="cta-find-sitters-btn" href="#" data-toggle="modal" data-target="#AgentSignUp" class="btn btn-danger btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                                <span class="us-text-17">Are you an <br class="visible-super-xs"><span class="weight-bold">Agent</span>?</span>
                                <div class="us-text-18 weight-semi-bold text-uppercase margin-t-15" style="font-weight: bold; color:#000;">
                                    SIgnup Here
                                </div>
                            </a>
                            <a id="cta-find-sitters-btn" href="#" data-toggle="modal" data-target="#CandidateSignUp" class="btn btn-danger btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                                <span class="us-text-17">Are you a <br class="visible-super-xs"><span class="weight-bold">Jobseeker</span>?</span>
                                <div class="us-text-18 weight-semi-bold text-uppercase margin-t-15" style="font-weight: bold;color:#000;">
                                    Signup Here
                                </div>
                            </a>
                            <a id="cta-find-sitters-btn" href="#" data-toggle="modal" data-target="#SignUp" class="btn btn-danger btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                                <span class="us-text-17">Do you need <br class="visible-super-xs"><span class="weight-bold">Househelp</span>?</span>
                                <div class="us-text-18 weight-semi-bold text-uppercase margin-t-15" style="font-weight: bold;color:#000;">
                                    Signup Here
                                </div>
                            </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="video_background_video_0" style="z-index: -1; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; overflow: hidden;">
            <?php echo $this->Html->image('banner1.jpg'); ?>
            <!--<video width="1676" height="700" autoplay="" loop="" onended="this.play()" style="position: absolute; top: 0px; left: -163px;">
                <source src="https://assets.urbansitter.com/us-sym/assets/videos/homepage_final.mp4" type="video/mp4">
            </video>-->
        </div>
    </section>
    <section id="four-pinheads" class="background-section">
        <div class="container">
            <div class="headline">
                <h3 class="marketing-text">Lorem ipsum dolor sit amet.</h3>
                <h4 class="marketing-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
            </div>
            <div class="row map-icons visible-lg visible-md">
                <div class="col-md-12">
                    <div class="bubble-container bubble-one skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">Lorem ipsum dolor sit amet</span>
                                <span class="number" data-final-value="14">0</span>
                                <span class="subtitle">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                    <div class="bubble-container bubble-two skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">Lorem ipsum dolor sit amet</span>
                                <span class="number" data-final-value="35">0</span>
                                <span class="subtitle">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                    <div class="bubble-container bubble-three skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">Lorem ipsum dolor sit amet</span>
                                <span class="number" data-final-value="43">0</span>
                                <span class="subtitle">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                    <div class="bubble-container bubble-four skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">Lorem ipsum dolor sit amet</span>
                                <span class="number" data-final-value="28">0</span>
                                <span class="subtitle">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="map-icons-carousel" class="carousel slide hidden-lg hidden-md" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">Lorem ipsum dolor sit amet</span>
                                    <span class="number" data-final-value="14">0</span>
                                    <span class="subtitle">Lorem ipsum dolor sit amet</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">Lorem ipsum dolor sit amet</span>
                                    <span class="number" data-final-value="35">0</span>
                                    <span class="subtitle">Lorem ipsum dolor sit amet</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">Lorem ipsum dolor sit amet</span>
                                    <span class="number" data-final-value="43">0</span>
                                    <span class="subtitle">Lorem ipsum dolor sit amet</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">Lorem ipsum dolor sit amet</span>
                                    <span class="number" data-final-value="28">0</span>
                                    <span class="subtitle">Lorem ipsum dolor sit amet</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cta-block green bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 text">
                        <h4 class="marketing-text pad-t-8">Sign up for free:</h4>
                    </div>
                    <div class="col-md-3 cta">
                        <a id="map-cta-signup-parent" class="btn btn-block btn-border-white half-width-sm-only upper margin-b-15 m-b-md-0 m-h-sm-auto" href="#" data-toggle="modal" data-target="#AgentSignUp">Are you an Agent? <br/><strong>CLICK HERE</strong></a>
                    </div>
                    <div class="col-md-3 cta">
                        <a id="map-cta-signup-sitter" class="btn btn-block btn-border-white half-width-sm-only upper m-h-sm-auto" href="#" data-toggle="modal" data-target="#CandidateSignUp">Are you a Jobseeker? <br/><strong>CLICK HERE</strong></a>
                    </div>
                    <div class="col-md-3 cta">
                        <a id="map-cta-signup-sitter" class="btn btn-block btn-border-white half-width-sm-only upper m-h-sm-auto" href="#" data-toggle="modal" data-target="#SignUp">Do you need Househelp? <br/><strong>CLICK HERE</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-three-bubbles" class="background-section">
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 headline">
                    <h1 class="promo-text">Lorem ipsum dolor sit amet<br><span class="us-text-22 weight-semi-bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></h1>
                </div>
            </div>
        </div>
        <div class="circles-container">
            <div class="container">
                <div class="row colorful-circle">
                    <div class="col-sm-10 col-sm-offset-1 headline visible-xs">
                        <h2 class="marketing-text">Lorem ipsum dolor sit amet<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit<br><span class="us-text-22 weight-semi-bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></h2>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="circle orange skrollable skrollable-before">
                            <i class="fa fa-user fa-3x"></i>
                            <span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
                            <span class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="circle orange skrollable skrollable-before" data-anchor-target="#section-three-bubbles">
                            <i class="fa fa-user fa-3x"></i>
                            <span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
                            <span class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="circle orange skrollable skrollable-before" data-anchor-target="#section-three-bubbles">
                            <i class="fa fa-user fa-3x"></i>
                            <span class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>
                            <span class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cta-block orange bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 text">
                            <h4 class="marketing-text pad-t-8">Join our community:</h4>
                        </div>
                        <div class="col-md-3 cta">
                            <a id="map-cta-signup-parent" class="btn btn-block btn-border-white half-width-sm-only upper margin-b-15 m-b-md-0 m-h-sm-auto" href="#" data-toggle="modal" data-target="#AgentSignUp">Are you an Agent? <br/><strong>CLICK HERE</strong></a>
                        </div>
                        <div class="col-md-3 cta">
                            <a id="map-cta-signup-sitter" class="btn btn-block btn-border-white half-width-sm-only upper m-h-sm-auto" href="#" data-toggle="modal" data-target="#CandidateSignUp">Are you a Jobseeker? <br/><strong>CLICK HERE</strong></a>
                        </div>
                        <div class="col-md-3 cta">
                            <a id="map-cta-signup-sitter" class="btn btn-block btn-border-white half-width-sm-only upper m-h-sm-auto" href="#" data-toggle="modal" data-target="#SignUp">Do you need Househelp? <br/><strong>CLICK HERE</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-four">
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 headline">
                    <h1 class="promo-text">Lorem ipsum dolor sit amet<br><span class="us-text-22 weight-semi-bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></h1>
                </div>
            </div>
        </div>
    </section>
</div>