<script type="text/javascript">
   $(document).ready(function() {
	$("#UserForgetPasswordForm").validationEngine({scroll:true})
   });
</script>

 <!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content bg-white pad-btm-z">
            <div class="container">
            	<?php echo $this->Form->create('User'); ?>
                	<div class="row" style="margin-bottom: 100px;">
                    <div class="col-sm-5">
                        <div class="form-group" style="margin-bottom: 30px;">
                            <h2 class="mrgn-tpz"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" style="width: 250px;" alt="HouseHelp4Hire"></h2><small style="font-size: 18px; font-weight: 300;">Submit your registered E-mail address.</small>
                        </div>
                        <div class="form-group">
                           
                            <?php echo $this->Form->input('email', array('id'=>'eml', 'label'=>'', 'placeholder'=>'Email Address', 'div'=>false, 'class'=>'form-control validate[required,custom[email]]')); ?>
                        </div>
                        <div class="form-group">
                            Back to <a class="link" href="#">Log In</a>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Reset" class="btn btn-primary">
                        </div>
                    </div>
                </div>
				<?php echo $this->Form->end(); ?>
            </div>
        </section>
        <!--/portfolio-->