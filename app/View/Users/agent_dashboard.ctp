<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#UserAdminProfileForm").validationEngine({scroll:true})
	});
//-->
</script>
 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Settings</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                          <?php echo $this->Form->create('User');?>
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Edit Account</h4>
                                    <?php echo $this->Form->input('id'); ?>
                                </div>
                                <div class="widget-body form">
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <label class="control-label">Email<span class="red">*</span></label>
                                            <div class="controls">
                                                <?php echo $this->Form->input('email', array('class' => 'span5 validate[required,length[0,50],custom[email]]', 'label'=>'','autocomplete'=>'off')); ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Old Password</label>
                                            <div class="controls">
                                           	 <?php  echo $this->Form->input('oldPassword', array('type' =>'password', 'label'=>'','class'=>'span5' )); ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">New Password</label>
                                            <div class="controls">
                                                <?php  echo $this->Form->input('newPassword', array('type' =>'password', 'label'=>'', 'class'=>'span5')); ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">Confirm Password</label>
                                            <div class="controls">
                                                <?php  echo $this->Form->input('confirmPassword', array('type' =>'password', 'label'=>'', 'class'=>'span5')); ?>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
<!-- END PAGE CONTAINER-->