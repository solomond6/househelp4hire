
<script type="text/javascript">
	var hsi = 1;
	function tiggerFire(value, formIdToSubmit, linkIdToClick){
		if(value == 'Cancelled' || value == 'Released'){
			if(value == 'Released'){
				$('#confTxt').html('Are you sure to release this staff?');
			}
			if(value == 'Cancelled'){
				$('#confTxt').html('Are you sure to cancel this booking?');
			}
			if(hsi == '1'){
				$( "#"+linkIdToClick ).trigger( "click" );
			} 
			hsi++;
		} else {
			document.getElementById(formIdToSubmit).submit();
			//$( "#" + formIdToSubmit ).submit();
		}
		return false;
	}
</script>
<section id="portfolio" class="clearfix content1">
    <div class="container">
        <div class="col-sm-offset-1 col-md-offset-2 col-sm-10 col-md-8 cta-gray-float-border text-center">
            <h2>Welcome, <?php if($profileInfo['User']['name']){echo $profileInfo['User']['name'];} else {echo '';} ?></h2>
            <h4>You easily search for a househelp of your choice using the search form below: </h4>
            <?php echo $this->Form->create('Staffs', array('controller'=>'Staffs', 'action'=>'index')); 
                if(isset($this->request->data['Staff']['search'])){
                    $searchFieldValue = $this->request->data['Staff']['search'];
                } else {
                    $searchFieldValue = null;
                }
            ?>
            <div class="input-group">
                <?php echo $this->Form->input('search', array('value'=>$searchFieldValue, 'class'=>"form-control", 'name'=>"data[Staff][search]", 'id'=>"appendedInputButton", 'placeholder'=>"Cooks, Drivers,etc....", 'label'=>false, 'div'=>false,'style'=>'height: 42px; font-size: 17px;')); ?>
                <span class="input-group-btn">
                    <button class="btn btn-danger" style="padding: 9px 18px;font-family: 'Roboto Condensed';" type="submit">Search</button>
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</section>
<!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content bg-white pad-btm-z" style="background: #e9eaed!important;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-big" style="margin-bottom: 25px;">
                            <h3><span style="color:#666;">My Account </span></h3>
                            <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-md-4" style="margin-bottom:10px">
                        <div class="profile-big min-h-300">
                            <h4 class="text-uppercase"><i class="fa fa-user"></i> Profile Information<a href="#" data-toggle="modal" data-target="#editinfo"><i class="fa fa-edit"></i></a></h4>
                            <p><span class="text-bold">Name </span><?php if($profileInfo['User']['name']){echo $profileInfo['User']['name'];} else {echo '';} ?></p>
                            <p><span class="text-bold">Unique ID </span><?php if($profileInfo['User']['emp-uid']){echo $profileInfo['User']['emp-uid'];} else {echo '<a class="hk-link" href="#myModal" role="button" data-toggle="modal" style="color: #B94A48;">Get Your UID</a>';} ?></p>
                            <p><span class="text-bold">Email </span> <?php if($profileInfo['User']['email']){echo $profileInfo['User']['email'];} else {echo '';} ?></p>
                            <p><span class="text-bold">Phone no </span> <?php if($profileInfo['User']['phone']){echo $profileInfo['User']['phone'];} else {echo '';} ?></p>
                            <p><span class="text-bold">Joined On </span> <?php echo date('F j, Y', strtotime($profileInfo['User']['registration_date'])); ?></p>
                            <p><span class="text-bold">Location </span> <?php if($profileInfo['Location']['name']){echo $profileInfo['Location']['name'];} else {echo '';} ?></p>
                            <h4 class="text-uppercase"><i class="fa fa-lock"></i> Change Password<a href="#" data-toggle="modal" data-target="#changepass"><i class="fa fa-edit"></i></a></h4>
                            <p>Change your password clicking on the Edit icon. </p>
                            <h4 class="text-uppercase"><i class="fa fa-lock"></i> Submit Feedback<a href="#" data-toggle="modal" data-target="#feedback"><i class="fa fa-edit"></i></a></h4>
                            <p>Submit your feedback/complaint by clicking on teh Icon. </p>
                            <div class="alert alert-success span12">
                                <?php echo $this->Session->flash(); ?>
                            </div>
                            <?php if(count($newsletter) == 0){ ?>
                            <?php echo $this->Form->create('Email', array('action' => 'add', 'novalidate'=>'')); ?>
                                <h4 class="text-uppercase"><i class="fa fa-envelope"></i> Subscribe to our newsletter</h4>
                                <div class="form-group">
                                <label>Your Email Address :</label>
                                <?php echo $this->Form->input('email', array('type'=>'email', 'label'=>false, 'div'=>false, 'placeholder'=>'Your email address', 'class'=>'form-control', 'value'=>$profileInfo['User']['email'], 'required'=>'required')); ?>
                                </div>
                                <div class="form-group"><input type="submit" class="btn btn-primary btn-sm" value="Subscribe"></div>
                            <?php echo $this->Form->end(); ?>
                            <?php } else { ?>
                                <h4 class="text-uppercase"><i class="fa fa-envelope"></i> Subscribe to our newsletter</h4>
                                <div class="form-group">
                                    <?php 
                                $string = $this->Session->read('Auth.User.email');
                                $convertedString = bin2hex(Security::cipher(serialize($string),Configure::read('Security.salt')));
                                echo $this->Html->link('Unsubscribe', array('controller'=>'Emails', 'action'=>'delete', $convertedString, 1), array('class'=>'btn btn-danger btn-primary btn-lg')); 
                                ?>                                
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom:10px">
                        <div class="profile-big min-h-500">
                            <h4 class="text-uppercase"><i class="fa fa-envelope"></i> Shortlisted Candidates</h4>
                            <div class="col-md-12">
                                <?php 
                                    if($shortlistedStaffs){
                                        foreach($shortlistedStaffs as $shortlistedStaff){
                                            foreach($shortlistedStaff as $shortlisted){
                                            echo '<li class="span2" style="display: inline-block;"><a target="_blank" href="'.DOMAIN_NAME_PATH.$shortlisted['Staff']['friendly_url'].'" class="thumbnail prf-dtl" title="'.$shortlisted['Category']['name_singular'].' '.$shortlisted['Staff']['name'].'">
                                                <div class="ch-item ch-img-1">
                                                    <div class="ch-info">
                                                        <img src="'.PAGE_IMAGES_URL.$shortlisted['Staff']['image1'].'" alt="" style="height:85px;width:85px" />
                                                    </div>
                                                </div>
                                            </a></li>';
                                            }
                                        }
                                    }else{
                                        echo '<h2 style="padding:20px;"> No candidate shortlisted yet</h2>';
                                    }  
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom:10px">
                        <div class="profile-big min-h-500">
                            <h4 class="text-uppercase"><i class="fa fa-envelope"></i> Contacted Candidates</h4>
                            <div class="col-md-12">
                                <?php 
                                    if($contactedStaffs){
                                        foreach($contactedStaffs as $contactedStaff){
                                            echo '<li class="span2" style="display: inline-block;"><a target="_blank" href="'.DOMAIN_NAME_PATH.$contactedStaff['Staff']['friendly_url'].'" class="thumbnail prf-dtl" title="'.$contactedStaff['Category']['name_singular'].' '.$contactedStaff['Staff']['name'].'">
                                                <div class="ch-item ch-img-1">
                                                    <div class="ch-info">
                                                        <img src="'.PAGE_IMAGES_URL.$contactedStaff['Staff']['image1'].'" alt="" style="height:80px;width:80px" />
                                                    </div>
                                                </div>
                                            </a></li>';
                                        }
                                    }else{
                                        echo '<h2 style="padding:20px;"> No candidate contacted yet</h2>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-big" style="margin-bottom: 100px;">
                            <h4 class="text-uppercase">Payment History</h4>
                            <?php if(count($payments) > 0){ ?>
                             <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                <thead style="background: #eee; color:#333;">
                                <tr>
                                    <th>Option</th>
                                    <th>Status</th>
                                    <th>Transaction ID</th>
                                    <th>Paymnet Date</th>
                                    <!--<th>Expiry Date</th>-->
                                    <th>Access Limit</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
	                           </thead>
                                <tbody class="pay-bor">
                                <?php foreach($payments as $payment){ ?>
		<tr>
		  <td><?php echo $payment['PaymentOption']['name']; ?></td>
		  <td><?php if($payment['Payment']['gtpay_tranx_state'] == 'completed'){echo 'Subscribed';}else if($payment['Payment']['gtpay_tranx_state'] == 'pending'){echo 'Pending';} else { echo 'Expired';} ?></td>
		  <td><?php echo $payment['Payment']['gtpay_tranx_id']; ?></td>
		  <td><?php echo date('F j, Y', strtotime($payment['Payment']['created_at'])); ?></td>
		  <!--<td><?php echo date('F j, Y', strtotime($payment['Payment']['expiry_date'])); ?></td>-->
		  <td><?php echo $payment['Payment']['quantity']; ?> Profile</td>
		  <td><?php echo $payment['Payment']['paid_amount']; ?> &#8358;</td>
		  <td><a href="javascript:void(0);" role="button" data-toggle="modal" data-target="#access" class="btn btn-danger btn-sm" onclick="accessList('<?php echo $payment['Payment']['id']; ?>');">Contacted Profile</a>
          <a href="javascript:void(0);" role="button" data-toggle="modal" data-target="#shortList" class="btn btn-success btn-sm" onclick="shortList('<?php echo $payment['Payment']['id']; ?>');">Shortlisted Profile</a>
          </td>
		</tr>
		<?php } ?>
			                     </tbody>
                            </table>
                            
                            <?php } else { ?>
								<p>No payment history yet</p>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!--/portfolio-->

<!-- Confirm Profile model popup -->
<div class="modal fade" id="editinfo" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <div class="modal-body pad-tpz">
                        <div class="row">
                            <div class="col-sm-12">
                            <?php echo $this->Form->create('User', array('action'=>'changeProfileInfo', 'novalidate'=>'')); ?>
						<?php echo $this->Form->input('id', array('div'=>false, 'type'=>'hidden', 'value'=>$profileInfo['User']['id'])); ?>
                                <div class="form-group mrgn-btm">
                                    <h3 class="mrgn-tpz">Update Profile Information</h3>
                                </div>
                                <fieldset style="width: 100%;">
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('first_name', array('div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['first_name'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('last_name', array('div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['last_name'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('email', array('type'=>'email', 'div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['email'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <?php echo $this->Form->input('phone', array('label'=>'Mobile Number', 'div'=>false, 'class'=>'form-control', 'value'=>$profileInfo['User']['phone'], 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                           <?php echo $this->Form->input('location_id', array('div'=>false, 'class'=>'form-control', 'empty'=>'Select Location', 'onchange'=>'locationChecking(this.value, "locMsg2")', 'selected'=>$profileInfo['User']['location_id'], 'required'=>'required')); ?>
										<br/>
										<div id="locMsg2" class="span5"></div>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <!-- <span style="color:red;" class="span5">* Required fields.</span> -->
                                </fieldset>
                                <div class="form-group">
                                    <input type="submit" value="Save" class="btn btn-primary">
                                </div>
                         <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
<!-- Change Password model popup -->
<div class="modal fade" id="changepass" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <div class="form-group mrgn-btm">
                            <h3 class="mrgn-tpz">Change Password</h3>
                        </div>
                    </div>
                    <div class="modal-body pad-tpz">
                        <div class="row">
                            <div class="col-sm-12">
                            <?php echo $this->Form->create('User', array('action'=>'changePassword', 'novalidate'=>'')); ?>
                                
                                <fieldset style="width: 100%;">
                                    <div class="form-group">
                                        <div class="controls">
                                           
                                            <?php echo $this->Form->input('old_password', array('type'=>'password', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required', 'onblur'=>'chek_old_password(this.value);')); ?>
										<span id="oldPass"></span>								
                                        <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                           <?php echo $this->Form->input('password', array('type'=>'password', 'label'=>'New Password', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required')); ?>						
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                           	<?php echo $this->Form->input('confirmpassword', array('type'=>'password', 'label'=>'Confirm New Password', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required', 'data-validation-matches-message'=>'Must match new password entered above', 'data-validation-matches-match'=>'data[User][password]')); ?>
                                           	<div class="help-block"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <input type="submit" value="Save" class="btn btn-primary">
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
<!-- Feedback model popup -->
<div class="modal fade" id="feedback" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <div class="form-group mrgn-btm">
                            <h3 class="mrgn-tpz">Feedback</h3>
                        </div>
                    </div>
                    <div class="modal-body pad-tpz">
                        <div class="row">
                            <div class="col-sm-12">
                            <?php echo $this->Form->create('Feedback', array('action'=>'add', 'novalidate'=>'')); ?>
                                
                                <fieldset style="width: 100%;">
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="hidden" name="data[Feedback][user_id]" value="<?php echo $this->Session->read('Auth.User.id'); ?>"
                                            <?php echo $this->Form->input('comment', array('type'=>'textarea', 'label'=>'Feedback comment', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required')); ?>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-danger">
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
<!-- Access History model popup -->
<div class="modal fade" id="access" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <div class="form-group mrgn-btm">
                    <h3 class="mrgn-tpz">Access History</h3>
                </div>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="acclist"></div>
                       <!--<p>No profile contact has been viewed by you!</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="shortList" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <div class="form-group mrgn-btm">
                    <h3 style="margin-top:0px;">Shortlist Candidates</h3>
                </div>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="shortList"></div>
                       <!--<p>No profile contact has been viewed by you!</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="rating" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <div class="form-group mrgn-btm">
                    <h3 class="mrgn-tpz">Rating</h3>
                </div>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12">
                    <?php echo $this->Form->create('Rating', array('action' => 'add')); ?>
                        <?php
                            if(isset($ratingConfigs[1])){
                                echo '<h3 style="margin:0px 0px;">' .$ratingConfigs[0]['RatingConfig']['description'].'</h3>';
                                echo '<fieldset id="demo1" class="rating">
                                    <input class="stars" type="radio" id="star5" name="value" value="1" />
                                    <label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                    <input class="stars" type="radio" id="star4" name="value" value="2" />
                                    <label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                    <input class="stars" type="radio" id="star3" name="value" value="3" />
                                    <label class = "full" for="star3" title="Meh - 3 stars"></label>
                                    <input class="stars" type="radio" id="star2" name="value" value="4" />
                                    <label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                    <input class="stars" type="radio" id="star1" name="value" value="5" />
                                    <label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                </fieldset>';
                            }
                        ?>
                        <input type="hidden" name="rating_config_id" value="<?php echo $ratingConfigs[0]['RatingConfig']['id']; ?>"/>
                        <input type="hidden" name="user_id" value="<?php echo $this->Session->read('Auth.User.id'); ?>"/>
                        <div class="form-group">
                            <input type="submit" value="Rate" class="btn btn-danger">
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if(!$ratings){
    echo '<script type="text/javascript">';
    echo 'jQuery("#rating").modal("show")';
    echo '</script>';
}else{
    echo '<script type="text/javascript">';
    echo 'jQuery("#rating").modal("hide")';
    echo '</script>';
}
?>
<script type="text/javascript">
    function accessList(optionId){
        jQuery('#shortList').modal('hide')
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Users/accesslist/'; ?>"+optionId,
            success: function(data){
                if(data){
                    jQuery('#access').modal('show')
                    jQuery('.acclist').html(data);
                }else{
                    jQuery('#access').modal('show')
                    jQuery('.acclist').html('Nothing found!');
                }
            }
        });
    }
    function shortList(optionId){
        jQuery('#access').modal('hide')
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Users/shortlist/'; ?>"+optionId,
            success: function(data){
                if(data){
                    jQuery('#shortList').modal('show')
                    jQuery('.shortList').html(data);
                }else{
                    jQuery('#shortList').modal('show')
                    jQuery('.shortList').html('Nothing found!');
                }
            }
        });
    }
	function chek_old_password(password){
		//alert(password);
		inputId = 'oldPass';
		if(password != ''){
			var dataString = "password=" + password;
			$.ajax({
				type: "POST",
				url: "<?php echo DOMAIN_NAME_PATH.'Users/chek_old_password/'; ?>",
				data: dataString,
				success: function(html){
					if(html){
						/*$("#passOldMatching").fadeOut("slow", function() {
							$(this).remove();
						});*/
						$("#passOldMatching").remove();
						$("#"+inputId).append($(html).fadeIn('slow'));
					}else{
						$("#passOldMatching").fadeOut("slow", function() {
							$(this).remove();
						});
					}
				}
			});
		} else {
			$("#passOldMatching").fadeOut("slow", function() {
				$(this).remove();
			});
		}
	}
</script>

