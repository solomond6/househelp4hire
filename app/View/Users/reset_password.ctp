<script type="text/javascript">
   $(document).ready(function() {
	$("#UserResetPasswordForm").validationEngine({scroll:true})
   });
</script>
 <section id="portfolio" class="clearfix content bg-white pad-btm-z">
            <div class="container">
            	<?php echo $this->Form->create('User'); ?>
                	<div class="row" style="margin-bottom: 100px;">
                    <div class="col-sm-5">
						<div class="form-group" style="margin-bottom: 30px;">
                            <h2 class="mrgn-tpz"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" style="width: 250px;" alt="HouseHelp4Hire"></h2><small style="font-size: 18px; font-weight: 300;">Password Recovery</small>
                        </div>
						 <?php echo $this->Form->input('tempString', array('id'=>'eml', 'label'=>'', 'type'=>'hidden', 'div'=>false, 'class'=>'validate[required]')); ?>
						 <div class="form-group">
							<label>Enter New Password:</label>
							
							<?php echo $this->Form->input('password', array('id'=>'passwd', 'label'=>'', 'placeholder'=>'New Password', 'div'=>false, 'class'=>'form-control validate[required]')); ?>
						 </div>
						 <div class="form-group">
							<label>Re Enter New Password:</label>
							
							<?php echo $this->Form->input('re_password', array('id'=>'re-pass', 'label'=>'', 'type'=>'password', 'placeholder'=>'Re Enter New Password', 'div'=>false, 'class'=>'form-control validate[required,equals[passwd]]')); ?>
							<div class="clearfix"></div>
						 </div>
						 <div class="form-group">
							<button type="submit" class="btn btn-primary">Reset</button>
						 </div>
					 </div>
                </div>
				<?php echo $this->Form->end(); ?>
            </div>
</section>