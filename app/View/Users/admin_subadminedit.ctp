 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Admin User Manager</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Edit Admin User</h4>
                                </div>
                                <?php echo $this->Form->create('User');?>
                                <?php echo $this->Form->input('id'); ?>
                                <div class="widget-body form">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                    <label class="control-label">First Name<span class="red">*</span></label>
                                                    <div class="controls">
                                                     <?php   echo $this->Form->input('first_name',array('class'=>'span12','label'=>false)); ?>
                                                    </div>

                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Last Name</label>
                                                    <div class="controls">
                                                        <?php echo $this->Form->input('last_name',array('class'=>'span12','label'=>false)); ?> 
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Password<span class="red">*</span></label>
                                                    <div class="controls">
                                                       <div class="input password required"><input name="data[User][password]" class="span12" type="password" value="" id="UserPassword"></div>
                                                    </div>
                                                </div>   
                                                <div class="control-group">
                                                    <label class="control-label">Email<span class="red">*</span></label>
                                                    <div class="controls">
                                                       <?php echo $this->Form->input('email',array('class'=>'span12','label'=>false)); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Address<span class="red">*</span></label>
                                                    <div class="controls">
                                                        <?php echo $this->Form->input('address',array('class'=>'span12','label'=>false)); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Phone<span class="red">*</span></label>
                                                    <div class="controls">
                                                        <?php echo $this->Form->input('phone',array('class'=>'span12','label'=>false)); ?>
                                                    </div>
                                                </div>  
                                                <div class="control-group">
                                                    <label class="control-label">Location<span class="red">*</span></label>
                                                    <div class="controls">
                                                        <?php echo $this->Form->input('location_id',array('class'=>'span12','label'=>false)); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Status<span class="red">*</span></label>
                                                    <div class="controls">
                                                       <?php echo $this->Form->input('status', array('type' => 'select', 'options' => array('Y' => 'Active', 'N' => 'Inactive'),array('class'=>'span5'))); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="control-group">
                                                    <label class="control-label">Choose Access Level</label>
                                                    <div class="controls">
                                                        <div class="row-fluid">
                                                            <div class="span4">
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Settings" <?php if(in_array('Settings', $s)){ ?> checked="checked"<?php }?>  /> Manage Settings
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Agents" <?php if(in_array('Agents', $s)){ ?> checked="checked"<?php }?>  /> Manage Agents
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Employers" <?php if(in_array('Employers', $s)){ ?> checked="checked"<?php }?>  /> Manage Subscribers
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Staffs" <?php if(in_array('Staffs', $s)){ ?> checked="checked"<?php }?> /> Manage Staffs
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id=""  value="Payments" <?php if(in_array('Payments', $s)){ ?> checked="checked"<?php }?> /> Manage Payments
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Bookings" <?php if(in_array('Bookings', $s)){ ?> checked="checked"<?php }?>  /> Manage Bookings
                                                                </label>
                                                                <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Admins" <?php if(in_array('Admins', $s)){ ?> checked="checked"<?php }?>  /> Manage Admin User
                                                                </label>
                                                            </div>
                                                            <div class="span4">
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Categories" <?php if(in_array('Categories', $s)){ ?> checked="checked"<?php }?>  /> Manage Categories
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Locations" <?php if(in_array('Locations', $s)){ ?> checked="checked"<?php }?>  /> Manage Locations
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Languages" <?php if(in_array('Languages', $s)){ ?> checked="checked"<?php }?>  />  Manage Languages
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Religions" <?php if(in_array('Religions', $s)){ ?> checked="checked"<?php }?>  /> Manage Religions
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Educations" <?php if(in_array('Educations', $s)){ ?> checked="checked"<?php }?>  /> Manage Educations
                                                                </label>
                                                                <label class="checkbox">
                                                                   <input type="checkbox" name="data[User][access][]" id="" value="Pages" <?php if(in_array('Pages', $s)){ ?> checked="checked"<?php }?>  /> Manage Pages
                                                                </label>
                                                            </div>
                                                            <div class="span4">
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Services" <?php if(in_array('Services', $s)){ ?> checked="checked"<?php }?>  /> Manage Services
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Contacts" <?php if(in_array('Contacts', $s)){ ?> checked="checked"<?php }?>  /> Manage Contacts
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="FAQs" <?php if(in_array('FAQs', $s)){ ?> checked="checked"<?php }?>  /> Manage FAQs
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="Newsletters" <?php if(in_array('Newsletters', $s)){ ?> checked="checked"<?php }?>  /> Manage Newsletter
                                                                </label>
															    <label class="checkbox">
                                                                <input type="checkbox" name="data[User][access][]" id="" value="Coupons" <?php if(in_array('Coupons', $s)){ ?> checked="checked"<?php }?>  /> Manage Coupons
                                                                </label>
                                                                <label class="checkbox">
                                                                    <input type="checkbox" name="data[User][access][]" id="" value="ImportExport" <?php if(in_array('ImportExport', $s)){ ?> checked="checked"<?php }?>  /> Manage Import & Export
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->