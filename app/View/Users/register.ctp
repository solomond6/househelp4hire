<section id="portfolio" class="clearfix content1">
    	<div class="container">
            <div class="col-sm-offset-2 col-md-offset-4 col-sm-8 col-md-6 cta-gray-float-border pull-right">
                <div class="form-group mrgn-btm col-md-12 text-center">
                   <h2>I Want Domestic Staff: <br/><span>Sign U

                          p Here For Free</span></h2>
                </div>
                <div class="col-md-12" style="padding:0px;">
                    <?php echo $this->Form->input('flag', array('id'=>'LoginflagVar', 'type'=>'hidden', 'label' => false, 'div' => false, 'value'=>0)); ?>
                        <?php echo $this->Html->script(array('oauthpopup')); ?>
                        <script type="text/javascript">
                        jQuery(document).ready(function(){
                            jQuery('#facebook').click(function(e){
                                jQuery.oauthpopup({
                                    path: '<?php echo DOMAIN_NAME_PATH; ?>FacebookDetails/login',
                                    width:600,
                                    height:300,
                                    callback: function(){
                                        window.location.reload();
                                    }
                                });
                                e.preventDefault();
                            });
                        });
                        </script>
                        <div class="form-group col-md-6">
                            
                            <?php if(!$this->Session->read('Auth.User.id')){ ?>
                                    <img src="<?php echo DOMAIN_NAME_PATH; ?>images/fb-signup.png" id="facebook" style="width:100%;height:40px;" alt="Sign In Facebook">
                            <?php } ?> 
                        </div>

                        <div class="form-group col-md-6">
                            <?php if(!$this->Session->read('Auth.User.id')){ ?>
                                <?php
                                    echo $this->Html->link($this->Html->image("linkedin-signup.png", array("alt" => "Sign In Linkedin", 'style'=>"width:100%;height:40px;")), array('controller' => 'LinkedinDetails','action'=>'connect'), array('escape'=>false)
                                    );
                                ?>
                            <?php } ?> 
                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="text-center" style="margin:20px 0px;"><h4>Or Signup With Your Email Address</h4></div>
                <?php echo $this->Form->create('User', array('action' => 'register', 'id'=>'UserRegisterFormHeader')); ?>
			        <div class="">
			         	<?php echo $this->Session->flash('auth'); ?>
			                <?php 
			                if(isset($this->request->data['Agent']['flag']) && $this->request->data['User']['flag'] != '0'){
			                    $flagVal = $this->request->data['User']['flag'];
			                } else {
			                    $flagVal = 0;
			                }
			                echo $this->Form->input('flag', array('type'=>'hidden', 'label' => false, 'div' => false, 'value'=>$flagVal)); 
			        	?>
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('first_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'First Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('last_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'Last Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('email', array('type'=>'email', 'div'=>false, 'class'=>'form-control','placeholder'=>'Email Address' ,'required'=>'required')); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('phone', array('label'=>'Mobile No.','placeholder'=>'Mobile No.', 'div'=>false, 'class'=>'form-control', 'required'=>'required','maxlength'=>'11')); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('password', array('type'=>'password', 'placeholder'=>'Password', 'div'=>false, 'class'=>'form-control pwd', 'value'=>'', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('confirm_password', array('type'=>'password', 'placeholder'=>'Confirm Password', 'div'=>false, 'class'=>'form-control cpwd', 'value'=>'', 'required'=>'required', 'data-validation-matches-message'=>'Passwords do not match', 'data-validation-matches-match'=>'data[User][password]')); ?>
                        </div>
                        <div class="form-group col-md-6">
                        	<label for="">State of Residence</label>
                            <select name="data[User][location_id]" class="form-control" onchange="locationChecking(this.value, 'locMsg')" required="required" id="UserLocationId" aria-invalid="false">
                                <option value="">Select Location</option>
                                <option value="1">Lagos</option>
                                <option value="2">Abuja</option>
                                <option value="3">Anambra</option>
                                <option value="4">Akwa Ibom</option>
                                <option value="5">Adamawa</option>
                                <option value="6">Abia</option>
                                <option value="7">Bauchi</option>
                                <option value="8">Bayelsa</option>
                                <option value="9">Benue</option>
                                <option value="10">Borno</option>
                                <option value="11">Cross River</option>
                                <option value="12">Delta</option>
                                <option value="13">Ebonyi</option>
                                <option value="14">Edo</option>
                                <option value="15">Ekiti</option>
                                <option value="16">Enugu</option>
                                <option value="17">Gombe</option>
                                <option value="18">Imo</option>
                                <option value="19">Jigawa</option>
                                <option value="20">Kaduna</option>
                                <option value="21">Kano</option>
                                <option value="22">Katsina</option>
                                <option value="23">Kebbi</option>
                                <option value="24">Kogi</option>
                                <option value="25">Kwara</option>
                                <option value="26">Nasarawa</option>
                                <option value="27">Niger</option>
                                <option value="28">Ogun</option>
                                <option value="29">Ondo</option>
                                <option value="30">Osun</option>
                                <option value="31">Oyo</option>
                                <option value="32">Plateau</option>
                                <option value="33">Rivers</option>
                                <option value="34">Sokoto</option>
                                <option value="35">Taraba</option>
                                <option value="36">Yobe</option>
                                <option value="37">Zamfara</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <p style="font-size: 12px;">By signing up, you agree to our  <a class="link" href="terms_of_service.php" target="_blank">Terms of Services</a> and  <a class="link" href="privacy_policy.php" target="_blank">Privacy Policy.</a></p>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="submit" value="Register" class="btn btn-danger">
                        </div>
			        </div>
			    <?php echo $this->Form->end(); ?>
            </div>
        </div>
</section>