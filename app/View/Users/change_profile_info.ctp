<?php
	echo $this->Html->css(array('style', 'validationEngine.jquery'));
	echo $this->Html->script(array('jquery.min', 'jquery.colorbox'));
	echo $this->Html->script(array('jquery.validationEngine', 'jquery.validationEngine-en', 'jconfirmaction.jquery'));
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#flashMessage").fadeOut(10000);
	$("#authMessage").fadeOut(10000);
	$('#overlay').click(function(){
		$('#overlay').fadeOut('fast');
	});
});
</script>
<script type="text/javascript">
   $(document).ready(function() {
	$("#UserChangeProfileInfoForm").validationEngine({scroll:true, promptPosition: "topLeft"})
   });
</script>
<h6>Update Member Information</h6>
<?php 
echo $this->Form->create('User', array('action'=>'changeProfileInfo'));
?>
<table class="pop" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td>
		<?php echo $this->Form->input('id', array('label'=>false, 'div'=>false)); ?>
		<p><span style="color:red;">*</span> First Name</p>
		<p><?php echo $this->Form->input('first_name', array('label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
    <td>
		<p><span style="color:red;">*</span> Last Name</p>
		<p><?php echo $this->Form->input('last_name', array('label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
  </tr>
  <tr>
	<td>
		<p><span style="color:red;">*</span> Email Addresse</p>
		<p><?php echo $this->Form->input('email', array('label'=>false, 'div'=>false, 'class'=>'validate[required,custom[email]]')); ?></p>
	</td>
    <td>
		<p>Phone Number (Optional)</p>
		<p><?php echo $this->Form->input('phone', array('label'=>false, 'div'=>false, 'class'=>'')); ?></p>
	</td>
  </tr>
  <tr>
    <td><span style="color:red;font-weight:bold;">** Required fields.</span></td>
	<td align="right" class="btn">
	<?php echo $this->Form->submit(__('Save', true), array('div'=>false, 'class'=>'', 'style'=>'height:34px; font-weight:bold;font-size:12px; color:#fff;width:100px;margin-right:20px;border:0')); ?>
	<!-- <input style="background:#CF3A08; padding:5px 15px; width:75px; color:#ffffff;" name="" type="button" value="Cancel" /> -->
	</td>
  </tr>
</table>
<?php echo $this->Form->end(); ?>