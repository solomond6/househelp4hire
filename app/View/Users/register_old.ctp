    <script type="text/javascript">
	   jQuery(document).ready(function() {
		jQuery("#UserRegisterForm").validationEngine({scroll:false, promptPosition: "topRight"})
	   });
	</script>
	<script type="text/javascript">
	   jQuery(document).ready(function() {
		jQuery("#UserLoginForm").validationEngine({scroll:false, promptPosition: "topRight"})
	   });
	</script>
	
 	<!--PORTFOLIO SECTION START-->
    <section id="portfolio" class="clearfix content bg-white pad-btm-z">
            <div class="container">
                <div class="row" style="margin-bottom: 100px;">
                <?php echo $this->Session->flash('auth'); ?>
				
				<?php 
				if(isset($this->request->data['User']['flag']) && $this->request->data['User']['flag'] != '0'){
					$flagVal = $this->request->data['User']['flag'];
				} else {
					$flagVal = 0;
				}
				echo $this->Form->input('flag', array('type'=>'hidden', 'label' => false, 'div' => false, 'value'=>$flagVal)); 
				?>
                    <div class="col-sm-5">
                    <?php echo $this->Form->create('User', array('action' => 'login')); ?>
                        <div class="form-group" style="margin-bottom: 30px;">
                            <h2 class="mrgn-tpz"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" style="width: 250px;" alt="HouseHelp4Hire"></h2><small style="font-size: 18px; font-weight: 300;">Please Sign In To Continue</small>
                        </div>
                        <div class="form-group">
                             <?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'class'=>'validate[required, custom[email]] form-control','placeholder'=>'Email Address')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('password', array('type'=>'password', 'label' => false, 'div' => false, 'class'=>'validate[required] form-control','placeholder'=>'Password')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Html->link('Forgot Password?', array('controller' => 'Users', 'action' => 'forgetPassword'), array('class'=>'link', 'style'=>'')); ?>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Sign In" class="btn btn-primary">
                        </div>
                   	 <?php echo $this->Form->end(); ?>
                   	 </div>
                   
                    <div class="col-sm-2 login-bg"></div>
                    <div class="col-sm-2 mobile-hr"></div>
                   
                    <div class="col-sm-5">
                     <?php echo $this->Form->create('User', array('action' => 'register')); ?>
                        <div class="form-group" style="margin-bottom: 30px;">
                            <h2 class="mrgn-tpz"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" style="width: 250px;" alt="HouseHelp4Hire"></h2><small style="font-size: 18px; font-weight: 300;">Please Sign Up To Continue</small>
                        </div>
                        <div class="form-group">
                           
                            <?php echo $this->Form->input('first_name', array('label' => false,'placeholder'=>'First Name', 'div' => false, 'class'=>'validate[required] form-control')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('last_name', array('label' => false,'placeholder'=>'Last Name', 'div' => false, 'class'=>'validate[required] form-control')); ?>
                        </div>
                        <div class="form-group">
                           <select class="form-control" id="UserLocationId" onchange="locationChecking(this.value, 'locMsg')" class="validate[required]" name="data[User][location_id]">
                                        <option value="">Select Location</option>
                                        <option value="1">Lagos</option>
                                        <option value="2">Abuja</option>
                                        <option value="3">Anambra</option>
                                        <option value="4">Akwa Ibom</option>
                                        <option value="5">Adamawa</option>
                                        <option value="6">Abia</option>
                                        <option value="7">Bauchi</option>
                                        <option value="8">Bayelsa</option>
                                        <option value="9">Benue</option>
                                        <option value="10">Borno</option>
                                        <option value="11">Cross River</option>
                                        <option value="12">Delta</option>
                                        <option value="13">Ebonyi</option>
                                        <option value="14">Edo</option>
                                        <option value="15">Ekiti</option>
                                        <option value="16">Enugu</option>
                                        <option value="17">Gombe</option>
                                        <option value="18">Imo</option>
                                        <option value="19">Jigawa</option>
                                        <option value="20">Kaduna</option>
                                        <option value="21">Kano</option>
                                        <option value="22">Katsina</option>
                                        <option value="23">Kebbi</option>
                                        <option value="24">Kogi</option>
                                        <option value="25">Kwara</option>
                                        <option value="26">Nasarawa</option>
                                        <option value="27">Niger</option>
                                        <option value="28">Ogun</option>
                                        <option value="29">Ondo</option>
                                        <option value="30">Osun</option>
                                        <option value="31">Oyo</option>
                                        <option value="32">Plateau</option>
                                        <option value="33">Rivers</option>
                                        <option value="34">Sokoto</option>
                                        <option value="35">Taraba</option>
                                        <option value="36">Yobe</option>
                                        <option value="37">Zamfara</option>
                                    </select>
						   <div id="locMsg" class="span2 pull-right"></div>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('email', array('label' => false,'placeholder'=>'Email Address' ,'div' => false, 'class'=>'validate[required,custom[email]] form-control')); ?>
                        </div>
                        <div class="form-group">
                             <?php echo $this->Form->input('password', array('id'=>'UserPasswordMain', 'type'=>'password', 'label' => false, 'div' => false, 'class'=>'validate[required] form-control','placeholder'=>'Password')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('confirm_password', array('id'=>'UserConfirmPassword', 'type'=>'password', 'label' => false, 'div' => false, 'class'=>'validate[required,equals[UserPasswordMain]] form-control','placeholder'=>'Confirm Password')); ?>
                        </div>
                        <div class="form-group">
                             <?php echo $this->Form->input('phone', array('label' => false, 'div' => false, 'class'=>'validate[required] form-control','maxlength'=>'11','placeholder'=>'Mobile No.')); ?>
                        </div>
                        <div class="form-group">
                            <p>By Signing up, you agree to <a class="link" data-toggle="modal" data-target="#tos">Terms of Services</a> </p>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Register" class="btn btn-primary">
                        </div>
					<?php echo $this->Form->end(); ?>
                    </div>
							
                </div>
            </div>
     </section>
    <!--/portfolio-->
    <!-- Log In model popup -->
    <div class="modal fade" id="tos" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <div class="modal-body pad-tpz">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mrgn-btm">
                                    <h2 class="mrgn-tpz" style="color: #f17939;">Terms of Service</h2></small>
                                </div>
                                <div class="form-group" style="height: 350px; overflow-y: scroll;">
                                 <fieldset>
                            <p><span style="font-size: small;">Welcome to our Terms of Service, we advise that you diligently read the information provided underneath, as using this website indicates your agreement to be bound by the terms stipulated below.</span></p>
                            <h5>Terms of Service</h5>
                            <p><span style="font-size: small;">This page states the Terms of Service ("Terms") under which you ("You") may use the HouseHelp4Hire Website (Site)and the Services (each as defined below in Liabilities) provided.
The Site reserves the right, to add, modify, change, or delete parts of the Terms at any time. Changes will be effective once posted on the Site with no other notice provided. Kindly check these Terms frequently for updates.
                                    <br>These Terms apply to the Site, and all of its Services.<br>These Terms constitute a binding agreement between You and 4Hire Ltd (4Hire) the owner and operator of the Site and are deemed accepted by You each time that You use or access the Site or Services. If You do not accept the Terms stated here, do not use the Site or Services.</span></p>

                            <h5>4Hire Content</h5>
                            <p><span style="font-size: small;">4Hire authorises You, subject to these Terms, to access and use the Site and the 4Hire Content (as defined below) and to download and print the content available on or from the Site solely for Your personal, non-commercial use. The contents of the Site, such as designs, text, graphics, images, video, information, logos, button icons, software, audio and other 4Hire content (collectively, "4Hire Content"), are protected under copyright, trademark and other laws. All 4Hire Content is the property of 4Hire. The compilation (meaning the collection, arrangement and assembly) of all content on the Site is the exclusive property of 4Hire and is protected by copyright, trademark, and other laws. Unauthorised use of the 4Hire Content may violate these laws and is strictly prohibited. You must preserve all copyright, trademarks, service mark and other proprietary notices contained in the original 4Hire Content on any authorised copy You make of the 4Hire Content.
Any code that 4Hire creates to generate or display any 4Hire Content or the pages making up any 4Hire Site is also protected by 4Hire's copyright and You may not copy or adapt such code subject to applicable law.
                                </span></p>
                            <p><span style="font-size: small;">You agree not to sell or modify the 4Hire Content or reproduce, display, publicly perform, distribute, or otherwise use the 4Hire Content in any way for any public or commercial purpose, in connection with products or services that are not those of the Site, in any other manner that is likely to cause confusion among consumers, that disparages or discredits 4Hire, that dilutes the strength of 4Hire, or that otherwise infringes 4Hires intellectual property rights. You further agree to in no other way misuse 4Hire Content. The use of the 4Hire Content on any other application, web site or in a networked computer environment for any purpose is prohibited. Any code that 4Hire creates to generate or display any 4Hire Content or the pages making up any Application or Service is also protected by 4Hire's copyright and you may not copy or adapt such code.</span></p>

                            <h5>Liabilities</h5>
                            <p><span style="font-size: small;">The SiteActs as, among other things, (1) an environment for employers to source and evaluate individuals locally listed on the Site pursuing domestic work, which fall under the categories of nannies, drivers, cooks, cleaners, carers and stewards(collectively "Jobseeker" or "Jobseekers") (2) a platform for Jobseekersto have their profiles and availability marketed to the Sites audience.
As a professional organisation that seeks to raise standards and produce excellence in all its processes, in fulfilling the Acts, 4Hirepersonally sight and screen each jobseeker at our office location(s), where we question and examine whether the jobseeker is suitable enough to be listed on the Site.
                                </span></p>
                            <p><span style="font-size: small;">In addition, 4Hire carries out checks on the background of the Jobseeker; these checks consist of visiting the address of the Jobseeker and address of the guarantor. However 4Hire does not assume any obligation to perform background checks and to the fullest extent permitted by law, disclaims any liability for failing to take any such action.</span></p>
                            <p><span style="font-size: small;">The questions and examinations that 4Hire carries out are limited and should not be taken as complete, accurate, up-to-date or conclusive of the individual’s aptness as an employee or labourer of domestic work. You are advised to make your own additional checks and examinations andcross reference the information provided to You by both the Site and the Jobseeker.
4Hire is not involved in and does not control the selection process or transaction between the Jobseeker and the employer.
                                </span></p>
                            <p><span style="font-size: small;">4Hire will not be liable for any damage, theft, deceits, misrepresentations or acts stipulated as illegal or unlawful subject to applicable law, carried out by any Jobseeker.<br>Even though each Jobseeker is sighted and screened at our office and with the background checks, please note that the jobseekers are still strangers in your home. There are risks, including but not limited to the risk of physical harm, of dealing with strangers or people acting under false pretences. You assume all risks associated with dealing with Jobseekersand individuals with whom You come in contact with through the Site or Services. We expect that You will use caution and common sense when engaging personally with a Jobseeker.</span></p>

                            <h5>Refund Policy</h5>
                            <p><span style="font-size: small;">All payments for the Site and Services are non-refundable and there are no refunds or credits for unused Services, partially used information or cancellations on the Site.</span></p>
                            <h5>Disclaimer of Warranty</h5>
                            <p><span style="font-size: small;">To the fullest extent possible by law, 4hire does not warrant that the site or any of the Services will operate error-free or that the Site and its servers are free of computer viruses or other harmful mechanisms. If Your use of the Site or the 4Hire Content results in the need for servicing or replacing equipment or data or any other costs, 4Hire is not responsible for those costs. The Site and 4Hire Content is provided on an "as is" basis without any warranties of any kind. 4Hire, to the fullest extent permitted by law, disclaims all warranties, whether express or implied, including the warranty of merchantability, fitness for particular purpose and non-infringement. 4Hire makes no warranties about the accuracy, reliability, completeness, or timeliness of the 4Hire Content, Services, software, text, graphics, and links.</span></p>
                            <h5>Links to Other Sites</h5>
                            <p><span style="font-size: small;">The Sitemay contain links to third party Web sites. These links are provided solely as a convenience to You and not as an endorsement by 4Hire of the contents on such third-party Web sites. 4Hire is not responsible for the content of linked third-party sites and does not make any representations regarding the content or accuracy of materials on such third party Web sites. If You decide to access linked third-party Web sites, You do so at Your own risk.</span></p>
                            <h5>No Resale or Unauthorised Commercial Use</h5>
                            <p><span style="font-size: small;">You agree not to resell or assign Your rights or obligations under these Terms. You also agree not to make any unauthorised commercial use of any 4Hire Site.</span></p>
                            <h5>Indemnity</h5>
                            <p><span style="font-size: small;">You agree to defend, indemnify, and hold harmless 4Hire, its affiliates, and their respective officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from Your use of the Site, Services or any 4Hire Content, or Your breach of these Terms.</span></p>
                            <h5>Governing Law</h5>
                            <p><span style="font-size: small;">These Terms shall be governed by and construed in accordance with the laws of the Federal Republic of Nigeria in force from time to time and any disputes arising out of or in connection with these Terms shall be subject to the exclusive jurisdiction of courts within Lagos State, Nigeria.</span></p>
                            <p>&nbsp;</p>

                        </fieldset> 
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>