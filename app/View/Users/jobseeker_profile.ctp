<!--PORTFOLIO SECTION START-->
<link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/jquery.steps.css" type="text/css" media="all" />
 <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<?php echo $this->Html->Script(array('jquery.steps')); ?>
<style>
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555555;
    vertical-align: middle;
    background-color: #ffffff;
    background-image: none;
    border: 1px solid #cccccc;
    border-radius: 1px;
    -webkit-box-shadow: none;
    box-shadow: none;
    -webkit-transition: none;
    transition: none;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function (){
    jQuery('[data-toggle="tooltip"]').tooltip();
    var StaffNationality = jQuery('#StaffNationality').val();
    var StaffNationality = jQuery('#StaffNationality').val();
    if(StaffNationality == 'Nigeria'){
        jQuery('#StaffImmigrationStatus').hide();
        jQuery('.immigrationStats').hide();
    }else{
        jQuery('#StaffImmigrationStatus').show();
        jQuery('.immigrationStats').show();
    }
    jQuery('#StaffNationality').change(function(){
        var StaffNationality = jQuery('#StaffNationality').val();
        console.log(StaffNationality);
        if(StaffNationality == 'Nigeria'){
            jQuery('#StaffImmigrationStatus').hide();
            jQuery('.immigrationStats').hide();
        }else{
            jQuery('#StaffImmigrationStatus').show();
            jQuery('.immigrationStats').show();
        }
    })
    var navListItems = jQuery('div.setup-panel div a'),
            allWells = jQuery('.setup-content'),
            allNextBtn = jQuery('.nextBtn');
            allPreviousBtn = jQuery('.previousBtn');
    allWells.hide();
    navListItems.click(function (e) {
        e.preventDefault();
        var $target = jQuery(jQuery(this).attr('href')),
                $item = jQuery(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    allPreviousBtn.click(function(){
        var curStep = jQuery(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepWizard = jQuery('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='file'],input[type='select']"),
            isValid = true;
        if (isValid)
            prevStepWizard.removeAttr('disabled').trigger('click');
    });
    allNextBtn.click(function(){
        var curStep = jQuery(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = jQuery('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='file'],input[type='select']"),
            isValid = true;
        jQuery(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                jQuery(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if(isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
    jQuery('div.setup-panel div a.btn-primary').trigger('click');
});
</script>

<section id="portfolio" class="clearfix content bg-white pad-btm-z" style="background: #e9eaed!important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="profile-big" style="margin-bottom: 25px;">
                    <h3><span style="color:#666;">My Account </span></h3>
                    <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
                </div>
            </div>
        </div>
        <div class="row" style="margin: 20px 0px;background: #fff;padding: 20px;">
            <div class="col-md-12">
            <h2 class="text-center">Profile Update</h2>
                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                            <p>Step 1</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p>Step 2</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                            <p>Step 3</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                            <p>Step 4</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                            <p>Step 5</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                            <p>Step 6</p>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->create('Staff', array('enctype' => 'multipart/form-data', 'class'=>'form-horizontal', 'role'=>'form', 'type' => 'file', 'novalidate'=>'novalidate'));?>
                    <div class="row setup-content" id="step-1">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step 1</h3>
                                <?php
                                    echo $this->Form->input('id', array('class'=>'form-control', 'div'=>'form-group', 'required'=>'required'));
                                    echo $this->Form->input('name', array('class'=>'form-control', 'div'=>'form-group', 'required'=>'required'));
                                    echo $this->Form->input('email', array('class'=>'form-control', 'div'=>'form-group', 'required'=>'required'));
                                    echo $this->Form->input('contact_number', array('type'=>'text','label'=>'Mobile Number 1', 'class'=>'form-control', 'div'=>'form-group', 'required'=>'required'));
                                    echo $this->Form->input('mobile_number',array('type'=>'text','label'=>'Mobile Number 2', 'class'=>'form-control', 'div'=>'form-group'));
                                    echo $this->Form->input('age',array('type'=>'text', 'class'=>'form-control', 'div'=>'form-group', 'required'=>'required'));
                                    echo $this->Form->input('sex', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group', 'required'=>'required', 'options' => array('1' => 'Male', '2' => 'Female')));
                                    echo $this->Form->input('country_id',array('selected'=>'31', 'class'=>'form-control', 'required'=>'required', 'div'=>'form-group'));
                                ?>
                                <div class="clearfix"></div>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-2">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step 2</h3>
                                <?php
                                    echo $this->Form->input('experience',array('label'=>'Experience(years)','type'=>'select', 'class'=>'form-control', 'div'=>'form-group', 'required'=>'required', 'options' => array('0'=>'0','1' => '1', '2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10')));
                                    echo $this->Form->input('marital_status', array('type' => 'select','class'=>'form-control','required'=>'required', 'div'=>'form-group', 'options' => array('2' => 'Single','1' => 'Married')));
                                    echo $this->Form->input('number_of_children', array('label'=>'Children','type' => 'select', 'class'=>'form-control', 'div'=>'form-group','required'=>'required','options' => array('0'=>'0','1' => '1', '2' => '2','3' => '3','4' => '4','5' => '5+')));
                                    echo $this->Form->input('prefered_location', array('type' => 'select', 'class'=>'form-control','required'=>'required', 'div'=>'form-group', 'options' => $locations, 'multiple'=>true, 'selected'=>$ploc));
                                    echo $this->Form->input('prefered_locality', array('label'=>'Residing Area', 'class'=>'form-control','required'=>'required', 'div'=>'form-group', 'type' => 'select', 'options' => $localities,'multiple'=>true, 'selected'=>$ploca));
                                    echo $this->Form->input('prefered_education', array('label'=>'Education <i class="fa fa-info-circle" data-toggle="tooltip" title="Can select multiple level education by holding the SHIFT or CTRL key"></i>', 'class'=>'form-control', 'div'=>'form-group', 'type' => 'select','required'=>'required', 'options' => $educations,'multiple'=>true,'selected'=>$pEdu));
                                    
                                    echo $this->Form->input('origin_id', array('label'=>'State of Origin','class'=>'form-control','required'=>'required', 'div'=>'form-group'));
                                    
                                    echo $this->Form->input('category_id', array('label'=>'Category <i class="fa fa-info-circle" data-toggle="tooltip" title="Can select multiple roles by holding the SHIFT or CTRL key"></i>', 'div'=>'form-group','required'=>'required' ,'type' => 'select','class'=>'form-control', 'multiple'=>true, 'selected'=>$pCat));
                                ?>
                                <div class="clearfix"></div><br/>
                                <button class="btn btn-primary previousBtn btn-lg pull-left" type="button">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-3">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step 3</h3>
                                <?php
                                    //echo $this->Form->input('english_speaking', array('style'=>'display:none' , 'div'=>'form-group','label'=>'','type' => 'select', 'options' => array('1' => 'Yes', '0' => 'No')));
                                    //echo $this->Form->input('language_id', array('label'=>'Mother Language','required'=>'required'));
                                    //echo $this->Form->input('language_id', array('label'=>'','style'=>'display:none', 'div'=>'form-group'));
                                    echo $this->Form->input('spoken_languages', array('label'=>'Spoken Languages <i class="fa fa-info-circle" data-toggle="tooltip" title="Can select multiple languages by holding the SHIFT or CTRL key"></i>','type' => 'select', 'class'=>'form-control','required'=>'required', 'div'=>'form-group', 'options' => $languages, 'multiple'=>true, 'style'=>'width:30%', 'selected'=>$pLang));
                                    echo $this->Form->input('religion_id', array('class'=>'form-control', 'div'=>'form-group','required'=>'required'));

                                    $minimum_salary = array('10000' => 'NGN 10,000', '15000' => 'NGN 15,000', '20000' => 'NGN 20,000', '25000' => 'NGN 25,000', '30000' => 'NGN 30,000', '35000' => 'NGN 35,000', '40000' => 'NGN 40,000', '45000' => 'NGN 45,000','50000' => 'NGN 50,000', '55000' => 'NGN 55,000', '60000' => 'NGN 60,000', '65000' => 'NGN 65,000', '70000' => 'NGN 70,000', '75000' => 'NGN 75,000', '80000' => 'NGN 80,000', '85000' => 'NGN 85,000', '90000' => 'NGN 90,000', '95000' => 'NGN 95,000', '100000' => 'NGN 100,000', '105000' => 'NGN 105,000', '110000' => 'NGN 110,000', '115000' => 'NGN 115,000', '120000' => 'NGN 120,000', '125000' => 'NGN 125,000', '130000' => 'NGN 130,000', '135000' => 'NGN 135,000', '140000' => 'NGN 140,000', '145000' => 'NGN 145,000', '150000' => 'NGN 150,000');
                                    echo $this->Form->input('base_salary', array('label'=>'Minimum Salary','type' => 'select', 'class'=>'form-control', 'div'=>'form-group','required'=>'required', 'options' => $minimum_salary));
                                   
                                    //echo $this->Form->input('max_salary',array('type'=>'text', 'options'=>$mimimul_salary, class'=>'form-control','required'=>'required', 'div'=>'form-group', 'label'=>'Expected Salary'));
                                    
                                    echo $this->Form->input('accommodation', array('label'=>'Accommodation <i class="fa fa-info-circle" data-toggle="tooltip" title="Live In – Do you need to live where you’ll be working? Live Out – Do you need to be coming from home to work? Flexible – You don’t mind any one"></i>','type' => 'select', 'class'=>'form-control', 'div'=>'form-group','required'=>'required', 'options' => array('1' => 'Live In', '2' => 'Live Out','3'=>'Flexible')));
                                    
                                    if($staffs['Staff']['image1'] != ""){
                                        echo $this->Form->input('image1',array('label'=>'Image 1 <i class="fa fa-info-circle" data-toggle="tooltip" title="Please keep your head straight and smile with your mouth closed"></i>', 'div'=>'form-group', 'type'=>'file', 'class'=>'form-control'));
                                        echo $this->Html->image(PAGE_IMAGES_URL.$staffs['Staff']['image1'], array('title'=>"image", 'alt'=>'image1', 'width'=>'150', 'style'=>'display: block;'));
                                    }else{
                                        echo $this->Form->input('image1',array('label'=>'Image 1 <i class="fa fa-info-circle" data-toggle="tooltip" title="Please keep your head straight and smile with your mouth closed"></i>', 'div'=>'form-group', 'type'=>'file', 'class'=>'form-control', 'required'=>'required'));
                                    }
                                    
                                    
                                    if($staffs['Staff']['image2'] != ""){
                                        echo $this->Form->input('image2',array('label'=>'Image 2 <i class="fa fa-info-circle" data-toggle="tooltip" title="Please Keep your head straight and make a big smile with your teeth"></i>', 'div'=>'form-group', 'class'=>'form-control', 'type'=>'file'));

                                        echo $this->Html->image(PAGE_IMAGES_URL.$staffs['Staff']['image2'], array('title'=>"image", 'alt'=>'image1', 'width'=>'150', 'style'=>'display: block;'));
                                    }else{
                                        echo $this->Form->input('image2',array('label'=>'Image 2 <i class="fa fa-info-circle" data-toggle="tooltip" title="Please Keep your head straight and make a big smile with your teeth"></i>', 'div'=>'form-group', 'class'=>'form-control', 'type'=>'file' ,'required'=>'required'));
                                    }

                                    echo '<br/>';
                                    if($staffs['Staff']['video'] != ""){
                                        echo $this->Form->input('video',array('label'=>'Tell us in 1 minute about yourself and what you like to do <i class="fa fa-info-circle" data-toggle="tooltip" title="Tell us in 1 minute about yourself and what you like to do (not more than 10MB)"></i>', 'div'=>'form-group', 'class'=>'form-control', 'type'=>'file'));

                                        echo $this->Html->media('user_videos/'.$staffs["Staff"]["video"], array('fullBase' => true, 'type'=>'video/mp4', 'width'=>'320', 'height'=>'240', 'controls'=>'controls'));
                                    }else{
                                        echo $this->Form->input('video',array('label'=>'Tell us in 1 minute about yourself and what you like to do <i class="fa fa-info-circle" data-toggle="tooltip" title="Tell us in 1 minute about yourself and what you like to do (not more than 10MB)"></i>', 'div'=>'form-group', 'class'=>'form-control', 'type'=>'file'));
                                    }
                                ?>

                                <div class="clearfix"></div><br/>
                                <button class="btn btn-primary previousBtn btn-lg pull-left" type="button">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-4">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step 4</h3>
                                <?php 
                                    //echo $this->Form->input('mini_biography', array('id' => 'pagecontent2', 'class'=>'pcontent','required'=>'required'));
                                    //echo $this->Form->input('english_speaking', array('type' => 'select','required'=>'required', 'options' => array('1' => 'Yes', '0' => 'No')));
                                    //echo $this->Form->input('status', array('type' => 'select','required'=>'required', 'options' => array('0' => 'Inactive', '1' => 'Active')));
                                    echo $this->Form->input('hired', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group','required'=>'required', 'options' => array('0' => 'Available', '1' => 'Hired')));
                                    
                                    //echo $this->Form->input('agent_id', array('type' => 'select', 'options' => $plans_list, 'multiple'=>false, 'style'=>'width:30%'));
                                    
                                    echo $this->Form->input('nationality', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group', 'class'=>'form-control', 'required'=>'required', 'options' => $nationality_list, 'multiple'=>false));

                                    echo $this->Form->input('immigration_status', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group immigrationStats', 'class'=>'form-control', 'required'=>'required', 'options' => $immigration_list, 'multiple'=>false));
                                    
                                    echo $this->Form->input('profile_description', array('id' => 'pagecontent1', 'class'=>'form-control', 'div'=>'form-group','required'=>'required', 'label'=>'About'));
                                    
                                    echo $this->Form->input('work_experience', array('id' => 'pagecontent3', 'class'=>'form-control pcontent','required'=>'required', 'div'=>'form-group'));
                                    
                                    echo $this->Form->input('hobbies', array('label'=>'Hobbies & Interests','id' => 'pagecontent4', 'class'=>'form-control pcontent','required'=>'required', 'div'=>'form-group'));
                                ?>
                                <div class="clearfix"></div><br/>
                                <button class="btn btn-primary previousBtn btn-lg pull-left" type="button">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-5">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step 5</h3>
                                <?php

                                    if($staffs['Staff']['identification'] != ""){
                                        echo $this->Form->input('means_identification', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group col-md-6', 'class'=>'form-control', 'required'=>'required', 'options' => array('International Passport' => 'International Passport', 'National ID Card' => 'National ID Card', 'Voters Card' => 'Voters Card', 'Drivers Licence' => 'Drivers Licence', 'Utility Bill' => 'Utility Bill'), 'multiple'=>false));

                                        echo $this->Form->input('identification',array('label'=>'<i class="fa fa-info-circle" data-toggle="tooltip" title="Scanned Copy of your means of identification in JPG/PNG"></i>', 'div'=>'form-group col-md-6', 'class'=>'form-control', 'type'=>'file'));

                                        echo '<div class="clearfix"></div>';

                                        echo $this->Html->image(PAGE_IMAGES_URL.$staffs['Staff']['identification'], array('title'=>"image", 'alt'=>'identification', 'width'=>'350', 'style'=>'display: block;'));
                                    }else{

                                        echo $this->Form->input('means_identification', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group col-md-6', 'class'=>'form-control', 'required'=>'required', 'options' => array('International Passport' => 'International Passport', 'National ID Card' => 'National ID Card', 'Voters Card' => 'Voters Card' , 'Drivers Licence' => 'Drivers Licence', 'Utility Bill' => 'Utility Bill'), 'multiple'=>false));

                                        echo $this->Form->input('identification',array('label'=>'<i class="fa fa-info-circle" data-toggle="tooltip" title="Scanned Copy of your means of identification in JPG/PNG"></i>', 'div'=>'form-group col-md-6', 'class'=>'form-control', 'type'=>'file' ,'required'=>'required'));
                                    }
                                    
                                    echo "<br/>";
                                    echo $this->Form->input('bvn', array('label'=>'BVN','id' => 'bvn', 'class'=>'form-control','required'=>'required', 'div'=>'form-group col-md-6', "onkeypress"=>"return isNumeric(event)", "oninput"=>"maxLengthCheck(this)", "type" => "number", "minlength" => "11", "maxlength" => "11", "min"=>"1"));

                                    echo '<div class="col-md-6" style="margin-top:25px;"><button class="btn btn-danger btn-sm pull-left bvn-button col-md-3" type="button">Confirm BVN</button><div class="processing" id="processing">'.$this->Html->image('preloader.gif', array('width'=>'60px', 'height'=>'30px')).'
                                        Checking BVN
                                    </div></div>';

                                    echo '<div class="clearfix"></div>';

                                    echo $this->Form->input('bvndetails', array('type'=>'text', 'label'=>'BVN Details', 'id' => 'bvn-details','readonly'=>'readonly' ,'class'=>'form-control', 'required'=>'required', 'div'=>'form-group col-md-12'));

                                ?>
                                <div class="clearfix"></div><br/>
                                <button class="btn btn-primary previousBtn btn-lg pull-left" type="button">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-6">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Step 6</h3>
                                <?php 
                                    //echo $this->Form->input('mini_biography', array('id' => 'pagecontent2', 'class'=>'pcontent','required'=>'required'));
                                    //echo $this->Form->input('english_speaking', array('type' => 'select','required'=>'required', 'options' => array('1' => 'Yes', '0' => 'No')));
                                    //echo $this->Form->input('status', array('type' => 'select','required'=>'required', 'options' => array('0' => 'Inactive', '1' => 'Active')));
                                    echo $this->Form->input('hired', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group','required'=>'required', 'options' => array('0' => 'Available', '1' => 'Hired')));
                                    
                                    //echo $this->Form->input('agent_id', array('type' => 'select', 'options' => $plans_list, 'multiple'=>false, 'style'=>'width:30%'));
                                    
                                    echo $this->Form->input('nationality', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group', 'class'=>'form-control', 'required'=>'required', 'options' => $nationality_list, 'multiple'=>false));

                                    echo $this->Form->input('immigration_status', array('type' => 'select', 'class'=>'form-control', 'div'=>'form-group immigrationStats', 'class'=>'form-control', 'required'=>'required', 'options' => $immigration_list, 'multiple'=>false));
                                    
                                    echo $this->Form->input('profile_description', array('id' => 'pagecontent1', 'class'=>'form-control', 'div'=>'form-group','required'=>'required', 'label'=>'About'));
                                    
                                    echo $this->Form->input('work_experience', array('id' => 'pagecontent3', 'class'=>'form-control pcontent','required'=>'required', 'div'=>'form-group'));
                                    
                                    echo $this->Form->input('hobbies', array('label'=>'Hobbies & Interests','id' => 'pagecontent4', 'class'=>'form-control pcontent','required'=>'required', 'div'=>'form-group'));
                                ?>
                                <div class="clearfix"></div><br/>
                                <button class="btn btn-primary previousBtn btn-lg pull-left" type="button">Previous</button>
                                <button class="btn btn-success btn-lg pull-right" id="finished" type="submit">Finish!</button>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</section>
<!--/portfolio-->
<div id="otpModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Enter OTP Sent to your phone</h4>
                </div>
            </div>
            <div class="modal-body">
                <form id="otpForm" class="inline-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="card_no">OTP:</label>
                                <input type="text" name="otp" class="form-control otp" placeholder="Enter OTP Sent to your phone" required="required">
                                <input type="hidden" name="ref" class="form-control ref" required="required">
                                <input type="hidden" name="bvn" class="form-control bvn2" required="required">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="processing1" id="processing1">
                    <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')); ?>
                    Processing Payment
                </div>
                <button type="button" class="btn btn-warning otpValidate">Validate OTP</button>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery(document).ready(function (){
        jQuery("#processing").hide();
        jQuery("#processing1").hide();
        jQuery('#finished').attr("disabled", true);
        jQuery(".bvn-button").click(function(){
            var bvn = jQuery('#bvn').val();
            jQuery("#processing").show();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo DOMAIN_NAME_PATH.'Users/bvnVet/'; ?>",
                data: {bvn: bvn},
                complete: function(){
                    jQuery("#processing").hide();
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);
                    console.log(data);
                    if(obj.data.responseCode == "00"){
                        jQuery('.ref').val(obj.data.transactionReference);
                        jQuery('.bvn2').val(bvn);
                        jQuery('#otpModal').modal('show');
                    }else{
                        jQuery("#bvn").addClass("has-error")
                    }
                }
            });
        });
    })

    jQuery(document).on('click', '.otpValidate', function(e){
        var amount = jQuery('.otp').val();
        var ref = jQuery('.ref').val();
        jQuery("#processing1").show();
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Users/validateOtpBvn/'; ?>",
            data: jQuery("#otpForm").serialize(),
            complete: function(){
                jQuery("#processing1").hide();
            },
            success: function(data){
                if(data == 'Failed'){
                    jQuery('#otpModal').modal('hide');
                    jQuery('#bvn-details').val('');
                    jQuery('#bvn').val('');
                    jQuery('#finished').attr("disabled", true);
                }else{
                    jQuery('#bvn-details').val(data);
                    jQuery('#otpModal').modal('hide');
                    jQuery('#finished').attr("disabled", false);
                }
            }
        });
    })

    if(jQuery('#bvn').val() == "" || jQuery('#bvn-details').val() == ""){
        jQuery('#finished').attr("disabled", true);
    }

    function maxLengthCheck(object) {
        if (object.value.length > object.maxLength){
          object.value = object.value.slice(0, object.maxLength);
        }
    }
        
    function isNumeric (evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode (key);
        var regex = /[0-9]|\./;
        if ( !regex.test(key) ) {
          theEvent.returnValue = false;
          if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>