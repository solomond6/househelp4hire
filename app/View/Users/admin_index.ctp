    <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Subscriber Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Manage Subscriber</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6" style="margin-bottom: 25px; border: 1px solid #ddd; border-radius: 3px; padding: 15px;">										<?php echo $this->Form->Create('User', array('action' => 'index'));?>
                                           <h4 style="margin: 0px 0px 15px 0px; font-size: 15px; color: #d9544f; font-weight: 700;  padding: 0px;"><i class="fa fa-search"></i> Search Employer</h4> 
                                            <label>Email / UID / Name</label>
                                            <div class="controls">
                                            	<?php echo $this->Form->input('key', array('label' => false,'class'=>'span12', 'div' => false, 'value' => isset($this->request->data['User']['key']) ? $this->request->data['User']['key'] : '')) ?>
                                            </div>
                                            <input type="submit" class="btn btn-success" value="Search">
                                             <?php echo $this->Form->end();?>
                                        </div>
                                        <div class="span6" style="margin-bottom: 25px;">
                                            <a href="Users/add" class="btn btn-primary pull-right"><i class="fa fa-user-plus"></i> Add New Subscriber</a>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        
                                    </div>
                                    <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
                                        <tr>
                                            <th><?php echo $this->Paginator->sort('id', 'UID'); ?></th>
											<th><?php echo $this->Paginator->sort('email');?></th>
											<th><?php echo $this->Paginator->sort('name', 'Name');?></th>
											<th><?php echo $this->Paginator->sort('registration_date', 'Registered On');?></th>
											<th style="text-align:center;"><?php __('');?>Actions</th>
                                        </tr>
                                     </thead>
                                        <tbody>
                                       <?php
	
										foreach ($users as $user):
										
										 if($user['User']['user_type'] == 2 )
										 {
										?>
										<tr>
		<td><?php echo $user['User']['emp-uid']==null ? 'N/A' : $user['User']['emp-uid']; ?>&nbsp;</td>
		<td><?php echo $user['User']['email']; ?>&nbsp;</td>
		<td><?php echo $user['User']['name']; ?></td>
		<td><?php echo date("d-M-Y",strtotime($user['User']['registration_date'])); ?></td>
		<td class="actions">
		
			<?php 
			if($user['User']['status'] == 1 )
			{
				echo $this->Html->link('<i class="fa fa-check"></i> Active', array('action' => 'suspend', $user['User']['id']), array('title' => 'Click here to deactive','class' => 'btn btn-default','escape' => FALSE));
			} 
			else
			{
				echo $this->Html->link('<i class="fa fa-check"></i> Inactive', array('action' => 'activate', $user['User']['id']), array('title' => 'Click here to active','class' => 'btn btn-default','escape' => FALSE));
			}?>
			
			
			<?php echo $this->Html->link('<i class="fa fa-eye"></i> View', array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'edit', $user['User']['id']),array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<a href="#conDialog" role="button"  class="btn btn-danger" data-toggle="modal" data-attr="<?php echo $user['User']['id']?>" id="deleteConfirm"><i class="fa fa-trash-o" ></i> Delete</a>&nbsp;
			
		</td>
	</tr>
									<?php } endforeach; ?>
									</tbody>
										</table>
								<p>
							<!--, showing %current% records out of %count% total, starting on record %start%, ending on %end%-->
					<?php
				
		echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%')));
	?>	</p>

	<div class="paging">
    	<?php echo $this->Paginator->prev('Previous', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
		<?php echo $this->Paginator->numbers(array('separator'=>'')); ?>
		<?php echo $this->Paginator->next('Next', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
	</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                 <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			                <h3 id="myModalLabel1">Are you sure ?</h3>
			            </div>
            
			            <div class="modal-footer">
			                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
			                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
			            </div>
        		</div>
<script>
     var record_id="";
     jQuery(document).ready(function(){
     		jQuery("#deleteConfirm").click(function(){
     			var id=jQuery(this).attr("data-attr");
     			record_id=id;
     		})
     })
	
	function Delete()
	{
		console.log("delete/"+record_id);
		document.location.href="Users/delete/"+record_id;
	}
</script>