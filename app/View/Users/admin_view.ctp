<!--
<hr class="soften">
<hr class="soften">
<hr class="soften">
<div class="container">
 <h4>Active Bookings</h4>
 <?php //pr($bookings); echo $this->Session->read('Auth.User.id');?>
 <?php if(count($activeBookings) > 0){ ?>
 <table class="table table-striped">
  <thead>
	<tr>
	  <th>Name</th>
	  <th>Category</th>
	  <th>Schedule</th>
	  <th>Status</th>
	  <th>&nbsp;</th>
	</tr>
  </thead>
  <tbody>
   <?php 
   foreach($activeBookings as $activeBooking){ 
		//$startTimeDB = strtotime($activeBooking['Booking']['start_time']);
		//$endTimeDB = strtotime($activeBooking['Booking']['end_time']);
		//$diiffTime = $endTimeDB - $startTimeDB;
		//$hour = ($diiffTime / (60*60));
		//$minute = (($diiffTime - ($hour*60*60)) / 60);
		//$second = ($diiffTime - ($hour*60*60) - ($minute*60));
		$date = date("F j, Y", strtotime($activeBooking['Booking']['date']));
		$startTime = date("g:i a",strtotime($activeBooking['Booking']['start_time']));
		//$endTime = date("g:i a",strtotime($activeBooking['Booking']['end_time']));
		//$duration = $hour.' Hour(s) '.$minute.' Minute(s) '.$second.' Second(s) ';
	?>
	<tr>
	  <td><?php echo $activeBooking['Staff']['name']; ?></td>
	  <td><?php echo $activeBooking['Staff']['Category']['name']; ?></td>
	  <td><?php echo $date.' '.$startTime.'<br/>Location: '.$activeBooking['Booking']['location']; ?></td>
	  <td>
		<?php 
		//echo $activeBooking['Booking']['status'];
		echo $this->Form->create('Booking', array('action'=>'status/'.$activeBooking['Booking']['id']), array('id'=>'form'.$activeBooking['Booking']['id']));
			echo $this->Form->input('status', array('onchange'=>'this.form.submit();', 'label'=>false, 'div'=>false, 'type' => 'select', 'options' => $bookingStatus, 'selected'=>$activeBooking['Booking']['status']));
		echo $this->Form->end();
		?>
	  </td>
	  <td>
	  <?php echo $this->Html->link('View', array('controller' => 'Staffs', 'action' => 'view',$activeBooking['Staff']['id']), array('class'=>'btn btn-danger btn-primary btn-lg')); ?>
	  <a href="#myModal4" role="button" data-toggle="modal" class="btn btn-warning btn-primary btn-lg" id="feedback" onclick="bookinhIdVal('<?php echo $activeBooking['Booking']['id']; ?>');">Feedback</a>
	  </td>
	</tr>
	<?php } ?>
  </tbody>
</table>
<?php } else { ?>
<p>No Active Bookings Available</p>
<?php } ?>
</div>
<hr class="soften">
<div class="container">
 <h4>Past Bookings</h4>
 <?php if(count($pastBookings) > 0){ ?>
 <table class="table table-striped">
  <thead>
	<tr>
	  <th>Name</th>
	  <th>Category</th>
	  <th>Schedule</th>
	  <th>Status</th>
	  <th>&nbsp;</th>
	</tr>
  </thead>
  <tbody>
   <?php 
   foreach($pastBookings as $pastBooking){ 
		//$startTimeDB = strtotime($pastBooking['Booking']['start_time']);
		//$endTimeDB = strtotime($pastBooking['Booking']['end_time']);
		//$diiffTime = $endTimeDB - $startTimeDB;
		//$hour = ($diiffTime / (60*60));
		//$minute = (($diiffTime - ($hour*60*60)) / 60);
		//$second = ($diiffTime - ($hour*60*60) - ($minute*60));
		$date = date("F j, Y", strtotime($pastBooking['Booking']['date']));
		$startTime = date("g:i a",strtotime($pastBooking['Booking']['start_time']));
		//$endTime = date("g:i a",strtotime($pastBooking['Booking']['end_time']));
		//$duration = $hour.' Hour(s) '.$minute.' Minute(s) '.$second.' Second(s) ';
	?>
	<tr>
	  <td><?php echo $pastBooking['Staff']['name']; ?></td>
	  <td><?php echo $pastBooking['Staff']['Category']['name']; ?></td>
	  <td><?php echo $date.' '.$startTime.'<br/>Location: '.$pastBooking['Booking']['location']; ?></td>
	  <td>
		<?php 
		echo $pastBooking['Booking']['status'];
		?>
	  </td>
	  <td>
	  <?php echo $this->Html->link('View', array('controller' => 'Staffs', 'action' => 'view',$pastBooking['Staff']['id']), array('class'=>'btn btn-danger btn-primary btn-lg')); ?>
	  </td>
	</tr>
	<?php } ?>
  </tbody>
</table>
<?php } else { ?>
<p>No Past Bookings Available</p>
<?php } ?>
</div>
<hr class="soften">
</div>
-->
 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Subscriber Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Subscriber</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $user['User']['id']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Name <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $user['User']['name']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Unique ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php echo $user['User']['emp-uid']==null ? 'N/A' : $user['User']['emp-uid']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Email Address <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php echo $user['User']['email']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Phone Number <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php echo $user['User']['phone']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Location <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php echo $this->Html->link($user['Location']['name'], array('controller'=>'locations', 'action'=>'view', $user['Location']['id'])); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Status <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  	<?php if($user['User']['status']=='1') 
					{
						 echo 'Active'; 
					}
					else
					{
						 echo 'Inactive'; 
					} ?></span><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s" style="color: #993!important;">Active Bookings <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">No Active Boking Available</span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s" style="color: #993!important;">Past Bookings <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">No Past Boking Available</span><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="<?php echo $this->request->referer(); ?>" class="btn btn-success btn-lg">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->