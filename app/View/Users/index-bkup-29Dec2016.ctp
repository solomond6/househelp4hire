 <!--HOME SECTION START-->
        <section id="home" class="clearfix" style="background: #8d665b url(<?php echo DOMAIN_NAME_PATH; ?>images/HNCK2757-1300x866.jpg)no-repeat center;background-size: cover;">
            <div class="top-strip">
            <?php if ($this->Session->check('Auth.User')) {?>
              <a download="<?php echo $siteSetting['Setting']['brochure']; ?>" href="<?php echo PDF_UP_URL.$siteSetting['Setting']['brochure']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><i class="fa fa-gift"></i> Click here to download The Ultimate Guide</a>
              <?php } else {?>
               <a><i class="fa fa-gift"></i> Login to download The Ultimate Guide </a>
               <?php } ?>
            </div>
            <div class="header clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-9 col-sm-4">
                            <!--<a class="logo" href="index.php"><img src="images/logo.png" alt="logo" /></a>-->
                            <?php echo $this->Html->link($this->Html->image('site_logo/'.$siteSetting['Setting']['site_logo'], array('alt'=>$siteSetting['Setting']['site_name'], 'title'=>$siteSetting['Setting']['site_name'])),array('controller' => 'Users','action' => 'index'),array('escape' => false, 'ondragstart'=>'return false', 'onselectstart'=>'return false','class'=>'logo'));?>
                        </div>
                        <!--/.col-md-4-->
                        <div class="col-md-8">
                            <!--MENU BUTTON(ON TABLET/MOBILE) START-->
                            <div class="menu-btn" data-target=".nav-collapse" data-toggle="collapse">
                                <span class="fa fa-th"></span>
                            </div>
                            <!--MENU BUTTON END-->
                            <!--NAVIGATION START-->
                            <div class="menu-sample-menu-for-homepage-container">
                                <ul id="menu-sample-menu-for-homepage" class="navigation desktop-menu menu">
                                	<?php
										if ($this->Session->check('Auth.User')) {
											echo '<li><a href="'.DOMAIN_NAME_PATH.'Users/logout"  class="hiw">Sign Out</a></li>';
											echo '<li><a href="'.DOMAIN_NAME_PATH.'pages/howitworks" class="hiw">How it works</a></li>';
											echo '<li><a href="'.DOMAIN_NAME_PATH.'Users/dashboard"  class="hiw">My Account</a></li>';
										}
										else{
										?>
										    <li><a href="#" data-toggle="modal" data-target="#SignUp">Sign Up</a></li>
                                    		<li><a href="<?php echo DOMAIN_NAME_PATH; ?>pages/howitworks" class="hiw">How it works</a></li>
                                    		<li><a href="#" data-toggle="modal" data-target="#SignIn">Sign In</a></li>
								<?php } ?>
                                </ul>
                            </div>
                            <ul class="nav-collapse mobile-menu">
                            </ul>
                            <!--NAVIGATION END-->
                        </div>
                        <!--/.col-md-4-->
                    </div>
                    <!--/.row-->
                </div>
                <!--/.container-->
            </div>
            <!--/.header-->
            <div class="clearfix"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-content clearfix">
                            <!-- Carousel-->
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <div class="carousel-inner" role="listbox">
                                <?php 
									if(count($categories) > 0){ 
									$counter=1;
										foreach ($categories as $category): 
											if($counter==1) {
												$counter=2;
											
								?>			
									<div class="item active">
								<?php } else {?>
									<div class="item">
								<?php } ?>
											<a href="<?php echo DOMAIN_NAME_PATH; ?>domestic-staffs/<?php echo $category['Category']['friendly_url']; ?>" class="hasmenu" id="banner-<?php echo $category['Category']['id']; ?>" oncontextmenu="customContext(this.id);">
											<?php echo $this->Html->image('category/'.$category['Category']['image'], array('alt'=>$category['Category']['name'], 'class'=>'third-slide', 'ondragstart'=>'return false', 'onselectstart'=>'return false')); ?></a> 
								 <div class="container">
                                            <div class="carousel-caption">
                                                <!--<h1>Cook</h1>-->
                                            </div>
                                 </div>
                                 </div>
								<?php
										endforeach; 
									} 
								?>
                                    
                                      
                                </div>
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <!-- /.carousel -->
                            <!--SLIDER CAPTION START-->
                            <!--**************************changes 26-04-2015*************************-->
                            <div class="title-caption t-cap-mrgn">
                                <h1 class="custom-head">Solutions to your household needs, by safe, reliable, vetted personnel.</h1>
                            </div>
                            <!--**************************changes 26-04-2015*************************-->
                            <!--/.title-caption-->
                            <!--SLIDER CAPTION END-->
                            <div class="row"> 
                            		  <?php echo $this->Form->create('Staffs', array('action' => 'index', 'id'=>'StaffsIndexFormHeader')); 
				                            if(isset($this->request->data['Staff']['search'])){
				                                    $searchFieldValue = $this->request->data['Staff']['search'];
				                            } else {
				                                    $searchFieldValue = null;
				                            }
                            			?>
                                <div class="col-md-6 col-md-offset-3"><h5 class="iwh">I want to hire a</h5>
                                    <div class="input-group home-s">
                                        <!--<input type="text" class="form-control big-search" placeholder="">-->
                                       
		                            <?php echo $this->Form->input('search', array('value'=>$searchFieldValue, 'class'=>"index-inp", 'name'=>"data[Staff][search]", 'id'=>"appendedInputButton", 'label'=>false, 'div'=>false)); ?>
		                            <select id="drpSearch" class="index-sel">
                                        <?php if(count($categories) > 0){ 
										  foreach ($categories as $category): 
										?>  
                                            <option><?php echo $category['Category']['name_singular'] ?></option>
                                            <?php endforeach; }?>
                                        </select>
                                        <span class="input-group-btn brdr-m">
                                            <button class="btn btn-default btn-custom big-search-btn" type="submit">Search</button>
                                        </span>
                                        
                                    </div>
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                            <!--/.centering-->
                        </div>
                        <!--/.slider-content-->
                    </div>
                    <!--/.col-sm-6-->
                </div>
                <!--/row-->
            </div>
            <!--/container-->
        </section>
        <!--/home-->
        <!--HOME SECTION END-->
          <!--CONTACT SECTION END-->
       
        <!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content" style="background: #fff;">
            <div class="container">
                <div class="title-content clearfix">
                    <div class="title-inner">
                        <h3 style="background: #fff;">How it Works</h3>
                    </div>
                    <!--/.title-inner-->
                </div>
                <!--/.title-content-->
                <!--AJAX PORTFOLIO WILL SHOWN HERE-->
                <div id="workslist">
                    <div class="clearfix worksajax">
                    </div>
                    <!--/.worksajax -->
                </div>
                <!--/workslist-->
                <!--AJAX PORTFOLIO END-->
                <!--TEAM LIST START-->
                <div class="row" id="team">
                    <div class="col-md-4 col-sm-4 team-box">
                        <a href="#" class="bwWrapper team-ajax text-center" data-link="#">
                            <i class="fa fa-search hpage-icon"></i>
                        </a>
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <p class="team-position mrgn-btm-zero"><?php echo $siteSetting['Setting']['how_it_works_block_1']; ?></p>
                        </div>
                        <!--/.white-bg-->
                    </div>
                    <!--/.col-md4-->
                    <div class="col-md-4 col-sm-4 team-box next-arrow">
                        <a href="#" class="bwWrapper team-ajax text-center" data-link="#">
                            <i class="fa fa-check hpage-icon"></i>
                        </a>
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <p class="team-position mrgn-btm-zero"><?php echo $siteSetting['Setting']['how_it_works_block_2']; ?></p>
                        </div>
                        <!--/.white-bg-->
                    </div>
                    <!--/.col-md4-->
                    <div class="col-md-4 col-sm-4 team-box next-arrow">
                        <a href="#" class="bwWrapper team-ajax text-center" data-link="#">
                            <i class="fa fa-phone hpage-icon"></i>
                        </a>
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <p class="team-position mrgn-btm-zero"><?php echo $siteSetting['Setting']['how_it_works_block_3']; ?> <!--<a href="#" style="color: #f17939;" data-toggle="modal" data-target="#Plans">view Plans</a>--></p>
                        </div>
                        <!--/.white-bg-->
                    </div>
                    <!--/.col-md4-->
                </div>
                <!--/.row-->
                <!--TEAM LIST END-->
                <div class="spacing40 clearfix"></div>
            </div>
            <!--/.container-->
        </section>
        <!--/portfolio-->
        <!--PORTFOLIO SECTION END-->
        <!--SERVICES SECTION START-->
        <section id="services" class="clearfix content">
            <div class="container">
                <div class="title-content clearfix">
                    <div class="title-inner">
                        <!--**************************changes 26-04-2015*************************-->
                        <h3 style="font-size:28px;">Click on what you’re looking for?</h3>
                        <!--**************************changes 26-04-2015*************************-->
                    </div>
                    <!--/.title-inner-->
                </div>
                <!--/.title-content-->
                <!--SERVICES TAB START-->
                <div id="tab-services" class="tab-content white-bg">
                    <div class="row" style="background: #f2f2f2;">
                        <!--PORTFOLIO LOOP START-->
                        <div class="col-md-4 col-sm-6 port-item" id="post-35">
                            <div class="port-inner white-bg clearfix">
                                <!--get original featured image link-->
                                <a class="box-40 hovers bwWrapper" href="domestic-staffs/cooks"><img width="600" height="600" src="<?php echo DOMAIN_NAME_PATH; ?>images/cooks-s.jpg" class="attachment-portfolio-image wp-post-image" alt="pic1" /></a>
                                <div class="padding box-60 back-white">
                                    <h3 class="text-center">Cooks</h3>
                                </div><!--/.padding-->
                            </div><!--/.port-inner-->
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6 port-item" id="post-35">
                            <div class="port-inner white-bg clearfix">
                                <!--get original featured image link-->
                                <a class="box-40 hovers bwWrapper" href="domestic-staffs/nannies"><img width="600" height="600" src="<?php echo DOMAIN_NAME_PATH; ?>images/nannies-s.jpg" class="attachment-portfolio-image wp-post-image" alt="pic1" /></a>
                                <div class="padding box-60 back-white">
                                    <h3 class="text-center">Nannies</h3>
                                </div><!--/.padding-->
                            </div><!--/.port-inner-->
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6 port-item" id="post-35">
                            <div class="port-inner white-bg clearfix">
                                <!--get original featured image link-->
                                <a class="box-40 hovers bwWrapper" href="domestic-staffs/drivers"><img width="600" height="600" src="<?php echo DOMAIN_NAME_PATH; ?>images/drivers-s.jpg" class="attachment-portfolio-image wp-post-image" alt="pic1" /></a>
                                <div class="padding box-60 back-white">
                                    <h3 class="text-center">Drivers</h3>
                                </div><!--/.padding-->
                            </div><!--/.port-inner-->
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6 port-item" id="post-35">
                            <div class="port-inner white-bg clearfix">
                                <!--get original featured image link-->
                                <a class="box-40 hovers bwWrapper" href="domestic-staffs/carers"><img width="600" height="600" src="<?php echo DOMAIN_NAME_PATH; ?>images/carers-s.jpg" class="attachment-portfolio-image wp-post-image" alt="pic1" /></a>
                                <div class="padding box-60 back-white">
                                    <h3 class="text-center">Carers</h3>
                                </div><!--/.padding-->
                            </div><!--/.port-inner-->
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6 port-item" id="post-35">
                            <div class="port-inner white-bg clearfix">
                                <!--get original featured image link-->
                                <a class="box-40 hovers bwWrapper" href="domestic-staffs/cleaners"><img width="600" height="600" src="<?php echo DOMAIN_NAME_PATH; ?>images/cleaners-s.jpg" class="attachment-portfolio-image wp-post-image" alt="pic1" /></a>
                                <div class="padding box-60 back-white">
                                    <h3 class="text-center">Cleaners</h3>
                                </div><!--/.padding-->
                            </div><!--/.port-inner-->
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6 port-item" id="post-35">
                            <div class="port-inner white-bg clearfix">
                                <!--get original featured image link-->
                                <a class="box-40 hovers bwWrapper" href="domestic-staffs/stewards"><img width="600" height="600" src="<?php echo DOMAIN_NAME_PATH; ?>images/stwards-s.jpg" class="attachment-portfolio-image wp-post-image" alt="pic1" /></a>
                                <div class="padding box-60 back-white">
                                    <h3 class="text-center">Stewards</h3>
                                </div><!--/.padding-->
                            </div><!--/.port-inner-->
                        </div><!--/.col-md-4-->
                        <!--PORTFOLIO LOOP END-->
                    </div>
                    <!--/.tab-content-->
                    <!--SERVICES TAB END-->
                </div>
                <!--/.container-->
        </section>
        <!--services-->
        <!--CONTACT SECTION END-->
         <div class="portfolio news-strip">
            <div class="container">
                <div class="col-md-7">
                    <h4 class="text-center news-tit text-uppercase">Subscribe To Our Newsletter<br> <span>Signup now! Stay informed on the latest from HouseHelp4Hire!</span></h4>
                </div>
                <div class="col-md-5">
                <?php echo $this->Form->create('Email', array('action' => 'add', 'novalidate'=>'')); ?>
                    <fieldset>
                        <div class="form-group nospace" style="margin-top: 17px;">
                            <div class="input-group">
                                <?php echo $this->Form->input('email', array('type'=>'email', 'label'=>false, 'div'=>false, 'placeholder'=>'Your email address', 'class'=>'form-control news-input', 'value'=>'')); ?>
                                <span class="input-group-btn">
                                	<button class="btn btn-primary news-btn" type="submit">Subscribe</button>
                                </span>
                            </div>
                        </div>
                    </fieldset>
               <?php echo $this->Form->end(); ?>
                </div>
                
            </div>
        </div>
        <!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content" style="background: #fff;">
            <div class="container">
                <div class="title-content clearfix">
                    <div class="title-inner">
                        <h3 style="background: #fff;">Why use <span>HouseHelp4Hire</span> ?</h3>
                    </div>
                    <!--/.title-inner-->
                </div>
                <!--/.title-content-->
                <!--AJAX PORTFOLIO WILL SHOWN HERE-->
                <div id="workslist">
                    <div class="clearfix worksajax">
                    </div>
                    <!--/.worksajax -->
                </div>
                <!--/workslist-->
                <!--AJAX PORTFOLIO END-->
                <!--TEAM LIST START-->
                <div class="row" id="team" style="border-bottom: 1px solid #eee;">
                    <div class="col-md-4  col-sm-4 why-u">
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <h2>
                                <i class="fa fa-shield"></i><br>Safty First</h2>
                            <p>We perform thorough background checks on each candidate and their guarantors before they are placed on our database. We also collect their photo ID and their personal information including their fingerprints.</p>
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-4 why-u" style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <h2>
                                <i class="fa fa-clock-o"></i><br>Save time & Money</h2>
                            <p>For a one-off fee, instantly contact multiple vetted domestic staff for an interview. No middle men, agents, hidden fees or levies.</p>
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-4 why-u">
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <h2>
                                <i class="fa fa-thumbs-up"></i><br>Vetting Process</h2>
                            <p>We personally interview each candidate at our office, collect their personal information, their guarantor’s information and carry out background checks. Those that do not meet our standards are not registered.</p>
                        </div>
                    </div>
                </div>
                <div class="row" id="team">
                    <div class="col-md-4  col-sm-4 why-u">
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <h2>
                                <i class="fa fa-umbrella"></i><br>Easy to Use</h2>
                            <a style="color: #666;" href="how_it_works.php"><p> Easily find a suitable candidate and call them instantly.</p></a>
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-4 why-u" style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <h2>
                                <span style="font-size: 23px; margin-bottom: -5px; display: block;">&#x20a6</span>  <br>One-Off Payment</h2>
                            <p>Make a single secure online payment and have the option to call multiple candidates.</p>
                        </div>
                    </div>
                    <div class="col-md-4  col-sm-4 why-u">
                        <div class="clearfix team-inner white-bg padding-new htc-shadow">
                            <h2>
                                <i class="fa fa-gift"></i><br>Trust</h2>
                            <p>We are a company you can trust</p>
                        </div>
                    </div>
                </div>
                <!--/.row-->
                <!--TEAM LIST END-->
                <div class="spacing40 clearfix"></div>
            </div>
            <!--/.container-->
        </section>
        <!--/portfolio-->
        <!--footer-->
    </div>