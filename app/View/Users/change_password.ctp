<?php
	echo $this->Html->css(array('style', 'validationEngine.jquery'));
	echo $this->Html->script(array('jquery.min', 'jquery.colorbox'));
	echo $this->Html->script(array('jquery.validationEngine', 'jquery.validationEngine-en', 'jconfirmaction.jquery'));
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#flashMessage").fadeOut(10000);
	$("#authMessage").fadeOut(10000);
	$('#overlay').click(function(){
		$('#overlay').fadeOut('fast');
	});
});
</script>
<script type="text/javascript">
   $(document).ready(function() {
	$("#UserChangePasswordForm").validationEngine({scroll:true, promptPosition: "topLeft"})
   });
</script>
<h6>Change Password</h6>
<?php 
echo $this->Form->create('User', array('action'=>'changePassword'));
?>
<table class="pop" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td>
		<?php echo $this->Form->input('id', array('label'=>false, 'div'=>false)); ?>
		<p><span style="color:red;">*</span> Old Password</p>
		<p><?php echo $this->Form->password('old_password', array('style'=>'width:436px;', 'label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
  </tr>
  <tr>
    <td>
		<?php echo $this->Form->input('id', array('label'=>false, 'div'=>false)); ?>
		<p><span style="color:red;">*</span> New Password</p>
		<p><?php echo $this->Form->input('password', array('style'=>'width:436px;', 'label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
  </tr>
  <tr>
	<td>
		<p><span style="color:red;">*</span> Confirm New Password</p>
		<p><?php echo $this->Form->password('confirmpassword', array('style'=>'width:436px;', 'label'=>false, 'div'=>false, 'class'=>'validate[required,equals[UserPassword]]')); ?></p>
	</td>
  </tr>
  <tr>
	<td align="left" class="btn">
	<span style="color:red;font-weight:bold;">** Required fields.</span> <?php echo $this->Form->submit(__('Save', true), array('div'=>false, 'class'=>'', 'style'=>'height:34px; font-weight:bold;font-size:12px; color:#fff;width:100px;margin-right:10px;border:0;float:right;')); ?>
	</td>
  </tr>
</table>
<?php echo $this->Form->end(); ?>