

     <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Agent Manager</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Agents</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span12" style="margin-bottom: 25px;">
                                            <a href="addagents" class="btn btn-primary pull-right"><i class="fa fa-user-plus"></i> Add New Agent</a>
                                        </div>
                                    </div>
                                     <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
                                        <tr>
                                            <th><?php echo $this->Paginator->sort('phone');?></th>
											<th><?php echo $this->Paginator->sort('name', 'Name');?></th>
											<th><?php echo $this->Paginator->sort('image', 'Image'); ?></th>
											<th class="actions" style="text-align:center;"><?php __('');?></th>
                                        </tr>
                                     	</thead>
                                        <tbody>
										  <?php
											$i = 0;
											foreach ($users as $user):
											?>
											<tr>
												<td><?php echo $user['Agent']['phone']; ?>&nbsp;</td>
												<td><?php echo $user['Agent']['name']; ?></td>
												<?php if(!empty($user['Agent']['image'])) {?>
												<td><?php echo $this->Html->image(PAGE_IMAGES_URL.$user['Agent']['image'], array('title'=>"image", 'alt'=>'image')); ?></td>
												<?php } else { ?>
												<td>N/A</td>
												<?php } ?>
												<td class="actions">
													<?php echo $this->Html->link('<i class="fa fa-eye"></i> View Staff', array('controller'=>'staffs','action' => 'viewstaff', $user['Agent']['id']),array('class' => 'btn btn-default','escape' => FALSE)); ?>
													<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'editagents', $user['Agent']['id']),array('class' => 'btn btn-default','escape' => FALSE)); ?>
													<a href="#conDialog" role="button"  class="btn btn-danger" data-toggle="modal" data-attr="<?php echo $user['Agent']['id']?>" id="deleteConfirm"><i class="fa fa-trash-o" ></i> Delete</a>
												</td>
											</tr>
										<?php endforeach; ?>
                                </tbody>
                                    </table>
                                    <p>
									<?php
									echo $this->Paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%')));
									?>	</p>

									<div class="paging">
								    	<?php echo $this->Paginator->prev('Previous', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
										<?php echo $this->Paginator->numbers(array('separator'=>'')); ?>
										<?php echo $this->Paginator->next('Next', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                 <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel1">Are you sure ?</h3>
            </div>
            
            <div class="modal-footer">
                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
            </div>
        </div>
        <script>
     var record_id="";
     jQuery(document).ready(function(){
     		jQuery("#deleteConfirm").click(function(){
     			var id=jQuery(this).attr("data-attr");
     			record_id=id;
     		})
     })
	
	function Delete()
	{
		console.log("agentdelete/"+record_id);
		document.location.href="agentdelete/"+record_id;
	}
</script>