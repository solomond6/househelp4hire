<div id="content-wrapper">
    <section id="section-hero" style="background-position: center center; z-index: 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="cta-bottom-float-border bkgd-quinary-100 text-center" style="background: none; border: none;">
                        <h1 class="promo-text">
                            Your community’s most trusted babysitters&nbsp;&amp;&nbsp;nannies. On‑demand.
                        </h1>
                            <a id="cta-find-sitters-btn" href="/signup/parent" class="btn btn-success btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                                <span class="us-text-17">Are you a <br class="visible-super-xs"><span class="weight-bold">parent</span>?</span>
                                <div class="us-text-18 weight-semi-bold text-uppercase margin-t-15">
                                    Find Sitters<i class="fa fa-angle-right margin-l-10" aria-hidden="true"></i>
                                </div>
                            </a>
                            <a id="cta-find-sitters-btn" href="/signup/parent" class="btn btn-success btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                                <span class="us-text-17">Are you a <br class="visible-super-xs"><span class="weight-bold">parent</span>?</span>
                                <div class="us-text-18 weight-semi-bold text-uppercase margin-t-15">
                                    Find Sitters<i class="fa fa-angle-right margin-l-10" aria-hidden="true"></i>
                                </div>
                            </a>
                    </div>
                </div>
            </div>
        </div>
        <!--<div id="video_background_video_0" style="z-index: -1; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; overflow: hidden;">
            <video width="1676" height="700" autoplay="" loop="" onended="this.play()" style="position: absolute; top: 0px; left: -163px;">
                <source src="https://assets.urbansitter.com/us-sym/assets/videos/homepage_final.mp4" type="video/mp4">
            </video>
        </div>-->
    </section>
    <section id="four-pinheads" class="background-section">
        <div class="container">
            <div class="headline">
                <h3 class="marketing-text">It's the <span class="weight-semi-bold">word-of-mouth</span> babysitter search made easy.</h3>
                <h4 class="marketing-text">There's no other site with local sitter knowledge this deep.</h4>
            </div>
            <div class="row map-icons visible-lg visible-md">
                <div class="col-md-12">
                    <div class="bubble-container bubble-one skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">LITTLE STARS SOCCER TEAM</span>
                                <span class="number" data-final-value="14">0</span>
                                <span class="subtitle">Recommended babysitters</span>
                            </div>
                        </div>
                    </div>
                    <div class="bubble-container bubble-two skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">ST. PETER'S ELEMENTARY SCHOOL</span>
                                <span class="number" data-final-value="35">0</span>
                                <span class="subtitle">Recommended babysitters</span>
                            </div>
                        </div>
                    </div>
                    <div class="bubble-container bubble-three skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">MOMMA'S ON THE GO</span>
                                <span class="number" data-final-value="43">0</span>
                                <span class="subtitle">Recommended babysitters</span>
                            </div>
                        </div>
                    </div>
                    <div class="bubble-container bubble-four skrollable skrollable-before" data-anchor-target="#four-pinheads">
                        <div class="bubble">
                            <div class="bubble-content">
                                <span class="title">HILLTOP COMMUNITY CENTER</span>
                                <span class="number" data-final-value="28">0</span>
                                <span class="subtitle">Recommended babysitters</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="map-icons-carousel" class="carousel slide hidden-lg hidden-md" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">LITTLE STARS SOCCER TEAM</span>
                                    <span class="number" data-final-value="14">0</span>
                                    <span class="subtitle">Recommended babysitters</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">ST. PETER'S ELEMENTARY SCHOOL</span>
                                    <span class="number" data-final-value="35">0</span>
                                    <span class="subtitle">Recommended babysitters</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">MOMMA'S ON THE GO</span>
                                    <span class="number" data-final-value="43">0</span>
                                    <span class="subtitle">Recommended babysitters</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bubble-container">
                            <div class="bubble">
                                <div class="bubble-content">
                                    <span class="title">HILLTOP COMMUNITY CENTER</span>
                                    <span class="number" data-final-value="28">0</span>
                                    <span class="subtitle">Recommended babysitters</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#map-icons-carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#map-icons-carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="cta-block green bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 text">
                        <h4 class="marketing-text pad-t-8">Sign up for free:</h4>
                    </div>
                    <div class="col-md-4 cta">
                        <a id="map-cta-signup-parent" class="btn btn-block btn-border-white half-width-sm-only upper margin-b-15 m-b-md-0 m-h-sm-auto" href="/signup/parent">Are you a parent? <strong>FIND SITTERS</strong></a>
                    </div>
                    <div class="col-md-4 cta">
                        <a id="map-cta-signup-sitter" class="btn btn-block btn-border-white half-width-sm-only upper m-h-sm-auto" href="/signup/sitter">Are you a sitter? <strong>FIND JOBS</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-three-bubbles" class="background-section">
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 headline">
                    <h2 class="promo-text">Need more than a date night sitter?<br>We have nannies, too!<br><span class="us-text-22 weight-semi-bold">Long term, short term. Part‑time, full‑time. Anytime.</span></h2>
                </div>
            </div>
        </div>
        <div class="circles-container">
            <div class="container">
                <div class="row colorful-circle">
                    <div class="col-sm-10 col-sm-offset-1 headline visible-xs">
                        <h2 class="marketing-text">Need more than a date night sitter?<br>We have nannies, too!<br><span class="us-text-22 weight-semi-bold">Long term, short term. Part‑time, full‑time. Anytime.</span></h2>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="circle orange skrollable skrollable-before" >
                            <img class="icon" src="https://assets.urbansitter.com/us-sym/assets/img/homepage/rocket.png" alt="rocket">
                            <span class="title">The Most Powerful Nanny &amp; Sitter&nbsp;Search</span>
                            <span class="description">Search by local schools and parent groups to find your neighborhood's top‑rated sitters and nannies.</span>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="circle orange skrollable skrollable-before" data-anchor-target="#section-three-bubbles">
                            <img class="icon" src="https://assets.urbansitter.com/us-sym/assets/img/homepage/shield.png" alt="shield">
                            <span class="title">Proven, Trustworthy Nannies&nbsp;&amp;&nbsp;Babysitters</span>
                            <span class="description">Get to know great caregivers through video profiles, parent reviews and repeat family badges.</span>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="circle orange skrollable skrollable-before" data-anchor-target="#section-three-bubbles">
                            <img class="icon" src="https://assets.urbansitter.com/us-sym/assets/img/homepage/check.png" alt="check">
                            <span class="title">Easily Book Interviews&nbsp;&amp;&nbsp;Jobs</span>
                            <span class="description">Review availability then book and pay online. It's quick, easy and you're off.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cta-block orange bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 text">
                            <h4 class="marketing-text pad-t-8">Join our community:</h4>
                        </div>
                        <div class="col-md-4 cta">
                            <a id="bubbles-cta-signup-parent" class="btn btn-border-white half-width-sm-only upper btn-block margin-b-15 m-b-md-0 m-h-sm-auto" href="/signup/parent">Are you a parent? <strong>FIND SITTERS</strong></a>
                        </div>
                        <div class="col-md-4 cta">
                            <a id="bubbles-cta-signup-sitter" class="btn btn-border-white half-width-sm-only upper btn-block m-h-sm-auto" href="/signup/sitter">Are you a sitter? <strong>FIND JOBS</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>