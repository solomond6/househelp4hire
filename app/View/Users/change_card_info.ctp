<?php
	echo $this->Html->css(array('style', 'validationEngine.jquery'));
	echo $this->Html->script(array('jquery.min', 'jquery.colorbox'));
	echo $this->Html->script(array('jquery.validationEngine', 'jquery.validationEngine-en', 'jconfirmaction.jquery'));
?>
<script type="text/javascript">
$(document).ready(function(){
	$("#flashMessage").fadeOut(10000);
	$("#authMessage").fadeOut(10000);
	$('#overlay').click(function(){
		$('#overlay').fadeOut('fast');
	});
});
</script>
<script type="text/javascript">
   $(document).ready(function() {
	$("#UserChangeCardInfoForm").validationEngine({scroll:true, promptPosition: "topLeft"})
   });
</script>
<h6>Store Credit Card Info</h6>
<?php 
echo $this->Form->create('User', array('action'=>'changeCardInfo'));
?>
<table class="pop" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td colspan="2">
		<?php echo $this->Form->input('id', array('label'=>false, 'div'=>false)); ?>
		<p><span style="color:red;">*</span> Name on Card</p>
		<p><?php echo $this->Form->input('name_on_card', array('style'=>'width:425px;', 'label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
  </tr>
  <tr>
    <td colspan="2">
		<p><span style="color:red;">*</span> Card Number <?php echo $this->Html->image('img_credit_cards.gif', array('alt'=>'')); ?></p>
		<p><?php echo $this->Form->input('card_number', array('style'=>'width:425px;', 'label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
  </tr>
  <tr>
	<td>
		<p><span style="color:red;">*</span> Card Type</p>
		<p><?php echo $this->Form->input('card_type', array('type'=>'select', 'empty'=>'Select Card Type', 'options'=>array('Visa'=>'Visa', 'Master'=>'Master', 'American Express'=>'American Express'), 'label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
    <td>
		<p><span style="color:red;">*</span> Card C.V.V.</p>
		<p><?php echo $this->Form->input('cvv', array('label'=>false, 'div'=>false, 'class'=>'validate[required]')); ?></p>
	</td>
  </tr>
  <tr>
	<td>
		<p><span style="color:red;">*</span> Card Expiry Month</p>
		<p><?php 
		$monthArr = array('01'=>'01 - Jan', '02'=>'02 - Feb', '03'=>'03 - Mar', '04'=>'04 - Apr', '05'=>'05 - May', '06'=>'06 - Jun', '07'=>'07 - Jul', '08'=>'08 - Aug', '09'=>'09 - Sep', '10'=>'10 - Oct', '11'=>'11 - Nov', '12'=>'12 - Dec');
		echo $this->Form->input('card_expiry_month', array('type'=>'select', 'empty'=>'Select Month', 'options'=>$monthArr, 'label'=>false, 'div'=>false, 'class'=>'validate[required]')); 
		?></p>
	</td>
    <td>
		<p><span style="color:red;">*</span> Card Expiry Year</p>
		<p><?php 
		$curyear = date('Y');
		for($i=$curyear; $i<($curyear+15); $i++){
			$yearArr[$i] = $i;
		}
		echo $this->Form->input('card_expiry_year', array('type'=>'select', 'options'=>$yearArr, 'empty'=>'Select Year', 'label'=>false, 'div'=>false, 'class'=>'validate[required]')); 
		?></p>
	</td>
  </tr>
  <tr>
    <td><span style="color:red;font-weight:bold;">** Required fields.</span></td>
	<td align="right" class="btn">
	<?php echo $this->Form->submit(__('Save', true), array('div'=>false, 'class'=>'', 'style'=>'height:34px; font-weight:bold;font-size:12px; color:#fff;width:100px;margin-right:20px;border:0')); ?>
	</td>
  </tr>
</table>
<?php echo $this->Form->end(); ?>