<?php echo $this->Session->flash('auth'); 
	echo $this->Form->create('User', array('action' => 'login'));
?>
<section class="container">
            <section class="form-container">
                <section class="form-group text-center"><h1>Sign In</h1>
                    <p>Please Sign In To continue</p>
                </section>
                <section class="form-group text-center">
                    <span class="glyphicon glyphicon-user canvas-img"></span>
                </section>
              
                <section class="form-group">
                    
                    <?php echo $this->Form->input('email', array('label' => '', 'size'=>'40','div'=>false,'placeholder'=>'Email','autofocus'=>'true','class'=>'form-control')); ?>
                </section>
                <section class="form-group">
                  
                    <?php echo $this->Form->input('password', array('label' => '','div'=>false, 'size'=>'40','placeholder'=>'Password','class'=>'form-control')); ?>
                </section>
                <!--
                <section class="form-group">
                    <section class="row">
                        <section class="col-xs-6"><a href="forgot-password.php">Forgot Password?</a></section>
                    </section>
                </section>
                -->
                <section class="form-group">
                    <section class="row">
                        <section class="col-xs-12 text-danger" id="validate">
                        </section>
                    </section>
                </section>
                <section class="form-group">
                    <input type="submit" id="btnLogin" class="btn btn-primary fullwidth" value="Login">
                </section>
              <section class="form-group">
                    <section class="row text-center" style="padding-top:20px;">
                        <small>© <?php echo date("Y") ?> househelp4hire.com | All Rights Resreved</small>
                    </section>
                </section>
            </section>
        </section>
<?php
	echo $this->Form->end();
?>