   <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Agent Manager</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                            <?php echo $this->Form->create('Agent', array('enctype' => 'multipart/form-data'));?>
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Add New Agents</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group">
                                                        <div style="max-width: 309px;">
                                                            <?php echo $this->Form->input('image',array('type'=>'file')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-fluid">

                                                <div class="span6">
                                                    <div class="control-group">
                                                       
                                                        <?php echo $this->Form->input('first_name',array('class'=>'span12')); ?>
                                                       
                                                     	<?php echo $this->Form->input('last_name',array('class'=>'span12')); ?>
                                                        
                                                        <?php echo $this->Form->input('email',array('class'=>'span12')); ?>
                                                        
                                                        <?php echo $this->Form->input('phone',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="control-group">
                                                         
                                                        
                                                        <?php echo $this->Form->input('user_mobile',array('maxlength'=>'11','class'=>'span12'));?>
                                                        
                                                        <div class="controls">
                                                         <?php echo $this->Form->input('location_id', array('options'=>$locations)); ?>
                                                        </div> 
                                                        
                                                        <?php echo $this->Form->input('address',array('class'=>'input-medium span12')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-lg">Submit</button>
                                    </div>
                                </div>
							<?php echo $this->Form->end();?>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
   <!-- END PAGE CONTAINER-->