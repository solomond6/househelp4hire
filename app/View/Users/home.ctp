<div id="content-wrapper">
    <section id="section-hero" style="background-position: center center; z-index: 0;">
        <div id="video_background_video_0" style="z-index: -1; position: relative; overflow: hidden;">
            <?php echo $this->Html->image('houseHelpBG.jpg'); ?>
            <!--<video width="1676" height="700" autoplay="" loop="" onended="this.play()" style="position: absolute; top: 0px; left: -163px;">
                <source src="https://assets.urbansitter.com/us-sym/assets/videos/homepage_final.mp4" type="video/mp4">
            </video>-->
        </div>
        <div class="container home-button">
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <div class="cta-bottom-float-border bkgd-quinary-100 text-right" style="background: none; border: none;">
                        <h1 class="promo-text">
                            The No.1 Source For <br/>
                            Domestic Staff
                        </h1>
                        <h3 class="promo-text">Find your perfect househelp here. It's easy!</h3>
                        <!--<a id="cta-find-sitters-btn" href="<?php echo DOMAIN_NAME_PATH; ?>users/addagents" class="btn btn-danger btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                            <span class="us-text-17">Are you an <br class="visible-super-xs"><span class="weight-bold">Agent</span>?</span>
                            <div class="us-text-18 weight-semi-bold text-uppercase margin-t-15" style="font-weight: bold; color:#000;">
                                SIgnup Here
                            </div>
                        </a>-->
                        <a id="cta-find-sitters-btn" href="<?php echo DOMAIN_NAME_PATH; ?> users/register" class="btn btn-warning btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                            <span class="us-text-17">i want <br class="visible-super-xs"><span class="weight-bold">domestic Staff</span></span>
                        </a>
                        <a id="cta-find-sitters-btn" href="<?php echo DOMAIN_NAME_PATH; ?>users/addcandidate" class="btn btn-danger btn-lg m-r-xs-only-10 m-r-sm-15 no-caps">
                            <span class="us-text-17">i am <br class="visible-super-xs"><span class="weight-bold">domestic staff</span></span>
                        </a>    
                    </div>
                </div>
            </div>
        </div>
        <div class="cta-block black bottom">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h4 class="marketing-text pad-t-8">Why not rather search?</h4>
                        <?php echo $this->Form->create('Staffs', array('controller'=>'Staffs', 'action'=>'index')); 
                            if(isset($this->request->data['Staff']['search'])){
                                $searchFieldValue = $this->request->data['Staff']['search'];
                            } else {
                                $searchFieldValue = null;
                            }
                        ?>
                        <div class="input-group">
                            <?php echo $this->Form->input('search', array('value'=>$searchFieldValue, 'class'=>"form-control", 'name'=>"data[Staff][search]", 'id'=>"appendedInputButton", 'placeholder'=>"Cooks, Drivers,etc....", 'label'=>false, 'div'=>false,'style'=>'height: 42px; font-size: 17px;')); ?>
                            <span class="input-group-btn">
                                <button class="btn btn-danger" style="padding: 9px 18px;font-family: 'Roboto Condensed';" type="submit">Search</button>
                            </span>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="four-pinheads" class="background-section">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="headline">
                    <h3 class="marketing-text">What We Offer</h3>
                    <p class="marketing-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
        </div>
        <div class="container offer-tab">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                      <div class="list-group">
                        <a href="#" class="list-group-item active text-left">
                            Nannies
                        </a>
                        <a href="#" class="list-group-item text-left">
                            Cooks
                        </a>
                        <a href="#" class="list-group-item text-left">
                            Drivers
                        </a>
                        <a href="#" class="list-group-item text-left">
                            Cleaners/Maids
                        </a>
                        <a href="#" class="list-group-item text-left">
                            Stewards
                        </a>
                        <a href="#" class="list-group-item text-left">
                            Gardeners
                        </a>
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                        <div class="bhoechie-tab-content active">
                            <div class="col-md-6">
                              <h2>Get all your househelp <br/> chores off your neck</h3>
                              <h3>Roles & Responsibility</h2>
                              <ul>
                                <li>Care for and/or supervise the child(ren)</li>
                                <li>May also perform household chores</li>
                                <li>May cook for the home or just the children</li>
                                <li>Market errands are very common</li>
                              </ul>
                              <button class="btn btn-danger btn-lg">Get Started</button>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Html->image('withChild.jpg'); ?>
                            </div>
                        </div>
                        <div class="bhoechie-tab-content">
                            <div class="col-md-6">
                              <h2>Get all your househelp <br/> chores off your neck</h3>
                              <h3>Roles & Responsibility</h2>
                              <ul>
                                <li>Care for and/or supervise the child(ren)</li>
                                <li>May also perform household chores</li>
                                <li>May cook for the home or just the children</li>
                                <li>Market errands are very common</li>
                              </ul>
                              <button class="btn btn-danger btn-lg">Get Started</button>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Html->image('withChild.jpg'); ?>
                            </div>
                        </div>
                        <div class="bhoechie-tab-content">
                            <div class="col-md-6">
                              <h2>Get all your househelp <br/> chores off your neck</h3>
                              <h3>Roles & Responsibility</h2>
                              <ul>
                                <li>Care for and/or supervise the child(ren)</li>
                                <li>May also perform household chores</li>
                                <li>May cook for the home or just the children</li>
                                <li>Market errands are very common</li>
                              </ul>
                              <button class="btn btn-danger btn-lg">Get Started</button>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Html->image('withChild.jpg'); ?>
                            </div>
                        </div>
                        <div class="bhoechie-tab-content">
                            <div class="col-md-6">
                              <h2>Get all your househelp <br/> chores off your neck</h3>
                              <h3>Roles & Responsibility</h2>
                              <ul>
                                <li>Care for and/or supervise the child(ren)</li>
                                <li>May also perform household chores</li>
                                <li>May cook for the home or just the children</li>
                                <li>Market errands are very common</li>
                              </ul>
                              <button class="btn btn-danger btn-lg">Get Started</button>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Html->image('withChild.jpg'); ?>
                            </div>
                        </div>
                        <div class="bhoechie-tab-content">
                            <div class="col-md-6">
                              <h2>Get all your househelp <br/> chores off your neck</h3>
                              <h3>Roles & Responsibility</h2>
                              <ul>
                                <li>Care for and/or supervise the child(ren)</li>
                                <li>May also perform household chores</li>
                                <li>May cook for the home or just the children</li>
                                <li>Market errands are very common</li>
                              </ul>
                              <button class="btn btn-danger btn-lg">Get Started</button>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Html->image('withChild.jpg'); ?>
                            </div>
                        </div>
                        <div class="bhoechie-tab-content">
                            <div class="col-md-6">
                              <h2>Get all your househelp <br/> chores off your neck</h3>
                              <h3>Roles & Responsibility</h2>
                              <ul>
                                <li>Care for and/or supervise the child(ren)</li>
                                <li>May also perform household chores</li>
                                <li>May cook for the home or just the children</li>
                                <li>Market errands are very common</li>
                              </ul>
                              <button class="btn btn-danger btn-lg">Get Started</button>
                            </div>
                            <div class="col-md-6">
                                <?php echo $this->Html->image('withChild.jpg'); ?>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </section>
    <section id="section-four" style="padding-bottom:100px;">
        <div class="container text-center">
            <div class="col-md-10 col-md-offset-1">
                <div class="headline">
                    <h3 class="marketing-text">How It Works</h3>             
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <th class="btn-warning"><h3>i want domestic staff</h3></th>
                        <th class="btn-danger"><h3>i am domestic staff</h3></th>
                    </thead>
                    <tr>
                        <td>
                            <span class="small">Step 1</span>
                            <h4>Sign Up</h4>
                            <?php echo $this->Html->image('signUp.png'); ?><br/>
                            <span>Register for free, quick and simple<span>
                        </td>
                        <td>
                            <span class="small">Step 1</span>
                            <h4>Sign Up</h4>
                            <?php echo $this->Html->image('signUp.png'); ?><br/>
                            <span>Register for free, quick and simple<span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="small">Step 2</span>
                            <h4>Review BVN Vetted Profiles</h4>
                            <?php echo $this->Html->image('review.png'); ?><br/>
                            <span>Shortlist for later, share or send to friend.<span>
                        </td>
                        <td>
                            <span class="small">Step 2</span>
                            <h4>List Preferences</h4>
                            <?php echo $this->Html->image('review.png'); ?><br/>
                            <span>Input your BVN (we use to verify who you are) <br/>and personal details. list your experience<span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="small">Step 3</span>
                            <h4>Call For Inteview</h4>
                            <?php echo $this->Html->image('interview.png'); ?><br/>
                            <span>Make Payemtn and call multiple <br/>candidates to interview and hire<span>
                            <br/>
                            <br/>
                            <a href="<?php echo DOMAIN_NAME_PATH; ?> users/register" class="btn btn-danger">
                               Get Started
                            </a>
                            <br/>
                            <br/>
                        </td>
                        <td>
                            <span class="small">Step 3</span>
                            <h4>Get Hired</h4>
                            <?php echo $this->Html->image('interview.png'); ?><br/>
                            <span>Expect calls from employers, look <br/>good and go for interview.<span>
                            <br/>
                            <br/>
                            <a href="<?php echo DOMAIN_NAME_PATH; ?>users/addcandidate" class="btn btn-danger">
                                Get Started
                            </a>
                            <br/>
                            <br/>
                        </td>
                    </tr>
                </table>
                <div class="headline"  style="padding-top:30px;">
                    <h3 class="marketing-text">Who We Are</h3>             
                </div>
                <p>househelp4hire.com is the largest and fastest growing service for finding high-quality carers for every facet of care, childcare, special needs care, elderly care, per care, housekeeping and more.</p>
            </div>
        </div>
    </section>
</div>

<script>
jQuery(document).ready(function() {
    jQuery("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        jQuery(this).siblings('a.active').removeClass("active");
        jQuery(this).addClass("active");
        var index = jQuery(this).index();
        jQuery("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        jQuery("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>