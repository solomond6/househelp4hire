<div class="main subpageWrap">
          <div class="col-left">
              <div class="left-nav">
                  <h3 class="tittle">Distributors</h3>
                  <div class="tree">
                  <ul>
						<?php foreach ($locations as $location): ?>
						<li><?php echo $this->Html->link($location['Location']['name'], array('action' => 'view', $location['Location']['id']),array('class'=>'mainlevel')); ?></li>
					   <p></p>
					   <?php endforeach; ?>
                  </ul>
                  </div>
              </div>
			  
					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div>
          </div>
          <div class="col-main f-right">
                  <ul class="breadcrumbs">
                      <li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a> ></li>
                      <li><strong>Locate Us</strong></li>
                  </ul>
                  
                  <h1>Locate Us</h1>
                  <p><?php echo $locationpage['Page']['content']?></p>
          </div>
          <div class="clearfix"></div>
      </div>
