<div class="ratingConfigs view">
<h2><?php  echo __('Rating Config'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ratingConfig['RatingConfig']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($ratingConfig['RatingConfig']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($ratingConfig['RatingConfig']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Rating Config'), array('action' => 'edit', $ratingConfig['RatingConfig']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Rating Config'), array('action' => 'delete', $ratingConfig['RatingConfig']['id']), null, __('Are you sure you want to delete # %s?', $ratingConfig['RatingConfig']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Rating Configs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rating Config'), array('action' => 'add')); ?> </li>
	</ul>
</div>
