<div class="ratingConfigs form">
<?php echo $this->Form->create('RatingConfig'); ?>
	<fieldset>
		<legend><?php echo __('Add Rating Config'); ?></legend>
	<?php
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Rating Configs'), array('action' => 'index')); ?></li>
	</ul>
</div>
