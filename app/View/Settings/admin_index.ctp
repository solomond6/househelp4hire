<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#SettingAdminIndexForm").validationEngine({scroll:true})
	});
//-->
</script>

<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Settings</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                    	 
                           <div class="span12">
                          
                            <!--error msg begin-->
                            <!--<div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>-->
                            <!--error msg end-->
                            <div class="widget">
                             <?php echo $this->Form->create('Setting', array('enctype' => 'multipart/form-data'));?>
                    	   	<?php echo $this->Form->input('id'); ?>
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Edit settings</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group">
                                                        <div style="max-width: 309px;">
                                                            <?php echo $this->Form->input('site_logo', array('type'=>'file')); ?>
															<?php echo '<p style="margin:10px 0 10px 0;">'.$this->Html->image(SITE_LOGO_URL.$this->request->data['Setting']['site_logo'], array('title'=>"Logo", 'alt'=>'Logo', 'width'=>'220')).'</p>'; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-fluid">

                                                <div class="span6">
                                                    <div class="control-group">
 														<?php echo $this->Form->input('copyright_by',array('class'=>'span12')); ?>
                                                        <?php echo $this->Form->input('site_phone',array('class'=>'span12')); ?>
                                                        <?php echo $this->Form->input('contact_email',array('class'=>'span12')); ?>
                                                        <?php echo $this->Form->input('support_email',array('class'=>'span12')); ?>
                                                        <?php echo $this->Form->input('noreply_email',array('class'=>'span12')); ?>
                                                        <?php echo $this->Form->input('facebook_link',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="control-group">
                                                        <?php echo $this->Form->input('twitter_link',array('class'=>'span12')); ?>
                                                       	<?php echo $this->Form->input('linkedin_link',array('class'=>'span12')); ?>
														<?php echo $this->Form->input('pinterest_link',array('class'=>'span12')); ?>
														<?php echo $this->Form->input('rss_link',array('class'=>'span12')); ?>
														
                                                        <?php echo $this->Form->input('booking_module', array('type'=>'select','class'=>'span12','options'=>array('0'=>'Disable', '1'=>'Enable'))); ?>
                                                        <?php echo $this->Form->input('company_number',array('class'=>'span12')); ?>
														<?php echo $this->Form->input('vat_number',array('class'=>'span12')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="control-group">
                                                       <?php echo $this->Form->input('youtube_video',array('class'=>'input-medium span12')); ?>
                                                       <?php echo $this->Form->input('address',array('class'=>'input-medium span12')); ?>
                                                       <?php echo $this->Form->input('how_it_works_block_1',array('class'=>'input-medium span12')); ?>
                                                       <?php echo $this->Form->input('how_it_works_block_2',array('class'=>'input-medium span12')); ?>
                                                       <?php echo $this->Form->input('how_it_works_block_3',array('class'=>'input-medium span12')); ?>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="control-group">
                                                      <?php echo $this->Form->input('google_analytic_code',array('class'=>'controls')); ?>
												      <?php echo $this->Form->input('meta_keyword',array('class'=>'controls')); ?>
													  <?php echo $this->Form->input('meta_description',array('class'=>'controls')); ?>
                                                      <?php echo $this->Form->input('brochure', array('type'=>'file','class'=>'controls')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-lg">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo $this->Form->end();?>
                        </div>
						
                    </div>
                    
                    <!-- END PAGE CONTENT-->
                </div>
<!-- END PAGE CONTAINER-->

