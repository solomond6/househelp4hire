

  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Page Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i>Manage Pages</h4>
                                </div>
                                <div class="widget-body">
                                <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                <thead>
									<tr>
											<th><?php echo $this->Paginator->sort('id');?></th>
											<th><?php echo $this->Paginator->sort('page_name');?></th>
											<th class="actions" style="text-align:center;"><?php echo __('Actions');?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								$i = 0;
								foreach ($pages as $page):
									$class = null;
									if ($i++ % 2 == 0) {
										$class = ' class="altrow"';
									}
								?>
								<tr<?php echo $class;?>>
									<td><?php echo $page['Page']['id']; ?>&nbsp;</td>
									<td><?php echo $page['Page']['page_name']; ?>&nbsp;</td>
									<td class="actions" style="text-align:center;">
										<?php echo $this->Html->link('<i class="fa fa-eye"></i> View', array('action' => 'view', $page['Page']['id']), array('class' => 'btn btn-success','escape' => FALSE)); ?>
										<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'edit', $page['Page']['id']), array('class' => 'btn btn-success','escape' => FALSE)); ?>
										<?php //echo $this->Html->link(__('Delete', true), array('action' => 'delete', $page['Page']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $page['Page']['id'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
									</tbody>
								</table>
                         		<p>
								<?php
								echo $this->Paginator->counter(array('format' => __("Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%")));
								?>	
								</p>

								<div class="paging">
									<?php echo $this->Paginator->prev('Previous', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
									<?php echo $this->Paginator->numbers(array('separator'=>'')); ?>
									<?php echo $this->Paginator->next('Next', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
								</div>	
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->