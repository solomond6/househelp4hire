		<div id="bodyWrapper">
			<div class="bodyLeft">
					<div id="moving_tab">
	<div class="tabs">
		<div class="lava"></div>
		<span class="item">Latest Jobs</span>
		<span class="item">Featured Jobs</span>

	</div>
					
	<div class="content">						
		<div class="panel">						
			<ul>
				<li style="background:#13318c; text-transform:uppercase; color:#ffffff; font-weight:bold;">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				  <tr>
					<td width="36%" > Job Title  </td>
					<td width="24%" >Company </td>
					<td width="40%">Location </td>
				  </tr>
				</table>
			</li>
				<?php 
				if($jobLatest){ 
					foreach($jobLatest as $latest){
				?>
				<li style="background:#b5c9ff; text-transform:uppercase; color:#3e3e3e; font-weight:bold;">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				  <tr>
					<td width="36%" ><?php echo $html->link($latest['Job']['job_title'], array('controller' => 'jobs', 'action' => 'view',$latest['Job']['id']), array('style'=>"color:#012690;", 'escape' => false)); ?></td>
					<td width="24%" ><?php echo $latest['Member']['company_name']; ?></td>
					<td width="40%" ><?php if(isset($latest['Job']['job_location']) && $latest['Job']['job_location']!=""){
					echo $latest['Job']['job_location'].', '.$latest['Country']['name'];
				}
				else{
					echo $latest['Country']['name'];
				}?></td>
				  </tr>
				</table>
				</li>
				<?php
					}
				}
				else{
					?>
				<li style="background:#b5c9ff; text-transform:uppercase; color:#3e3e3e; font-weight:bold;">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				  <tr>
					<td width="100%" >No latest job found!</td>
				  </tr>
				</table>
				</li>
					<?php
				}
					?>
			</ul>
			<ul>
				<li style="background:#13318c; text-transform:uppercase; color:#ffffff; font-weight:bold;">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				  <tr>
					<td width="36%" > Job Title  </td>
					<td width="24%" >Company </td>
					<td width="40%">Location </td>
				  </tr>
				</table>
				</li>
				<?php 
				if($jobFeatured){ 
					foreach($jobFeatured as $featured){
				?>
				<li style="background:#b5c9ff; text-transform:uppercase; color:#3e3e3e; font-weight:bold;">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				  <tr>
					<td width="36%" ><?php echo $html->link($featured['Job']['job_title'], array('controller' => 'jobs', 'action' => 'view',$featured['Job']['id']), array('style'=>"color:#012690;", 'escape' => false)); ?></td>
					<td width="24%" ><?php echo $featured['Member']['company_name']; ?></td>
					<td width="40%" ><?php if(isset($featured['Job']['job_location']) && $featured['Job']['job_location']!=""){
					echo $featured['Job']['job_location'].', '.$featured['Country']['name'];
				}
				else{
					echo $featured['Country']['name'];
				}?></td>
				  </tr>
				</table>
				</li>
				<?php
					}
				}
				else{
					?>
				<li style="background:#b5c9ff; text-transform:uppercase; color:#3e3e3e; font-weight:bold;">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				  <tr>
					<td width="100%" >No featured job found!</td>
				  </tr>
				</table>
				</li>
					<?php
				}
					?>
			</ul>			
		</div>

	</div>	
</div>
					<div class="bodyPart">
						<div class="lf-part">
							<div class="lf-part1">
								<h2>Employers</h2>
							</div>
							<div class="lf-part2">
								<ul>
								<li><?php echo $this->Html->link('Register', array('controller'=>'members', 'action'=>'addemployer')); ?></li>
								<li><?php echo $this->Html->link('Log In', array('controller'=>'members', 'action'=>'login')); ?></li>
								<li><?php echo $this->Html->link('Post Job', array('controller'=>'jobs', 'action'=>'add')); ?></li>
								<li><?php echo $this->Html->link('Search Resumes', array('controller'=>'resumes', 'action'=>'search')); ?></li>
							</ul>
							</div>
							<?php echo $html->image('l3.png');?>
						</div>
						<div class="lf-part">
							<div class="lf-part1">
								<h2>Job Seekers</h2>
								
							</div>
							<div class="lf-part2">
								<ul>
								<li><?php echo $this->Html->link('Register', array('controller'=>'members', 'action'=>'addjobseeker')); ?></li>
								<li><?php echo $this->Html->link('Log In', array('controller'=>'members', 'action'=>'login')); ?></li>
								<li><?php echo $this->Html->link('Post Resume', array('controller'=>'resumes', 'action'=>'add')); ?></li>
								<li><?php echo $this->Html->link('Search Jobs', array('controller'=>'jobs', 'action'=>'search')); ?></li>

							</ul>
							</div>
							<?php echo $html->image('l3.png');?>
						</div>
						
					</div>
					<div class="bodyPart">
						<div class="lf-log">
							<div class="lf-log1">
								<h2>Featured Companies</h2>
							</div>
							<div class="lf-log2">
							<?php
							if(count($companyFeatured)>0){ 
								foreach($companyFeatured as $company){ 
									if($company['Member']['company_logo'] != ''){
							 ?>
								<div class="lf-log2-img-div"><a href="<?php echo $company['Member']['company_website']; ?>" target="_blank"><?php echo $html->image(COMANY_LOGO_URL.$company['Member']['company_logo']);?></a></div>
							 <?php 
									}
								 }
							 ?>
							 <div class="clear"></div>
							 <?php
							 }
							 else{
								 echo '<div style="color:red;text-align:center;">No featured company has been found till now!</div>';
							 }
							 ?>
								
							</div>
							<?php echo $html->image('b3.png');?>
						</div>
						
					</div>
				</div>
				<?php echo $this->element('frontend_right_nav'); ?>
		</div>