 <!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content bg-white pad-btm-z">
            <div class="container">
                <div class="title-content clearfix mrgn-btm-z">
                    <div>
                        <h3 class="hiw-top-title">How <span>HouseHelp4Hire</span> Works</h3>
                        <!--<p class="hiw-top-para">Have you always wanted a one stop portal for domestic staff? HouseHelp4Hire is solution for you!</p>-->
                    </div>
                    
                    <div class="row mrgn-tp-150" style="margin-top:40px">
                        <div class="col-sm-6 w-hidden">
                            <img src="<?php echo DOMAIN_NAME_PATH; ?>images/HIW-pic-zero.png" alt="Browse">
                        </div>
                        <div class="col-sm-6 m-h-230">
                            <div class="form-group">
                                <span class="hiw-circle">1</span>
                                <h4 class="hiw-para">Login or Sign up and create an account, it's free and simple to do.</h4>
                            </div>
                        </div>
                    </div>

                    <!--/.title-inner-->
                </div>
                <!--/.title-content-->
                <!--AJAX PORTFOLIO WILL SHOWN HERE-->
                <div id="workslist">
                    <div class="clearfix worksajax">
                    </div>
                    <!--/.worksajax -->
                </div>
                <!--/workslist-->
            </div>
            <!--/.container-->
        </section>
        <section class="bg-theme">
            <div class="container">
                <div class="row pad-top-30">
                    <div class="col-sm-6 m-h-230">
                        <div class="form-group">
                            <span class="hiw-circle bg-white" style="margin-top: 15px;">2</span>
                            <h4 class="hiw-para color-w">Using the search box, look for the category of staff you want. E.g. type nannies if you’re looking for nannies
</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 w-hidden" style="background: url(<?php echo DOMAIN_NAME_PATH; ?>images/HIW-pic-one.png)no-repeat center bottom; height: 322px;">

                    </div>
                </div>
            </div>
        </section>
        <section class="bg-white" style="position: relative;">
            <div class="container">
                <div class="row pad-top-30">
                    <div class="col-sm-6 w-hidden" style="background: url(<?php echo DOMAIN_NAME_PATH; ?>images/HIW-pic-two.png)no-repeat center bottom; height: 322px;">

                    </div>
                    <div class="col-sm-6 m-h-230">
                        <div class="form-group">
                            <span class="hiw-circle" style="margin-top: 20px;">3</span>
                            <h4 class="hiw-para">Review the mini profiles of your search result and ‘click to view’ your favourite candidates full profile including their work experience, salary expectations and other personal information.</h4>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section class="bg-theme">
            <div class="container">
                <div class="row pad-top-30">
                    <div class="col-sm-6 m-h-230">
                        <div class="form-group">
                            <span class="hiw-circle bg-white">4</span>
                            <h4 class="hiw-para color-w">Call, interview and hire the candidate that works for you.
</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 w-hidden" style="background: url(<?php echo DOMAIN_NAME_PATH; ?>images/HIW-pic-three.png)no-repeat center bottom; height: 322px;">

                    </div>
                </div>
            </div>
        </section>
        <section style="position: relative; background: #f9f9f9; height: 100px;">

        </section>
        <!--/portfolio-->
