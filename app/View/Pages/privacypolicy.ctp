 <!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content bg-white pad-btm-z">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="margin-bottom: 30px;">Privacy Policy</h2>
                            <p class="text-justify">This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as used in Federal Republic of Nigeria privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>

                            <h5>What personal information do we collect from the people that visit our blog, website or app?</h5>
                            <p class="text-justify">When subscribing or registering on our site, as appropriate, you may be asked to enter your name, email address, phone number or other details to help you with your experience.</p>

                            <h5>When do we collect information?</h5>
                            <p class="text-justify">We collect information from you when you register on our site, subscribe to a newsletter or enter information on our site.</p>

                            <h5>How do we use your information?</h5>
                            <p class="text-justify">We may use the information we collect from you when you register, make a purchase,respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
                            <ul style="font-size: 14px;font-weight: normal; padding-left: 30px;">
                                <li>To personalise user's experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
                                <li>To quickly process your transaction.</li>
                                <li>To send periodic emails regarding your order or other products and services</li>
                            </ul>

                            <h5>How do we protect visitor information?</h5>
                            <p class="text-justify">We do not use vulnerability scanning and/or scanning to PCI standards.
                                <br>
                                <br>We do not use Malware Scanning.
                                <br>
                                <br>We do not use an SSL certificate</p>
                            <ul style="font-size: 14px;font-weight: normal; padding-left: 30px;">
                                <li>We do not need an SSL because:
                                    <ul style="font-size: 14px;font-weight: normal; list-style-type: none;">
                                        <li>We use a payment gateway provider and we do not share personal or private information with anyone.</li>
                                    </ul>
                                </li>
                            </ul>

                            <h5>Do we use 'cookies'?</h5>
                            <p class="text-justify">We do not use cookies for tracking purposes
                                <br>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies.If you disable cookies off, some features will be disabled that make your site experience more efficient and some of our services may not function properly.
                                <br>However you can still make purchases.</p>

                            <h5>Third Party Disclosure</h5>
                            <p class="text-justify">We do not sell, trade, or otherwise transfer to outside parties your personally Identifiable Information</p>

                            <h5>Third Party Links</h5>
                            <p class="text-justify">Occasionally at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>

                            <h5>Google</h5>
                            <p class="text-justify">Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>
                            </p>

                            <h5>We have implemented the following:</h5>
                            <ul style="font-size: 14px;font-weight: normal; padding-left: 30px;">
                                <li>Google Display Network Impression Reporting</li>
                                <li>Demographics and Interest Reporting</li>
                            </ul>
                            <p class="text-justify">We along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions, and other ad service functions as they relate to our website.</p>
                            <p class="text-justify" style="font-style:italic;">Opting out:</p>
                            <p class="text-justify" style="font-style:italic;">Users can set preferences for how Google advertise to you using the Google Ad settings page. Alternatively, you can opt out by visiting the Network Advertising initiative opt out page or permanently using the Google Analytics Opt Out Browser add on.</p>
                    </div>
                </div>
            </div>
        </section>
        <!--/portfolio-->