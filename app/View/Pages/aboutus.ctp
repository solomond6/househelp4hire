 <!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content bg-white pad-btm-z">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="text-center" style="font-size: 60px; font-weight: 400; margin-bottom: 40px;">About us</h1>
                        <p class="text-center" style="font-size:20px; color: #666;">Shouldn’t it be easy to find, safe vetted domestic staff?<br>
                       Wouldn’t it be great to be able to hire reliable staff on demand?<br>
                        Looking for a place to source cooks, nannies and drivers?</p>
                        <br><p class="text-center" style="font-size: 16px; max-width:910px; margin: 0px auto 45px auto; line-height: 29px;">If you’ve answered yes to any of the above, HouseHelp4Hire is the solution for you!<br>HouseHelp4Hire is the first online portal for sourcing domestic staff in Nigeria. View the full profiles including photos, work experience and other personal information of vetted jobseekers before you interview them. <br>Registration of each candidate is performed at our office were they are thoroughly interviewed and provide their personal information and fingerprints. We perform background checks on every candidate that is registered at our office.<br>We deliver a professional service able to meet the demand of both private and commercial customers and set ourselves apart from the competition with our innovation and proficiency.</p>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 100px;">
                    <div class="col-sm-6">
                        <h2 class="about-head-btm">Vision</h2>
                        <p class="text-center" style="font-size: 16px;">Our vision is to be the primary source for domestic staff in Nigeria by 2017.</p>
                    </div>
                    <div class="col-sm-6">
                        <h2 class="about-head-btm">Mission</h2>
                        <p class="text-center" style="font-size: 16px;">Through innovation, commitment and creativity, our mission is to make the process of hiring domestic staff and the process of looking for domestic work simple, safe and swift. </p>
                    </div>
                </div>
            </div>
        </section>
        <!--/portfolio-->