        <!--PORTFOLIO SECTION START-->
        <section id="portfolio" class="clearfix content bg-white pad-btm-z">
            <div class="container">
                <div class="row" style="margin-bottom: 100px;">
                    <!--<div class="col-sm-12">
                    <div class="map_canvas"></div>
                    </div>-->
                    <div class="col-md-5">
                        <div class="padding">
                            <h2 class="content-title">Leave Message</h2>
                        </div>
                        <ul class="address-list">
                            <!--<li><i class="fa fa-map-marker"></i>Suite C37, Road 1, Ikota Shopping Complex, VGC, Lagos</li>-->
                            <li><i class="fa fa-phone"></i> 090 9123 4509</li>
                            <li><i class="fa fa-suitcase"></i>8am – 6pm Mon-Fri & 10am – 4pm Sat</li>
                            <li><i class="fa fa-envelope"></i> <a href="#">hello@househelp4hire.com</a></li>
                            
                        </ul>
                    </div>
                    <div class="col-sm-7">

                        <?php echo $this->Form->create('Contact'); ?>
                            <p>
                            <span class="wpcf7-form-control-wrap name">
                                    
                               <?php echo $this->Form->input('name',array('label'=>false,'required'=>'required','placeholder'=>'Your Name','div'=>false, 'class'=>'form-control validate[required]')); ?>
                               </span> 
                            </p>
                            <p> 
                            <span class="wpcf7-form-control-wrap your-email">
                            <?php echo $this->Form->input('email',array('label'=>false,'type'=>'email', 'div'=>false,'required'=>'required', 'placeholder'=>'Your Email','class'=>'form-control validate[required, custom[email]]')); ?>        
                            </span> 
                            </p>
                            <p><span class="wpcf7-form-control-wrap tel-997">
                                    <?php echo $this->Form->input('phone',array('label'=>false, 'div'=>false, 'placeholder'=>'Your Telephone','class'=>'form-control')); ?>
                                    </span>
                            </p>
                            <p><span class="wpcf7-form-control-wrap textarea-640">
                               <?php echo $this->Form->input('message',array('label'=>false,'required'=>'required', 'div'=>false,'placeholder'=>'Your Message','class'=>'form-control')); ?>     
                               </span></p>
                            <p><input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit contact-btn" /></p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                       <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </section>
        <!--/portfolio-->