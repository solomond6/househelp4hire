<section class="footer clearfix">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p> a <a href="#"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/4-Hire.png" alt="4Hire" style="margin-top: -10px;"> company</a> 
            <br/>
                <a href="<?php echo DOMAIN_NAME_PATH; ?>pages/aboutus">About Us</a> |  
                <a href="<?php echo DOMAIN_NAME_PATH; ?>pages/contactus">Contact us</a> |
                <a href="<?php echo DOMAIN_NAME_PATH; ?>pages/howitworks">How It Works</a> |
                <a href="<?php echo DOMAIN_NAME_PATH; ?>pages/privacypolicy">Satefy Center</a> |  
                <a href="<?php echo DOMAIN_NAME_PATH; ?>pages/termsofservice">Tip & Advice</a>
                <ul class="soc-footer">
                    <li><a href="https://www.facebook.com/househelp4hire"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/HouseHelp4Hire"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://instagram.com/househelp4hire/"><i class="fa fa-instagram"></i></a></li>
                    <!--ANOTHER SOCIAL ICON LIST END-->
                </ul>
                <!--/.soc-footer-->
            </div>
            <!--/.col-sm-6-->
        </div>
        <!--/.row-->
    </div>
</section>
<!-- Log In model popup -->
<div class="modal fade" id="SignIn" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        
         <?php echo $this->Form->create('User', array('action' => 'login', 'id'=>'UserLoginFormHeader', 'novalidate'=>'')); ?>
         <?php echo $this->Session->flash('auth'); ?>
				
				<?php 
				if(isset($this->request->data['User']['flag']) && $this->request->data['User']['flag'] != '0'){
					$flagVal = $this->request->data['User']['flag'];
				} else {
					$flagVal = 0;
				}
				echo $this->Form->input('flag', array('type'=>'hidden', 'label' => false, 'div' => false, 'value'=>$flagVal)); 
		?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12"><h3><small>Please Sign In To Continue</small></h3></div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?php echo $this->Form->input('email', array('type'=>'email', 'div'=>false, 'class'=>'form-control','placeholder'=>'Email Address', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('password', array('type'=>'password', 'label'=>'Password', 'div'=>false, 'class'=>'form-control', 'value'=>'', 'required'=>'required')); ?>
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" value="Sign In" class="btn btn-danger">
                        </div>
                    <?php echo $this->Form->end(); ?>
                        <div class="form-group">
                            Don't have an account? <a class="link" data-target="#SignUp" data-toggle="modal" href="#">Create here</a><br>
                          
                            <?php echo $this->Html->link('Forgot Password?', array('controller' => 'Users', 'action' => 'forgetPassword'), array('class'=>'link', 'style'=>'')); ?>
                           
                        </div>
                        <?php echo $this->Form->input('flag', array('id'=>'LoginflagVar', 'type'=>'hidden', 'label' => false, 'div' => false, 'value'=>0)); ?>
                        <?php echo $this->Html->script(array('oauthpopup')); ?>
                        <script type="text/javascript">
                        jQuery(document).ready(function(){
                            jQuery('#facebook').click(function(e){
                                jQuery.oauthpopup({
                                    path: '<?php echo DOMAIN_NAME_PATH; ?>FacebookDetails/login',
                                    width:600,
                                    height:300,
                                    callback: function(){
                                        window.location.reload();
                                    }
                                });
                                e.preventDefault();
                            });
                        });
                        </script>
                        <div class="form-group">
                            
                            <?php if(!$this->Session->read('Auth.User.id')){ ?>
                                    <img src="<?php echo DOMAIN_NAME_PATH; ?>images/facebook.png" id="facebook" style="width:165px;" alt="Sign In Facebook">
                            <?php } ?> 
                        </div>

                        <div class="form-group">
                            <?php if(!$this->Session->read('Auth.User.id')){ ?>
                                <?php
                                    echo $this->Html->link($this->Html->image("linkedin_login.png", array("alt" => "Sign In Linkedin", 'style'=>"width:165px;")), array('controller' => 'LinkedinDetails','action'=>'connect'), array('escape'=>false)
                                    );
                                ?>
                            <?php } ?> 
                        </div>
                    </div>
                </div>
            </div>

        </div>
		
    </div>
</div>

<!-- Register in model popup -->
<div class="modal fade" id="SignUp" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <?php echo $this->Form->create('User', array('action' => 'register', 'id'=>'UserRegisterFormHeader')); ?>
        <div class="modal-content">
         <?php echo $this->Session->flash('auth'); ?>
				
				<?php 
				if(isset($this->request->data['User']['flag']) && $this->request->data['User']['flag'] != '0'){
					$flagVal = $this->request->data['User']['flag'];
				} else {
					$flagVal = 0;
				}
				echo $this->Form->input('flag', array('type'=>'hidden', 'label' => false, 'div' => false, 'value'=>$flagVal)); 
		?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12 ">
                        <h4>Please Sign Up To Continue</h4>
                        <div class="form-group">
                            <?php echo $this->Form->input('first_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'First Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('last_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'Last Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            <select name="data[User][location_id]" class="form-control" onchange="locationChecking(this.value, 'locMsg')" required="required" id="UserLocationId" aria-invalid="false">
                                <option value="">Select Location</option>
                                <option value="1">Lagos</option>
                                <option value="2">Abuja</option>
                                <option value="3">Anambra</option>
                                <option value="4">Akwa Ibom</option>
                                <option value="5">Adamawa</option>
                                <option value="6">Abia</option>
                                <option value="7">Bauchi</option>
                                <option value="8">Bayelsa</option>
                                <option value="9">Benue</option>
                                <option value="10">Borno</option>
                                <option value="11">Cross River</option>
                                <option value="12">Delta</option>
                                <option value="13">Ebonyi</option>
                                <option value="14">Edo</option>
                                <option value="15">Ekiti</option>
                                <option value="16">Enugu</option>
                                <option value="17">Gombe</option>
                                <option value="18">Imo</option>
                                <option value="19">Jigawa</option>
                                <option value="20">Kaduna</option>
                                <option value="21">Kano</option>
                                <option value="22">Katsina</option>
                                <option value="23">Kebbi</option>
                                <option value="24">Kogi</option>
                                <option value="25">Kwara</option>
                                <option value="26">Nasarawa</option>
                                <option value="27">Niger</option>
                                <option value="28">Ogun</option>
                                <option value="29">Ondo</option>
                                <option value="30">Osun</option>
                                <option value="31">Oyo</option>
                                <option value="32">Plateau</option>
                                <option value="33">Rivers</option>
                                <option value="34">Sokoto</option>
                                <option value="35">Taraba</option>
                                <option value="36">Yobe</option>
                                <option value="37">Zamfara</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('email', array('type'=>'email', 'div'=>false, 'class'=>'form-control','placeholder'=>'Email Address' ,'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('password', array('type'=>'password', 'placeholder'=>'Password', 'div'=>false, 'class'=>'form-control pwd', 'value'=>'', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('confirm_password', array('type'=>'password', 'placeholder'=>'Confirm Password', 'div'=>false, 'class'=>'form-control cpwd', 'value'=>'', 'required'=>'required', 'data-validation-matches-message'=>'Passwords do not match', 'data-validation-matches-match'=>'data[User][password]')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('phone', array('label'=>'Mobile No.','placeholder'=>'Mobile No.', 'div'=>false, 'class'=>'form-control', 'required'=>'required','maxlength'=>'11')); ?>
                        </div>
                        <div class="form-group">
                            <p style="font-size: 12px;">By signing up, you agree to our  <a class="link" href="terms_of_service.php" target="_blank">Terms of Services</a> and  <a class="link" href="privacy_policy.php" target="_blank">Privacy Policy.</a></p>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Register" class="btn btn-danger">
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php echo $this->Form->end(); ?>
    </div>
</div>

<div class="modal fade" id="AgentSignUp" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <?php echo $this->Form->create('User', array('action' => 'addagents', 'id'=>'UserRegisterFormHeader')); ?>
        <div class="modal-content">
         <?php echo $this->Session->flash('auth'); ?>
                
                <?php 
                if(isset($this->request->data['Agent']['flag']) && $this->request->data['User']['flag'] != '0'){
                    $flagVal = $this->request->data['User']['flag'];
                } else {
                    $flagVal = 0;
                }
                echo $this->Form->input('flag', array('type'=>'hidden', 'label' => false, 'div' => false, 'value'=>$flagVal)); 
        ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group mrgn-btm">
                           <h4>Please Sign Up As An Agent</h4>
                        </div>
                        <div class="form-group">
                           
                            <?php echo $this->Form->input('first_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'First Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('last_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'Last Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <select name="data[User][location_id]" class="form-control" onchange="locationChecking(this.value, 'locMsg')" required="required" id="UserLocationId" aria-invalid="false">
                                <option value="">Select Location</option>
                                <option value="1">Lagos</option>
                                <option value="2">Abuja</option>
                                <option value="3">Anambra</option>
                                <option value="4">Akwa Ibom</option>
                                <option value="5">Adamawa</option>
                                <option value="6">Abia</option>
                                <option value="7">Bauchi</option>
                                <option value="8">Bayelsa</option>
                                <option value="9">Benue</option>
                                <option value="10">Borno</option>
                                <option value="11">Cross River</option>
                                <option value="12">Delta</option>
                                <option value="13">Ebonyi</option>
                                <option value="14">Edo</option>
                                <option value="15">Ekiti</option>
                                <option value="16">Enugu</option>
                                <option value="17">Gombe</option>
                                <option value="18">Imo</option>
                                <option value="19">Jigawa</option>
                                <option value="20">Kaduna</option>
                                <option value="21">Kano</option>
                                <option value="22">Katsina</option>
                                <option value="23">Kebbi</option>
                                <option value="24">Kogi</option>
                                <option value="25">Kwara</option>
                                <option value="26">Nasarawa</option>
                                <option value="27">Niger</option>
                                <option value="28">Ogun</option>
                                <option value="29">Ondo</option>
                                <option value="30">Osun</option>
                                <option value="31">Oyo</option>
                                <option value="32">Plateau</option>
                                <option value="33">Rivers</option>
                                <option value="34">Sokoto</option>
                                <option value="35">Taraba</option>
                                <option value="36">Yobe</option>
                                <option value="37">Zamfara</option>
                            </select>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('email', array('type'=>'email', 'div'=>false, 'class'=>'form-control','placeholder'=>'Email Address' ,'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('password', array('type'=>'password', 'placeholder'=>'Password', 'div'=>false, 'class'=>'form-control pwd', 'value'=>'', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('confirm_password', array('type'=>'password', 'placeholder'=>'Confirm Password', 'div'=>false, 'class'=>'form-control cpwd', 'value'=>'', 'required'=>'required', 'data-validation-matches-message'=>'Passwords do not match', 'data-validation-matches-match'=>'data[User][password]')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('phone', array('label'=>'Mobile No.','placeholder'=>'Mobile No.', 'div'=>false, 'class'=>'form-control', 'required'=>'required','maxlength'=>'11')); ?>
                        </div>
                        <div class="form-group">
                            <p style="font-size: 12px;">By signing up, you agree to our  <a class="link" href="terms_of_service.php" target="_blank">Terms of Services</a> and  <a class="link" href="privacy_policy.php" target="_blank">Privacy Policy.</a></p>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Register" class="btn btn-danger">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<div class="modal fade" id="CandidateSignUp" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <?php echo $this->Form->create('User', array('action' => 'addcandidate', 'id'=>'UserRegisterFormHeader')); ?>
        <div class="modal-content">
         <?php echo $this->Session->flash('auth'); ?>
                
                <?php 
                if(isset($this->request->data['Agent']['flag']) && $this->request->data['User']['flag'] != '0'){
                    $flagVal = $this->request->data['User']['flag'];
                } else {
                    $flagVal = 0;
                }
                echo $this->Form->input('flag', array('type'=>'hidden', 'label' => false, 'div' => false, 'value'=>$flagVal)); 
        ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body pad-tpz">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group mrgn-btm">
                            <h4>Please Sign Up As A Job Seeker</h4>
                        </div>
                        <div class="form-group">
                           
                            <?php echo $this->Form->input('first_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'First Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('last_name', array('div'=>false, 'class'=>'form-control','placeholder'=>'Last Name', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <select name="data[User][location_id]" class="form-control" onchange="locationChecking(this.value, 'locMsg')" required="required" id="UserLocationId" aria-invalid="false">
                                <option value="">Select Location</option>
                                <option value="1">Lagos</option>
                                <option value="2">Abuja</option>
                                <option value="3">Anambra</option>
                                <option value="4">Akwa Ibom</option>
                                <option value="5">Adamawa</option>
                                <option value="6">Abia</option>
                                <option value="7">Bauchi</option>
                                <option value="8">Bayelsa</option>
                                <option value="9">Benue</option>
                                <option value="10">Borno</option>
                                <option value="11">Cross River</option>
                                <option value="12">Delta</option>
                                <option value="13">Ebonyi</option>
                                <option value="14">Edo</option>
                                <option value="15">Ekiti</option>
                                <option value="16">Enugu</option>
                                <option value="17">Gombe</option>
                                <option value="18">Imo</option>
                                <option value="19">Jigawa</option>
                                <option value="20">Kaduna</option>
                                <option value="21">Kano</option>
                                <option value="22">Katsina</option>
                                <option value="23">Kebbi</option>
                                <option value="24">Kogi</option>
                                <option value="25">Kwara</option>
                                <option value="26">Nasarawa</option>
                                <option value="27">Niger</option>
                                <option value="28">Ogun</option>
                                <option value="29">Ondo</option>
                                <option value="30">Osun</option>
                                <option value="31">Oyo</option>
                                <option value="32">Plateau</option>
                                <option value="33">Rivers</option>
                                <option value="34">Sokoto</option>
                                <option value="35">Taraba</option>
                                <option value="36">Yobe</option>
                                <option value="37">Zamfara</option>
                            </select>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('email', array('type'=>'email', 'div'=>false, 'class'=>'form-control','placeholder'=>'Email Address' ,'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('password', array('type'=>'password', 'placeholder'=>'Password', 'div'=>false, 'class'=>'form-control pwd', 'value'=>'', 'required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('confirm_password', array('type'=>'password', 'placeholder'=>'Confirm Password', 'div'=>false, 'class'=>'form-control cpwd', 'value'=>'', 'required'=>'required', 'data-validation-matches-message'=>'Passwords do not match', 'data-validation-matches-match'=>'data[User][password]')); ?>
                        </div>
                        <div class="form-group">
                            
                            <?php echo $this->Form->input('phone', array('label'=>'Mobile No.','placeholder'=>'Mobile No.', 'div'=>false, 'class'=>'form-control', 'required'=>'required','maxlength'=>'11')); ?>
                        </div>
                        <div class="form-group">
                            <p style="font-size: 12px;">By signing up, you agree to our  <a class="link" href="terms_of_service.php" target="_blank">Terms of Services</a> and  <a class="link" href="privacy_policy.php" target="_blank">Privacy Policy.</a></p>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Register" class="btn btn-danger">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<!-- Log In model popup -->



<?php 
if($this->Session->read('Message.flash')){
				if($this->Session->read('Message.flash.params.class') == 'success'){
					?>
<!-- Success model popup -->
<div class="modal fade" id="myModalSuccess" role="dialog">
    <div class="modal-dialog" style="max-width: 450px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body plan-outer p-b-25">
                <div class="row text-center">
                    <div class="col-sm-12 col-md-12 p-content">
                       <i class="fa fa-check success-icon"></i><span style="font-size: 17px;"><?php echo $this->Session->read('Message.flash.message'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
            </div>
        </div>
    </div>
</div>
<script>
	showModalPopup("myModalSuccess");
</script>
<?php
				} else if ($this->Session->read('Message.flash.params.class') == 'payment_success') {?> 
<!--Payment success model popup -->
<div class="modal fade" id="myModalSuccess" role="dialog">
    <div class="modal-dialog" style="max-width: 450px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body plan-outer p-b-25">
                <div class="row text-center">
                    <h3 class="title-mng text-green m-b-15"><i class="fa fa-check success-icon"></i>Your Transaction was successful !</h3>
                    <div class="col-sm-12 col-md-12 p-content text-center">
                       <p><?php echo $this->Session->read('Message.flash.message'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
            </div>
        </div>
    </div>
</div>
<script>
	showModalPopup("myModalSuccess");
</script>
<?php } else if($this->Session->read('Message.flash.params.class') == 'payment_error') {?> 
<!--Payment error model popup -->
<div class="modal fade" id="myModalSuccess" role="dialog">
    <div class="modal-dialog" style="max-width: 450px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body plan-outer p-b-25">
                <div class="row text-center">
                    <h3 class="title-mng text-red m-b-15"><i class="fa fa-close error-icon"></i>Your Transaction was not successful !</h3>
                    <div class="col-sm-12 col-md-12 p-content text-center">
                       <p><?php echo $this->Session->read('Message.flash.message'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
            </div>
        </div>
    </div>
</div>
<script>
	showModalPopup("myModalSuccess");
</script>
<?php } else {
					?>
<!--Error model popup -->
<div class="modal fade" id="myModalSuccess" role="dialog">
    <div class="modal-dialog" style="max-width: 450px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="mrgn-tpz" id="usLoginModalLabel"><img src="<?php echo DOMAIN_NAME_PATH; ?>images/logo-c.png" class="responsive-image" alt="HouseHelp4Hire"></h4>
            </div>
            <div class="modal-body plan-outer p-b-25">
                <div class="row text-center">
                    <div class="col-sm-12 col-md-12 p-content text-center">
                       <p><i class="fa fa-close error-icon"></i><?php echo $this->Session->read('Message.flash.message'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
            </div>
        </div>
    </div>
</div>
<script>
	showModalPopup("myModalSuccess");
</script>
<?php
				}
			}
?>

    </div>
</div>
<script>
	
	var password = document.getElementsByClassName("pwd")[0], 
    confirm_password = document.getElementsByClassName("cpwd")[0];
    
	function validatePassword(){
			
			if(password.value != confirm_password.value) {
    			confirm_password.setCustomValidity("Passwords Don't Match");
  			}
  			else {
   				 confirm_password.setCustomValidity('');
  			}
	}
	
	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;
	
    jQuery(document).ready(function () {
        jQuery('.close').click(function () {
            jQuery('body').removeClass('modal-open');
        });
    })
</script>
