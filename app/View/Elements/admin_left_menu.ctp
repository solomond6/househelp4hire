<div id="sidebar" class="nav-collapse collapse">
<?php 
if($this->Session->read('Auth.User.id')==1 || $this->Session->read('Auth.User.is_admin')==1){
	if($this->Session->read('Auth.User.id')!=1)
	{
		$acStr = $this->Session->read('Auth.User.acces');
		$ac = explode(',', $acStr);
	}
?>
    <div class="sidebar-toggler hidden-phone"></div>
    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <div class="navbar-inverse">
        <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" style="width: 83%;" />
        </form>
    </div>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="sidebar-menu">
       <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Settings', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
	
        <li class="has-sub" id="settings">
            <a href="javascript:;" class="">
                <i class="fa fa-cog"></i> Settings
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Account Settings', array('controller' => 'users','action' => 'admin_profile'), array('class' => ($this->params['action'] == 'admin_profile' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            <li><?php echo $this->Html->link('General Settings', array('controller' => 'settings', 'action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'settings') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            <!--
			<li><?php echo $this->Html->link('Manage Countries', array('controller' => 'countries', 'action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'countries') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			-->
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Admins', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="aum">
            <a href="javascript:;" class="">
                <i class="fa fa-user-plus"></i> Admin User Manager
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('View  Admin Users', array('controller' => 'users','action' => 'admin_indexsubadmin'), array('class' => ($this->params['action'] == 'admin_indexsubadmin' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            <li><?php echo $this->Html->link('Add Admin User', array('controller' => 'users','action' => 'admin_addsubadmin'), array('class' => ($this->params['action'] == 'admin_addsubadmin' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Agents', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="am">
            <a href="javascript:;" class="">
                <i class="fa fa-user"></i> Agent Manager
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('View  Agents', array('controller' => 'users','action' => 'admin_agents'), array('class' => ($this->params['action'] == 'admin_agents' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            <li><?php echo $this->Html->link('Add Agents', array('controller' => 'users','action' => 'admin_addagents'), array('class' => ($this->params['action'] == 'admin_addagents' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <li class="has-sub" id="sm">
            <a href="javascript:;" class="">
                <i class="fa fa-search"></i> Vetting Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Vetting Categories', array('controller' => 'vetting_categories','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'admin') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
               <li><?php echo $this->Html->link('New Vetting Category', array('controller' => 'vetting_categories','action' => 'admin_add'), array('class' => ($this->params['action'] == 'admin_add' && $this->params['controller'] == 'admin') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
               <li><?php echo $this->Html->link('Vetting Companies', array('controller' => 'vetting_companies','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'admin') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
               <li><?php echo $this->Html->link('New Vetting Companies', array('controller' => 'users','action' => 'admin_addvets'), array('class' => ($this->params['action'] == 'admin_add' && $this->params['controller'] == 'admin') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
               <li><?php echo $this->Html->link('Vetting  Candidates', array('controller' => 'vetted_candidates','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'vetted_candidates') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>

        <li class="has-sub" id="sm">
            <a href="javascript:;" class="">
                <i class="fa fa-briefcase"></i> Subscriber Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Subscriber', array('controller' => 'users','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Staffs', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        
        <li class="has-sub" id="stm">
            <a href="javascript:;" class="">
                <i class="fa fa-group"></i> Staff Managemnet
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Staffs', array('controller' => 'staffs','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Staff Monitoring', array('controller' => 'staffs','action' => 'admin_monitor'), array('class' => ($this->params['action'] == 'admin_monitor' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Payments', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="pm">
            <a href="javascript:;" class="">
                <i class="">&#8358</i> Payment Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
             <li><?php echo $this->Html->link('Manage Payment Option', array('controller' => 'PaymentOptions','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'PaymentOptions') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Paid Subscribers', array('controller' => 'Payments','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Payments') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Manage Transactions', array('controller' => 'Payments','action' => 'admin_transactions'), array('class' => ($this->params['action'] == 'admin_transactions' && $this->params['controller'] == 'Payments') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Categories', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="cm">
            <a href="javascript:;" class="">
                <i class="fa fa-list"></i> Category Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Category', array('controller' => 'categories','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'categories') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php }?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Locations', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="lm">
            <a href="javascript:;" class="">
                <i class="fa fa-map-marker"></i> Location Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               	<li><?php echo $this->Html->link('Manage Location', array('controller' => 'locations','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'locations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Manage Localities', array('controller' => 'Localities','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Localities') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Manage Origin', array('controller' => 'origins','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'origins') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Languages', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="lam">
            <a href="javascript:;" class="">
                <i class="fa fa-volume-up"></i> Language Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Language', array('controller' => 'languages','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'languages') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Religions', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="rm">
            <a href="javascript:;" class="">
                <i class="fa fa-flag-o"></i> Religion Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Religion', array('controller' => 'religions','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'religions') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Educations', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="em">
            <a href="javascript:;" class="">
                <i class="fa fa-mortar-board"></i> Education Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Education', array('controller' => 'Educations','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Educations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Pages', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="pgm">
            <a href="javascript:;" class="">
                <i class="fa fa-file"></i> Page Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                	<li><?php echo $this->Html->link('Manage Pages', array('controller' => 'pages','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'pages') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <li class="has-sub" id="fdr">
            <a href="javascript:;" class="">
                <i class="fa fa-file"></i> Feedback & Ratings
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                    <li><?php echo $this->Html->link('Manage feedback', array('controller' => 'feedbacks','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'feedbacks') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
                    <li><?php echo $this->Html->link('Rating Config', array('controller' => 'rating_configs','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'rating_configs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
                    <li><?php echo $this->Html->link('New Rating Config', array('controller' => 'rating_configs','action' => 'admin_add'), array('class' => ($this->params['action'] == 'admin_add' && $this->params['controller'] == 'rating_configs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
                    <li><?php echo $this->Html->link('Manage Rating', array('controller' => 'ratings','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'ratings') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Services', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="sem">
            <a href="javascript:;" class="">
                <i class="fa fa-glass"></i> Service Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Services', array('controller' => 'Services','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Services') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Contacts', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="mcm">
            <a href="javascript:;" class="">
                <i class="fa fa-phone"></i> Manage Contact Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Contact Message', array('controller' => 'contacts','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'contacts') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('FAQs', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="faq">
            <a href="javascript:;" class="">
                <i class="fa fa-question-circle"></i> FAQ Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage FAQs', array('controller' => 'faqs','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'faqs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
         <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Newsletters', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="nmg">
            <a href="javascript:;" class="">
                <i class="fa fa-newspaper-o"></i> Newsletter Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Subscriber', array('controller' => 'emails','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'emails') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Newsletter Types', array('controller' => 'Newsletters','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Newsletters') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Newsletter Templates', array('controller' => 'mailers','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'mailers') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Send Newsletter', array('controller' => 'Newsletters','action' => 'admin_send'), array('class' => ($this->params['action'] == 'admin_send' && $this->params['controller'] == 'Newsletters') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
        <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('ImportExport', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="ie">
            <a href="javascript:;" class="">
                <i class="fa fa-cloud-download"></i> Import & Export
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Newsletter Subscriber', array('controller' => 'Emails','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Emails') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Locations', array('controller' => 'Locations','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Locations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Educations', array('controller' => 'Educations','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Educations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Localities', array('controller' => 'Localities','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Localities') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Origins', array('controller' => 'Origins','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Origins') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Languages', array('controller' => 'Languages','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Languages') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Paid Subscribers', array('controller' => 'Users','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
         <?php if(($this->Session->read('Auth.User.id')!=1 && in_array('Coupons', $ac)) || ($this->Session->read('Auth.User.id')==1)) { ?>
        <li class="has-sub" id="com">
            <a class="" href="#">
                <i class="fa fa-gift"></i> Coupon Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Coupons', array('controller' => 'Coupons','action' => 'admin_manage'), array('class' => ($this->params['action'] == 'admin_manage' && $this->params['controller'] == 'Coupons') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Add New Coupon', array('controller' => 'Coupons','action' => 'admin_addcoupon'), array('class' => ($this->params['action'] == 'admin_addcoupon' && $this->params['controller'] == 'Coupons') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
            </ul>
    <?php } elseif($this->Session->read('Auth.User.user_type')==3){ 
		$acStr = $this->Session->read('Auth.User.acces');
		$ac = explode(',', $acStr);
	?>
	<div class="sidebar-toggler hidden-phone"></div>
	 <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <div class="navbar-inverse">
        <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" style="width: 83%;" />
        </form>
    </div>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="sidebar-menu">
    	 <li class="has-sub" id="settings">
            <a href="javascript:;" class="">
                <i class="fa fa-user-plus"></i> Settings
                <span class="arrow"></span>
            </a>
            <ul class="sub">
              <li><?php echo $this->Html->link('Account Settings', array('controller' => 'users','action' => 'admin_profile'), array('class' => ($this->params['action'] == 'admin_profile' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<?php  if(in_array('Settings', $ac)) {?>
            <li><?php echo $this->Html->link('General Settings', array('controller' => 'settings', 'action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'settings') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<!--
			<li><?php echo $this->Html->link('Manage Countries', array('controller' => 'countries', 'action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'countries') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			-->
			<?php } ?>
            </ul>
        </li>
		<?php  if(in_array('Agents', $ac)) {?>
			<li class="has-sub" id="am">
            <a href="javascript:;" class="">
                <i class="fa fa-user"></i> Agent Manager
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('View Agents', array('controller' => 'users','action' => 'admin_agents'), array('class' => ($this->params['action'] == 'admin_agents' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            <li><?php echo $this->Html->link('Add Agents', array('controller' => 'users','action' => 'admin_addagents'), array('class' => ($this->params['action'] == 'admin_addagents' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
       <?php } ?>
		<?php  if(in_array('Employers', $ac)) {?>
		 <li class="has-sub" id="sm">
            <a href="javascript:;" class="">
                <i class="fa fa-briefcase"></i> Subscriber Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Subscribers', array('controller' => 'users','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php } ?>
		<?php  if(in_array('Staffs', $ac)) {?>
		 <li class="has-sub" id="stm">
            <a href="javascript:;" class="">
                <i class="fa fa-group"></i> Staff Managemnet
                <span class="arrow"></span>
            </a>
            <ul class="sub">
              <li><?php echo $this->Html->link('Manage Staffs', array('controller' => 'staffs','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Staff Monitoring', array('controller' => 'staffs','action' => 'admin_monitor'), array('class' => ($this->params['action'] == 'admin_monitor' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php }?>
		<?php  if(in_array('Payments', $ac)) {?>
		<li class="has-sub" id="pm">
            <a href="javascript:;" class="">
                <i class="">&#8358</i> Payment Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
           <li><?php echo $this->Html->link('Manage Payment Option', array('controller' => 'PaymentOptions','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'PaymentOptions') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Paid Subscribers', array('controller' => 'Payments','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Payments') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('Categories', $ac)) {?>
		 <li class="has-sub" id="cm">
            <a href="javascript:;" class="">
                <i class="fa fa-list"></i> Category Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Category', array('controller' => 'categories','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'categories') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('Locations', $ac)) {?>
		 <li class="has-sub" id="lm">
            <a href="javascript:;" class="">
                <i class="fa fa-map-marker"></i> Location Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
            <li><?php echo $this->Html->link('Manage Location', array('controller' => 'locations','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'locations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Manage Localities', array('controller' => 'Localities','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Localities') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Manage Origin', array('controller' => 'origins','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'origins') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('Languages', $ac)) {?>
		  <li class="has-sub" id="lam">
            <a href="javascript:;" class="">
                <i class="fa fa-volume-up"></i> Language Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Language', array('controller' => 'languages','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'languages') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('Religions', $ac)) {?>
		<li class="has-sub" id="rm">
            <a href="javascript:;" class="">
                <i class="fa fa-flag-o"></i> Religion Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Religion', array('controller' => 'religions','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'religions') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('Educations', $ac)) {?>
		  <li class="has-sub" id="em">
            <a href="javascript:;" class="">
                <i class="fa fa-mortar-board"></i> Education Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Education', array('controller' => 'Educations','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Educations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('Pages', $ac)) {?>
		<li class="has-sub" id="pmg">
            <a href="javascript:;" class="">
                <i class="fa fa-file"></i> Page Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                	<li><?php echo $this->Html->link('Manage Pages', array('controller' => 'pages','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'pages') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
        <?php }?>
		<?php  if(in_array('Services', $ac)) {?>
		<li class="has-sub" id="sem">
            <a href="javascript:;" class="">
                <i class="fa fa-glass"></i> Service Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Services', array('controller' => 'Services','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Services') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php }?>
		<?php  if(in_array('Contacts', $ac)) {?>
		<li class="has-sub" id="mcm">
            <a href="javascript:;" class="">
                <i class="fa fa-phone"></i> Manage Contact Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage Contact Message', array('controller' => 'contacts','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'contacts') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('FAQs', $ac)) {?>
		 <li class="has-sub" id="faq">
            <a href="javascript:;" class="">
                <i class="fa fa-question-circle"></i> FAQ Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><?php echo $this->Html->link('Manage FAQs', array('controller' => 'faqs','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'faqs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('Newsletters', $ac)) {?>
		 <li class="has-sub" id="nm">
            <a href="javascript:;" class="">
                <i class="fa fa-newspaper-o"></i> Newsletter Management
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Manage Subscriber', array('controller' => 'emails','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'emails') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Newsletter Types', array('controller' => 'Newsletters','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'Newsletters') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Newsletter Templates', array('controller' => 'mailers','action' => 'admin_index'), array('class' => ($this->params['action'] == 'admin_index' && $this->params['controller'] == 'mailers') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Send Newsletter', array('controller' => 'Newsletters','action' => 'admin_send'), array('class' => ($this->params['action'] == 'admin_send' && $this->params['controller'] == 'Newsletters') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		<?php  if(in_array('ImportExport', $ac)) {?>
		<li class="has-sub" id="ie">
            <a href="javascript:;" class="">
                <i class="fa fa-cloud-download"></i> Import & Export
                <span class="arrow"></span>
            </a>
            <ul class="sub">
               <li><?php echo $this->Html->link('Newsletter Subscriber', array('controller' => 'Emails','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Emails') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Locations', array('controller' => 'Locations','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Locations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Educations', array('controller' => 'Educations','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Educations') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Localities', array('controller' => 'Localities','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Localities') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Origins', array('controller' => 'Origins','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Origins') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Languages', array('controller' => 'Languages','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Languages') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
			<li><?php echo $this->Html->link('Paid Subscribers', array('controller' => 'Users','action' => 'admin_import_export'), array('class' => ($this->params['action'] == 'admin_import_export' && $this->params['controller'] == 'Users') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
            </ul>
        </li>
		<?php } ?>
		</ul>
		<?php }?>
    <!-- END SIDEBAR MENU -->
</div>