<div style="width:1080px; margin:15px auto; border:1px solid #cccccc; padding:5px; border-radius:5px;" id="Div_Print">
	<table  width="100%" border="0" cellspacing="8" cellpadding="0">
	  <tr>
		<td width="50%" align="left" valign="top" style="border:1px solid #cccccc; padding:5px; border-radius:5px;">
			<?php //echo $this->Html->image($settingMain['Setting']['site_logo'], array('alt'=>$settingMain['Setting']['site_name'], 'title'=>$settingMain['Setting']['site_name'])); ?>
			Six Snow Flakes
		</td>
		<td style="border:1px solid #cccccc; padding:5px; border-radius:5px;" width="50%" align="left" valign="middle">
			<h4 style="font-family:Arial, Helvetica, sans-serif;font-size:26px; color:#ed217c; padding:0; margin:0;	text-transform:uppercase;"><?php if($invoice['Order']['payment_status']=='Y'){echo '<span style="color:green;">Paid</span>';}else{echo '<span style="color:red;">Unpaid</span>';} ?></h4>
			<p style="padding:10px 0;">
			<?php 
			if($invoice['Order']['payment_status']=='N'){
				if($invoice['Order']['payment_method']=='1'){
					echo $this->Html->link('Pay Using PayPal Direct Checkout', array('controller' => 'orders', 'action' => 'ccCheckout', $invoice['Order']['id']), array('style'=>'color:#0AB1F0; font-weight:bold; font-size:14px;'));
				}
				else if($invoice['Order']['payment_method']=='2'){
					echo $this->Html->link('Pay Using Stripe Checkout', array('controller' => 'orders', 'action' => 'ccCheckout', $invoice['Order']['id']), array('style'=>'color:#0AB1F0; font-weight:bold; font-size:14px;'));
				} 
				else{
					echo $this->Html->link('Pay Using Other Option', array('controller' => 'orders', 'action' => 'ccCheckout', $invoice['Order']['id']), array('style'=>'color:#0AB1F0; font-weight:bold; font-size:14px;'));
				}
			}
			else{
				if($invoice['Order']['payment_method']=='1'){
					echo 'Using PayPal Direct Checkout';
				}
				else if($invoice['Order']['payment_method']=='2'){
					echo 'Using Stripe Checkout';
				} 
				else{
					echo 'Using Other Option';
				}
			}
			?>
			</p>
			<p>
			<?php
			if($invoice['Order']['payment_status']=='Y'){
				echo 'On '.date("F j, Y, g:i a", strtotime($invoice['Order']['payment_date']));
			}
			else{
				echo 'Due Date: '.date("F j, Y, g:i a", (strtotime($invoice['Order']['order_date'])+(24*7*60*60)));
			}
			?>
			</p>
		</td>
	  </tr>
	  <tr>
		<td align="left" valign="top" style="border:1px solid #cccccc; padding:5px; border-radius:5px;">
			<h5 style="font-family:Arial, Helvetica, sans-serif;font-size:28px;color:#333333;padding:0;margin:15px 0;text-transform:uppercase;">Invoiced To</h5>
			<p style="padding:0 0 10px 0; color:#ED217C;"><?php echo $invoice['User']['first_name'].' '.$invoice['User']['last_name']; ?></p>
			<p style="padding:0 0 10px 0; color:#ED217C;"><?php echo $invoice['User']['address_1']; ?></p>
			<p style="padding:0 0 10px 0; color:#ED217C;"><?php echo $invoice['User']['address_2']; ?></p>
		</td>
		<td align="left" valign="top" style="border:1px solid #cccccc; padding:5px; border-radius:5px;">
			<h5 style="font-family:Arial, Helvetica, sans-serif;font-size:28px;color:#333333;padding:0;margin:15px 0;text-transform:uppercase;">Pay To </h5>
			<p style="padding:0 0 10px 0; color:#ED217C;"><?php echo $settingMain['Setting']['site_name']; ?></p>
			<p style="padding:0 0 10px 0; color:#ED217C;"><?php echo $settingMain['Setting']['site_address']; ?></p>
			<p style="padding:0 0 10px 0; color:#ED217C;">Phone : <?php echo $settingMain['Setting']['site_phone']; ?></p>
		</td>
	  </tr>
	</table>
	<div style="padding:10px;">
		<p style="color:#0AB1F0; font-weight:bold;">Invoice <?php echo $invoice['Order']['invoice_number']; ?></p>
		<p style="padding:10px 0;">Invoice Date: <?php echo date("F j, Y", (strtotime($invoice['Order']['order_date']))); ?></p>
		<p>
		<?php
		  if($invoice['Order']['payment_status']=='Y'){
				echo 'Paid On: '.date("F j, Y, g:i a", strtotime($invoice['Order']['payment_date']));
			}
			else{
				echo 'Due Date: '.date("F j, Y, g:i a", (strtotime($invoice['Order']['order_date'])+(24*7*60*60)));
			}
		  ?>
		</p>
	</div>
	<table  border="0" cellpadding="10" cellspacing="8" width="100%">
	  <tbody>
		<tr>
		  <td  style="border:1px solid #cccccc; padding:8px; border-radius:5px;" colspan="3" align="center" valign="top" width="75%"><h1 style="color:#0AB1F0;">Description</h1></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" rowspan="2" align="center" valign="middle" width="20%"><h1 style="color:#0AB1F0;">Amount</h1></td>
		</tr>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top"><strong>Prouct Name</strong></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top"><strong>Quantity</strong></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top" width="120"><strong>Unit Price</strong></td>
		</tr>
		<?php
		if(isset($invoice['OrderDetail'])){
			foreach($invoice['OrderDetail'] as $productBill){
		?>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="left" valign="top"><?php echo $productBill['Product']['name']; ?></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="left" valign="top"><?php echo $productBill['quantity']; ?></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="left" valign="top">$<?php echo number_format($productBill['unit_price'], 2); ?></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="left" valign="top">$<?php echo number_format(($productBill['quantity']*$productBill['unit_price']), 2); ?></td>
		</tr>
		<?php
			}
		}
		?>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" colspan="3" align="right" valign="top"><strong>Sub Total:</strong></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="right" valign="top"><strong>$<?php echo $invoice['Order']['order_amount']; ?></strong></td>
		</tr>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;"   colspan="3" align="right" valign="top"><strong>(+) Shipping Charge:</strong></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;"  align="right" valign="top"><strong>$<?php echo $invoice['Order']['shipping_amount']; ?></strong></td>
		</tr>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;"   colspan="3" align="right" valign="top"><strong>(+) Tax:</strong></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;"   align="right" valign="top"><strong>$<?php echo $invoice['Order']['tax_amount']; ?></strong></td>
		</tr>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" colspan="3" align="right" valign="top"><strong>(-) Gift Card Discount:</strong></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="right" valign="top"><strong>$<?php echo $invoice['Order']['gift_card_amount']; ?></strong></td>
		</tr>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" colspan="3" align="right" valign="top"><strong>(-) Coupone Discount:</strong></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="right" valign="top"><strong>$<?php echo $invoice['Order']['discount_amount']; ?></strong></td>
		</tr>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" colspan="3" align="right" valign="top"><strong>Total:</strong></td>
		  <td  style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="right" valign="top"><strong>$<?php echo (($invoice['Order']['order_amount']+$invoice['Order']['shipping_amount']+$invoice['Order']['tax_amount'])-$invoice['Order']['gift_card_amount']-$invoice['Order']['discount_amount']); ?></strong></td>
		</tr>
	  </tbody>
	</table>
	<div style="padding:10px;">
		<p style="color:#0AB1F0; font-weight:bold;">Transactions On Invoice <?php echo $invoice['Order']['invoice_number']; ?></p>
		<p style="padding:10px 0;">Invoice Date: <?php echo date("F j, Y", (strtotime($invoice['Order']['order_date']))); ?></p>
		<p>
		<?php
		  if($invoice['Order']['payment_status']=='Y'){
				echo 'Paid On: '.date("F j, Y, g:i a", strtotime($invoice['Order']['payment_date']));
			}
			else{
				echo 'Due Date: '.date("F j, Y, g:i a", (strtotime($invoice['Order']['order_date'])+(24*7*60*60)));
			}
		  ?>
		</p>
	</div>
	<table border="0" cellpadding="10" cellspacing="8" width="100%">
	  <tbody>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top" width="24%"><h1 style="color:#0AB1F0;">Transaction Date</h1></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top" width="26%"><h1 style="color:#0AB1F0;">Gateway</h1></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top" width="25%"><h1 style="color:#0AB1F0;">Transaction ID</h1></td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top" width="25%"><h1 style="color:#0AB1F0;">Amount</h1></td>
		</tr>
		<tr>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top"> 
			<?php
				if($invoice['Order']['payment_status']=='Y'){
					echo date("F j, Y, g:i a", strtotime($invoice['Order']['payment_status']));
				}
				else{
					echo 'Unpaid';
				}
			?> 
		  </td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top">
			<?php
				if($invoice['Order']['payment_method']=='1'){
					echo 'Authorize.net';
				}
				else if($invoice['Order']['payment_method']=='2'){
					echo 'Paypal Checkout';
				} 
				else{
					echo 'Other Option';
				}
			?>
		  </td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top">
		  <?php
			if($invoice['Order']['transactionID']!=''){
				echo $invoice['Order']['transactionID'];
			}
			else{
				echo 'Unpaid';
			}
		  ?>
		  </td>
		  <td style="border:1px solid #cccccc; padding:8px; border-radius:5px;" align="center" valign="top">
		  <?php 
		  if($invoice['Order']['transactionID']!=''){
			 echo '$'.(($invoice['Order']['order_amount']+$invoice['Order']['shipping_amount']+$invoice['Order']['tax_amount'])-$invoice['Order']['gift_card_amount']-$invoice['Order']['discount_amount']).' USD';
		  }
		  else{
			  echo 'Unpaid';
		  }
		  ?>
		  </td>
		</tr>
	  </tbody>
	</table>
</div>  