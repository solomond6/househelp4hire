 <!--HOME SECTION START-->
        <section id="home" class="clearfix">
            <div class="header clearfix">
                <nav class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                            <?php echo $this->Html->link($this->Html->image('site_logo/'.$siteSetting['Setting']['site_logo'], array('alt'=>$siteSetting['Setting']['site_name'], 'title'=>$siteSetting['Setting']['site_name'])),array('controller' => 'Users','action' => 'home'),array('escape' => false, 'ondragstart'=>'return false', 'onselectstart'=>'return false','class'=>'logo navbar-brand'));?>
                        </div>
                        <!-- Changes 16-Fen-2016 -->
                        <!--MENU BUTTON END-->
                        <!--NAVIGATION START-->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right menu navigation">
                                <?php
                                    if ($this->Session->check('Auth.User')) {
                                        echo '<li><a href="'.DOMAIN_NAME_PATH.'users/logout"  class="hiw">Sign Out</a></li>';
                                        echo '<li><a href="'.DOMAIN_NAME_PATH.'pages/howitworks"  class="hiw">How it works</a></li>';
                                        echo '<li>'.$this->Html->link('My Account', array("controller" => "users","action" => "dashboard"), array("class"=>"hiw")).'</li>';
                                    }
                                    else{
                                    ?>
                                        <li><a href="<?php echo DOMAIN_NAME_PATH; ?>pages/howitworks" class="hiw">How it works</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#SignIn">Safety Zone</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#SignIn"><strong>Sign In</strong></a></li>
                                        <li class="signup"><a href="<?php echo DOMAIN_NAME_PATH; ?>users/register">Sign Up</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                     <!--**************************changes 26-04-2015*************************-->
                    <div class="row" style="display: none">
                        <div class="col-md-12">
                            <div class="slider-content clearfix" style="padding: 0px 0px 20px 0px;">
                                <div class="row">
                                   
                                </div>
                                <!--/.centering-->
                            </div>
                            <!--/.slider-content-->
                        </div>
                        <!--/.col-sm-6-->
                    </div>
                     <!--**************************changes 26-04-2015*************************-->
                    <!--/.row-->
                </div>
                <!--/.container-->
            </div>
            <!--/.header-->
            </div>
        </section>
        <!--/home-->
        <!--HOME SECTION END-->