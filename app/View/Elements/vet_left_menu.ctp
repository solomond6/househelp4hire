<div id="sidebar" class="nav-collapse collapse">

    <div class="sidebar-toggler hidden-phone"></div>
    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <div class="navbar-inverse">
        <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" style="width: 83%;" />
        </form>
    </div>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="sidebar-menu">
        <li style="padding-left:20px;"><?php echo $this->Html->link('All My  Candidates', array('controller' => 'vetted_candidates','action' => 'vet_index'), array('class' => ($this->params['action'] == 'vet_index' && $this->params['controller'] == 'vetted_candidates') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
    </ul>
</div>