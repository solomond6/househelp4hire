	<script type="text/javascript">
		$(document).ready(function() {
			$("#ContactIndexForm").validationEngine({scroll:true, promptPosition: "topRight"})
		});
	</script>
	<script type="text/javascript">
		function nameCopy(){
			if (document.getElementById('ContactName').value != null){
				document.getElementById('nameCopy').innerHTML = document.getElementById('ContactName').value;
			} else {
				document.getElementById('nameCopy').innerHTML = '';
			}
		}
	</script>	
	<?php if($this->params['action'] == 'index' && $this->params['controller'] == 'Users'){ ?>
	<!-- Footer
	================================================== -->
	<span id="contactTab"></span>
	<footer class="footer">
		<div class="container">
			<hr class="soften">
			<hr class="soften">
			<hr class="soften">
			<hr class="soften">
			<hr class="soften">
			<h1 class="pull-left f-hed">Hellooooo!</h1>
			<div class="clearfix"></div>
			<!-- <p class="white2">I'm Jams, can I help?</p> -->
			<div class="clearfix mrg2"></div>
			<div class="span6 pull-left">
				<?php echo $this->Form->create('Contact'); ?>
				<p>
					<label>Please tell us your name</label>
					<?php echo $this->Form->input('name',array('onkeyup'=>'nameCopy();', 'label'=>false, 'div'=>false, 'class'=>'input-xxlarge validate[required]')); ?>
				</p>
				<p>
					<label>What would you like to talk about <span id="nameCopy"></span>?</label>
					<?php echo $this->Form->input('about_user',array('label'=>false, 'div'=>false, 'class'=>'input-xxlarge validate[required]')); ?>
				</p>
				<p>
					<label>Ok shoot, tell me all about it.</label>
					<?php echo $this->Form->input('message',array('label'=>false, 'div'=>false, 'class'=>'input-xxlarge validate[required]')); ?>
				</p>
				<p>
					<label>Which email address can we reach you on?</label>
					<?php echo $this->Form->input('email',array('label'=>false, 'div'=>false, 'class'=>'input-xxlarge validate[required, custom[email]]')); ?>
				</p>
				<p>
					<label>Would you like to leave your Phone number?</label>
					<?php echo $this->Form->input('phone',array('label'=>false, 'div'=>false, 'class'=>'input-xxlarge')); ?>
				</p>
				<p class="white">One last thing, please could you help me with this sum?</p>
				<p class="white">
					<div class="captcha" id="captchaId">
						<span class="num1"><?php echo $captcha['firstnum']; ?></span>
						<span class="opr"><?php echo $captcha['operators']; ?></span>
						<span class="opr"><?php echo $captcha['secondnum']; ?></span>
						<span class="num2">=</span>
						<span class="inp"></span>
						<?php echo $this->Form->input('captcha', array('class'=>'input-small validate[required]', 'value' => '', 'label' => false, 'div' => false));?>
						<button class="btn btn-danger large pad2">SEND</button>
					</div>
				</p>
				<?php echo $this->Form->end(); ?>
			</div>
			<div class="span4 pull-right">
				<!-- <div class="round3 img-rounded"> <img src="<?php echo DOMAIN_NAME_PATH; ?>img/ft.png" alt="" /> </div> -->
				<div class="round">
					<p><?php echo $siteSetting['Setting']['address']; ?></p>
					<p><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/cl2.png" class="pad-icon" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" style="padding:0;height:16px;width:16px;" /> <?php echo $siteSetting['Setting']['site_phone']; ?></p>
					<p><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/ml2.png" class="pad-icon" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" style="padding:0;height:16px;width:16px;" /> <?php echo $siteSetting['Setting']['contact_email']; ?></p>
					<!-- <p>company number: <?php echo $siteSetting['Setting']['company_number']; ?></p>
					<p>VAT number: <?php echo $siteSetting['Setting']['vat_number']; ?></p> -->
				</div>
				<div class="round2 img-rounded"> <img src="<?php echo DOMAIN_NAME_PATH; ?>img/map.png" alt="" class="pad-icon" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" /> </div>
			</div>
			<div class="clearfix" style="padding:0 0 15px 0;"></div>
			<div class="span10 cent top-pad"> 
				<a href="<?php echo $siteSetting['Setting']['facebook_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/f.png" alt="" /></a>
				<a href="<?php echo $siteSetting['Setting']['twitter_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/t.png" alt="" /></a>
				<!-- <a href="<?php echo $siteSetting['Setting']['linkedin_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/l.png" alt="" /></a> -->
				<a href="<?php echo $siteSetting['Setting']['pinterest_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/p.png" alt="" /></a>
				<a href="mailto: <?php echo $siteSetting['Setting']['contact_email']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/e.png" alt="" /></a>
				<a href="<?php echo $siteSetting['Setting']['rss_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/r.png" alt="" /></a>
			</div>
			<div class="span10 top-pad">Copyright 2014 <img style="padding:0 2px 0 0; vertical-align:-5px;" src="<?php echo DOMAIN_NAME_PATH; ?>img/4-Hire.png" alt="" width="50" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" />. All Rights Reserved<?php //echo $siteSetting['Setting']['copyright_by']; ?></div>
			<!-- <p>Code licensed under <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License v2.0</a>, documentation under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
			<p><a href="http://glyphicons.com">Glyphicons Free</a> licensed under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
			<ul class="footer-links">
			  <li><a href="http://blog.getbootstrap.com">Blog</a></li>
			  <li class="muted">&middot;</li>
			  <li><a href="https://github.com/twitter/bootstrap/issues?state=open">Issues</a></li>
			  <li class="muted">&middot;</li>
			  <li><a href="https://github.com/twitter/bootstrap/blob/master/CHANGELOG.md">Changelog</a></li>
			</ul>--> 
		</div>
	</footer>
	<?php } else { ?>
	<div class="container">
    	<div class="yalo">
        	<div class="span6 pull-left yalo-l">
            	<h1>CONTACT US</h1>
                <p><a href="#myModalFaq" role="button" data-toggle="modal">Questions? Click link to read our FAQ</a></p>
            </div>
            <div class="span4 pull-right yalo-r">
            	<p><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/cl.png" class="pad-icon" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" /><?php echo $siteSetting['Setting']['site_phone']; ?></p>
                <p><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/ml.png" class="pad-icon" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" /><?php echo $siteSetting['Setting']['contact_email']; ?></p>
                <p>
                        <img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/lnk.png" class="pad-icon" oncontextmenu="return false" ondragstart="return false" onselectstart="return false">
                        <a href="<?php echo $siteSetting['Setting']['facebook_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/f.png" /></a>
                        <a href="<?php echo $siteSetting['Setting']['twitter_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/t.png" /></a>
                        <a href="mailto: <?php echo $siteSetting['Setting']['contact_email']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/e.png" /></a>
                        <!-- <a href="<?php echo $siteSetting['Setting']['linkedin_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/l.png" /></a> 
                        <a href="<?php echo $siteSetting['Setting']['pinterest_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/p.png" /></a>
                        <a href="<?php echo $siteSetting['Setting']['rss_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/r.png" /></a> -->
                </p>
            </div>
           	<div class="clearfix"></div>
        </div>
    </div>
	<?php } ?>
	<?php if(isset($secondTabMenu) && ($secondTabMenu=='4hireTabLink' || $secondTabMenu=='aboutTabLink' || $secondTabMenu=='serviceTabLink' || $secondTabMenu=='contactTabLink')){ ?>
	<script>		
		$(document).ready(function() {
			$( "#<?php echo $secondTabMenu; ?>" ).trigger( "click" );
		});
	</script>
	<?php } ?>
	<?php if(isset($this->params['pass'][0]) && ($this->params['pass'][0]=='myModalFaq')){ ?>
	<script>		
		$(document).ready(function() {
			$('#myModalFaq').modal('show');
		});
	</script>
	<?php } ?>
	<?php if(isset($secondTabMenu) && $secondTabMenu=='ContactEmail'){ ?>
	<script>		
		$(document).ready(function() {
			$('html,body').animate({ scrollTop: $('#<?php echo $secondTabMenu; ?>').offset().top }, 2000);
		});
	</script>
	<?php } ?>