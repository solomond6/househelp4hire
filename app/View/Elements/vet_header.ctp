<div id="header" class="navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="navbar-inner">
                <div class="container-fluid">
                    <!-- BEGIN LOGO -->
                  
                    <a class="brand" href="<?php echo DOMAIN_NAME_PATH; ?>/agent"><?php echo $this->Html->image(SITE_LOGO_URL.$siteSetting['Setting']['site_logo']); ?></a>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="arrow"></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- END  NOTIFICATION -->
                    <?php if($this->Session->read('Auth.User.id')){ ?>
                    <div class="top-nav ">
                        <ul class="nav pull-right top-menu">
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo(DOMAIN_NAME_PATH);?>admin/img/avatar-mini.png" alt="agent">
                                    <span class="username">Vet Company</span>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo(DOMAIN_NAME_PATH);?>vet/users/logout"><i class="icon-key"></i> Log Out</a></li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                        <!-- END TOP NAVIGATION MENU -->
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- END TOP NAVIGATION BAR -->
</div>