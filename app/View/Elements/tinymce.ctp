<?php echo $this->Html->script("tiny_mce/tiny_mce.js"); ?>

<?php
    echo $this->Html->scriptBlock(
        "function fileBrowserCallBack(field_name, url, type, win) {
            browserField = field_name;
            browserWin = win;
            window.open('".Helper::url(array('controller' => 'images'))."', 'browserWindow', 'modal,width=600,height=400,scrollbars=yes');
        }"
    );
?>
<?php echo $this->Html->Script(array('tiny_mce/jquery.tinymce.min')); ?>
<?php echo $this->Html->Script(array('tiny_mce/tinymce.min')); ?>
<script type="text/javascript">
    tinymce.init({
      selector: 'textarea',
      height: 500,
      browser_spellcheck: true,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ]
    });
</script>
<?php
    /*echo $this->Html->scriptBlock(
        "tinyMCE.init({
            mode : 'textareas',
            theme : 'advanced',
            theme_advanced_buttons1 : 'forecolor, bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull',
            theme_advanced_buttons2 : 'bullist,numlist,|,link,unlink,|,image,emotions,code',
            theme_advanced_buttons3 : '',
            theme_advanced_toolbar_location : 'top',
            theme_advanced_toolbar_align : 'left',
            theme_advanced_path_location : 'bottom',
            extended_valid_elements : 'a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]',
            file_browser_callback: 'fileBrowserCallBack',
            width: '300',
            height: '300',
            relative_urls : false
        });"
    );*/
?> 