<div id="sidebar" class="nav-collapse collapse">

    <div class="sidebar-toggler hidden-phone"></div>
    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <div class="navbar-inverse">
        <form class="navbar-search visible-phone">
            <input type="text" class="search-query" placeholder="Search" style="width: 83%;" />
        </form>
    </div>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="sidebar-menu">
        <li style="padding-left:20px;"><?php echo $this->Html->link('All Candidates', array('controller' => 'staffs','action' => 'agent_index'), array('class' => ($this->params['action'] == 'agent_index' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
        <li style="padding-left:20px;"><?php echo $this->Html->link('New Candidate', array('controller' => 'staffs', 'action' => 'agent_add'), array('class' => ($this->params['action'] == 'agent_addstaff' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
        <li style="padding-left:20px;"><?php echo $this->Html->link('Contacted Candidates', array('controller' => 'staffs', 'action' => 'agent_contacted'), array('class' => ($this->params['action'] == 'agent_contacted' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
        <li style="padding-left:20px;"><?php echo $this->Html->link('Shortlisted Candidates', array('controller' => 'staffs', 'action' => 'agent_shortlisted'), array('class' => ($this->params['action'] == 'agent_shortlisted' && $this->params['controller'] == 'staffs') ? 'selected_left_menu' : 'not_selected_left_menu'));?></li>
    </ul>
</div>