<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Contact Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Contact</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">Name <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($contact['Contact']['name']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Email <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($contact['Contact']['email']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Phone <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($contact['Contact']['phone']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Message <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left" style="max-width:700px;"><?php echo h($contact['Contact']['message']); ?> </span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Date <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($contact['Contact']['date']); ?></span><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <?php echo $this->Html->link('Back', array('action' => 'index'),array('class'=>'btn btn-success')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
