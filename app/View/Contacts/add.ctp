<!-- <div class="contacts form">
<?php echo $this->Form->create('Contact'); ?>
	<fieldset>
		<legend><?php echo __('Add Contact'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('phone');
		echo $this->Form->input('message');
		echo $this->Form->input('read');
		echo $this->Form->input('date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?></li>
	</ul>
</div> -->
<script type="text/javascript">

	$(document).ready(function() {
		$("#ContactAddForm").validationEngine({scroll:true})
	});

</script>
 <div class="main subpageWrap">
           <div class="col-main f-right">
               <ul class="breadcrumbs">
                  <li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a> ></li>
                  <li><strong>Contact Us</strong></li>
               </ul>
			    <h1>Contact Us</h1>
               <p><?php echo $contactpage['Page']['content'];?></p>
			   <?php echo $this->Form->create('Contact'); ?>
				<fieldset>
					<legend><?php echo __('Add Contact'); ?></legend>
						<div class="field">
						  <label class="required" for="ContactName"><em>*</em>Name</label>
						  <div class="input-box">
						   <input type="text" class="input-text required-entry validate[required]" name="data[Contact][name]"  maxlength="255" id="ContactName">
						  </div>
						 </div>
						 <div class="field">
						  <label class="required" for="ContactEmail"><em>*</em>Email</label>
						  <div class="input-box">
						   <input type="text" class="input-text required-entry validate[required,custom[email]]"  name="data[Contact][email]"  maxlength="255" id="ContactEmail">
						  </div>
						 </div>
						 <div class="field">
						  <label  for="ContactPhone">Phone</label>
						  <div class="input-box">
						   <input type="text" class="input-text"  name="data[Contact][phone]"  maxlength="255" id="ContactPhone">
						  </div>
						 </div>
						  <div class="field">
						  <label  for="ContactMessage">Message</label>
						  <div class="input-box">
						   <textarea name="data[Contact][message]" cols="42" rows="6" id="ContactMessage"></textarea>
						  </div>
						 </div>
				<!-- <?php
					echo $this->Form->input('name',array('class'=>'validate[required]'));
					echo $this->Form->input('email',array('class'=>'validate[required,custom[email]]'));
					echo $this->Form->input('phone');
					echo $this->Form->input('message');
				?> -->
				</fieldset>
			<?php echo $this->Form->end(__('Submit')); ?>
			   
</div>
           <div class="clearfix"></div>
      </div>
