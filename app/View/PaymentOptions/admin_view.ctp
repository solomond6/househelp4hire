 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Payment Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Payments</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $paymentOption['PaymentOption']['id']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Option Name <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $paymentOption['PaymentOption']['name']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Amount <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php echo $paymentOption['PaymentOption']['amount']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Type <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php if($paymentOption['PaymentOption']['type'] == '1'){
				echo 'View Profile Contacts';
			}?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Quantity <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php if($paymentOption['PaymentOption']['quantity'] != ''){echo $paymentOption['PaymentOption']['quantity'];}else{echo 'N/A';} ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Validity <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">  <?php if($paymentOption['PaymentOption']['validity'] != ''){echo $paymentOption['PaymentOption']['validity'];}else{echo 'N/A';} ?></span><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <?php echo $this->Html->link('Back', array('controller' => 'PaymentOptions', 'action' => 'index'), array('class'=>'btn btn-success btn-lg')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->