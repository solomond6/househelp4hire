<script>
	function blockNone(type){
		if(type != ''){
			if(type == 1){
				document.getElementById('radioDiv').style.display = 'block';
			}
		}
		else{
			document.getElementById('radioDiv').style.display = 'none';
		}
	}
	$(document).ready(function() {
		<?php
		if(isset($this->request->data['PaymentOption']['type'])){
			echo "blockNone(".$this->request->data['PaymentOption']['type'].")";
		}
		?>
	});
</script>
 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Payment Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-plus-sign"></i>Edit Payment Option</h4>
                                </div>
                                <?php echo $this->Form->create('PaymentOption');?>
                                <div class="widget-body form">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="form-horizontal">
                                             <?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('label'=>'Option Name','class'=>'span12'));
		echo $this->Form->input('type', array('onchange'=>'blockNone(this.value);', 'type' => 'select', 'options' => array('' => 'Select Payment Type', '1' => 'View Profile Contacts')));
		?>
		<div style="display:none;" id="radioDiv"></div>
		<?php
		echo 'Leave quantity field blank if you want to provide access of Unlimited Profile Contacts';
		echo $this->Form->input('quantity',array('class'=>'span12'));
		echo 'Leave quantity field blank if you want to provide access for life time';
		echo $this->Form->input('validity',array('class'=>'span12'));
		echo $this->Form->input('amount',array('class'=>'span12'));
		?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid"></div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                    </div>
                                </div>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->