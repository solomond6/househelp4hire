  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Location Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Location</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">Name <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($location['Location']['name']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                    </div> 
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">Content <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $location['Location']['content']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <?php echo $this->Html->link('Back', array('action' => 'index'),array('class'=>'btn btn-success')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->