 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Payment Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Manage Transactions</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                     <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
                                        <tr>
                                            
                                            <th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('user_id','User');?></th>
			<th><?php echo $this->Paginator->sort('gtpay_tranx_id','Transaction');?></th>
			<th><?php echo $this->Paginator->sort('created_at','Date');?></th>
			<th><?php echo $this->Paginator->sort('paid_amount','Amount');?></th>
			<th><?php echo $this->Paginator->sort('gtpay_tranx_state','Status');?></th>
			<th><?php echo $this->Paginator->sort('gtpay_tranx_status_code','Code');?></th>
			<th><?php echo $this->Paginator->sort('payment_option_id');?></th>
		
			<th class="actions" style="text-align:center;"><?php __('Actions');?></th>
                                        </tr>
                                     </thead>
                                        <tbody>
                                      
                                      <?php foreach ($payments as $payment): ?>
										<tr>
											<td><?php echo $payment['Payment']['id']; ?>&nbsp;</td>
											<td>
												<?php echo $this->Html->link($payment['User']['name'], array('controller' => 'users', 'action' => 'view', $payment['User']['id'])); ?>
											</td>
											<td><?php echo $payment['Payment']['gtpay_tranx_id']; ?>&nbsp;</td>
											<td><?php echo $payment['Payment']['created_at']; ?>&nbsp;</td>
											<td>&#8358; <?php echo number_format($payment['Payment']['paid_amount'],2,".",","); ?>&nbsp;</td>
											<td>
												<?php
												
													echo $payment['Payment']['gtpay_tranx_state'];
												
												?>
											</td>
											<td>
												<?php
												
													 if(empty($payment['Payment']['gtpay_tranx_status_code']))
													 echo "--";
													 else
													 echo $payment['Payment']['gtpay_tranx_status_code'];
													  
												
												?>
											</td>
											<td>
												<?php echo $this->Html->link($payment['PaymentOption']['name'], array('controller' => 'PaymentOptions', 'action' => 'view', $payment['PaymentOption']['id'])); ?>
											</td>
											
											<td class="actions" style="text-align:center;">
												<?php if($payment['Payment']['gtpay_tranx_state']=="pending") {
													echo $this->Html->link('<i class="fa fa-refresh"></i> ReQuery', array('action' => 'requery', $payment['Payment']['id']),array('class'=>'btn btn-success','escape'=>FALSE)); } else {
														echo "None";
													}
												?>
												
											</td>
										</tr>
	<?php endforeach; ?>
		
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __(' previous '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
                                
                                    
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->