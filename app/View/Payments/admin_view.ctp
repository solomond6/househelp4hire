<div>
<h2><?php echo __('Payment');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $payment['Payment']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Employer'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($payment['User']['name'], array('controller' => 'Users', 'action' => 'view', $payment['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Payment Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php
			if($payment['Payment']['gtpay_tranx_state'] == 'completed'){
				echo 'Subscribed';
			}
			else if($payment['Payment']['gtpay_tranx_state'] == 'pending'){
				echo 'Unpaid';
			}
			else{
				echo 'Expired';
			}
			?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Payment Option'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($payment['PaymentOption']['name'], array('controller' => 'PaymentOptions', 'action' => 'view', $payment['PaymentOption']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Paid Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $payment['Payment']['paid_amount'].' USD'; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Quantity'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $payment['Payment']['quantity']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Already Viewed'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<input type="number" value="<?php echo $payment['Payment']['quantity_viewed']; ?>" id="txtQuantityViewed"/>
			<a href="javascript:void(0);" onclick="javascript:updateQty(<?php echo $payment['Payment']['id'];?>);">Update Quantity</a>
			&nbsp;
		</dd>	
		<dt  class="pUpdate" style="display:none"></dt>
		<dd  class="pUpdate" style="display:none;margin-top: 1px;">
			Update Successful
		</dd>	
		<!--
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Validity'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $payment['Payment']['validity'].' Days'; ?>
			&nbsp;
		</dd>
		-->
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Date Of Payment'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $payment['Payment']['created_at']; ?>
			&nbsp;
		</dd>
		<!--
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php echo __('Expiry Date'); ?></dt>
		
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $payment['Payment']['expiry_date']; ?>
			&nbsp;
		</dd>
		-->
		
		<dt<?php if ($i % 2 == 0) echo $class;?>>&nbsp;</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link('Back', array('action' => 'index')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
