<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $this->Html->charset(); ?>
	<title>Alero</title>
  
    <link rel="shortcut icon" href="<?php echo DOMAIN_NAME_PATH;?>ico/favicon.png">
    <?php
		 echo $this->Html->script(array('jquery'));
		 //echo $this->Html->script(array());
		//echo $this->Html->meta('icon');
		//echo $this->Html->css(array(THEME_FOLDER.'bootstrap', THEME_FOLDER.'bootstrap-responsive', THEME_FOLDER.'docs', THEME_FOLDER.'prettify'));
		echo $this->Html->css(array('bootstrap', 'popup_window'));
	?>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo DOMAIN_NAME_PATH;?>js/html5shiv.js"></script>
    <![endif]-->    
    <script>var DOMAIN_NAME_PATH = '<?php echo DOMAIN_NAME_PATH;?>';</script>
  </head>
  <body>
  <?php echo $this->Session->flash(); ?>  
  <?php echo $this->fetch('content'); ?>
	
  </body>
</html>

