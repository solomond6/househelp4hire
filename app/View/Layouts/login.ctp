<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.console.libs.templates.skel.views.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>	
	<link rel="shortcut icon" type="image/ico" href="<?php echo $this->Html->url('/img/favicon.gif', true);?>"/>
	<?php
	echo $this->Html->css(array('style'));
	echo $this->Html->css(array('validationEngine.jquery'));
	echo $this->Html->css(array('popup'));
	echo $this->Html->script(array('jquery-1', 'menu_injection_handler'));
	echo $this->Html->script(array('menu_injection_handler'));
	echo $this->Html->script(array('bootstrap-modal', 'bootstrap-modalmanager'));
	echo $this->Html->script(array('jquery-1.9.0.min', 'side_scripts'));
	echo $this->Html->script(array('jquery.validationEngine', 'jquery.validationEngine-en'));
	?>

<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#flashMessage").fadeOut(10000);
	$("#authMessage").fadeOut(10000);
	$('#overlay').click(function(){
		$('#overlay').fadeOut('fast');
	});
});

//-->
</script>
<style type="text/css">@media print{body .SnapABug_Button{display:none;}</style>
</head>
<body>
	<div id="mainWrapper">
		<?php echo $this->element('header'); ?>
		<?php echo $this->Session->flash(); ?>
		<?php //echo $this->Session->flash('auth'); ?>
		<div id="bodyWrapper">
            <?php echo $content_for_layout; ?>
        </div>
        <div class="clear"></div>
        <?php echo $this->element('footer'); ?>
    </div>
</body>
<?php //echo $this->element('sql_dump'); ?>
</html>