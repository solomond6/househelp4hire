<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.console.libs.templates.skel.views.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>HouseHelp4Hire</title>
	<link rel="shortcut icon" type="image/ico" href="<?php echo DOMAIN_NAME_PATH.'img/favicon.png';?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="<?php echo DOMAIN_NAME_PATH; ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo DOMAIN_NAME_PATH; ?>css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo DOMAIN_NAME_PATH; ?>css/docs.css" rel="stylesheet">
    <link href="<?php echo DOMAIN_NAME_PATH; ?>js/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="<?php echo DOMAIN_NAME_PATH; ?>css/hh4h.css" rel="stylesheet">
    <link href="<?php echo DOMAIN_NAME_PATH; ?>css/stylesheet.css" rel="stylesheet">
	<link href="<?php echo DOMAIN_NAME_PATH; ?>css/bootstrap-select.css" rel="stylesheet">
	<link href="<?php echo DOMAIN_NAME_PATH; ?>css/main_banner.css" rel="stylesheet">
	<link href="<?php echo DOMAIN_NAME_PATH; ?>css/jquery-ui.css" rel="stylesheet" media="all" type="text/css" />
	<link href="<?php echo DOMAIN_NAME_PATH; ?>css/jquery-ui-timepicker-addon.css" rel="stylesheet" media="all" type="text/css" />
	<?php echo $this->Html->css(array('validationEngine.jquery')); ?>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery-1.8.3.min.js"></script>
	<?php echo $this->Html->script(array('jquery.validationEngine', 'jquery.validationEngine-en'));	?>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.ui.core.js"></script>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.ui.datepicker.js"></script>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/widgets.js" type="text/javascript"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-transition.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-alert.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-modal.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-dropdown.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-scrollspy.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-tab.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-tooltip.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-popover.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-button.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-collapse.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-carousel.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-typeahead.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-affix.js"></script> 
	<!-- <script src="<?php //echo DOMAIN_NAME_PATH; ?>js/holder/holder.js"></script>  -->
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/google-code-prettify/prettify.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/application.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap-select.js"></script> 
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
	<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.cloud9carousel.js"></script> 
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo DOMAIN_NAME_PATH; ?>js/html5shiv.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
	<script type="text/javascript">
		$(document).ready(function(){
			$("#flashMessage").fadeOut(50000);
			$("#authMessage").fadeOut(50000);
			/*$('#overlay').click(function(){
				$('#overlay').fadeOut('fast');
			});*/
		});
	</script>
	<!-- Le javascript
	================================================== --> 
	<!-- Placed at the end of the document so the pages load faster --> 
	<script type="text/javascript">
        $(window).on('load', function () {
			 $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });
        });
    </script>
	<script type="text/javascript">
		$('.modal').bind('hide', function () {
			$('.formError').remove();
		});
	</script>
	<script type="text/javascript">
	$(function() {
	  var showcase = $("#showcase")

	  showcase.Cloud9Carousel(
		  {
			yPos: 42,
			yRadius: 48,
			mirrorOptions: {
			  gap: 12,
			  height: 0.2
			},
			buttonLeft: $(".nav > .left"),
			buttonRight: $(".nav > .right"),
			autoPlay: true,
			bringToFront: true,
			onRendered: showcaseUpdated,
			onLoaded: function() {
			  showcase.css( 'visibility', 'visible' )
			  showcase.css( 'display', 'none' )
			  showcase.fadeIn( 1500 )
			}
		  } 
	  )

	  function showcaseUpdated( showcase ) {
		var title = $('#item-title').html(
		  $(showcase.nearestItem()).attr( 'alt' )
		)
		var c = Math.cos((showcase.floatIndex() % 1) * 2 * Math.PI)
		title.css('opacity', 0.5 + (0.5 * c))
	  }

	  // Simulate physical button click effect
	  $('.nav > button').click( function( e ) {
		var b = $(e.target).addClass( 'down' )
		setTimeout( function() { b.removeClass( 'down' ) }, 80 )
	  } )

	  $(document).keydown( function( e ) {
		//
		// More codes: http://www.javascripter.net/faq/keycodes.htm
		//
		switch( e.keyCode ) {
		  /* left arrow */
		  case 37:
			$('.nav > .left').click()
			break

		  /* right arrow */
		  case 39:
			$('.nav > .right').click()
		}
	  } )
	})
	</script>
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
	<div class="span1 pull-right affix right rt-hk">
		<a href="<?php echo $siteSetting['Setting']['facebook_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/f.png"></a>
		<a href="<?php echo $siteSetting['Setting']['twitter_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/t.png"></a>
		<!-- <a href="<?php echo $siteSetting['Setting']['linkedin_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/l.png"></a> -->
		<a href="<?php echo $siteSetting['Setting']['pinterest_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/p.png"></a>
	</div>
	<div class="span2 pull-right affix bottom"><a download="<?php echo $siteSetting['Setting']['brochure']; ?>" href="<?php echo PDF_UP_URL.$siteSetting['Setting']['brochure']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img src="<?php echo DOMAIN_NAME_PATH; ?>img/brochure_link.png" alt="" /></a></div>
	<?php echo $this->element('header'); ?>
	<?php
	if($this->params['controller'] == 'testimonials' || $this->params['controller'] == 'pages'  || $this->params['controller'] == 'members'   || $this->params['controller'] == 'contacts'){}else{}
	?>
	<?php 
	if($this->params['action'] == 'index' && $this->params['controller'] == 'users')
	{
	//echo $this->element('banner');
	}?>
	<div class="main">
		<hr class="soften pad-top">
		<div class="container"> 
			<?php 
			if($this->params['action'] == 'index' && $this->params['controller'] == 'Users'){ 
				if(!$this->Session->check('NL_PopUp')){
					$this->Session->write('NL_PopUp', 'true');
				?>
			<div id="myModalNL" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelThanks" aria-hidden="true">
				<div class="frm-wrap">
				<?php echo $this->Form->create('Email', array('action' => 'add')); ?>
				<div class="modal-body modal-edit">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 id="myModalLabelNL">Subscribe to our newsletter</h3>
				    <div class="control-group">
						<label class="control-label nl-label1" for="inputIcon">Signup now! Stay informed on the latest from HouseHelp4Hire!</label>
						<div class="input-append">
							<?php echo $this->Form->input('email', array('placeholder'=>'Your email address', 'label' => false, 'div' => false, 'class'=>'span3 validate[required,custom[email]]')); ?>
							<button type="submit" class="btn btn-warning">Subscribe</button>
						</div>
						<label class="control-label nl-label2" for="inputIcon">Don't worry you'll not be spammed</label>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
				</div>
			</div>
			<script>		
			   $(document).ready(function() {
				$("#EmailAddForm").validationEngine({scroll:false, promptPosition: "topLeft"})
			   });
				$(document).ready(function() {
					$('#myModalNL').modal('show');
				});
			</script>
			<?php 
				}
			} 
			?>
			<?php //echo $this->Session->flash('true'); ?>
			<?php 
			if($this->Session->read('Message.flash')){
				if($this->Session->read('Message.flash.params.class') == 'success'){
					?>
					<div id="myModalSuccess" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelThanks" aria-hidden="true" style="top: 40%;">
						<div class="modal-body">
							<fieldset style="text-align:left; font-size: 16px; font-weight: bold; color: green;">
								<div class="pull-left" style="margin-right:10px;"><img src="<?php echo DOMAIN_NAME_PATH; ?>img/valid2.png" alt="" /></div><div class=""><?php echo $this->Session->read('Message.flash.message'); ?></div>
								<div class="clearfix"></div>
							</fieldset>
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
					<script>		
						$(document).ready(function() {
							$('#myModalSuccess').modal('show');
						});
					</script>
					<?php
				} else {
					?>
					<div id="myModalError" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelThanks" aria-hidden="true" style="top: 40%;">
						<div class="modal-body">
							<fieldset style="text-align:left; font-size: 16px; font-weight: bold; color: red;">
								<div class="pull-left" style="margin-right:10px;"><img src="<?php echo DOMAIN_NAME_PATH; ?>img/error2.png" alt="" /></div><div class=""><?php echo $this->Session->read('Message.flash.message'); ?></div>
								<div class="clearfix"></div>
							</fieldset>
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
					<script>		
						$(document).ready(function() {
							$('#myModalError').modal('show');
						});
					</script>
					<?php
				}
			}
			echo $this->Session->flash(); 
			?>
		</div>
		<?php echo $content_for_layout; ?>
	</div>
	<?php echo $this->element('footer'); ?>

	<?php //echo $this->element('sql_dump'); ?>
	<!-- http://www.mirkopagliai.it/cakephp-custom-flash-messages/ -->
	<script type="text/javascript">
		$('#basic_example_2').timepicker({
			hourMin: 8,
			hourMax: 16 
		});
		$('#basic_example_3').timepicker();
	</script>
	<style>
	.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current{display:none;}
	</style>
</body>
</html>