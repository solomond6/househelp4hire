<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php __('Welcome to Printrly.com'); ?>
		</title>
        <link rel="shortcut icon" type="image/ico" href="<?php echo $this->Html->url('/img/favicon.gif', true);?>"/>
		<?php
            echo $this->Html->css(array('style', 'validationEngine.jquery'));
            echo $this->Html->script(array( 'jquery-1.4.2.min','jquery.validationEngine', 'jquery.validationEngine-en'));
        ?>
		<script type="text/javascript" language="javascript">
			$(document).ready(function() {
				$("#flashMessage").fadeOut(10000);
				});
		</script>
	</head>
	<body>
		<center>
			<!-- body section -->	
				<?php echo $this->Session->flash(); ?>
				<?php echo $content_for_layout; ?>
			<!-- body section -->			
		</center>
	</body>
</html>