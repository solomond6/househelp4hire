<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.console.libs.templates.skel.views.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html>
<html>
<?php if ($this->request->here == '/' || $this->request->here=='/househelpmay/' || $this->request->here=='/househelpmay/users/index' || $this->request->here=='/users/index') {?>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>HouseHelp4Hire</title>
	
	<link rel="shortcut icon" type="favicon/ico" sizes="32x32" href="<?php echo DOMAIN_NAME_PATH.'img/favicon-16x16.png';?>"/>
	<!--<link rel="shortcut icon" href="<?php echo DOMAIN_NAME_PATH.'img/favicon.ico';?>" type="favicon/ico"/>-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $siteSetting['Setting']['meta_description']; ?>">
    <meta name="author" content="">
    <meta name="google-site-verification" content="YbAUij7NaZjfkNZv9F9Rm_Q1hl9j6rKRo_2YaJDitk0" />
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo DOMAIN_NAME_PATH; ?>js/html5shiv.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    	<link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/styles.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/style.css" type="text/css" media="all" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/bootstrap.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/font-awesome.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/prettyPhoto.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/owl.carousel.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/style-normal.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/newstyle.css" type="text/css" media="all" />
        <!--<link rel="stylesheet" href="css/supersized.css" type="text/css" media="all" />-->
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/dynamic.css" type="text/css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo DOMAIN_NAME_PATH; ?>css/validationEngine.jquery.css" />
        
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/comment-reply.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/scripts.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/modernizr.custom.61080.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.easing.js"></script>
        <script> 
        function showModalPopup(ctrl)
            {
				jQuery("#"+ctrl).modal('show');
			}
		</script>
        <style type="text/css">
            @media (max-width:480px) {
                .header{ height: 105px!important;}
                .menu-btn{top: 60px!important;}
                .logo{padding-top: 35px!important;}
            }
        </style>
</head>
<?php } else { ?>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>HouseHelp4Hire</title>
	<link rel="shortcut icon" type="favicon/ico" sizes="32x32" href="<?php echo DOMAIN_NAME_PATH.'img/favicon-16x16.png';?>"/>
	<!--<link rel="shortcut icon" href="<?php echo DOMAIN_NAME_PATH.'img/favicon.ico';?>" type="favicon/ico"/>-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $siteSetting['Setting']['meta_description']; ?>">
    <meta name="author" content="">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo DOMAIN_NAME_PATH; ?>js/html5shiv.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    	 <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/styles.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/style.css" type="text/css" media="all" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/bootstrap.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/font-awesome.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/prettyPhoto.css" type="text/css" media="all" />
        <!-- <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/owl.carousel.css" type="text/css" media="all" /> -->
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/style-normal.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/supersized.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/dynamic.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>css/newstyle.css" type="text/css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo DOMAIN_NAME_PATH; ?>css/validationEngine.jquery.css" />
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/scripts.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/modernizr.custom.61080.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.easing.js"></script>
        <script> 
        function showModalPopup(ctrl)
            {
				jQuery("#"+ctrl).modal('show');
			}
		</script>
		
		<?php if ($this->request->here == '/my-account/' || $this->request->here == '/my-account'  || $this->request->here=='/househelpmay/my-account') {?>
		<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/dataTables.responsive.css">
        <style type="text/css" class="init">
            body { font-size: 140% }
            table.dataTable th,
            table.dataTable td {
            white-space: nowrap;
            }
        </style>
     
        <script type="text/javascript" language="javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" class="init">
            jQuery(document).ready(function() {
                jQuery('#example').DataTable();
            } );
        </script>
        <?php } ?>
</head>
<?php } ?>
<?php if ($this->request->here == '/' || $this->request->here=='/househelpmay/'  || $this->request->here=='/househelpmay/users/index' || $this->request->here=='/users/index') {?>
<body>
<?php } else { ?>
<body class="home page page-id-114 page-template page-template-homepage-slider-php">
<?php } ?>
	
	<?php echo $this->element('header'); ?>
	
	<?php echo $content_for_layout; ?>
	<?php echo $this->element('footer'); ?>
	<?php echo $siteSetting['Setting']['google_analytic_code'] ?>
	<style>
	.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current{display:none;}
	</style>
<?php if ($this->request->here == '/' || $this->request->here=='/househelpmay/'  || $this->request->here=='/househelpmay/users/index' || $this->request->here=='/users/index') {?>	
        <script type="text/javascript" src="js/jquery.form.min.js?ver=3.46.0-2013.11.21"></script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var _wpcf7 = {"loaderUrl":"images\/ajax-loader.gif","sending":"Sending ...","cached":"1"};
            /* ]]> */
        </script>
       
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/superfish.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.fitvids.js"></script>
        <!-- <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.prettyPhoto.js"></script> -->
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.nav.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.scrollTo.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.sticky.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/ticker.js"></script>
       
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/filter.js"></script>
        <!-- <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/script.js"></script> -->
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.peelback.js"></script>
        <!--<script type="text/javascript" src="js/supersized.3.2.7.min.js"></script>
        <script type="text/javascript" src="js/supersized.shutter.js"></script>-->
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.cycle.all.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.validationEngine-en.js"></script>
		<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jqBootstrapValidation.js"></script>
        <script type="text/javascript">
            (function ($) {
                "use strict";
               
                jQuery(document).ready(function ($) {
                    $("#appendedInputButton").val($("#drpSearch").val());
            
                    $("#drpSearch").change(function () {
                        var currentVal = $(this).val();
                        if (currentVal != "") {
                            $("#appendedInputButton").val(currentVal);
                        }
                    })
                    
                    $('body').peelback({
				        adImage  : 'images/peel-ad.png',
				        peelImage  : 'images/peel-image.png',
				        clickURL : '<?php echo PDF_UP_URL.$siteSetting['Setting']['brochure']; ?>',
				        gaTrack  : true,
				        gaLabel  : '#1 HopuseHelp4Hire',
				        autoAnimate: true
		      		});  
					$('#peelback').find("a").attr("title","Click to download");
					$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
                });
                
            })(jQuery);
		</script>
<?php echo $this->Session->flash(); } else { ?>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/comment-reply.min.js"></script>
       
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery-migrate.min.js"></script>
        
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.form.min.js?ver=3.46.0-2013.11.21"></script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var _wpcf7 = {"loaderUrl":"images\/ajax-loader.gif","sending":"Sending ...","cached":"1"};
            /* ]]> */
        </script>
       
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/superfish.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.fitvids.js"></script>
        <!-- <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.prettyPhoto.js"></script> -->
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.nav.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.scrollTo.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.sticky.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/ticker.js"></script>
        
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/filter.js"></script>
        <!-- <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/script.js"></script> -->
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/supersized.3.2.7.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/supersized.shutter.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/testimonial.js"></script>
      
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true&#038;ver=3.8.4"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jflickrfeed.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.cycle.all.min.js"></script>
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.validationEngine-en.js"></script>
		<script src="<?php echo DOMAIN_NAME_PATH; ?>js/jqBootstrapValidation.js"></script>

        <script type="text/javascript">
            (function ($) {
            "use strict";
            jQuery(document).ready(function($) {
            
                $.supersized({
            
                    // Functionality
                    slide_interval: 6000, // Length between transitions
                    transition: 1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                    transition_speed: 2500, // Speed of transition
            
                    // Components							
                    slide_links: 'false', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
                    slides: [ // Slideshow Images
            
                        {
                    image: '<?php echo DOMAIN_NAME_PATH; ?>images/hiw-bg.jpg',
                    title: '' },									
                    ]
            
                });

				$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
            });
            })(jQuery);
	         function loginRedirect(linkId){
				if(linkId == 'LogInHeader'){
					
					document.getElementById('LoginflagVar').value = 0;
				} else {
					
					var id = linkId.substr(16);
					document.getElementById('LoginflagVar').value = id;
				}
			}

		</script>
        
<?php echo $this->Session->flash(); } ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58eee958f7bbaa72709c601a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>