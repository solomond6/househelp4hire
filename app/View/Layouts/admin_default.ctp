<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2009, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.console.libs.templates.skel.views.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
 #print_r($this->params);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php __('Administration Panel'); ?>
		<?php echo 'Admin Panel || HouseHelp4Hire'; //$title_for_layout ?>
	</title>
	<link rel="shortcut icon" type="image/ico" href="<?php echo DOMAIN_NAME_PATH.'img/favicon.png';?>"/>
	
		<?php if (strtolower($this->request->here) == strtolower('/admin/Users/login') || strtolower($this->request->here)==strtolower('/househelpmay/admin/Users/login') || strtolower($this->request->here)==strtolower('/househelpmay/admin/Users/login') || strtolower($this->request->here)==strtolower('/admin/Users/login')) {?>
			<link rel="stylesheet" href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/bootstrap.css" type="text/css" media="all" />
		<?php } else { ?>
			<link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/bootstrap.min.css" rel="stylesheet" />
	        <link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/bootstrap-responsive.min.css" rel="stylesheet" />
	        <link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/bootstrap-fileupload.css" rel="stylesheet" />
	        <link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/font-awesome.css" rel="stylesheet" />
	        <link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/style.css" rel="stylesheet" />
	        <link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/style_responsive.css" rel="stylesheet" />
	        <link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/style_default.css" rel="stylesheet" id="style_color" />
	        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
	        <link href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/jquery.fancybox.css" rel="stylesheet" />
	        <link rel="stylesheet" type="text/css" href="<?php echo DOMAIN_NAME_PATH; ?>admin/css/uniform.default.css" />
	        <link rel="stylesheet" type="text/css" href="<?php echo DOMAIN_NAME_PATH; ?>css/dataTables.bootstrap.css" />
        	<link rel="stylesheet" type="text/css" href="<?php echo DOMAIN_NAME_PATH; ?>css/dataTables.responsive.css" />
	       <?php 
	        //echo $this->Html->meta('icon');
			//echo $this->Html->css('cake.generic');
			//echo $this->Html->css('admin');
			//echo $this->Html->css('jquery-ui');
			echo $this->Html->css('colorbox');
			echo $this->Html->css('validationEngine.jquery');
			echo $this->Html->css('jquery.fancybox');
			echo $this->Html->css('jquery.ui.all');
			//echo $this->Html->css('demos');
		   } ?>
		<?php
		
		echo $this->Html->script(array('jquery-1.9.1', 'jquery-1.7.2.min', 'jquery.ui.core','jquery.ui.widget','jquery.ui.datepicker','ddaccordion','side_scripts','jquery-1','jquery.validationEngine', 'jquery.validationEngine-en','fancybox/jquery.fancybox-1.2.0','jconfirmaction.jquery'));
		
		echo $scripts_for_layout;
		?>		
		<script src="<?php echo DOMAIN_NAME_PATH; ?>admin/js/bootstrap.min.js"></script> 
		<script src="<?php echo DOMAIN_NAME_PATH; ?>admin/js/scripts.js"></script>
       
        <script>
            jQuery(document).ready(function() {       
                // initiate layout and plugins
                App.init();
            });
        </script>
	
	
	
</head>
		<?php if (strtolower($this->request->here) == strtolower('/admin/Users/login') || strtolower($this->request->here)==strtolower('/househelpmay/admin/Users/login') || strtolower($this->request->here)==strtolower('/househelpmay/admin/Users/login') || strtolower($this->request->here)==strtolower('/admin/Users/login')) {?>
<body>
<?php } else { ?>
<body class="fixed-top">
<?php } ?>
<?php if (strtolower($this->request->here) == strtolower('/admin/Users/login') || strtolower($this->request->here)==strtolower('/househelpmay/admin/Users/login') || strtolower($this->request->here)==strtolower('/househelpmay/admin/Users/login') || strtolower($this->request->here)==strtolower('/admin/Users/login')) {?>
<?php echo $content_for_layout; ?>
<?php } else { ?>

<?php echo $this->element('admin_header'); ?>

<div id="container" class="row-fluid">
<?php echo $this->element('admin_left_menu'); ?>
	<div id="main-content">
		
		<div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <div class="alert alert-error span12" style="margin: 10px 0px -10px 10px;">
                    <?php echo $this->Session->flash(); ?>
					<strong><?php echo $this->Session->flash('auth'); ?></strong>
					<div class="tn-progress"></div>
                </div>
            </div>
        </div>
		<?php echo $content_for_layout; ?>
	</div>
</div>

<?php echo $this->element('admin_footer'); ?>



<?php } ?>

</body>
</html>