   <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Edit Vetting Category</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Edit Vetting Category</h4>
                                </div>
                                <div class="widget-body form">
                                <?php 
									echo $this->Form->create('VettingCategory');
									echo $this->Form->input('name');
									//echo $this->Form->input('created_at');
									echo $this->Form->end(__('Submit'));
								?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->