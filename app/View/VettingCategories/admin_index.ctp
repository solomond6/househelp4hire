   <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Vetting Categories</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Vetting Categories</h4>
                                </div>
                                <div class="widget-body form">
                                <table cellpadding="0" cellspacing="0" class="table">
									<tr>
											<th><?php echo $this->Paginator->sort('id'); ?></th>
											<th><?php echo $this->Paginator->sort('name'); ?></th>
											<th class="actions"><?php echo __('Actions'); ?></th>
									</tr>
									<?php
									foreach ($vettingCategories as $vettingCategory): ?>
									<tr>
										<td><?php echo h($vettingCategory['VettingCategory']['id']); ?>&nbsp;</td>
										<td><?php echo h($vettingCategory['VettingCategory']['name']); ?>&nbsp;</td>
										<td class="actions">
											<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vettingCategory['VettingCategory']['id'])); ?>
											<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vettingCategory['VettingCategory']['id']), null, __('Are you sure you want to delete # %s?', $vettingCategory['VettingCategory']['id'])); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</table>
                                </div>
                                <div class="vettingCategories index">
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>	</p>

									<div class="paging">
									<?php
										echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
										echo $this->Paginator->numbers(array('separator' => ''));
										echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
									?>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
