<div class="vettingCategories view">
<h2><?php  echo __('Vetting Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vettingCategory['VettingCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($vettingCategory['VettingCategory']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created At'); ?></dt>
		<dd>
			<?php echo h($vettingCategory['VettingCategory']['created_at']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vetting Category'), array('action' => 'edit', $vettingCategory['VettingCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vetting Category'), array('action' => 'delete', $vettingCategory['VettingCategory']['id']), null, __('Are you sure you want to delete # %s?', $vettingCategory['VettingCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetting Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Category'), array('action' => 'add')); ?> </li>
	</ul>
</div>
