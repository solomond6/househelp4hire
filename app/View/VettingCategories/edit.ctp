<div class="vettingCategories form">
<?php echo $this->Form->create('VettingCategory'); ?>
	<fieldset>
		<legend><?php echo __('Edit Vetting Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('created_at');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VettingCategory.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('VettingCategory.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Vetting Categories'), array('action' => 'index')); ?></li>
	</ul>
</div>
