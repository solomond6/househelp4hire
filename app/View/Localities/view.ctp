<div class="localities view">
<h2><?php  echo __('Locality'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($locality['Locality']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($locality['Locality']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Location'); ?></dt>
		<dd>
			<?php echo $this->Html->link($locality['Location']['name'], array('controller' => 'locations', 'action' => 'view', $locality['Location']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Locality'), array('action' => 'edit', $locality['Locality']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Locality'), array('action' => 'delete', $locality['Locality']['id']), null, __('Are you sure you want to delete # %s?', $locality['Locality']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Localities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Locality'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Locations'), array('controller' => 'locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Location'), array('controller' => 'locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Projects'); ?></h3>
	<?php if (!empty($locality['Project'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Bedroom'); ?></th>
		<th><?php echo __('Bathroom'); ?></th>
		<th><?php echo __('Apartment Type Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Locality Id'); ?></th>
		<th><?php echo __('Project Type Id'); ?></th>
		<th><?php echo __('Area Ft'); ?></th>
		<th><?php echo __('Area Yard'); ?></th>
		<th><?php echo __('Rate'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Short Descrp'); ?></th>
		<th><?php echo __('Long Descrp'); ?></th>
		<th><?php echo __('Buildig Id'); ?></th>
		<th><?php echo __('Availability Id'); ?></th>
		<th><?php echo __('Sale Type'); ?></th>
		<th><?php echo __('Is Featured'); ?></th>
		<th><?php echo __('Date Of Post'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Project Type'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($locality['Project'] as $project): ?>
		<tr>
			<td><?php echo $project['id']; ?></td>
			<td><?php echo $project['user_id']; ?></td>
			<td><?php echo $project['bedroom']; ?></td>
			<td><?php echo $project['bathroom']; ?></td>
			<td><?php echo $project['apartment_type_id']; ?></td>
			<td><?php echo $project['location_id']; ?></td>
			<td><?php echo $project['locality_id']; ?></td>
			<td><?php echo $project['project_type_id']; ?></td>
			<td><?php echo $project['area_ft']; ?></td>
			<td><?php echo $project['area_yard']; ?></td>
			<td><?php echo $project['rate']; ?></td>
			<td><?php echo $project['price']; ?></td>
			<td><?php echo $project['short_descrp']; ?></td>
			<td><?php echo $project['long_descrp']; ?></td>
			<td><?php echo $project['buildig_id']; ?></td>
			<td><?php echo $project['availability_id']; ?></td>
			<td><?php echo $project['sale_type']; ?></td>
			<td><?php echo $project['is_featured']; ?></td>
			<td><?php echo $project['date_of_post']; ?></td>
			<td><?php echo $project['is_active']; ?></td>
			<td><?php echo $project['project_type']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'projects', 'action' => 'view', $project['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'projects', 'action' => 'edit', $project['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'projects', 'action' => 'delete', $project['id']), null, __('Are you sure you want to delete # %s?', $project['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
