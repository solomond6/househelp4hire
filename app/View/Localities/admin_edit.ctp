   <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Location Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Edit Localities</h4>
                                </div>
                                <div class="widget-body form">
                                <?php echo $this->Form->create('Locality'); ?>
                               
                                <?php 
                                	
                                	$localities_arr=$this->data[0]["hh_localities"];
                                	$id=$localities_arr["id"];
                                	$name=$localities_arr["name"];
                                	$location_id=$localities_arr["location_id"];
                                 ?>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="form-horizontal">
                                                <div class="control-group">
                                                   <?php
													echo $this->Form->input('id',array('default'=>$id));
													echo $this->Form->input('name',array('default'=>$name));
													?>
                                                </div>
                                                <div class="control-group">
                                                   <?php echo $this->Form->input('location_id',array('default'=>$location_id)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->