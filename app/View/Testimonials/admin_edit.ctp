<?php echo $this->Html->Script(array('tiny_mce/jquery.tinymce.min')); ?>
<?php echo $this->Html->Script(array('tiny_mce/tinymce.min')); ?>
<script type="text/javascript">
    tinymce.init({
      selector: 'textarea',
      height: 500,
      browser_spellcheck: true,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ]
    });
</script>
<div >
<?php echo $this->Form->create('Testimonial', array('enctype' => 'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Testimonial'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('image',array('type'=>'file'));
		echo '<p style="margin:10px 0 10px 0;">'.$this->Html->image(PAGE_IMAGES_URL.$this->request->data['Testimonial']['image'], array('title'=>"image", 'alt'=>'image', 'width'=>'150')).'</p>';
		echo $this->Form->input('content', array('id' => 'pagecontent1', 'class'=>'pcontent'));
		//echo $this->Form->input('status');
		//echo $this->Form->input('date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

