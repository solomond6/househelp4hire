<div >
<h2><?php  echo __('Testimonial'); ?></h2>
	<dl>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo $this->Html->image(PAGE_IMAGES_URL.$testimonial['Testimonial']['image'], array('title'=>"image", 'alt'=>'image', 'width'=>'150')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo ($testimonial['Testimonial']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($testimonial['Testimonial']['date']); ?>
			&nbsp;
		</dd>
		<dt>&nbsp;</dt>
		<dd class="actions"><?php echo $this->Html->link(__('Back', true), array('action' => 'index')); ?> &nbsp;</dd>
	</dl>
</div>

