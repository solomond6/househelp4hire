<div >
	<h2><?php echo __('Testimonials'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('content'); ?></th> -->
			<!-- <th><?php echo $this->Paginator->sort('status'); ?></th> -->
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th class="actions" style="text-align:center"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($testimonials as $testimonial): ?>
	<tr>
		<td><?php echo h($testimonial['Testimonial']['id']); ?>&nbsp;</td>
		<td><?php echo $this->Html->image(PAGE_IMAGES_URL.$testimonial['Testimonial']['image'], array('title'=>"image", 'alt'=>'image', 'width'=>'100')); ?>&nbsp;</td>
		<!-- <td><?php echo h($testimonial['Testimonial']['content']); ?>&nbsp;</td> -->
		<!-- <td><?php echo h($testimonial['Testimonial']['status']); ?>&nbsp;</td> -->
		<td><?php echo h($testimonial['Testimonial']['date']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $testimonial['Testimonial']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $testimonial['Testimonial']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $testimonial['Testimonial']['id']), null, __('Are you sure you want to delete?', $testimonial['Testimonial']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
