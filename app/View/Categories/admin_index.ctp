  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Category Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Manage Category</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span12" style="margin-bottom: 25px;">
                                            <a href="categories/add" class="btn btn-primary pull-right"><i class="fa fa-user-plus"></i> Add New Categories</a>
                                        </div>
                                    </div>
                                     <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
	                                        <tr>
	                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
												<th><?php echo $this->Paginator->sort('name'); ?></th>
												<th><?php echo $this->Paginator->sort('image'); ?></th>
												<th><?php echo $this->Paginator->sort('parent_id'); ?></th>
												<th class="actions"><?php echo __('Actions'); ?></th>
	                                        </tr>
                                     	</thead>
                                        <tbody>
                                       
                                <?php foreach ($categories as $category): ?>
									<tr>
		<td><?php echo h($category['Category']['id']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['name']); ?>&nbsp;</td>
		<td><?php echo $this->Html->image(CATEGORY_IMAGES_URL.$category['Category']['thumb_image'], array('width'=>'180'))?>&nbsp;</td>
		<td><?php if($category['Category']['parent_id']==null){echo h('Root');}else{echo h('child');}?>&nbsp;</td>
		<td class="actions">
			<?php 
				if($category['Category']['status']=='Y') 
				{
					$status = 'Active';
					$action = 'deactivate';
				}
				else
				{
					$status = 'Inactive';
					$action = 'activate';
				}
				echo $this->Html->link(__($status, true), array('action' => 'status', $category['Category']['id']), array('title' => 'Click here to '.$action.' category')); 
			?>
			<?php echo $this->Html->link('<i class="fa fa-eye"></i> View', array('action' => 'view', $category['Category']['id']), array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'edit', $category['Category']['id']),array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<a href="#conDialog" role="button"  class="btn btn-danger" data-toggle="modal" data-attr="<?php echo $category['Category']['id']?>" id="deleteConfirm"><i class="fa fa-trash-o" ></i> Delete</a>&nbsp;
		</td>
	</tr>
								<?php endforeach; ?>
										</tbody>
									</table>
									<p>
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>	</p>

									<div class="paging">
									<?php
										echo $this->Paginator->prev('< ' . __(' previous '), array(), null, array('class' => 'prev disabled'));
										echo $this->Paginator->numbers(array('separator' => ' '));
										echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
									?>
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                 <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			                <h3 id="myModalLabel1">Are you sure ?</h3>
			            </div>
            
			            <div class="modal-footer">
			                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
			                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
			            </div>
        		</div>
<script>
     var record_id="";
     jQuery(document).ready(function(){
     		jQuery("#deleteConfirm").click(function(){
     			var id=jQuery(this).attr("data-attr");
     			record_id=id;
     		})
     })
	
	function Delete()
	{
		console.log("delete/"+record_id);
		document.location.href="categories/delete/"+record_id;
	}
</script>