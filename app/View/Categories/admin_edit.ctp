<?php echo $this->Html->Script(array('tiny_mce/jquery.tinymce.min')); ?>
<?php echo $this->Html->Script(array('tiny_mce/tinymce.min')); ?>
<script type="text/javascript">
    tinymce.init({
      selector: 'textarea',
      height: 500,
      browser_spellcheck: true,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ]
    });
</script>
  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Category Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-list"></i> Edit Category</h4>
                                </div>
                                <div class="widget-body form">
                                <?php echo $this->Form->create('Category', array('enctype' => 'multipart/form-data')); ?>
                                    <div class="row-fluid">
                                    
                                  			<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('class'=>'span12'));
		echo $this->Form->input('name_singular',array('class'=>'span12'));
		echo $this->Form->input('tag_line',array('class'=>'span12'));
		echo $this->Form->input('parent_id', array('type'=>'select', 'label'=>'Category', 'legend' => false, 'options'=>$options, 'empty'=>'Root Category'));
		echo $this->Form->input('image', array('type'=>'file'));
		echo $this->Form->input('previmage', array('type'=>'hidden'));
		echo '<div>'.$this->Html->image(CATEGORY_IMAGES_URL.$this->request->data['Category']['previmage'], array('height'=>'100')).'</div>';
		echo $this->Form->input('thumb_image', array('type'=>'file', 'label'=>'Thumbnail Image'));
		echo $this->Form->input('prevthumbimage', array('type'=>'hidden'));
		echo '<div>'.$this->Html->image(CATEGORY_IMAGES_URL.$this->request->data['Category']['prevthumbimage'], array('height'=>'100')).'</div>';
		
		echo $this->Form->input('short_description',array('class'=>'span12'));
		echo $this->Form->input('description', array('id' => 'pagecontent1', 'class'=>'pcontent'));
     echo '<br/>';
    echo '<label>Category Questions</label><div class="input_fields_wrap"><div><input type="text" name="questions[]" class="span10" ><a href="#" class="add_field_button btn btn-success" style="margin-top: -10px;">Add Question</a></div></div>';
		echo $this->Form->input('status', array('type' => 'select', 'options' => array('Y' => 'Active', 'N' => 'Inactive')));
	?></div>
										</div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->

<script>
  $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="questions[]" class="span10"/><a href="#" class="remove_field btn btn-danger" style="margin-top: -10px;">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
  });
</script>