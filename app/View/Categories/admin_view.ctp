<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Category Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Category</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">Banner image <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $this->Html->image(CATEGORY_IMAGES_URL.$category['Category']['image'], array('height'=>'100')) ?></span><br>
                                                        <div style="clear: both;"></div>
                                                    </div>
                                                </div>
                                                <div class="span6">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">Thumbnail image <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $this->Html->image(CATEGORY_IMAGES_URL.$category['Category']['thumb_image'], array('height'=>'100')) ?></span><br>
                                                        <div style="clear: both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <label class="pull-left view-s">ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($category['Category']['id']); ?></span><br>
                                            <div style="clear: both;"></div>
                                            <label class="pull-left view-s">Name <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($category['Category']['name']); ?></span><br>
                                            <div style="clear: both;"></div>
                                            <label class="pull-left view-s">Name Singular <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($category['Category']['name_singular']); ?></span><br>
                                            <div style="clear: both;"></div>
                                            <label class="pull-left view-s">Tag Line <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($category['Category']['tag_line']); ?></span><br>
                                            <div style="clear: both;"></div>
                                            <label class="pull-left view-s">Short Description <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($category['Category']['short_description']); ?></span><br>
                                            <div style="clear: both;"></div>
                                            <label class="pull-left view-s">Description <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $category['Category']['description']; ?></span><br>
                                            <div style="clear: both;"></div>
                                            <label class="pull-left view-s">Status <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if($category['Category']['status'] == 'Y') { echo h('Active'); } else { echo h('Inactive'); } ?></span><br>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        
                                        <?php echo $this->Html->link('Back', array('action' => 'index'),array('class'=>'btn btn-success')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->