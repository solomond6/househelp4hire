<?php
 if(count($news) > 0){
	foreach ($news as $news): ?>
			<div style="margin-bottom:10px" class="boxholder">
				<h1><?php echo $news['News']['name']; ?></h1>
				<figure class="imgl boxholder">
					<?php
					$imgPath=$this->Html->url('/resize.php?pic='.NEWS_IMAGES.$news['News']['image'].'&w=150&h=200&ar=0',true);
					echo $this->Html->image($imgPath, array('label' => false, 'div' => false, 'border' => '0', 'alt' => ''));
					?>
				</figure>
				<?php if(strlen($news['News']['description']) > 600){ ?>
				<p><?php echo substr($news['News']['description'], 0, 600).' (...)'; ?></p>
				<?php } else { ?>
				<p><?php echo $news['News']['description']; ?></p>
				<?php } ?>
				<p><?php echo $this->Html->link('View Details', array('controller' => 'News','action' => 'view', $news['News']['id']), array('class'=>'button small gradient orange')); ?></p>
			</div>
	<?php endforeach; 
 } else {?>
		<h4 style="text-align: center;">No news has been found!</h4>
 <?php } ?>