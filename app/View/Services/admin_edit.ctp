<?php echo $this->Html->Script(array('tiny_mce/jquery.tinymce.min')); ?>
<?php echo $this->Html->Script(array('tiny_mce/tinymce.min')); ?>
<script type="text/javascript">
    tinymce.init({
      selector: 'textarea',
      height: 500,
      browser_spellcheck: true,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ]
    });
</script>
  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Services Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-list"></i> Edit Services</h4>
                                </div>
                                <div class="widget-body form">
                              <?php echo $this->Form->create('Service', array('enctype' => 'multipart/form-data')); ?>
                                    <div class="row-fluid">
                                  	<?php
										echo $this->Form->input('id');
										echo $this->Form->input('name', array('label'=>'Head Line','class'=>'span12'));
										echo $this->Form->input('description', array('class'=>'pcontent', 'label'=>'News Details'));
										
										echo $this->Form->input('image', array('type'=>'file', 'label'=>'Cover Image'));
										echo $this->Form->input('prev_image', array('type'=>'hidden', 'value'=>$this->data['Service']['image']));
										echo '<div>'.$this->Html->image(NEWS_IMAGES_URL.$this->request->data['Service']['image'], array('height'=>'100')).'</div>';
										
										echo $this->Form->input('status', array('type' => 'select', 'options' => array('Y' => 'Active', 'N' => 'Inactive')));
									?>
									</div>
										</div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->