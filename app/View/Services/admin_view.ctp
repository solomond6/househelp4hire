    <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">View Services</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Services</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($service['Service']['id']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s"><?php echo __('Name'); ?> <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($service['Service']['name']); ?></span><br> <div style="clear: both;"></div>
                                                        <label class="pull-left view-s"><?php echo __('Description'); ?> <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">	<?php echo $service['Service']['description']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s"><?php echo __('Image'); ?> <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left">
                                                        <?php
			$imgPath=$this->Html->url('/resize.php?pic='.NEWS_IMAGES.$service['Service']['image'].'&w=100&h=150&ar=0',true);
			echo $this->Html->image($imgPath, array('label' => false, 'div' => false, 'border' => '0', 'alt' => '', 'width'=>'200'));
			?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s"><?php echo __('Status'); ?> <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php 
			if($service['Service']['status'] == 'Y' )
			{
				echo 'Active';
			} 
			else
			{
				echo 'InActive';
			}?></span><br>
                                                      
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            
                                        </div>
                                    </div>
	                                </div>
	                                <div class="form-actions">
                                        <?php echo $this->Html->link('Back', array('action' => 'index'),array('class'=>'btn btn-success')); ?>
                                    </div>
                           		 </div>
                        	</div>
                    	</div>
                    <!-- END PAGE CONTENT-->
                	</div>
				</div>                	
                <!-- END PAGE CONTAINER-->
