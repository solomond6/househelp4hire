 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">FAQ Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i>FAQ Manager</h4>
                                </div>
                                <div class="widget-body">
                                <div class="row-fluid">
                                  <div class="span12" style="margin-bottom: 25px;">
                                         <a href="faqs/add" class="btn btn-primary pull-right"><i class="fa fa-user-plus"></i> Add New FAQ</a>
                                        </div>
                                      </div>
                                      <div class="row-fluid">
                                      </div>
                                    <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
	                                        <tr>
	                                            <th><?php echo __('SL#');?></th>
												<th><?php echo $this->Paginator->sort('question', 'Question');?></th>
												<th style="text-align:center;"><?php echo __('Actions');?></th>
	                                        </tr>
                                     	</thead>
                                        <tbody>
                                       		<?php $i = 0; foreach ($Faqs as $faq): $i++;?>
												<tr>		

													<td><?php echo $i; ?>&nbsp;</td>
													<td><?php echo $faq['Faq']['question']; ?>&nbsp;</td>
													<!-- <td><?php echo ($faq['Faq']['status']==1 ? 'Active' : 'Inactive'); ?>&nbsp;</td> -->
													
													<td class="actions">
														
														<?php 
														if($faq['Faq']['status'] == 1 )
														{
															echo $this->Html->link(__('Active', true), array('action' => 'deactivate', $faq['Faq']['id']), array('title' => 'Click here to deactivate'));
														} 
														else
														{
															echo $this->Html->link(__('Inactive', true), array('action' => 'activate', $faq['Faq']['id']), array('title' => 'Click here to activate'));
														}?>
														<?php echo $this->Html->link('<i class="fa fa-eye"></i> View', array('action' => 'view', $faq['Faq']['id']), array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'edit', $faq['Faq']['id']),array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<a href="#conDialog" role="button"  class="btn btn-danger" data-toggle="modal" data-attr="<?php echo $faq['Faq']['id']?>" id="deleteConfirm"><i class="fa fa-trash-o" ></i> Delete</a>&nbsp;
													</td>
												</tr>
											<?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <p>
									<?php
									echo $this->Paginator->counter(array('format' => __("Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%")));
									?>	</p>

									<div class="paging">
										<?php echo $this->Paginator->prev(' Previous ', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
										<?php echo $this->Paginator->numbers(array('separator'=>' ')); ?>
										<?php echo $this->Paginator->next(' Next', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			                <h3 id="myModalLabel1">Are you sure ?</h3>
			            </div>
            
			            <div class="modal-footer">
			                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
			                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
			            </div>
        		</div>
<script>
     var record_id="";
     jQuery(document).ready(function(){
     		jQuery("#deleteConfirm").click(function(){
     			var id=jQuery(this).attr("data-attr");
     			record_id=id;
     		})
     })
	
	function Delete()
	{
		console.log("faqs/delete/"+record_id);
		document.location.href="faqs/delete/"+record_id;
	}
</script>