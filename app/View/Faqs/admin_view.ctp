   <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">FAQ Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View FAQ</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $faq['Faq']['id']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Question <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $faq['Faq']['question']; ?> </span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Answer <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left" style="max-width:700px;"><?php echo $faq['Faq']['solution']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Status <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if($faq['Faq']['status']=='1') 
					{
						 echo 'Active'; 
					}
					else
					{
						 echo 'Inactive'; 
					} ?></span><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <?php echo $this->Html->link(__('Back', true), array('action' => 'index'),array('class'=>'btn btn-success btn-lg')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->