<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#FaqAdminAddForm").validationEngine({scroll:true})
	});
//-->
</script>
<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">FAQ Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-plus-sign"></i> Add FAQ</h4>
                                </div>
                                <div class="widget-body form">
                                <?php echo $this->Form->create('Faq', array('enctype' => 'multipart/form-data'));?>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <div class="form-horizontal">
                                             <?php
		echo $this->Form->input('question', array('class' => 'span12 validate[required]'));
		echo $this->Form->input('solution', array('class' => 'span12 validate[required]'));
		echo $this->Form->input('status', array('type' => 'select', 'options' => array('1' => 'Active', '0' => 'Inactive')));
		?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <?php echo $this->Form->end();?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->