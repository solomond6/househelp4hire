<div>
	<div style="float: left"><h2><?php echo 'Manage Bookings';?></h2></div>
	<div style="float: right; padding-bottom:5px;"><?php //echo $this->Html->link(__('(Add New Bookings)', true), array('action' => 'add'), array('style'=>'color:#003D4C;')); ?></div> 
	<div style="clear: both"></div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('staff_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id','Employer'); ?></th>
			<th><?php echo $this->Paginator->sort('date', 'Interview Date'); ?></th>
			<th class="actions" style='text-align:center;'><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($bookings as $booking): ?>
	<tr>
		<td><?php echo h($booking['Booking']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($booking['Staff']['name'], array('controller' => 'staffs', 'action' => 'view', $booking['Staff']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($booking['User']['name'], array('controller' => 'users', 'action' => 'view', $booking['User']['id'])); ?>
		</td>
		<td><?php echo date("F j, Y", strtotime($booking['Booking']['date'])); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $booking['Booking']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $booking['Booking']['id'])); ?>
			<!-- <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $booking['Booking']['id']), null, __('Are you sure you want to delete?', $booking['Booking']['id'])); ?> -->
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
