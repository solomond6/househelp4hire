<div >
<h2><?php  echo __('Booking'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Staff'); ?></dt>
		<dd>
			<?php echo $this->Html->link($booking['Staff']['name'], array('controller' => 'staffs', 'action' => 'view', $booking['Staff']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($booking['User']['name'], array('controller' => 'users', 'action' => 'view', $booking['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Location'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['location']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Interview date'); ?></dt>
		<dd>
			<?php echo date("F j, Y", strtotime($booking['Booking']['date'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Interview Strt Time'); ?></dt>
		<dd>
			<?php echo date("g:i a", strtotime($booking['Booking']['start_time'])); ?>
			&nbsp;
		</dd>
		<!-- <dt><?php echo __('End Time'); ?></dt>
		<dd>
			<?php echo date("g:i a", strtotime($booking['Booking']['end_time'])); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Feedback'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['feedback']); ?>
			&nbsp;
		</dd>
		<dt>&nbsp;</dt>
		<dd class="actions"><?php echo $this->Html->link(__('Back', true), array('action' => 'index')); ?> &nbsp;</dd>
	</dl>
</div>
