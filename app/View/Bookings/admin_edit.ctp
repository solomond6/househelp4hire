<div>
<?php //pr($this->request->data['Booking']); ?>
<?php echo $this->Form->create('Booking'); ?>
	<fieldset>
		<legend><?php echo __('Edit Booking'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->hidden('staff_id',array('empty'=>'choose stuff'));
		//echo 'Staff Name: '.$this->request->data['Booking']['staff_id'];
		echo $this->Form->hidden('user_id',array('empty'=>'choose employer', 'label'=>'Employer'));
		echo $this->Form->hidden('date',array('type'=>'text','id'=>"datepicker"));
		echo '<p>Staff Name: '.$staffs[$this->request->data['Booking']['staff_id']].'</p>';
		echo '<p>Employer Name: '.$users[$this->request->data['Booking']['user_id']].'</p>';
		echo '<p>Booking Date: '.$this->request->data['Booking']['date'].'</p><br/>';

		$interviewTImeS = strtotime($this->request->data['Booking']['date'].' '.$this->request->data['Booking']['start_time']);
		$curDateS = strtotime(date("Y-m-d h:i:s"));
		$timeDiffS = $interviewTImeS - $curDateS;
		if($this->request->data['Booking']['status'] == 'Pending' && ($timeDiffS < 0)){
			echo $this->Form->input('status', array('type' => 'select', 'options' => array('Pending' => 'Pending', 'Cancelled'=>'Cancelled', 'Interviewed' => 'Interviewed', 'Absent' => 'Absent')));
		} else if ($this->request->data['Booking']['status'] == 'Pending'){
			echo $this->Form->input('status', array('type' => 'select', 'options' => array('Pending' => 'Pending', 'Cancelled'=>'Cancelled')));
		} else if ($this->request->data['Booking']['status'] == 'Interviewed'){
			echo $this->Form->input('status', array('type' => 'select', 'options' => array('Interviewed' => 'Interviewed', 'Selected' => 'Selected', 'Rejected' => 'Rejected')));
		} else if ($this->request->data['Booking']['status'] == 'Selected'){
			echo $this->Form->input('status', array('type' => 'select', 'options' => array('Selected' => 'Selected', 'Hired' => 'Hired', 'Released'=>'Released')));
		} else if ($this->request->data['Booking']['status'] == 'Hired'){
			echo $this->Form->input('status', array('type' => 'select', 'options' => array('Hired' => 'Hired', 'Released'=>'Released')));
		} else {
			echo '<p>Booking Status: Released</p><br/>';
		}

		//if($this->request->data['Booking']['status'] == 'Selected'){
			//echo $this->Form->input('status', array('type' => 'select', 'options' => array('Selected' => 'Selected', 'Hired'=>'Hired', 'Released'=>'Released', 'Pending' => 'Pending', 'Cancelled'=>'Cancelled', 'Interviewed' => 'Interviewed', 'Absent' => 'Absent', 'Rejected' => 'Rejected')));
		//}
		echo $this->Form->input('location',array('type'=>'text'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script>
	jQuery(function() {
		jQuery( "#datepicker" ).datepicker({ 
			minDate: "+1D",
			maxDate: "+2M" ,
			dateFormat: "yy-mm-dd"
		});
	});
	</script>
