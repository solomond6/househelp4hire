<div>
<?php echo $this->Form->create('Booking'); ?>
	<fieldset>
		<legend><?php echo __('Add Booking'); ?></legend>
	<?php
		echo $this->Form->input('staff_id',array('empty'=>'choose stuff'));
		echo $this->Form->input('user_id',array('empty'=>'choose employer','label'=>'Employer'));
		echo $this->Form->input('date',array('type'=>'text','id'=>"datepicker"));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script>
	jQuery(function() {
		jQuery( "#datepicker" ).datepicker({ minDate: "+1D", maxDate: "+2M" ,dateFormat: "yy-mm-dd" });
	});
	</script>
