<script type="text/javascript">
/*
	$(document).ready(function() {
		$("#FaqCategoryAdminEditForm").validationEngine({scroll:true})
	});
*/
</script>

<div>
<?php echo $this->Form->create('NewsCategory', array('enctype' => 'multipart/form-data'));?>
	<fieldset>
 		<legend><?php echo __('Edit News Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('label' => 'Category Name'));
		echo $this->Form->input('status', array('type' => 'select', 'options' => array('1' => 'Active', '0' => 'Inactive')));
		?>
	</fieldset>
<?php echo $this->Form->end(__('Update', true));?>
</div>