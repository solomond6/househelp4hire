<script type="text/javascript">
/*
	$(document).ready(function() {
		$("#FaqCategoryAdminAddForm").validationEngine({scroll:true})
	});
*/
</script>
<div>
<?php echo $this->Form->create('NewsCategory', array('enctype' => 'multipart/form-data'));?>
	<fieldset>
 		<legend><?php echo __('Add New News Category'); ?></legend>
		<?php
		echo $this->Form->input('name', array('label' => 'Category Name'));
		echo $this->Form->input('status', array('type' => 'select', 'options' => array('1' => 'Active', '0' => 'Inactive')));
		?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>