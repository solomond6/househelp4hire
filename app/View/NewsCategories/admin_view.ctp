<div>
<h2>News Category</h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Id</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $newsCategory['NewsCategory']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Name</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $newsCategory['NewsCategory']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Status</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php if($newsCategory['NewsCategory']['status']=='1') 
					{
						 echo 'Active'; 
					}
					else
					{
						 echo 'Inactive'; 
					} ?>
			&nbsp;
		</dd>
		<dt>&nbsp;</dt>
		<dd class="actions"><?php echo $this->Html->link(__('Back', true), array('action' => 'index')); ?> &nbsp;</dd>
	</dl>
</div>