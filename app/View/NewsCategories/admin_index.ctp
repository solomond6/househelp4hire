<div>
	<div style="float:left;"><h2>News Category Manager</h2></div>
	<div style="float:right; padding-bottom:5px; color:#003D4C;"><?php echo $this->Html->link(__('(Add New News Category)', true), array('action' => 'add'), array('style'=>'color:#003D4C;')); ?></div> 
	<div style="clear: both"></div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th width="10%"><?php echo __('SL#');?></th>
			<th width="50%"><?php echo $this->Paginator->sort('name');?></th>
			<th width="20%" style="text-align:center;"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($newsCategories as $newsCategory):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>		

		<td><?php echo $i; ?>&nbsp;</td>
		<td><?php echo $newsCategory['NewsCategory']['name']; ?>&nbsp;</td>
		<!-- <td><?php echo ($faqCategory['FaqCategory']['status']==1 ? 'Active' : 'Inactive'); ?>&nbsp;</td>	 -->	
		<td class="actions">
			
			<?php 
			if($newsCategory['NewsCategory']['status'] == 1 )
			{
				echo $this->Html->link(__('Active', true), array('action' => 'deactivate', $newsCategory['NewsCategory']['id']), array('title' => 'Click here to deactivate'));
			} 
			else
			{
				echo $this->Html->link(__('Inactive', true), array('action' => 'activate', $newsCategory['NewsCategory']['id']), array('title' => 'Click here to activate'));
			} ?>
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $newsCategory['NewsCategory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $newsCategory['NewsCategory']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $newsCategory['NewsCategory']['id']),array('class' => 'ask'), null, sprintf(__('Are you sure you want to delete # %s?', true), $newsCategory['NewsCategory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array('format' => __("Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%")));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('Previous', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
		<?php echo $this->Paginator->numbers(array('separator'=>'')); ?>
		<?php echo $this->Paginator->next('Next', array('escape'=>false), null, array('escape'=>false, 'class'=>'disabled')); ?>
	</div>
</div>