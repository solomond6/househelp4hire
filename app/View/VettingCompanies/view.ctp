<div class="vettingCompanies view">
<h2><?php  echo __('Vetting Company'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vettingCompany['VettingCompany']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($vettingCompany['VettingCompany']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vetting Categories'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vettingCompany['VettingCategories']['name'], array('controller' => 'vetting_categories', 'action' => 'view', $vettingCompany['VettingCategories']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($vettingCompany['VettingCompany']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created At'); ?></dt>
		<dd>
			<?php echo h($vettingCompany['VettingCompany']['created_at']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vetting Company'), array('action' => 'edit', $vettingCompany['VettingCompany']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vetting Company'), array('action' => 'delete', $vettingCompany['VettingCompany']['id']), null, __('Are you sure you want to delete # %s?', $vettingCompany['VettingCompany']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetting Companies'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Company'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetting Categories'), array('controller' => 'vetting_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Categories'), array('controller' => 'vetting_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
