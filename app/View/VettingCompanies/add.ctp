<div class="vettingCompanies form">
<?php echo $this->Form->create('VettingCompany'); ?>
	<fieldset>
		<legend><?php echo __('Add Vetting Company'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('vetting_categories_id');
		echo $this->Form->input('price');
		echo $this->Form->input('created_at');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Vetting Companies'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Vetting Categories'), array('controller' => 'vetting_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Categories'), array('controller' => 'vetting_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
