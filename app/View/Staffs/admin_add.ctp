<?php echo $this->Html->Script(array('tiny_mce/tiny_mce')); ?>
<script type="text/javascript">
	tinyMCE.init({
		theme : "advanced",
		relative_urls : false,
		mode : "textareas",
			// mode : "exact",
		editor_selector : "pcontent",
		width : "340",
		convert_urls : false,
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,help",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,|,forecolor,backcolor,|,fullscreen,preview,|,ltr,rtl",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
	});
</script>
<style type="text/css">
form .input {color: #111111;margin: 5px 0px 5px 5px;background: #eee;width: 47%;float: left;padding: 6px;}
select{ width:100%!important;  font-size:106%; padding:3px 0px;}
input{padding: 3px 5px; width: 95%;}
#StaffSpokenLanguages{width:30%; height:87px;}
#StaffPreferedEducation{ height:131px;}
.textarea{ width:97%!important; clear:both;}
fieldset{ padding:16px 9px!important;}
#StaffPreferedLocalion{height:87px;}
#StaffPreferedLocality{height:87px;}
@-moz-document url-prefix() {select { font-size: 96%; } }
</style>


     <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Manage Staff</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-user-plus"></i> Add Staff</h4>
                                </div> 
                                <?php echo $this->Form->create('Staff', array('enctype' => 'multipart/form-data')); ?>
                                <div class="widget-body">
                               <div class="row-fluid">
                                 
                                  	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('contact_number',array('type'=>'text','label'=>'Mobile Number 1','maxlength'=>'11'));
		echo $this->Form->input('mobile_number',array('type'=>'text','label'=>'Mobile Number 2','maxlength'=>'11'));
		echo $this->Form->input('age',array('type'=>'text'));
		echo $this->Form->input('sex', array('type' => 'select', 'options' => array('1' => 'Male', '2' => 'Female')));
		echo $this->Form->input('country_id',array('selected'=>'31'));
		
		//echo $this->Form->input('mini_biography', array('id' => 'pagecontent2', 'class'=>'pcontent'));
		echo $this->Form->input('experience',array('label'=>'Experience(years)','type'=>'text'));
		
		
		echo $this->Form->input('marital_status', array('type' => 'select', 'options' => array('2' => 'Single','1' => 'Married')));
		echo $this->Form->input('number_of_children', array('label'=>'Children','type' => 'select', 'options' => array('0'=>'0','1' => '1', '2' => '2','3' => '3','4' => '4','5' => '5+')));
		echo $this->Form->input('prefered_location', array('type' => 'select', 'options' => $locations,'multiple'=>true,'style'=>'width:30%'));
		echo $this->Form->input('prefered_locality', array('label'=>'Residing Area', 'type' => 'select', 'options' => $localities,'multiple'=>true,'style'=>'width:30%'));
		echo $this->Form->input('prefered_education', array('label'=>'Education','type' => 'select', 'options' => $educations,'multiple'=>true,'style'=>'width:30%'));
		echo $this->Form->input('origin_id', array('label'=>'State of Origin'));
		echo $this->Form->input('category_id', array('label'=>'Category <span style="color:red">*</span>','type' => 'select', 'multiple'=>true,'style'=>'width:30%','default'=>1));
		//echo $this->Form->input('english_speaking', array('type' => 'select', 'options' => array('1' => 'Yes', '0' => 'No')));
		echo $this->Form->input('english_speaking', array('style'=>'display:none','label'=>'','type' => 'select', 'options' => array('1' => 'Yes', '0' => 'No')));
		//echo $this->Form->input('language_id', array('label'=>'Mother Language'));
		echo $this->Form->input('language_id', array('label'=>'','style'=>'display:none'));
		echo $this->Form->input('spoken_languages', array('type' => 'select', 'options' => $languages, 'multiple'=>true, 'style'=>'width:30%'));
		echo $this->Form->input('religion_id');
		echo $this->Form->input('base_salary',array('type'=>'text', 'label'=>'Current Salary'));
		echo $this->Form->input('max_salary',array('type'=>'text', 'label'=>'Expected Salary'));
		echo $this->Form->input('accommodation', array('type' => 'select', 'options' => array('1' => 'Live In', '2' => 'Live Out','3'=>'Flexible')));
		echo $this->Form->input('image1',array('type'=>'file'));
		echo $this->Form->input('image2',array('type'=>'file'));
		echo $this->Form->input('status', array('type' => 'select', 'options' => array('0' => 'Inactive', '1' => 'Active')));
		echo $this->Form->input('hired', array('type' => 'select', 'options' => array('0' => 'Available', '1' => 'Hired')));
		echo $this->Form->input('agent_id', array('type' => 'select', 'options' => $plans_list, 'multiple'=>false, 'style'=>'width:30%'));
		
		echo $this->Form->input('immigration_status', array('type' => 'select', 'options' => $immigration_list, 'multiple'=>false, 'style'=>'width:30%'));
		echo $this->Form->input('nationality', array('type' => 'select', 'options' => $nationality_list, 'multiple'=>false, 'style'=>'width:30%'));

		echo $this->Form->input('profile_description', array('id' => 'pagecontent1', 'class'=>'pcontent', 'label'=>'About'));

		echo $this->Form->input('work_experience', array('id' => 'pagecontent3', 'class'=>'pcontent'));
		
		echo $this->Form->input('hobbies', array('label'=>'Hobbies & Interests','id' => 'pagecontent4', 'class'=>'pcontent'));
	?>
	</div>
									<div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-lg">Submit</button>
                                    </div>
                                   
                                    
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
