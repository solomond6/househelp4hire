	<?php 
	if(count($staffs)>0){ ?>
		<div>
		<?php
		$i=1;
		$j=1;
		foreach($staffs as $staff){
			$img1=$staff['Staff']['id']."img1";
			$img2=$staff['Staff']['id']."img2";
			if(strlen($staff['Staff']['name'])>25){
				$name = substr(strip_tags($staff['Staff']['name']),0,25).'...';
			}else{
				$name = strip_tags($staff['Staff']['name']);
			}
			if(strlen($staff['Staff']['mini_biography'])>75){
				$content = substr(strip_tags($staff['Staff']['mini_biography']),0,75).'...';
			}else{
				$content = strip_tags($staff['Staff']['mini_biography']);
			}
			
			$hired=$staff['Staff']['hired'];
		?>
			<!--/.col-md-3 //previous .col-md-4-->
                        <div class="col-md-12 col-sm-12 col-xs-12 port-item padding box-60">
                        	<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
								<div style="float:left; width:150px;height:150px; margin-right:20px">
									<?php if($hired=="") { ?>
										<div class="mini-hired-tag"><span>Hired</span></div>
									<?php } ?>
	                                <a class="box-40 hovers bwWrapper prf" href="<?php echo DOMAIN_NAME_PATH.$staff['Staff']['friendly_url']?>">
	                                	<img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image2']; ?>" alt=""  id="<?php echo $img1; ?>" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" class="bottom"/>	
	                                    <img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image1']; ?>" alt="" id="<?php echo $img2; ?>" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" class="top"/>
	                                </a>
	                            </div>
                            	<div>
	                                <h3 class="no-margin">
	                                	<?php 
	                                		$name = explode(" ", $name);
	                                		echo $name[0]; 
	                                	?>
	                                </h3>
	                                <span class="desc">
	                                <?php
	                                	if (strlen($staff['Staff']['profile_description']) > 150) {
										    // truncate string
										    $descCut = substr($staff['Staff']['profile_description'], 0, 150);
										    // make sure it ends in a word so assassinate doesn't become ass...
										    $desc = substr($descCut, 0, strrpos($descCut, ' ')).'...'; 
										    echo $desc;
										}else{
											echo $staff['Staff']['profile_description'];
										}
									?>
									</span>
									<br/>
	                                <?php 
	                                echo $this->Html->link('Click to view profile', DOMAIN_NAME_PATH.$staff['Staff']['friendly_url'], array('class'=>'btn btn-danger btn-sm'));	
	                                ?>
	                            </div>
	                        </div>
                            <div class="col-md-3 col-sm-3 col-xs-12" style="font-size: 16px;">
                            	<span style="color: #333;"><i class="fa fa-calendar"></i> Age : </span><?php echo $staff['Staff']['age'];?><br>
                                <span style="color: #333;"><i class="fa fa-globe"></i> State of Origin : </span><br/><?php echo $staff['Origin']['name']; ?><br>
                                <span style="color: #333;"><i class="fa fa-globe"></i> Nationality : </span><br/><?php echo $staff['Staff']['nationality']; ?>
                            </div>
                        </div>
		<?php
				if($i==8){
					$i=1;
					echo '<div class="loadmore">
								  <div class="row">
								    <div class="col-md-12 text-center mrgn-btm-47">
								      <a href="javascript:void(0);" onclick="loadmore();" class="btn btn-primary btn-lg br-none mrgn-btm-47">Load More</a>										<div class="bounce"><i class="fa fa-arrow-down"></i></div>
								      </div>
								   </div>
							</div>';
				}
				$i++;
			} 
		?>
		</div>
	<?php } else { echo '<p>Sorry, nothing was found that matches your criteria</p>'; } ?>