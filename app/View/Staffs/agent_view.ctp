<style>
#image {
/* the image you want to 'watermark' */
height: 200px; /* or whatever, equal to the image you want 'watermarked' */
width: 200px; /* as above */
background-position: 0 0;
background-repeat: no-repeat;
position: relative;
}

#image h3{
    position: absolute;
    top: 30px;
    width: 130px;
    color: #fff;
    font-size: 15px;
    font-weight: bold;
    left: 10px;
    background: rgba(0,0,0,0.6);
    z-index: 10;
    padding: 10px;
    text-align: center;
}
#image img {
/* the actual 'watermark' */
position: absolute;
top: 0; /* or whatever */
left: 0; /* or whatever, position according to taste */
opacity: 0.7; /* Firefox, Chrome, Safari, Opera, IE >= 9 (preview) */
filter:alpha(opacity=90); /* for <= IE 8 */
}
</style>
    <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Candidate Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i> View Candidate</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                              
                                                    <div class="control-group">
                                                        <div class="row-fluid" style="margin-bottom: 20px;">
                                                            <div class="span6" style="width: 170px;">
                                                            <label>Image 1</label>
                                                            <?php if($staff['Staff']['status']==0){?>
                                                                <div id="image">
                                                                    <h3>Not Verified</h3>
                                                                    <?php echo $this->Html->image(PAGE_IMAGES_URL.$staff['Staff']['image1'], array('title'=>"image", 'alt'=>'image1', 'width'=>'150','style'=>'display: block;float: left;position: relative;padding-right:70px;margin-left:10px;')); ?>
                                                                </div>
                                                            <?php }else{ ?>
                                                                <?php echo $this->Html->image(PAGE_IMAGES_URL.$staff['Staff']['image1'], array('title'=>"image", 'alt'=>'image1', 'width'=>'150','style'=>'display: block;float: left;position: relative;padding-right:70px;margin-left:10px;')); ?>
                                                            <?php } ?>
                                                            </div>
                                                            <div class="span6">
                                                            <label>Image 2</label>
                                                            <?php if($staff['Staff']['status']==0){?>
                                                                <div id="image">
                                                                    <h3>Not Verified</h3>
                                                                    <?php echo $this->Html->image(PAGE_IMAGES_URL.$staff['Staff']['image2'], array('title'=>"image", 'alt'=>'image1', 'width'=>'150','style'=>'display: block;')); ?>
                                                                </div>
                                                                <?php }else{ ?>
                                                                    <?php echo $this->Html->image(PAGE_IMAGES_URL.$staff['Staff']['image2'], array('title'=>"image", 'alt'=>'image1', 'width'=>'150','style'=>'display: block;')); ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <label class="pull-left view-s">ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['id']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Name <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['name']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Unique ID <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $staff['Staff']['staff-uid']==null ? 'N/A' : $staff['Staff']['staff-uid']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Email Address <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['email']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Mobile Number 1<i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['contact_number']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Mobile Number 2 <i class="fa fa-long-arrow-right pull-right"></i></label>
                                                        <span class="pull-left"><?php echo h($staff['Staff']['mobile_number']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Age <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['age']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Sex <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if($staff['Staff']['sex']==1)
					{
						echo 'Male';
					}
					else
					{
						echo 'Female';
					}
			?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Country <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $this->Html->link($staff['Country']['name'], array('controller' => 'countries', 'action' => 'view', $staff['Country']['id'])); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">About <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $staff['Staff']['profile_description']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Hobbies & Interests <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $staff['Staff']['hobbies']; ?></span><br>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="control-group" style="font-size: 14px;!important">
                                                        <h4>Mini Biography </h4>
                                                        <div style="clear: both;"><?php echo $staff['Staff']['mini_biography']; ?></div>
                                                        <label class="pull-left view-s">Experience[Years] <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $staff['Staff']['experience']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Work Experience <i class="fa fa-long-arrow-right pull-right"></i></label>
                                                        <span class="pull-left">
                                                   <?php echo $staff['Staff']['work_experience']; ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Marital Status <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if($staff['Staff']['marital_status']==1)
					{
						echo 'Married';
					}
					else
					{
						echo 'Single';
					}
			?>	</span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Prefrered Location <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if(isset($PreferedLocation)){echo $PreferedLocation;} else {echo 'N/A';} ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Residing Area <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if(isset($PreferedLocality)){echo $PreferedLocality;} else {echo 'N/A';} ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Spoken Language <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if(isset($PreferedLanguage)){echo $PreferedLanguage;} else {echo 'N/A';} ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Education <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if(isset($PreferedEducation)){echo $PreferedEducation;} else {echo 'N/A';} ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">State of origin <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $this->Html->link($staff['Origin']['name'], array('controller' => 'origins', 'action' => 'view', $staff['Origin']['id'])); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Category<i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php 

			if(isset($PreferedCategory)){echo $PreferedCategory;} else {echo 'N/A';}
			?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">English Speaking <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if($staff['Staff']['english_speaking']==1)
					{
						echo 'Yes';
					}
					else
					{
						echo 'No';
					}
			?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Religion <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo $this->Html->link($staff['Religion']['name'], array('controller' => 'religion', 'action' => 'view', $staff['Religion']['id'])); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Current Salary <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['base_salary']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Expected Salary <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['max_salary']); ?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Accomodation <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if($staff['Staff']['accommodation']==1)
					{
						echo 'Live In';
					}
					else if($staff['Staff']['accommodation']==2)
					{
						echo 'Live Out';
					}
					else 
					{
						echo 'Flexible';
					}
			?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Status <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php if($staff['Staff']['status']==1)
					{
						echo 'Active';
					}
					else
					{
						echo 'inactive';
					}
			?></span><br>
                                                        <div style="clear: both;"></div>
                                                        <label class="pull-left view-s">Reg Date <i class="fa fa-long-arrow-right pull-right"></i></label><span class="pull-left"><?php echo h($staff['Staff']['reg_date']); ?></span><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $staff['Staff']['id']),array('class'=>'btn btn-success')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
