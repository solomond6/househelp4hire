 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Staff Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Staff Monitoring</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                  <div class="row-fluid">
                                  	<div class="controls">
                                  		<?php echo '<span class="btn btn-danger"> Monitor Staffs</span>';?> &nbsp; <?php echo $this->Html->link('Staff View By Users', array('controller' => 'staffs','action' => 'admin_userview'), array('class' => 'btn btn-success'));?>
                                  		
                                  	</div>
                                  </div>
                                  <div class="row-fluid">
                                  	<br/>
                                  </div>
                                    <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
                                        <tr>
                                            	<th><?php echo $this->Paginator->sort('id', 'UID'); ?></th>
												<th><?php echo $this->Paginator->sort('name'); ?></th>
												<th><?php echo $this->Paginator->sort('email'); ?></th>
												
												<th><?php echo $this->Paginator->sort('hired'); ?></th>
												<th><?php echo $this->Paginator->sort('image1', 'Image'); ?></th>
												<th><?php echo $this->Paginator->sort('reg_date', 'Reg Date'); ?></th>
												<th class="actions"><?php echo __('Actions'); ?></th>
                                        </tr>
                                     </thead>
                                        <tbody>
                                        
                                     <?php foreach ($staffs as $staff): ?>
										<tr>
											
											
											<td><?php echo $staff['Staff']['staff-uid']==null ? 'N/A' : $staff['Staff']['staff-uid']; ?>&nbsp;</td>
											<td><?php echo h($staff['Staff']['name']); ?>&nbsp;</td>
											<td><?php echo h($staff['Staff']['email']); ?>&nbsp;</td>
											
											<td>
												<?php echo $staff['Staff']['hired']=="1" ? "Hired":"Available";?>
											</td>
											<td><?php echo $this->Html->image(PAGE_IMAGES_URL.$staff['Staff']['image1'], array('title'=>"image", 'alt'=>'image', 'width'=>'40')); ?></td>
											<td>
												<?php echo $staff['Staff']['reg_date'];?>
											</td>
											<td class="actions">
												<?php if($staff['Staff']['hired']=="1") { ?>
													<p style="margin-bottom:10px;"><?php echo $this->Html->link(__('Available'), array('action' => 'available', $staff['Staff']['id'])); ?></p>
												<?php } else { ?>
													<p style="margin-bottom:10px;"><?php echo $this->Html->link(__('Hired'), array('action' => 'hired', $staff['Staff']['id'])); ?></p>
												<?php } ?>
											</td>
										</tr>
									<?php endforeach; ?>
	</table>
	

	
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __(' previous '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->