        <div class="container">
             <div class="bor-thank text-center">
             	<h2>That's all, Thnak you!</h2>
                <div class="bor-thank-con">
					<?php if(isset($szCrossReference) && count($order)>0){ ?>
					<p>Thank you for your payment with us.  We appreciate your initiative. </p>
					<p>Your payment number is <span style="color:#0AB1F0;font-weight:bold;"><?php echo($order['Payment']['id']); ?></span> and transaction number is <span style="color:#0AB1F0;font-weight:bold;"><?php echo $order['Payment']['txn-id']; ?></span>. Please go to <?php echo $this->Html->link('My Account', DOMAIN_NAME_PATH.'my-account', array('style'=>'color: #ED217C;font-weight:bold;')); ?> page to view your order details.</p>
					<?php }	else { ?>
					<p><?php echo $Response; ?></p>
					<?php } ?>
                </div>
                <p class="pad-ad red-col"><strong></strong> </p>
            </div>
        </div>