<?php
$staff_name=explode(' ',$staff['Staff']['name']);
$first_name=$staff_name[0];
?>
<section id="portfolio" class="clearfix content bg-white pad-btm-z">
  <div class="container" style="min-height: 550px;">
      <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb">
               <li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a><span class="divider"></span></li>
              <li><?php echo $this->Html->link('Domestic Staff', array('controller' =>  $this->params['controller'], 'action' => 'index')); ?><span class="divider"></span></li>
              <li class="active"><?php echo $staff['Staff']['name']; ?></li>
            </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-3">
          <div class="pro-big-pic">
            <img class="xyz" data-alt-src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image2']; ?>" src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image1']; ?>" alt="&nbsp;" />
          </div>
          <a href="#" class="btn btn-danger btn-2" data-toggle="modal" class="btn btn-large btn-primary btn-danger">&nbsp; <?php echo $staff['Staff']['name']; ?></a>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
            <h3 style="height:30px;">Select A Category</h3>
            <table class="table table-striped">
              <?php foreach ($vetCategories as $key => $vetCategory) { ?>
                  <tr>
                    <td>
                      <button class="btn btn-danger btn-sm vetCatId" data-attr="<?php echo $vetCategory['hh_vetting_categories']['id']; ?>"><?php echo $vetCategory['hh_vetting_categories']['name']; ?>
                      </button>
                    </td>
                  </tr>
              <?php } ?>
            </table> 
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
          <h4>Select From Each Category</h4>
          <table class="table" style="margin-bottom: 0px;">
            <thead>
              <th class="name">Company</th>
              <th class="price">Price</th>
              <th class="action">Action</th>
            </thead>
          </table>
          <table class="table table-striped vetComapny">
              <tbody>
                <tr></tr>
              </tbody>
          </table>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3">
          <h4>Summary</h4>
          <form id="selectedComData" class="selectedComData">
            <table class="table table-striped selectedComapny">
                <thead>
                    <th class="name">Name</th>
                    <th class="price">Price</th>
                </thead>
                
                <tbody>
                  <tr></tr>
                  <tr style="font-weight: bold;"><td>Total</td><td class="total"></td></tr>
                </tbody>
            </table>
            <input type="hidden" name="totalValue" id="totalValue"/>
            <button class="btn btn-danger btn-sm payNow" style="width: 100%;" data-toggle="modal" data-target="#pay">Pay</button>
          </form>
        </div>
      </div>
  </div>
</section>
<div class="modal fade" id="pay" role="dialog" st>
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="cardDetails">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">x</button>
                    <h2 class="title-mng">Make Payment</h2>
                </div>
                <div class="modal-body plan-outer">
                    <div class="row">
                      <form id="paymentForm" class="inline-form">
                          <div class="form-group col-md-12">
                              <label class="control-label" for="card_no">Card Number:</label>
                              <input type="text" name="card_no" class="form-control card_no" placeholder="Enter Your Card No" required="required">
                              <input type="hidden" name="amount" class="amount" required="required">
                              <input type="hidden" name="staff_id" class="staff_id"  value="<?php echo $staff['Staff']['id']; ?>" required="required">
                              <input type="hidden" name="comIdcom" class="comIdcom" required="required">
                              <input type="hidden" name="comCatcom" class="comCatcom" required="required">
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="control-label" for="expiry_month">Expiry Month:</label>
                                  <select name="expiry_month" class="form-control expiry_month" required="required">
                                      <option value="">Expiry Month On Card</option>
                                      <option value="01">January</option>
                                      <option value="02">Febraury</option>
                                      <option value="03">March</option>
                                      <option value="04">April</option>
                                      <option value="05">May</option>
                                      <option value="06">June</option>
                                      <option value="07">July</option>
                                      <option value="08">August</option>
                                      <option value="09">September</option>
                                      <option value="10">October</option>
                                      <option value="11">November</option>
                                      <option value="12">December</option>
                                  </select>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="control-label" for="expiry_year">Expiry Year:</label>
                                  <select name="expiry_year" class="form-control expiry_year" required="required">
                                      <option value="">Expiry Year On Card</option>
                                      <option value="17">2017</option>
                                      <option value="18">2018</option>
                                      <option value="19">2019</option>
                                      <option value="20">2020</option>
                                      <option value="21">2021</option>
                                      <option value="22">2022</option>
                                      <option value="23">2023</option>
                                      <option value="24">2024</option>
                                      <option value="25">2025</option>
                                      <option value="26">2026</option>
                                      <option value="27">2027</option>
                                      <option value="28">2028</option>
                                      <option value="29">2029</option>
                                      <option value="30">2030</option>
                                      <option value="31">2031</option>
                                      <option value="32">2032</option>
                                      <option value="33">2033</option>
                                      <option value="34">2034</option>
                                      <option value="35">2035</option>
                                      <option value="36">2036</option>
                                      <option value="37">2037</option>
                                      <option value="38">2038</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group col-md-6">
                              <label class="control-label" for="cvv">Card CVV:</label>
                              <input type="text" name="cvv" class="form-control cvv" placeholder="Enter your card CVV" required="required">
                          </div>
                          <div class="form-group col-md-6">
                              <label class="control-label" for="pin">Pin:</label>
                              <input type="text" name="pin" class="form-control pin" placeholder="Expiry Year" required="required">
                          </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="processing" id="processing">
                                    <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')) ?>
                                    Processing Payment...Pls wait
                                </div>
                                <button type="button" class="btn btn-primary paymentbutton"  style="margin-top: 0px; width:100%;">Pay</button>
                                   <!-- <input type="submit" class="btn btn-primary btn-md pull-right" style="margin-top: 0px; width:100%;" value="Proceed to Payment"> -->
                            </div>
                      </form>
                      <div class="row" style="margin-top: 20px;">
                          <div class="col-sm-12 col-md-12 text-center">
                              <img src="<?php echo DOMAIN_NAME_PATH; ?>images/transfer-icon.png" alt="Payment Mode">
                          </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="otpDetails">
                <div class="modal-header" style="height:40px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                    <h4 class="modal-title">Enter OTP Sent to your phone</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <form id="otpForm" class="inline-form">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label" for="card_no">OTP:</label>
                                    <input type="text" name="otp" class="form-control otp" placeholder="Enter OTP Sent to your phone" required="required">
                                    <input type="hidden" name="ref" class="form-control ref" required="required">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="processing1" id="processing1">
                            <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')) ?>
                            Processing...Pls wait
                        </div>
                        <button type="button" class="btn btn-warning otpValidate">Validate OTP</button>
                    </div>
                </div>
            </div>
            <div class="failedDetails">
                <div class="modal-header" style="height:40px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                    <h4 class="modal-title">Failed Transaction</h4>
                </div>
                <div class="modal-body">
                    <h3>Your Payment Failed, Please try again</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
                </div>
            </div>
            <div class="successDetails">
                <div class="modal-header" style="height:30px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.load('my-account')">&times;</button>
                    <h4 class="modal-title">Successful Transaction</h4>
                </div>
                <div class="modal-body">
                    <h3>You have successfully paid for verification for this candidate</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick='javascript:window.location.href = "<?php echo DOMAIN_NAME_PATH; ?>my-account"'>Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/additional-methods.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery('.cardDetails').show();
  jQuery("#processing").hide();
  jQuery("#processing1").hide();
  jQuery('.otpDetails').hide();
  jQuery(".failedDetails").hide();
  jQuery(".successDetails").hide();
  jQuery('.modal-backdrop').remove();
  jQuery('.payNow').hide();
  jQuery(document).on('click', '.vetCatId', function(){
      var vetCatId = jQuery(this).attr("data-attr");
      jQuery(this).prop('disabled',true);
      jQuery('.vetComapny').find('tr').empty();
      jQuery.ajax({
          type: "POST",
          url: "<?php echo DOMAIN_NAME_PATH.'Staffs/vettinCompanies/'; ?>"+vetCatId,
          success: function(data){
              if(data){
                  var opts = jQuery.parseJSON(data);
                  if(opts.length >= 1){
                    jQuery.each(opts, function(i, d){ 
                        tr = jQuery('<tr/>');
                        tr.append(jQuery('<td/>').html(d.hh_vetting_companies.name));
                        tr.append(jQuery('<td/>').html('NGN ' +d.hh_vetting_companies.price));
                        tr.append(jQuery('<td/>').html('<button class="btn btn-danger selectedVetCom" data-comName="'+d.hh_vetting_companies.name+'" data-price="'+d.hh_vetting_companies.price+'" data-comid="'+d.hh_vetting_companies.id+'" data-comcat="'+d.hh_vetting_companies.vetting_category_id+'" >Select</button>'));
                        jQuery('.vetComapny tr:last').after(tr);
                    });
                  }else{
                      tr = jQuery('<tr/>');
                      tr.append(jQuery('<td/>').html('No Company found In this category!'));
                      jQuery('.vetComapny tr:first').after(tr);
                  }
              }else{
                  tr = jQuery('<tr/>');
                  tr.append(jQuery('<td/>').html('No Company found In this category!'));
                  jQuery('.vetComapny tr:last').after(tr);
              }
          }
      });
  });

  jQuery(document).on('click','.selectedVetCom', function(){
      var comName = jQuery(this).attr("data-comName");
      var comPrice = jQuery(this).attr("data-price");
      var comId = jQuery(this).attr("data-comid");
      var comCat = jQuery(this).attr("data-comcat");
      jQuery(this).prop('disabled',true);
      tr = jQuery('<tr/>');
      tr.append(jQuery('<td/>').html(comName + '<input type="hidden" name="comId" class="comId" value="'+comId+'"/><input type="hidden" name="comCat" class="comCat" value="'+comCat+'"/>'));
      tr.append(jQuery('<td class="comPrice"/>').html(comPrice));
      var sum = parseFloat(comPrice);
      jQuery('tr').find('.comPrice').each(function(){
        var comPr = jQuery(this).text();
        sum += parseFloat(comPr);
      });
      jQuery('.total').html('NGN '+sum);
      jQuery('#totalValue').val(sum);
      if(sum >= 0){
        jQuery('.payNow').show();    
      }
      jQuery('.selectedComapny tr:first').after(tr);
  });

  jQuery(document).on('click', '.payNow', function(){
    var comIds = jQuery('[class^=comId]').map(function () {
        return jQuery(this).val();
    }).get().join(',').replace(/,(?!.*,)/gmi, '');

    var comCats = jQuery('[class^=comCat]').map(function () {
        return jQuery(this).val();
    }).get().join(',').replace(/,(?!.*,)/gmi, '');
    
    var totalSum = jQuery('#totalValue').val();
    jQuery('.comIdcom').val(comIds);
    jQuery('.amount').val(totalSum);
    jQuery('.comCatcom').val(comCats);
  });

  jQuery("#paymentForm").validate({
      rules: {
          card_no: {
            required: true,
            number: true,
            minlength: 16,
            maxlength: 16
          },
          cvv: {
            required: true,
            number: true,
            minlength: 3,
            maxlength: 3
          },
          expiry_month: {
            required: true,
            number: true,
          },
          expiry_year: {
            required: true,
            number: true,
          },
          pin: {
            required: true,
            number: true,
            minlength: 4,
            maxlength: 4
          }
      },
      messages: {
          card_no: {
            required: "Card Number Required",
            minlength: "At least 16 characters required!"
          },
          cvv: {
            required: "Card CVV Required",
            minlength: "At least 3 characters required!"
          },
          expiry_month: {
            required: "Expiry Month Required",
            minlength: "At least 2 characters required!"
          },
          expiry_year: {
            required: "Expiry Year Required",
            minlength: "At least 2 characters required!"
          },
          pin: {
            required: "Pin Required",
            minlength: "At least 4 characters required!"
          }
      },
      highlight: function(element){
          jQuery(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element){
          jQuery(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
  });
  jQuery("#otpForm").validate({
      rules: {
          otp: {
            required: true,
            number: true,
            minlength: 4,
            maxlength: 16
          }
      },
      messages: {
          otp: {
            required: "OTP Required",
            minlength: "At least 4 characters required!"
          }
      },
      highlight: function(element){
          jQuery(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element){
          jQuery(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
  });

  jQuery(document).on('click', '.paymentbutton', function(e){
      var amount = jQuery('.amount').val();
      var comIdcom = jQuery('.comIdcom').val();
      var comCatcom = jQuery('.comCatcom').val();
      var staff_id = jQuery('.staff_id').val();
      var card_no = jQuery('.card_no').val();
      var cvv = jQuery('.cvv').val();
      var expiry_month = jQuery('.expiry_month').val();
      var expiry_year = jQuery('.expiry_year').val();
      if(jQuery("#paymentForm").valid()){
          jQuery("#processing").show();
          jQuery.ajax({
              type: "POST",
              url: "<?php echo DOMAIN_NAME_PATH.'Staffs/vetPayment/'; ?>",
              data: jQuery("#paymentForm").serialize(),
              complete: function(){
                  jQuery("#processing").hide();
              },
              success: function(data){
                  var obj = jQuery.parseJSON(data);
                  if(obj.status = 'success'){
                      jQuery('.cardDetails').hide();
                      jQuery(".otpDetails").show();
                      jQuery(".failedDetails").hide();
                      jQuery("#processing").hide();
                      jQuery(".successDetails").hide();
                      jQuery('.modal-backdrop').remove();
                      jQuery('.ref').val(obj.data.transactionreference);
                  }else{
                      jQuery('.selectVetCom').hide();
                      jQuery('.cardDetails').hide();
                      jQuery(".otpDetails").hide();
                     jQuery(".failedDetails").show();
                     jQuery("#processing").hide();
                     jQuery("#processing").hide();
                     jQuery(".successDetails").hide();
                     jQuery('.modal-backdrop').remove();
                  }
              }
          });  
      }else{
          return false;
      }
        //e.preventDefault();
    });

    jQuery(document).on('click', '.otpValidate', function(e){
        var amount = jQuery('.otp').val();
        var ref = jQuery('.ref').val();
        if(jQuery("#otpForm").valid()){
            jQuery("#processing1").show();
            jQuery.ajax({
                type: "POST",
                url: "<?php echo DOMAIN_NAME_PATH.'Staffs/validateOtp2/'; ?>",
                data: jQuery("#otpForm").serialize(),
                complete: function(){
                    jQuery("#processing1").hide();
                },
                success: function(data){
                    if(data == 'Failed'){
                        jQuery('.selectVetCom').hide();
                        jQuery('.cardDetails').hide();
                        jQuery(".otpDetails").hide();
                        jQuery(".failedDetails").show();
                        jQuery(".successDetails").hide();
                        jQuery("#processing1").hide();
                        jQuery('.modal-backdrop').remove();
                    }else if(data == 'Successful'){
                        jQuery('.selectVetCom').hide();
                        jQuery('.cardDetails').hide();
                        jQuery(".otpDetails").hide();
                        jQuery(".failedDetails").hide();
                        jQuery("#processing1").hide();
                        jQuery(".successDetails").show();
                        jQuery('.modal-backdrop').remove();
                    }
                }
            });  
        }else{
            return false;
        }
        //e.preventDefault();
    });
})
</script>