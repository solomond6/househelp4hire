  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">My Candidate</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Manage Candidate</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="span6" style="margin-bottom: 25px; border: 1px solid #ddd; border-radius: 3px; padding: 15px;"><?php echo $this->Form->Create('Staff', array('action' => 'index'));?>
                                           <h4 style="margin: 0px 0px 15px 0px; font-size: 15px; color: #d9544f; font-weight: 700;  padding: 0px;"><i class="fa fa-search"></i> Search Candidate</h4> 
                                            <label>Email / UID / Category / Name / Registration Date [yyyy-mm-dd]</label>
                                            <div class="controls">
                                            
                                             <?php echo $this->Form->input('key', array('label' => false,'class'=>'span12', 'div' => false, 'value' => isset($this->request->data['Staff']['key']) ? $this->request->data['Staff']['key'] : '')) ?></div>
                                            <input type="submit" class="btn btn-success" value="Search">
                                            <?php echo $this->Form->end();?>
                                        </div>
                                        <div class="span6" style="margin-bottom: 25px;">
                                            <a href="staffs/add" class="btn btn-primary pull-right"><i class="fa fa-user-plus"></i> Add New Candidate</a>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        
                                    </div>
                                    <?php echo $this->Form->Create('Staff', array('action' => 'sendmail'));?>
                                    <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
	                                        <tr>
	                                            <th style='text-align:center;'><?php echo __('<input type="checkbox" name="user_email" value="-1"/>',array('class'=>'group-checkable')); ?></th>
												<th><?php echo $this->Paginator->sort('id', 'UID'); ?></th>
												<th><?php echo $this->Paginator->sort('name'); ?></th>
												<th><?php echo $this->Paginator->sort('contact_number'); ?></th>
												
												<th><?php echo $this->Paginator->sort('hired'); ?></th>
												<th><?php echo $this->Paginator->sort('status'); ?></th>
												<th class="actions"><?php echo __('Actions'); ?></th>
	                                        </tr>
                                     	</thead>
                                        <tbody>
                                  	<?php foreach ($staffs as $staff): ?>
                                  	<tr>
										<td>
										<input type="checkbox" class="checkboxes"  name="data[Staff][user_email][]" value="<?php echo $staff['Staff']['id'] ?>" /></td>
										<td><?php echo $staff['Staff']['staff-uid']==null ? 'N/A' : $staff['Staff']['staff-uid']; ?>&nbsp;</td>
										<td><?php echo h($staff['Staff']['name']); ?>&nbsp;</td>
										<td><?php echo h($staff['Staff']['contact_number']); ?>&nbsp;</td>
										
										<td>
											<?php echo $staff['Staff']['hired']=="1" ? "Hired":"Available";?>
										</td>
                    <td>
                      <?php echo $staff['Staff']['status']=="1" ? "Verified":"Not Verified";?>
                    </td>
										
										<td class="actions">
											<?php echo $this->Html->link('<i class="fa fa-eye"></i> View', array('action' => 'view', $staff['Staff']['id']), array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'edit', $staff['Staff']['id']),array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<a href="#conDialog" role="button"  class="btn btn-danger" data-toggle="modal" data-attr="<?php echo $staff['Staff']['id']?>" id="deleteConfirm"><i class="fa fa-trash-o" ></i> Delete</a>&nbsp;
										</td>
									</tr>
								<?php endforeach; ?>
                                </tbody>
                                    </table>
									<?php echo $this->Form->submit('Send Email', array('div' => false,'class'=>'btn btn-success'));?>
									<?php echo $this->Form->end();?>
									<p>
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>	
									</p>
									<div class="paging">
									<?php
										echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
										echo $this->Paginator->numbers(array('separator' => ' '));
										echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
									?>
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                 <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			                <h3 id="myModalLabel1">Are you sure ?</h3>
			            </div>
            
			            <div class="modal-footer">
			                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
			                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
			            </div>
        		</div>
<script>
     var record_id="";
     jQuery(document).ready(function(){
     		jQuery("#deleteConfirm").click(function(){
     			var id=jQuery(this).attr("data-attr");
     			record_id=id;
     		})
     })
	
	function Delete()
	{
		console.log("staffs/delete/"+record_id);
		document.location.href="staffs/delete/"+record_id;
	}
</script>