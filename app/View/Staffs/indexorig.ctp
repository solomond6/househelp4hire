<?php
$keyWord = 'false';
$type = 'false';
if(count($category) > 0){
	$url = DOMAIN_NAME_PATH.'Staffs/loadmore/'.$category['Category']['friendly_url'].'/';
} else {
	$url = DOMAIN_NAME_PATH.'Staffs/loadmore/';
	if(!empty($this->request->data)){
		$keyWord =  serialize($this->request->data);
		if (isset($this->request->data['Staff']['search'])) {
			$type = 'headerSearch';
		} else {
			$type = 'advancedSearch';
		}
	}
}
?>
<script type="text/javascript">
	var scrollCall = 1;
	$(window).scroll(function(){
		if($(window).scrollTop() == $(document).height() - $(window).height()){
			if(scrollCall == 1){
				scrollCall = 0;
				if($('#pageCountUp').val() != 0){
					var curPage = $('#pageCountUp').val();
					var searchString = '<?php echo $keyWord; ?>';
					var searchType = '<?php echo $type; ?>';
					var dataString = "page=" + curPage +"& search=" + searchString +"& searchType=" + searchType;
					$('div#loadmoreajaxloader').show();
					$.ajax({
						type: "POST",
						url: "<?php echo $url; ?>",
						data: dataString,
						success: function(html){
							if(html){
								$('#pageCountUp').val(eval(curPage) + 1);
								$("#postswrapper").append($(html).fadeIn('slow'));
								$('div#loadmoreajaxloader').hide();
								scrollCall = 1;
							}else{
								$('#pageCountUp').val(eval(2));
								$('div#loadmoreajaxloader').html('<center>No more domestic staff to display</center>');
							}
						}
					});
				}
			}
		}
	});
</script>
<input type="hidden" name="pageCount" value="2" id="pageCountUp">
<div class="container">
	<ul class="breadcrumb">
		<li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a><span class="divider"> / </span></li> 
		<?php
		if(count($category) > 0){
		echo '<li>'.$this->Html->link('Domestic Staff', array('controller' =>  $this->params['controller'], 'action' => $this->params['action'])).'<span class="divider"> / </span></li>';
		echo '<li class="active">'.$category['Category']['name'].'</li>';
		} else {
		echo '<li class="active">Domestic Staff</li>';
		}
		?>
	</ul>
	<div class="span10 advanced-search-box" style="border:1px solid #FA7D01; padding:5px 13px; margin:0 0 10px 0; background:#ffefc8; display:none;" id="search-sec">
        <div class="span10" id="search-sec-inner" style="opacity: 0.1">
		<?php echo $this->Form->create('Staff', array('action' => 'index')); ?>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('category_id', array('div' => false, 'class' => 'selectpicker', 'empty'=>'Select One')); ?>
            </div>
			<!-- <div class="span2 pull-left">
				<?php echo $this->Form->input('experience', array('div' => false, 'class' => 'input-medium hk-selt')); ?>
            </div> -->
			<!-- <div class="span2 pull-left">
				<?php echo $this->Form->input('experience', array('div' => false, 'class' => 'selectpicker', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$experience)); ?>
            </div>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('age', array('div' => false, 'class' => 'selectpicker', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$age)); ?>
            </div> -->
			<div class="span2 pull-left">
				<?php echo $this->Form->input('experience', array('div' => false, 'class' => 'selectpicker', 'empty'=>'Select One', 'options'=>array(1=>'0-1 Years', 2=>'1-3 Year', 3=>'3-5 Years', 4=>'5-10 Years', 5=>'10+ Years'))); ?>
            </div>
			<!-- <div class="span2 pull-left">
				<?php echo $this->Form->input('age', array('div' => false, 'class' => 'input-medium hk-selt')); ?>
            </div> -->
			<div class="span2 pull-left">
				<?php echo $this->Form->input('age', array('div' => false, 'class' => 'selectpicker', 'empty'=>'Select One', 'options'=>array(1=>'18-25 Years', 2=>'25-30 Years', 3=>'30-35 Years', 4=>'35-40 Years', 5=>'40+ Years'))); ?>
            </div>
            <div class="span2 pull-left">
				<?php echo $this->Form->input('sex', array('div' => false, 'class' => 'selectpicker', 'type' => 'select', 'options' => array('1' => 'Male', '2' => 'Female'), 'empty'=>'Select Sex')); ?>
            </div>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('origin_id', array('label'=>'State of Origin', 'div' => false, 'class' => 'selectpicker', 'empty'=>'Select Origin')); ?>
            </div>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('accommodation', array('div' => false, 'class' => 'selectpicker', 'empty'=>'Select One', 'options' => array('1' => 'Live In', '2' => 'Live Out'))); ?>
            </div>
            <div class="span2 pull-left" style="display:none;">
				<?php echo $this->Form->input('english_speaking', array('div' => false, 'class' => 'selectpicker', 'type' => 'select', 'options' => array('1' => 'Yes', '0' => 'No'), 'empty'=>'Select One')); ?>
            </div>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('spoken_language', array('label'=>'Spoken Language(s)', 'div' => false, 'class' => 'selectpicker', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$languages)); ?>
            </div>
			<!-- <div class="span2 pull-left">
				<?php echo $this->Form->input('prefered_location', array('label'=>'Prefered Location', 'div' => false, 'class' => 'selectpicker', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$locations)); ?>
            </div> -->
			<div class="span2 pull-left">
				<?php echo $this->Form->input('prefered_locality', array('label'=>'Residing LGA', 'div' => false, 'class' => 'selectpicker', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$localities)); ?>
            </div>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('marital_status', array('div' => false, 'class' => 'selectpicker', 'data-selected-text-format'=>'count', 'options' => array('1' => 'Married', '2' => 'Single'), 'empty'=>'Select One')); ?>
            </div>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('religion_id', array('div' => false, 'class' => 'selectpicker', 'empty'=>'Select Religion')); ?>
            </div>
			<div class="span2 pull-left">
				<?php echo $this->Form->input('education', array('label'=>'Education', 'div' => false, 'class' => 'selectpicker', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$educations)); ?>
            </div>
			<!-- <div class="span2 pull-left">
				<?php //echo $this->Form->input('base_salary', array('div' => false, 'class' => 'input-medium hk-selt')); ?>
            </div>
			<div class="span2 pull-left">
				<?php //echo $this->Form->input('max_salary', array('div' => false, 'class' => 'input-medium hk-selt')); ?>
            </div> -->
            <!-- <div class="span2 pull-left">
				<label for="CategoryNameSingular">Children</label>
               <select name="" class="input-medium hk-selt"></select>
            </div>
            <div class="span2 pull-left">
				<label for="CategoryNameSingular">State of Origin</label>
               <select name="" class="input-medium hk-selt"></select>
            </div>
            <div class="span2 pull-left">
				<label for="CategoryNameSingular">Language</label>
                <select class="selectpicker" multiple data-selected-text-format="count">
                <option>Language</option>
                <option>Building</option>
                <option>Bunglow</option>
                </select>
            </div>
            <div class="span3 pull-left">
				<label for="CategoryNameSingular">Staff Category</label>
                <select  class="selectpicker show-tick form-control"  data-live-search="true">
					<option>Jaipur</option>
					<option>Chandigarh</option>
					<option class="get-class" disabled>Jabalpur</option>
					<optgroup label="Delhi" data-subtext="Delhi(NCR)" data-icon="icon-ok">
						<option>Delhi East</option>
						<option selected>Delhi West</option>
						<option>Delhi North</option>
						<option>Delhi South</option>
					</optgroup>
					 <optgroup label="Chennai" data-subtext="Chennai" data-icon="icon-ok">
						<option>Chennai East</option>
						<option >Chennai West</option>
						<option>Chennai North</option>
						<option>Chennai South</option>
					</optgroup>
				</select>
            </div> -->
			<div class="span1 pull-left">
				<label for="CategoryNameSingular">&nbsp;</label>
				<button class="btn btn-warning" type="submit">Search</button>
			</div>
		<?php echo $this->Form->end(); ?>
        </div>                
    </div>
	<div class="row-fluid">
	<div id="postswrapper">
	<?php 
	if(count($staffs)>0){ ?>
		<ul class="thumbnails example-sites">
		<?php
		$i=1;
		foreach($staffs as $staff){
			$img1=$staff['Staff']['id']."img1";
			$img2=$staff['Staff']['id']."img2";
			if(strlen($staff['Staff']['name'])>25){
				$name = substr(strip_tags($staff['Staff']['name']),0,25).'...';
			}else{
				$name = strip_tags($staff['Staff']['name']);
			}
			if(strlen($staff['Staff']['mini_biography'])>75){
				$content = substr(strip_tags($staff['Staff']['mini_biography']),0,75).'...';
			}else{
				$content = strip_tags($staff['Staff']['mini_biography']);
			}
		?>
			<li class="span4 thumbnail" onmouseover="flipImgOver('<?php echo $img1; ?>', '<?php echo $img2; ?>');" onmouseout="flipImgOut('<?php echo $img1; ?>', '<?php echo $img2; ?>');">
				<img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image1']; ?>" alt="" style="display:block;height:247px;" id="<?php echo $img1; ?>" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" />
				<img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image2']; ?>" alt="" style="display:none;height:247px;" id="<?php echo $img2; ?>" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" />
				<h4><?php echo $name; ?></h4>
				<!-- <h2><?php echo $staff['Category']['name']; ?></h2> -->
				<p class="mini-bio"><strong>Age: </strong><?php echo $staff['Staff']['age']; //echo $content; ?><br/><strong>State of Origin: </strong><?php echo $staff['Origin']['name']; ?><br/></p>
				<?php
				if ($this->Session->check('Auth.User')) {
					//echo $this->Html->link('View Profile', array('controller' => 'Staffs', 'action' => 'view',$staff['Staff']['id']), array('class'=>'btn btn-warning btn-primary btn-lg'));
					echo $this->Html->link('View Profile', DOMAIN_NAME_PATH.$staff['Staff']['friendly_url'], array('class'=>'btn btn-warning btn-primary btn-lg'));
					?><!-- <a href="<?php echo DOMAIN_NAME_PATH.$staff['Staff']['friendly_url']; ?>" title="<?php echo $staff['Staff']['friendly_url']?>"><?php echo $staff['Staff']['friendly_url']?></a> --><?php
				}
				else{
				?>
					<a href="#myModal2" role="button" data-toggle="modal" class="btn btn-warning btn-primary btn-lg" id="ViewProfileLogin<?php echo $staff['Staff']['friendly_url']; ?>" onclick="loginRedirect(this.id);">View Profile</a>
				<?php
				}
				?>
			</li>
		<?php
				if($i==3){
					$i=1;
					echo '</ul><ul class="thumbnails example-sites">';
				}
				$i++;
			} 
		?>
		</ul>
		</div>
		<div id="loadmoreajaxloader" class="yalo" style="margin-bottom:25px;font-weight:bold;display:none;"><center><img src="<?php echo DOMAIN_NAME_PATH; ?>img/ajax-loader.gif"></center></div>
	<?php } else {
			if(isset($this->params['pass'][1])){
				//echo '<p>'.$this->params['pass'][1].' Not Found!</p>';
			} else {
				//echo '<p>Staff Not Found!</p>';
			}
			echo '<p>Sorry, nothing was found that matches your criteria</p>';
		}?>
	</div>
</div>
<script type="text/javascript">
$(window).load(function(){
	//document.getElementById("search-sec").style.display = "block";
	//$("#search-sec").fadeIn(1000);
	/*$( "#search-sec" ).slideDown( "slow", function() {
		//Animation complete.
	});*/
	$( "#search-sec" ).animate({
	height: "toggle"
	}, 1000, function() {
		//$("#search-sec-inner").fadeTo(200000, 9.4);;
		$("#search-sec-inner").css('opacity', 0.1).fadeTo(2000, 1.0)
	// Animation complete.
	});
})
</script>