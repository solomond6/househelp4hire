<?php
$staff_name=explode(' ',$staff['Staff']['name']);
$first_name=$staff_name[0];
?>
<section id="portfolio" class="clearfix content bg-white pad-btm-z">
<div class="container">
    <div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
           <li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a><span class="divider"></span></li>
          <li><?php echo $this->Html->link('Domestic Staff', array('controller' =>  $this->params['controller'], 'action' => 'index')); ?><span class="divider"></span></li>
          <li>
          <?php
          $categoreies=explode(',',$CategoryList); foreach($categoreies as $categoryLink)
          {
            echo $this->Html->link($categoryLink, DOMAIN_NAME_PATH.'domestic-staffs/'.strtolower(str_replace(" ","", $categoryLink))); 
          }
          ?>
          <span class="divider"></span>
          </li>
          <li class="active"><?php echo $first_name; ?></li>
        </ol>
    </div>
</div>
    <div class="row">
        <div class="col-sm-7 col-md-8 col-lg-9">
          
        <?php if($staff['Staff']['hired']=="1") { ?>
             <div class="hired-big-back"></div>
                        <span class="hired-big">Hired</span>
        <?php } ?>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="pro-big-pic">
                    <img class="xyz" data-alt-src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image2']; ?>" src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image1']; ?>" alt="&nbsp;" />
                </div>
                <?php if($staff['Staff']['hired']=="1") { ?>
                    <a href="javascript:void(0);" role="button" class="btn btn-large btn-primary btn-danger" disabled="disabled"><i class="fa fa-phone-square"></i>&nbsp; <?php echo $staff['Staff']['name']; ?></a>
                <?php } else { ?>
                <?php if ($this->Session->check('Auth.User')) { ?>
                    <a href="#" class="btn btn-danger btn-2" data-toggle="modal" onclick="paymentContact();" data-target="#Plans" class="btn btn-large btn-primary btn-danger"><i class="fa fa-phone-square"></i>&nbsp; Call <?php echo $first_name; ?></a>
                    <a href="#" class="btn btn-danger btn-2" data-toggle="modal" onclick="shortlistContact();" data-target="#Plans" class="btn btn-large btn-primary btn-danger"><i class="fa fa-save"></i>&nbsp; Shortlist <?php echo $first_name; ?></a>
                    <div class="btn btn-danger btn-2">
                    Share Profile: 
                        <?php
                            $services = array(
                                'facebook' => __('Share on Facebook'),
                                'gplus' => __('Share on Google+'),
                                'linkedin' => __('Share on LinkedIn'),
                                'twitter' => __('Share on Twitter'),
                                'email' => __('Email')
                            );

                            echo '<ul style="list-style: none; margin-bottom:0px">';
                            foreach ($services as $service => $linkText) {
                                echo '<li style="display:inline-block;padding:5px 10px; color:#fff !important;">' . $this->SocialShare->fa(
                                    $service,
                                    $linkText
                                ) . '</li>';
                            }
                            echo '</ul>';
                        ?>
                    </div>
                <?php } else { ?>
                    <a href="#" class="btn btn-danger btn-2" data-toggle="modal" onclick="loginRedirect(this.id);" data-target="#SignIn" class="btn btn-large btn-primary btn-danger" id="ViewProfileLogin<?php echo $staff['Staff']['friendly_url']; ?>"><i class="fa fa-phone-square"></i>&nbsp; Call <?php echo $first_name; ?></a>
                <?php } ?>
                <?php } ?>
                   <div class="col-md-12" style="margin-top: 5px;">
                                    <div class="row">
                                        <?php if(!empty($staff['Staff']['immigration_status']) && $staff['Staff']['immigration_status']!="N/A"  ) { ?>
                                        <div class="col-md-12 imi-box"><h5>Immigration Status<br><span style="color: #333;"><?php echo $staff['Staff']['immigration_status'] ?></span></h5>
                                         </div>
                                        <?php } ?>
                                    </div>
                   </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9">
                                <div class="row">
                                    <div class="col-sm-12 full-d">
                                        <div class="border-pr">
                                        <h2 class="padd-m2">
                                            <?php
                                                $name = explode(" ", $staff['Staff']['name']);
                                                if($name[1]){
                                                    $prefix = substr($name[1], 0, 1);
                                                    echo $name[0].' '. $prefix.'.';
                                                }else{
                                                    echo $name[0];
                                                }
                                            ?>
                                        </h2>
                                        <ul class="list-g">
                                            <li><span>unique ID </span><?php echo $staff['Staff']['staff-uid']==null ? 'N/A' : $staff['Staff']['staff-uid']; ?> </li>
                                            <li><span>age </span><?php echo $staff['Staff']['age']; ?> Years</li>
                                            <li><span>state of origin </span><?php echo $staff['Origin']['name']; ?></li>
                                            <li><span>experience </span><?php echo $staff['Staff']['experience']; ?> Years</li>
                                            <li><span>religion </span>Christian </li>
                                            <li><span>sex</span><?php echo $staff['Staff']['sex']==1 ? 'Male' : 'Female'; ?> </li>
                                            <li><span>marital status </span><?php echo $staff['Staff']['marital_status']==1 ? 'Married' : 'Single'; ?> </li>
                                            <li class="moz-manage"></li>
                                            <li><span>Children </span> <?php echo $staff['Staff']['number_of_children'] ?> </li>
                                            <li><span>Category </span><?php if(isset($CategoryList)){echo $CategoryList;} else {echo 'N/A';} ?> </li>
                                            <li><span>Education </span><?php if(isset($PreferedEducation)){echo $PreferedEducation;} else {echo 'N/A';} ?> </li>
                                            <li><span>Nationality </span><?php echo $staff['Staff']['nationality'] ?> </li>
                                            <li><span>Accomodation </span><?php if($staff['Staff']['accommodation']==1)                                             {
                                                echo 'Live In';
                                            }
                                            else if($staff['Staff']['accommodation']==2)
                                            {
                                                echo 'Live Out';
                                            }
                                            else 
                                            {
                                                echo 'Flexible';
                                            }
                                            ?>
                                          </li>
                                          <li><span>Minimum Salary </span> &#8358; <?php if(isset($staff['Staff']['base_salary'])) {
                                            echo number_format($staff['Staff']['base_salary']);} else {echo 'N/A';} ?> </li>
                                          <!--<li><span>Expected Salary </span><?php if(isset($staff['Staff']['max_salary'])){echo $staff['Staff']['max_salary'];} else {echo 'N/A';} ?> </li>-->
                                        </ul>
                                        </div>
                                    </div>
                                </div>
                </div>
            </div>
            <div class="row">
                            <div class="col-sm-12 w-description">
                                <div class="mobile-d">
                                <h2 style="border-top: none;">About - <?php echo $staff['Staff']['name']; ?></h2>
                                 <?php $about=str_replace("<p>&nbsp;</p>","",$staff['Staff']['profile_description']);?>
                                <p><?php echo $about; ?></p>
                                </div>
                                <div class="mobile-d">
                                <h2>Spoken Languages</h2>
                                <p><?php echo $staff['Staff']['name']?> speaks - <?php
                                if(count($spokenLanguages)>0){
                                    foreach($spokenLanguages as $spokenLanguage){
                                
                                        echo $spokenLanguage.","; 
                                
                                    }
                                } else {
                                    echo 'Spoken Language Not Specified!';
                                }
                                ?>
                                    
                                </p>
                                </div>
                                <div class="mobile-d">
                                <h2>Residing Area</h2>
                                <?php
                                if(count($preferedLocalities)>0){
                                    foreach($preferedLocalities as $preferedLocality){
                                ?>
                                <p><?php echo $preferedLocality; ?></p>
                                <?php 
                                    }
                                } else {
                                    echo '<li>Preferred Locality Not Selected!</li>';
                                }
                                ?>
                                    </div>
                                    <div class="mobile-d">
                                    <h2>Work Experience</h2>
                                     <?php $workex=str_replace("<p>&nbsp;</p>","",$staff['Staff']['work_experience']);?>
                                        <p><?php echo $workex; ?></p>
                                    </div>
                                <div class="mobile-d">
                                <h2>Hobbies & Interests</h2>
                                <p><?php echo $staff['Staff']['hobbies']; ?></p></div>
                            </div>
                        </div>
        </div>
    
    <div class="col-sm-5 col-md-4 col-lg-3">
                        <div class="left-side-b">
                            <h4 data-toggle="collapse" data-target="#R-serach">Related Search<span class="pull-right">+</span></h4>
                            <div id="R-serach" class="collapse in  left-side-b-inner">
                                <div class="list-group" style="margin-bottom: 0px;">
                            <?php 
                                        if(count($relatedStaffs)>0){
                                            $j=2;
                                            foreach($relatedStaffs as $relatedStaff){
                                                $img1=$j."img1";
                                                $img2=$j."img2";
                                                
                                                $r_hired=$relatedStaff['Staff']['hired'];
                                                //$r_hired="1";
                                                //echo "Hired-".$r_hired;
                                        ?>
                                         <div class="list-group-item" style="cursor:pointer" onclick="document.location.href='<?php echo DOMAIN_NAME_PATH.$relatedStaff['Staff']['friendly_url']?>'">
                                            <?php if ($r_hired=="1") { ?>
                                              <div class="hired-small-back"></div>
                                                <span class="hired-small">Hired</span>
                                              
                                            <?php }  ?>
                                             <div class="row">
                                             <div class="col-sm-4 col-xs-4"><div class="rel-side-img" style="background:#000 url('<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $relatedStaff['Staff']['image1']; ?>')no-repeat center;"></div></div>
                        <div class="col-sm-8 s-des">
                                                <h4<?php 
                                                    $name = explode(" ", $relatedStaff['Staff']['name']);
                                                    if($name[1]){
                                                        $prefix = substr($name[1], 0, 1);
                                                        $name = $name[0].' '. $prefix.'.';
                                                    }else{
                                                        $name = $name[0];
                                                    }
                                                    echo $this->Html->link($name, DOMAIN_NAME_PATH.$relatedStaff['Staff']['friendly_url'], array('style'=>'color:#333333;')); ?></h4>
                                                <p><span>Age :</span> <?php echo $relatedStaff['Staff']['age']; ?> years</p>
                                                <p><span>Experience :</span> <?php echo $relatedStaff['Staff']['experience']; ?> Years</p>
                                            </div>
                                        </div>                   
                                        </div>
                                        <?php 
                                                $j++;
                                            }
                                        } else {
                                            echo '<a>Staff Not Found!</a>';
                                        }
                                        ?>
                                </div>
                            </div>
                        </div>
                        <div class="left-side-b">
                            <h4 data-toggle="collapse" data-target="#Cat">Categories<span class="pull-right">+</span></h4>
                            <div id="Cat" class="collapse in left-side-b-inner">
                                <div class="list-group" style="margin-bottom: 0px;">
                                <?php 
                               
                                    if(count($categoriesRelated)>0){
                                        foreach($categoriesRelated as $categoryId=>$categoryName){
                                        
                                        $img_src=DOMAIN_NAME_PATH.'images/'.$categoryName.'-icon.png';
                                            
                                    ?>
                                     
                                      <a class="list-group-item" href="<?php echo DOMAIN_NAME_PATH.'domestic-staffs/'.$categoryName ;?>"><img src="<?php echo $img_src?>" alt="Cooks"><?php echo $categoryName ; ?></a>
                                     
                                    <?php 
                                        }
                                    } else {
                                        echo '<li>Category Not Found!</li>';
                                    }
                                    ?>
                                    
                                </div>
                            </div>
                        </div>

    </div>
    
    
    </div>
    </div>
    <!--/.container-->
</section>

<!-- Confirm Plans model popup -->
<div class="modal fade" id="daftCheck" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="cardDetails">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h2 class="title-mng">Applying For Daft Check</h2>
                </div>
                <div class="modal-body plan-outer">
                    <div class="row text-center">
                        <div class="col-sm-12 col-md-12">
                            <div id="divCheck"></div>
                        </div>
                    </div>
                    <div class="row">
                    <form id="paymentForm2" class="inline-form">
                        <div class="form-group col-md-12">
                            <label class="control-label" for="card_no">Card Number:</label>
                            <input type="text" name="card_no" class="form-control card_no" placeholder="Enter Your Card No" required="required">
                            <input type="hidden" name="amount" class="form-control amount" value="10000" required="required">
                            <input type="hidden" name="staff_id" class="form-control staff_id" required="required">
                            <input type="hidden" name="comId" class="form-control comId" value="100" required="required">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="expiry_month">Expiry Month:</label>
                                <select name="expiry_month" class="form-control expiry_month" required="required">
                                    <option value="">Expiry Month On Card</option>
                                    <option value="01">January</option>
                                    <option value="02">Febraury</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="expiry_year">Expiry Year:</label>
                                <select name="expiry_year" class="form-control expiry_year" required="required">
                                    <option value="">Expiry Year On Card</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                    <option value="25">2025</option>
                                    <option value="26">2026</option>
                                    <option value="27">2027</option>
                                    <option value="28">2028</option>
                                    <option value="29">2029</option>
                                    <option value="30">2030</option>
                                    <option value="31">2031</option>
                                    <option value="32">2032</option>
                                    <option value="33">2033</option>
                                    <option value="34">2034</option>
                                    <option value="35">2035</option>
                                    <option value="36">2036</option>
                                    <option value="37">2037</option>
                                    <option value="38">2038</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="cvv">Card CVV:</label>
                            <input type="text" name="cvv" class="form-control cvv" placeholder="Enter your card CVV" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="pin">Pin:</label>
                            <input type="text" name="pin" class="form-control pin" placeholder="Expiry Year" required="required">
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="processing2" id="processing2">
                                <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')) ?>
                                Processing Payment...Pls wait
                            </div>
                            <button type="button" class="btn btn-primary paymentbutton2"  style="margin-top: 0px; width:100%;">Pay</button>
                               <!-- <input type="submit" class="btn btn-primary btn-md pull-right" style="margin-top: 0px; width:100%;" value="Proceed to Payment"> -->
                            </div>
                        </div>
                    </form>
                     <div class="row" style="margin-top: 20px;">
                        <div class="col-sm-12 col-md-12 text-center">
                            <img src="images/transfer-icon.png" alt="Payment Mode">
                        </div>
                    </div>
                </div>
            </div>
            <div class="otpDetails">
                <div class="modal-header" style="height:40px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Enter OTP Sent to your phone</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <form id="otpForm2" class="inline-form">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label" for="card_no">OTP:</label>
                                    <input type="text" name="otp" class="form-control otp" placeholder="Enter OTP Sent to your phone" required="required">
                                    <input type="hidden" name="ref" class="form-control ref" required="required">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="processing3" id="processing3">
                            <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')) ?>
                            Processing...Pls wait
                        </div>
                        <button type="button" class="btn btn-warning otpValidate2">Validate OTP</button>
                    </div>
                </div>
            </div>
            <div class="failedDetails">
                <div class="modal-header" style="height:40px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                    <h4 class="modal-title">Failed Transaction</h4>
                </div>
                <div class="modal-body">
                    <h3>Your Payment Failed, Please try again</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
                </div>
            </div>
            <div class="successDetails">
                <div class="modal-header" style="height:30px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                    <h4 class="modal-title">Successful Transaction</h4>
                </div>
                <div class="modal-body">
                    <h3>Your Payment was successfull</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ConfirmPlans" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="cardDetails">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">x</button>
                    <h2 class="title-mng">Confirm & Make Payment</h2>
                </div>
                <div class="modal-body plan-outer">
                    <div class="row text-center">
                        <div class="col-sm-12 col-md-12">
                            <div id="divSummary"></div>
                        </div>
                    </div>
                    <div class="row">
                    <form id="paymentForm" class="inline-form">
                        <div class="form-group col-md-12">
                            <label class="control-label" for="card_no">Card Number:</label>
                            <input type="text" name="card_no" class="form-control card_no" placeholder="Enter Your Card No" required="required">
                            <input type="hidden" name="amount" class="form-control amount" required="required">
                            <input type="hidden" name="payment_option_id" class="form-control payment_option_id" required="required">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="expiry_month">Expiry Month:</label>
                                <select name="expiry_month" class="form-control expiry_month" required="required">
                                    <option value="">Expiry Month On Card</option>
                                    <option value="01">January</option>
                                    <option value="02">Febraury</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="expiry_year">Expiry Year:</label>
                                <select name="expiry_year" class="form-control expiry_year" required="required">
                                    <option value="">Expiry Year On Card</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                    <option value="25">2025</option>
                                    <option value="26">2026</option>
                                    <option value="27">2027</option>
                                    <option value="28">2028</option>
                                    <option value="29">2029</option>
                                    <option value="30">2030</option>
                                    <option value="31">2031</option>
                                    <option value="32">2032</option>
                                    <option value="33">2033</option>
                                    <option value="34">2034</option>
                                    <option value="35">2035</option>
                                    <option value="36">2036</option>
                                    <option value="37">2037</option>
                                    <option value="38">2038</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="cvv">Card CVV:</label>
                            <input type="text" name="cvv" class="form-control cvv" placeholder="Enter your card CVV" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="pin">Pin:</label>
                            <input type="text" name="pin" class="form-control pin" placeholder="Expiry Year" required="required">
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="form-inline" style="padding-top:10px; padding-bottom:10px; background:#e1e1e1; border-top: 1px solid #c2c2c2; padding-bottom: 9px; margin-top: -7px;">
                                <div class="form-group ci-manage">
                                    <input id="txtCoupons" type="text" name="data[Staff][coupon_code]" placeholder="Coupon Code" class="form-control" style="height: 31px;">
                                </div>
                                <div class="form-group cb-manage">
                                    <input type="submit" class="btn btn-default btn-sm" value="Apply" onclick="javascript:applyCoupons(event);">
                                </div>
                                </div>
                                <div class="form-group" style=" background:#e1e1e1; padding: 0px 0px 15px 0px;">
                                <div class="validate text-center" style="display: none;">
                                        <span class="text-danger coupon" style="display: none;"></span>
                                        <span class="text-success coupon"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12 col-md-12">
                        <div class="processing" id="processing">
                            <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')) ?>
                            Processing Payment...Pls wait
                        </div>
                            <button type="button" class="btn btn-primary paymentbutton"  style="margin-top: 0px; width:100%;">Pay</button>
                               <!-- <input type="submit" class="btn btn-primary btn-md pull-right" style="margin-top: 0px; width:100%;" value="Proceed to Payment"> -->
                            </div>
                        </div>
                    </form>
                     <div class="row" style="margin-top: 20px;">
                        <div class="col-sm-12 col-md-12 text-center">
                            <img src="images/transfer-icon.png" alt="Payment Mode">
                        </div>
                    </div>
                </div>
            </div>
            <div class="otpDetails">
                <div class="modal-header" style="height:40px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                    <h4 class="modal-title">Enter OTP Sent to your phone</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <form id="otpForm" class="inline-form">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label" for="card_no">OTP:</label>
                                    <input type="text" name="otp" class="form-control otp" placeholder="Enter OTP Sent to your phone" required="required">
                                    <input type="hidden" name="ref" class="form-control ref" required="required">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="processing1" id="processing1">
                            <?php echo $this->Html->image('preloader.gif', array('width'=>'150px', 'height'=>'30px')) ?>
                            Processing...Pls wait
                        </div>
                        <button type="button" class="btn btn-warning otpValidate">Validate OTP</button>
                    </div>
                </div>
            </div>
            <div class="failedDetails">
                <div class="modal-header" style="height:40px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                    <h4 class="modal-title">Failed Transaction</h4>
                </div>
                <div class="modal-body">
                    <h3>Your Payment Failed, Please try again</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
                </div>
            </div>
            <div class="successDetails">
                <div class="modal-header" style="height:30px;">
                    <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
                    <h4 class="modal-title">Successful Transaction</h4>
                </div>
                <div class="modal-body">
                    <h3>You have successfully paid for verification for this candidate</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Plans model popup -->
<div class="modal fade" id="Plans" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="height:40px;">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <!-- <h2 class="title-mng">Select Plans</h2> -->
            </div>
            <div class="modal-body plan-outer">
                <div class="row text-center">
                    <div id="contactPayHolder">
                            <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>js/additional-methods.min.js"></script>

<script type="text/javascript">

    // jQuery('.button-primary').click(function(){
    //     console.log('ffffff');
    //     jQuery('#ConfirmPlans').hide();
    // })
    function paymentContact(){
        jQuery("#contactPayHolder").html(jQuery('').fadeIn('slow'));
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Staffs/contacts/'.$staff['Staff']['id']; ?>",
            success: function(data){
                // console.log(data)
                if(data){
                    jQuery("#contactPayHolder").html(jQuery(data).fadeIn('slow'));
                    if(jQuery(".payment_complete").length)
                    {
                        var name_staff=jQuery("#contactPayLabelPay").attr("data-attr");
                        jQuery("#contactPayLabelPay").html("Contacts of "+name_staff);
                    }
                }else{
                    jQuery("#contactPayHolder").html(jQuery('Nothing found!').fadeIn('slow'));
                }
            }
        });
    }

    function shortlistContact(){
        jQuery("#contactPayHolder").html(jQuery('').fadeIn('slow'));
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Staffs/shortlists/'.$staff['Staff']['id']; ?>",
            success: function(data){
                console.log(data);
                if(data){
                    jQuery("#contactPayHolder").html(jQuery(data).fadeIn('slow'));
                    if(jQuery(".payment_complete").length)
                    {
                        var name_staff=jQuery("#contactPayLabelPay").attr("data-attr");
                        jQuery("#contactPayLabelPay").html("Contacts of "+name_staff);
                    }
                }else{
                    jQuery("#contactPayHolder").html(jQuery('Nothing found!').fadeIn('slow'));
                }
            }
        });
    }
    
    function getSummary(ctrl)
    {
        var content=jQuery(ctrl).attr("data-attr");
        var content=content.split("|");
        jQuery('.amount').val(content[4]);
        if(content[4] == '5000.00'){
            jQuery('.payment_option_id').val(1);
        }else{
            jQuery('.payment_option_id').val(2);
        }
        var summary='<ul class="list-inline c-pop">';
            summary+='<li class="col-md-2"><b>Option</b><br/>'+content[0]+'</li>';
            summary+='<li class="col-md-2"><b>Quantity</b><br/>'+content[1]+'</li>';
            summary+='<li class="col-md-2"><b>Validity</b><br/>'+content[2]+'</li>';
            summary+='<li class="col-md-3"><b>Unit Price</b><br/><span style="font-size: 15px; font-weight: 400;"></span>'+content[3]+'</li>';
            summary+='<li class="col-md-3"><b>Price</b><br/>&#x20a6<span style="font-size: 15px; font-weight: 400;" class="cprice">'+content[4]+'</span></li>';
        summary+="</ul>";
        jQuery("#divSummary").html(summary);
        jQuery("#Plans").hide();
        jQuery('.modal-backdrop').remove();
        $(this).removeData();
    }

    function daftCheck(ctrl)
    {
        var staffid=jQuery(ctrl).attr("data-attr");
        jQuery('.staff_id').val(staffid)
        var check='<ul class="list-inline">';
            check+='<li class="col-md-12 text-left"><h4>Daft Check consist of address verification, guarantors verification, background check</h4></li>';
            check+='<li class="col-md-12 text-left"><h4>Cost: N10,000</h4></li>';
            check+="</ul>";
        jQuery("#divCheck").html(check);
        jQuery("#Plans").hide();
        jQuery('.modal-backdrop').remove();
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    function applyCoupons(event)
    {
        event.preventDefault();
        jQuery(".validate").hide();
        var couponCode=jQuery.trim(jQuery("#txtCoupons").val());
        jQuery.ajax({
            type: "POST",
            url: "<?php echo DOMAIN_NAME_PATH.'Coupons/applyCoupon/'?>"+couponCode,
            success: function(data){
                var discount_percent=parseFloat(jQuery.trim(data));
                if(jQuery.trim(data)!="-1"){
                    var price=parseFloat(jQuery(".cprice").text().toString().replace(/,/g,""));
                    var price_after_discount=price-((price*discount_percent)/100);
                price_after_discount=parseFloat(Math.round(price_after_discount * 100) / 100).toFixed(2);
                    var content='Price After Coupon: <br/>&#8358;'+addCommas(price_after_discount);
                    jQuery(".coupon").html(content);
                    jQuery(".coupon").show();
                    jQuery(".validate").show();
                    jQuery(".text-danger").hide();
                    jQuery(".text-success").show();
                }else{
                    jQuery(".coupon").html('Invalid Coupon Code');
                    jQuery(".coupon").show();
                    jQuery(".validate").show();
                    jQuery(".text-success").hide();
                    jQuery(".text-danger").show();
                }
            }
        });
    }
    
    var sourceSwap = function () {
        var newSource = jQuery(this).data('alt-src');
        jQuery(this).data('alt-src', jQuery(this).attr('src'));
        jQuery(this).attr('src', newSource);
    }

    jQuery(function () {
        jQuery('img.xyz').hover(sourceSwap, sourceSwap);
    });

    jQuery(document).ready(function(){
        jQuery('.cardDetails').show();
        jQuery("#processing").hide();
        jQuery('.otpDetails').hide();
        jQuery(".failedDetails").hide();
        jQuery(".otpDetails").hide();
        jQuery(".successDetails").hide();
        jQuery('.modal-backdrop').remove();
        jQuery("#paymentForm").validate({
            rules: {
                card_no: {
                  required: true,
                  number: true,
                  minlength: 16,
                  maxlength: 16
                },
                cvv: {
                  required: true,
                  number: true,
                  minlength: 3,
                  maxlength: 3
                },
                expiry_month: {
                  required: true,
                  number: true,
                },
                expiry_year: {
                  required: true,
                  number: true,
                },
                pin: {
                  required: true,
                  number: true,
                  minlength: 4,
                  maxlength: 4
                }
            },
            messages: {
                card_no: {
                  required: "Card Number Required",
                  minlength: "At least 16 characters required!"
                },
                cvv: {
                  required: "Card CVV Required",
                  minlength: "At least 3 characters required!"
                },
                expiry_month: {
                  required: "Expiry Month Required",
                  minlength: "At least 2 characters required!"
                },
                expiry_year: {
                  required: "Expiry Year Required",
                  minlength: "At least 2 characters required!"
                },
                pin: {
                  required: "Pin Required",
                  minlength: "At least 4 characters required!"
                }
            },
            highlight: function(element){
                jQuery(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element){
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
        });
        jQuery("#otpForm").validate({
            rules: {
                otp: {
                  required: true,
                  number: true,
                  minlength: 4,
                  maxlength: 16
                }
            },
            messages: {
                otp: {
                  required: "OTP Required",
                  minlength: "At least 4 characters required!"
                }
            },
            highlight: function(element){
                jQuery(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element){
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
        });

        jQuery('.modal').on('hidden.bs.modal', function(){
            jQuery(this).find('form')[0].reset();
            jQuery(this).find('form')[1].reset();
            jQuery('.modal-backdrop').remove();
            $(this).removeData();
        });

        jQuery("#ConfirmPlans").on("hidden.bs.modal", function(){
            jQuery('.cardDetails').hide();
            jQuery("#processing").hide();
            jQuery('.otpDetails').hide();
            jQuery(".failedDetails").hide();
            jQuery(".successDetails").hide();
            jQuery('.modal-backdrop').remove();
            $(this).removeData();
        });

        jQuery(document).on('click', '.paymentbutton', function(e){
            var amount = jQuery('.amount').val();
            var comId = jQuery('.comId').val();
            var card_no = jQuery('.card_no').val();
            var cvv = jQuery('.cvv').val();
            var expiry_month = jQuery('.expiry_month').val();
            var expiry_year = jQuery('.expiry_year').val();
            if(jQuery("#paymentForm").valid()){
                jQuery("#processing").show();
                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo DOMAIN_NAME_PATH.'Staffs/flutterwave/'; ?>",
                    data: jQuery("#paymentForm").serialize(),
                    complete: function(){
                        jQuery("#processing").hide();
                    },
                    success: function(data){
                        console.log(data);
                        var obj = jQuery.parseJSON(data);
                        if(obj.status = 'success'){
                            jQuery('.cardDetails').hide();
                            jQuery(".otpDetails").show();
                            jQuery(".failedDetails").hide();
                            jQuery("#processing1").hide();
                            jQuery(".successDetails").hide();
                            jQuery('.modal-backdrop').remove();
                            jQuery('.ref').val(obj.data.transactionreference);
                        }else{
                            jQuery('.selectVetCom').hide();
                            jQuery('.cardDetails').hide();
                            jQuery(".otpDetails").hide();
                           jQuery(".failedDetails").show();
                           jQuery("#processing1").hide();
                           jQuery(".successDetails").hide();
                           jQuery('.modal-backdrop').remove();
                        }
                    }
                });  
            }else{
                return false;
            }
            //e.preventDefault();
        });

        jQuery(document).on('click', '.otpValidate', function(e){
            var amount = jQuery('.otp').val();
            var ref = jQuery('.ref').val();
            if(jQuery("#otpForm").valid()){
                jQuery("#processing1").show();
                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo DOMAIN_NAME_PATH.'Staffs/validateOtp/'; ?>",
                    data: jQuery("#otpForm").serialize(),
                    complete: function(){
                        jQuery("#processing1").hide();
                    },
                    success: function(data){
                        if(data == 'Failed'){
                            jQuery('.selectVetCom').hide();
                            jQuery('.cardDetails').hide();
                            jQuery(".otpDetails").hide();
                            jQuery(".failedDetails").show();
                            jQuery(".successDetails").hide();
                            jQuery("#processing1").hide();
                            jQuery('.modal-backdrop').remove();
                        }else if(data == 'Successful'){
                            jQuery('.selectVetCom').hide();
                            jQuery('.cardDetails').hide();
                            jQuery(".otpDetails").hide();
                            jQuery(".failedDetails").hide();
                            jQuery("#processing1").hide();
                            jQuery(".successDetails").show();
                            jQuery('.modal-backdrop').remove();
                        }
                    }
                });  
            }else{
                return false;
            }
            //e.preventDefault();
        });

    });
</script>
