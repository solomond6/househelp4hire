<style>
 .dataTables_wrapper .row{
 	margin-left:0px!important;
 }
 .dataTables_length,.dataTables_filter{
 	display:none;
 }
 .pagination > li > a, .pagination > li > span{
 	border:none!important;
 }
 
</style>
 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Staff Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Staff Monitoring</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                  <div class="row-fluid">
                                  	<div class="controls">
                                  		<?php echo '<span class="btn btn-danger"> Staff View By User</span>';?> &nbsp; <?php echo $this->Html->link('Monitor Staff', array('controller' => 'staffs','action' => 'admin_monitor'), array('class' => 'btn btn-success'));?>
                                  	</div>
                                  </div>
                                  <div class="row-fluid">
                                  	<br/>
                                  </div>
                                   <div class="row-fluid">
                                   <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
                                        <tr>
                                            	<th><?php echo 'User ID' ?></th>
												<th><?php echo 'User Name' ?></th>
												<th><?php echo 'Staff ID' ?></th>
												<th><?php echo 'Staff Name' ?></th>
												<th><?php echo 'Image' ?></th>
												<th><?php echo 'Hire Date' ?></th>
                                        </tr>
                                     </thead>
                                        <tbody>
                                     		<?php
	
												foreach ($staffs as $key=>$value):
												$staff=$value['s'];
												$user=$value['u'];
												$user_name=$user['first_name']." ".$user['last_name'];
											 ?>
											<tr>
												<td><?php echo $user['user_id']==null ? 'N/A' : $user['user_id']; ?>&nbsp;</td>
												<td><?php echo h($user_name); ?>&nbsp;</td>
												<td><?php echo $staff['staff_id']==null ? 'N/A' : $staff['staff_id']; ?>&nbsp;</td>
												<td><?php echo h($staff['name']); ?>&nbsp;</td>
												<td><?php echo $this->Html->image(PAGE_IMAGES_URL.$staff['image1'], array('title'=>"image", 'alt'=>'image', 'width'=>'40')); ?></td>											<td><?php echo h($staff['hired_date']); ?>&nbsp;</td>
											</tr>
										<?php endforeach; ?>
										</tbody>
									</table>
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                
        <script type="text/javascript" language="javascript" src="<?php echo DOMAIN_NAME_PATH; ?>admin/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo DOMAIN_NAME_PATH; ?>admin/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo DOMAIN_NAME_PATH; ?>admin/js/dataTables.bootstrap.js"></script>

        
        <script type="text/javascript" src="<?php echo DOMAIN_NAME_PATH; ?>admin/js/jquery.uniform.min.js"></script>
        <script src="<?php echo DOMAIN_NAME_PATH; ?>admin/js/scripts.js"></script>
        <script>
            jQuery(document).ready(function() {       
                // initiate layout and plugins
                //App.init();
                $('#example').DataTable({"aaSorting": []});
               
            });
        </script>