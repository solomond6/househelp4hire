  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Staff Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Manage Staff</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                  
                                    <div class="row-fluid">
                                        
                                    </div>
                                    
                                    <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
	                                        <tr>
	                                        <th><?php echo $this->Paginator->sort('id', 'UID'); ?></th>
											<th><?php echo $this->Paginator->sort('name'); ?></th>
											<th><?php echo $this->Paginator->sort('email'); ?></th>
											<!--<th><?php echo $this->Paginator->sort('category_id'); ?></th>-->
											<th><?php echo $this->Paginator->sort('hired'); ?></th>
											<th><?php echo $this->Paginator->sort('image1', 'Image'); ?></th>
											<th class="actions" style='text-align:center;'><?php echo __('Actions'); ?></th>
	                                        </tr>
                                     	</thead>
                                        <tbody>
                                  	<?php
	foreach ($staffs as $staff): ?>
	<tr>
		
		<td><?php echo $staff['Staff']['staff-uid']==null ? 'N/A' : $staff['Staff']['staff-uid']; ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['name']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['email']); ?>&nbsp;</td>
		<!--
		<td>
			<?php echo $this->Html->link($staff['Category']['name'], array('controller' => 'categories', 'action' => 'view', $staff['Category']['id'])); ?>
		</td>
		-->
		<td>
			<?php echo $staff['Staff']['hired']=="1" ? "Hired":"Available";?>
		</td>
		<td><?php echo $this->Html->image(PAGE_IMAGES_URL.$staff['Staff']['image1'], array('title'=>"image", 'alt'=>'image', 'width'=>'100')); ?></td>
		<td class="actions">
			<?php echo $this->Html->link('<i class="fa fa-eye"></i> View', array('action' => 'view', $staff['Staff']['id']), array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'edit', $staff['Staff']['id']),array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<a href="#conDialog" role="button"  class="btn btn-danger" data-toggle="modal" data-attr="<?php echo $staff['Staff']['id']?>" id="deleteConfirm"><i class="fa fa-trash-o" ></i> Delete</a>&nbsp;
		</td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
									<p>
									<?php
									echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>	
									</p>
									<div class="paging">
									<?php
										echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
										echo $this->Paginator->numbers(array('separator' => ' '));
										echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
									?>
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			                <h3 id="myModalLabel1">Are you sure ?</h3>
			            </div>
            
			            <div class="modal-footer">
			                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
			                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
			            </div>
        		</div>
<script>
     var record_id="";
     jQuery(document).ready(function(){
     		jQuery("#deleteConfirm").click(function(){
     			var id=jQuery(this).attr("data-attr");
     			record_id=id;
     		})
     })
	
	function Delete()
	{
		console.log("staffs/delete/"+record_id);
		document.location.href="staffs/delete/"+record_id;
	}
</script>