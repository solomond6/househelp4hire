  <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Contacted Candidates</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-cog"></i>Contacted Candidates</h4>
                                    <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                    </span>
                                </div>
                                <div class="widget-body">
                                    <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
	                                        <tr>
	                                            <th style='text-align:center;'></th>
                      												<th>Candidate Name</th>
                      												<th>Shortlisted By</th>
	                                        </tr>
                                     	</thead>
                                      <tbody>
                                    	<?php foreach ($shortlisted as $shortlist): ?>
                                    	<tr>
                                        <td></td>
                    										<td><?php echo $shortlist['a']['name']; ?>&nbsp;</td>
                    										<td><?php echo h($shortlist['c']['name']); ?>&nbsp;</td>
                    									</tr>
  								                    <?php endforeach; ?>
                                      </tbody>
                                    </table>
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                 <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			                <h3 id="myModalLabel1">Are you sure ?</h3>
			            </div>
            
			            <div class="modal-footer">
			                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
			                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
			            </div>
        		</div>