<?php
$keyWord = 'false';
$type = 'false';
if(count($category) > 0){
	$url = DOMAIN_NAME_PATH.'Staffs/loadmore/'.$category['Category']['friendly_url'].'/';
} else {
	$url = DOMAIN_NAME_PATH.'Staffs/loadmore/';
	if(!empty($this->request->data)){
		$keyWord =  base64_encode(serialize($this->request->data));
		if (isset($this->request->data['Staff']['search'])) {
			$type = 'headerSearch';
		} else {
			$type = 'advancedSearch';
		}
	}
}
?>
<script type="text/javascript">
	var scrollCall = 1;
	jQuery(window).scroll(function(){
		if(jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
			if(scrollCall == 1){
				scrollCall = 0;
				
			}
		}
	});
	
	function loadmore()
	{
		if(jQuery('#pageCountUp').val() != 0){
			var curPage = jQuery('#pageCountUp').val();
			var searchString = '<?php echo $keyWord; ?>';
			var searchType = '<?php echo $type; ?>';
			var dataString = "page=" + curPage +"& search=" + searchString +"& searchType=" + searchType;
			jQuery('div#loadmoreajaxloader').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo $url; ?>",
				data: dataString,
				success: function(html){
					jQuery(".loadmore").hide()
					if(html){
						jQuery('#pageCountUp').val(eval(curPage) + 1);
						jQuery("#postswrapper").append(jQuery(html).fadeIn('slow'));
						jQuery('div#loadmoreajaxloader').hide();
						scrollCall = 1;
					}else{
						jQuery('#pageCountUp').val(eval(2));
						jQuery('div#loadmoreajaxloader').html('<center>No more domestic staff to display</center>');
					}
				}
			});
		}
	}
</script>
<input type="hidden" name="pageCount" value="2" id="pageCountUp">
<section id="portfolio" class="clearfix content bg-white pad-btm-z">
          <div class="container">
          <div class="row">
          <!-- Search Filters -->
 				 <div class="col-sm-5 col-md-4 col-lg-3 no-padding">
                    <div style=" padding: 7px 7px 1px 7px; margin-bottom: 30px; background: #fabe22;">
                        <h4 data-toggle="collapse" data-target="#demo" style="cursor: pointer; padding: 9px 11px 6px 11px;color:#444">Refine Search<span class="pull-right">+</span></h4>
                        <div id="demo" class="collapse in" style=" background: #eee; margin-left: -7px; margin-right: -7px;  padding: 15px;">
                        <?php echo $this->Form->create('Staff', array('action' => 'index')); ?>
                          <div class="form-group" style="padding-top: 10px;">
                        	 <?php echo $this->Form->input('category_id', array('div' => false, 'class' => 'form-control', 'empty'=>'Select One')); ?>
                                
                          </div>
                          
                          <div class="form-group">
                        	<?php echo $this->Form->input('experience', array('div' => false, 'class' => 'form-control', 'empty'=>'Select One', 'options'=>array(1=>'0-1 Years', 2=>'1-3 Year', 3=>'3-5 Years', 4=>'5-10 Years', 5=>'10+ Years'))); ?>
                          </div>
                          <div class="form-group">
                          <?php echo $this->Form->input('age', array('div' => false, 'class' => 'form-control', 'empty'=>'Select One', 'options'=>array(1=>'18-25 Years', 2=>'25-30 Years', 3=>'30-35 Years', 4=>'35-40 Years', 5=>'40+ Years'))); ?>
                          </div>
                          <div class="form-group">
                          <?php echo $this->Form->input('sex', array('div' => false, 'class' => 'form-control', 'type' => 'select', 'options' => array('1' => 'Male', '2' => 'Female'), 'empty'=>'Select Sex')); ?> 
                          </div> 
                          <div class="form-group"> 
                          <?php echo $this->Form->input('origin_id', array('label'=>'State of Origin', 'div' => false, 'class' => 'form-control', 'empty'=>'Select Origin')); ?>
                          </div>  
                          <div class="form-group"> 
                          <?php echo $this->Form->input('accommodation', array('div' => false, 'class' => 'form-control', 'empty'=>'Select One', 'options' => array('1' => 'Live In', '2' => 'Live Out','3'=>'Flexible'))); ?>
                          </div>
                          <div class="form-group"> 
                          <?php echo $this->Form->input('spoken_language', array('label'=>'Spoken Language(s)', 'div' => false, 'class' => 'form-control', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$languages)); ?>
                          </div>
                          <div class="form-group"> 
                          <?php echo $this->Form->input('prefered_locality', array('label'=>'Residing Area', 'div' => false, 'class' => 'form-control', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$localities)); ?>
                          </div>
                          <div class="form-group"> 
                          <?php echo $this->Form->input('marital_status', array('div' => false, 'class' => 'form-control', 'data-selected-text-format'=>'count', 'options' => array('1' => 'Married', '2' => 'Single'), 'empty'=>'Select One')); ?>
                          </div>
                          <div class="form-group"> 
                          <?php echo $this->Form->input('religion_id', array('div' => false, 'class' => 'form-control', 'empty'=>'Select Religion')); ?>
                          </div>
                          <div class="form-group"> 
                          <?php echo $this->Form->input('education', array('label'=>'Education', 'div' => false, 'class' => 'form-control', 'data-selected-text-format'=>'count', 'multiple'=>'multiple', 'options'=>$educations)); ?>
                          </div>
                          <!-- Nationality -->
                           <div class="form-group"> 
                          <?php echo $this->Form->input('nationality', array('label'=>'Nationality', 'div' => false, 'class' => 'form-control', 'data-selected-text-format'=>'count', 'options'=>$nationality_list)); ?>
                          </div>
                          <div class="form-group"> 
                          <?php echo $this->Form->input('immigration_status', array('label'=>'Immigration', 'div' => false, 'class' => 'form-control', 'data-selected-text-format'=>'count', 'options'=>$immigration_list)); ?>
                          </div>
                          <div class="form-group">
                          <?php $verification = array('1'=>'Yes', '0'=>'No'); ?>
                          <?php echo $this->Form->input('status', array('label'=>'Verified', 'div' => false, 'class' => 'form-control', 'options'=>$verification)); ?>
                          </div>
                          <div class="form-group">
						    <col-sm-6>
						     <input type="submit" value="Search" class="btn btn-danger" style="width: 45%;"> 
						    </col-sm-6>
						   <col-sm-6>
						    <input type="reset" value="Reset" class="btn btn-danger pull-right" style="width: 45%;"onclick="resetForm();"> 
						   </col-sm-6>
						   </div>
                         <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
 			<!-- Search Filters End Here -->
             <div class="col-sm-7 col-md-8 col-lg-9">
				<div class="row">
			     <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a><span class="divider">  </span></li> 
							<?php
							if(count($category) > 0){
							echo '<li>'.$this->Html->link('Domestic Staff', array('controller' =>  $this->params['controller'], 'action' => $this->params['action'])).'<span class="divider">  </span></li>';
							echo '<li class="active">'.$category['Category']['name'].'</li>';
							} else {
							echo '<li class="active">Domestic Staff</li>';
							}
							?>
                        </ol>
                    </div>
				</div>
 				<div class="" id="postswrapper">
 					<?php if(count($staffs)>0) { 
 						$i=1;
 						foreach($staffs as $staff) {
						$img1=$staff['Staff']['id']."img1";
						$img2=$staff['Staff']['id']."img2";
						if(strlen($staff['Staff']['name'])>25){
							$name = substr(strip_tags($staff['Staff']['name']),0,25).'...';
						}else{
							$name = strip_tags($staff['Staff']['name']);
						}
						if(strlen($staff['Staff']['mini_biography'])>75){
							$content = substr(strip_tags($staff['Staff']['mini_biography']),0,75).'...';
						}else{
							$content = strip_tags($staff['Staff']['mini_biography']);
						}
						
						$hired=$staff['Staff']['hired'];
 					?>
 					<!--/.col-md-3 //previous .col-md-4 -->
                        <div class="col-md-12 col-sm-12 col-xs-12 port-item padding box-60">
                        	<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
								<div style="float:left; width:150px;height:150px; margin-right:20px">
									<?php if($hired=="") { ?>
										<div class="mini-hired-tag"><span>Hired</span></div>
									<?php } ?>
	                                <a class="box-40 hovers bwWrapper prf" href="<?php echo DOMAIN_NAME_PATH.$staff['Staff']['friendly_url']?>">
	                                	<img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image2']; ?>" alt=""  id="<?php echo $img1; ?>" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" class="bottom"/>	
	                                    <img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image1']; ?>" alt="" id="<?php echo $img2; ?>" oncontextmenu="return false" ondragstart="return false" onselectstart="return false" class="top"/>
	                                </a>
	                            </div>
                            	<div>
	                                <h3 class="no-margin">
	                                	<?php 
	                                		$name = explode(" ", $name);
	                                		echo $name[0]; 
	                                	?>
	                                </h3>
	                                <span class="desc">
	                                <?php
	                                	if (strlen($staff['Staff']['profile_description']) > 150) {
										    // truncate string
										    $descCut = substr($staff['Staff']['profile_description'], 0, 150);
										    // make sure it ends in a word so assassinate doesn't become ass...
										    $desc = substr($descCut, 0, strrpos($descCut, ' ')).'...'; 
										    echo $desc;
										}else{
											echo $staff['Staff']['profile_description'];
										}
									?>
									</span>
									<br/>
	                                <?php 
	                                echo $this->Html->link('Click to view profile', DOMAIN_NAME_PATH.$staff['Staff']['friendly_url'], array('class'=>'btn btn-danger btn-sm'));	
	                                ?>
	                            </div>
	                        </div>
                            <div class="col-md-3 col-sm-3 col-xs-12" style="font-size: 16px;">
                            	<span style="color: #333;"><i class="fa fa-calendar"></i> Age : </span><?php echo $staff['Staff']['age'];?><br>
                                <span style="color: #333;"><i class="fa fa-globe"></i> State of Origin : </span><br/><?php echo $staff['Origin']['name']; ?><br>
                                <span style="color: #333;"><i class="fa fa-globe"></i> Nationality : </span><br/><?php echo $staff['Staff']['nationality']; ?>
                            </div>
                        </div>
				<?php 
					if($i==8){
						$i=1;
						echo '<div class="loadmore">
								  <div class="row">
								    <div class="col-md-12 text-center mrgn-btm-47">
								      <a href="javascript:void(0);" onclick="loadmore();" class="btn btn-danger btn-lg br-none mrgn-btm-47">Load More</a>										<div class="bounce"><i class="fa fa-arrow-down"></i></div>
								      </div>
								   </div>
							</div>';
					}
					$i++;
				   }
				 } else {
				 	echo '<p style="text-align: center;font-size: 20px;margin-top: 12px;">
<span style="color: #f8991a;display: block;font-size: 25px;text-transform: uppercase;">
<i class="fa fa-exclamation" style="background-color: #f8991a;color: #fff;width: 30px;height: 30px;border-radius: 50%;line-height: 32px;font-size: 19px;"></i> 
Sorry,</span> nothing was found that matches your criteria</p>';
				 }
				?>
				</div>
 				</div>
 			</div>
 		</div> <!-- Container -->	
</section>

<script type="text/javascript">

//$(window).load(function(){
	//document.getElementById("search-sec").style.display = "block";
	//$("#search-sec").fadeIn(1000);
	/*$( "#search-sec" ).slideDown( "slow", function() {
		//Animation complete.
	});*/
//	$( "#search-sec" ).animate({
//	height: "toggle"
//	}, 1000, function() {
		//$("#search-sec-inner").fadeTo(200000, 9.4);;
		//$("#search-sec-inner").css('opacity', 0.1).fadeTo(2000, 1.0)
	// Animation complete.
//	});
//})

function resetForm()
{
	jQuery("#StaffCategoryId").val('');
	jQuery("#StaffExperience").val('');
	jQuery("#StaffAge").val('');
	jQuery("#StaffSex").val('');
	jQuery("#StaffOriginId").val('');
	jQuery("#StaffAccommodation").val('');
	jQuery("#StaffSpokenLanguage").val('');
	jQuery("#StaffPreferedLocality").val('');
	jQuery("#StaffMaritalStatus").val('');
	jQuery("#StaffReligionId").val('');
	
	jQuery("#StaffEducation").val('');
	jQuery("#StaffNationality").val('');
	jQuery("#StaffImmigrationStatus").val('');
	
}
</script>