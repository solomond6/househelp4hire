<script type="text/javascript">
   $(document).ready(function() {
	$("#BookingAddForm").validationEngine({binded: false, scroll:false, promptPosition: "topRight"})
   });
</script>
<div class="container">
    <ul class="breadcrumb">
          <li><a href="<?php echo DOMAIN_NAME_PATH; ?>">Home</a><span class="divider"> / </span></li>
          <li><?php echo $this->Html->link('Domestic Staff', array('controller' =>  $this->params['controller'], 'action' => 'index')); ?><span class="divider"> / </span></li>
		  <li><?php echo $this->Html->link($staff['Category']['name'], DOMAIN_NAME_PATH.'domestic-staffs/'.$staff['Category']['friendly_url']); ?><span class="divider"> / </span></li>
          <li class="active"><?php echo $staff['Staff']['name']; ?></li>
    </ul>
	<div class="span3 pull-left">
		<h4>Categories</h4>
		<ul class="span3 nav nav-list bs-docs-sidenav">
		<?php 
		if(count($categoriesRelated)>0){
			foreach($categoriesRelated as $categoryId=>$categoryName){
		?>
		  <li><?php echo $this->Html->link($categoryName, DOMAIN_NAME_PATH.'domestic-staffs/'.$categoryName ); ?></li>
		<?php 
			}
		} else {
			echo '<li>Category Not Found!</li>';
		}
		?>
		</ul>
		<h4>Related Search</h4>
		<ul class="span3 hack-1 nav nav-list bs-docs-sidenav">
		<?php 
		if(count($relatedStaffs)>0){
			$j=2;
			foreach($relatedStaffs as $relatedStaff){
				$img1=$j."img1";
				$img2=$j."img2";
		?>
			<li class="left-cat"> 
				<div style=" position:relative;" onmouseover="flipImgOver('<?php echo $img1; ?>', '<?php echo $img2; ?>');" onmouseout="flipImgOut('<?php echo $img1; ?>', '<?php echo $img2; ?>');" oncontextmenu="return false" ondragstart="return false" onselectstart="return false">
					<img class="pull-left" src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $relatedStaff['Staff']['image1']; ?>" alt="" style="display:block;width:60px;height:60px;" id="<?php echo $img1; ?>" />
					<img class="pull-left" src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $relatedStaff['Staff']['image2']; ?>" alt="" style="display:none;width:60px;height:60px;" id="<?php echo $img2; ?>" />
				</div>
				<div class="pull-right span2">
				  <h5><?php echo $this->Html->link($relatedStaff['Staff']['name'], DOMAIN_NAME_PATH.$relatedStaff['Staff']['friendly_url'], array('style'=>'color:#333333;')); ?></h5>
				  <p class="pad">Age : <?php echo $relatedStaff['Staff']['age']; ?> years</p>
				  <p class="pad">Experience : <?php echo $relatedStaff['Staff']['experience']; ?> years</p>
				</div>
				<div class="clearfix"></div>
			</li>
		<?php 
				$j++;
			}
		} else {
			echo '<li>Staff Not Found!</li>';
		}
		?>
		</ul>
	</div>

	<div class="span7 pull-right">
		<div class="span3 pull-left">
			<div onmouseout="flipImgOut('1img1', '1img2');" onmouseover="flipImgOver('1img1', '1img2');" oncontextmenu="return false" ondragstart="return false" onselectstart="return false">
				<?php if(isset($booked['Booking']['status']) && $booked['Booking']['status'] == 'Hired'){ ?>
				<div style="position:absolute; width:250px; height:250px; background:url('<?php echo DOMAIN_NAME_PATH; ?>img/hired.png');"></div>
				<?php } ?>
				<?php if(count($hired)>0){ ?>
				<div style="position:absolute; width:250px; height:250px; background:url('<?php echo DOMAIN_NAME_PATH; ?>img/hired.png');"></div>
				<?php } ?>
				<img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image1']; ?>" alt="" style="display:block;height:247px;" id="1img1" />
				<img src="<?php echo DOMAIN_NAME_PATH; ?>img/static_images/<?php echo $staff['Staff']['image2']; ?>" alt="" style="display:none;height:247px;" id="1img2" />
			</div>
		  <h4><?php echo $staff['Staff']['name']; ?></h4>
		  <?php //pr($staff); ?>
		  <p><strong>Unique ID :</strong> <?php echo $staff['Staff']['staff-uid']==null ? 'N/A' : $staff['Staff']['staff-uid']; ?> </p>
		  <p><strong>Age : </strong> <?php echo $staff['Staff']['age']; ?> Years</p>
		  <p><strong>State Of Origin :</strong> <?php echo $staff['Origin']['name']; ?> </p>
		  <p><strong>Experience :</strong> <?php echo $staff['Staff']['experience']; ?> Years</p>
		  <p><strong>Religion :</strong> <?php echo $staff['Religion']['name']; ?></p>
		  <p><strong>Sex :</strong>  <?php echo $staff['Staff']['sex']==1 ? 'Male' : 'Female'; ?></p>
		  <p><strong>Marital Status :</strong> <?php echo $staff['Staff']['marital_status']==1 ? 'Married' : 'Single'; ?></p>
		  <p><strong>Children :</strong> <?php echo $staff['Staff']['number_of_children']==null ? 'N/A' : $staff['Staff']['number_of_children']; ?></p>
		  <p><strong>Category :</strong> <?php echo $staff['Category']['name_singular']; ?></p>
		  <p><strong>Education :</strong> <?php if(isset($PreferedEducation)){echo $PreferedEducation;} else {echo 'N/A';} ?></p>
		  <p><strong>Accommodation :</strong> <?php echo $staff['Staff']['accommodation']==1 ? 'Live In' : 'Live Out'; ?></p>
		  
			<div class="well">
				<script type="text/javascript">
					function paymentContact(){
						$("#contactPayHolder").html($('').fadeIn('slow'));
						$.ajax({
							type: "POST",
							url: "<?php echo DOMAIN_NAME_PATH.'Staffs/contacts/'.$staff['Staff']['id']; ?>",
							success: function(data){
								if(data){
									$("#contactPayHolder").html($(data).fadeIn('slow'));
								}else{
									$("#contactPayHolder").html($('Nothing found!').fadeIn('slow'));
								}
							}
						});
					}
				</script>
				<div id="contactPay" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="contactPayLabel" aria-hidden="true">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 id="contactPayLabelPay">Contact of <?php echo $staff['Staff']['name']; ?></h3>
					</div>
					<div class="modal-body">
						<fieldset id="contactPayHolder">
							<p></p>
						</fieldset>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal">Close</button>
					</div>
				</div>
				<div id="inisiatePay" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="inisiatePayLabel" aria-hidden="true">
					<?php echo $this->Form->create('Staff', array('action'=>'payment', 'novalidate'=>'')); ?>
					<?php echo $this->Form->input('payment_option_id', array('type'=>'hidden', 'value'=>'')); ?>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 id="inisiatePayLabelPay">Make Payment</h3>
					</div>
					<div class="modal-body">
						<fieldset>		
                        	<div class="row-fluid">	
                                <div class="control-group span6">
                                    <div class="controls">
                                        <?php echo $this->Form->input('name_on_card', array('div'=>false, 'class'=>'input-large span12', 'required'=>'required')); ?>
                                    </div>
                                </div>			
                                <div class="control-group span6">
                                    <div class="controls">
                                        <?php echo $this->Form->input('card_number', array('div'=>false, 'class'=>'input-large span12', 'required'=>'required')); ?>
                                    </div>
                                </div>	
                            </div>	
                            <div class="row-fluid">	
                                <div class="control-group span4">
                                    <div class="controls">
                                        <?php 
                                        $monthArr = array('01'=>'01 - Jan', '02'=>'02 - Feb', '03'=>'03 - Mar', '04'=>'04 - Apr', '05'=>'05 - May', '06'=>'06 - Jun', '07'=>'07 - Jul', '08'=>'08 - Aug', '09'=>'09 - Sep', '10'=>'10 - Oct', '11'=>'11 - Nov', '12'=>'12 - Dec');
                                        echo $this->Form->input('card_expiry_month', array('type'=>'select','options'=>$monthArr, 'div'=>false, 'class'=>'input-large span12', 'required'=>'required')); 
                                        ?>
                                    </div>
                                </div>			
                                <div class="control-group span4">
                                    <div class="controls">
                                        <?php
                                        $curyear = date('y');
                                        for($i=$curyear; $i<($curyear+15); $i++){
                                            $yearArr[$i] = $i;
                                        }
                                        echo $this->Form->input('card_expiry_year', array('type'=>'select', 'options'=>$yearArr, 'div'=>false, 'class'=>'input-large span12', 'required'=>'required')); ?>
                                    </div>
                                </div>
                                <div class="control-group span4">
                                    <div class="controls">
                                        <?php echo $this->Form->input('cvv', array('type'=>'password', 'label'=>'CVV', 'div'=>false, 'class'=>'input-large span12', 'value'=>'', 'required'=>'required')); ?>
                                    </div>
                                </div>	
                            </div>		
						</fieldset>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal">Close</button>
						<button class="btn btn-primary btn-warning" type="submit">Pay Now</button>
					</div>
					<?php echo $this->Form->end(); ?>
				</div>
				<a href="#contactPay" role="button" data-toggle="modal" id="" onclick="paymentContact();" class="btn btn-large btn-primary btn-danger">View Contacts</a>
			</div>
		  <!-- section for bookimg module -->
		  <?php if($siteSetting['Setting']['booking_module']==1){ ?>
		  <?php
		  if(count($selected)==0 && count($hired)==0){
			if(count($booked)==0){ ?>
			<script type="text/javascript">
				function timeGreater(field, rules, i, options){
					/*var StartTime = $("#basic_example_2").val();
					var StartHour = eval(StartTime.substr(0,2));
					var StartHourToMin = eval(StartTime.substr(0,2))*60;
					var StartMin = eval(StartTime.substr(3,2));
					var StartMinTot = eval(StartHourToMin+StartMin);
					var EmdTime = $("#basic_example_3").val();
					var EmdHour = eval(EmdTime.substr(0,2));
					var EmdHourToMin = eval(EmdTime.substr(0,2))*60;
					var EmdMin = eval(EmdTime.substr(3,2));
					var EmdMinTot = eval(EmdHourToMin+EmdMin);
					if(StartMinTot < EmdMinTot){
						return true;
					} else {
						return options.allrules.timeGreater.alertText;
					}*/
				}
				$(document).ready(function () {
					var arrDisabledDates = {};
					<?php 
					if(count($staff['Booking'])>0){
						foreach($staff['Booking'] as $booking){
							if($booking['status'] == 'Pending'){
							$date = date('Y/m/d', (strtotime($booking['date'])));
					?>
						arrDisabledDates[new Date('<?php echo $date; ?>')] = new Date('<?php echo $date; ?>');
					<?php 
							}
						}
					} else { ?>
						arrDisabledDates[new Date('2014/01/01')] = new Date('2014/01/01');
					<?php } ?>
					$('#basic_example_1').datepicker({
						minDate: "+2D",
						maxDate: "+2M" ,
						dateFormat: "yy-mm-dd",
						onSelect: function (offError) {$('.basic_example_1formError').remove()},
						beforeShowDay: function (dt) {
							var bDisable = arrDisabledDates[dt];
							if (bDisable) return [false, '', ''];
							else return [true, '', ''];
						}
					});
				});
			</script>
			<div class="well">
				<?php
				echo $this->Form->create('Booking', array('action' => 'add'));
				echo $this->Form->input('staff_id', array('type'=>'hidden', 'value'=>$staff['Staff']['id']));
				echo $this->Form->input('staff-uid', array('type'=>'hidden', 'value'=>$staff['Staff']['staff-uid']));
				echo $this->Form->input('friendly_url', array('type'=>'hidden', 'value'=>$staff['Staff']['friendly_url']));
				echo $this->Form->input('user_id', array('type'=>'hidden', 'value'=>$this->Session->read('Auth.User.id')));
				echo $this->Form->input('staff_name', array('type'=>'hidden', 'value'=>$staff['Staff']['name']));
				echo $this->Form->input('staff_email', array('type'=>'hidden', 'value'=>$staff['Staff']['email']));
				echo $this->Form->input('staff_category', array('type'=>'hidden', 'value'=>$staff['Category']['name_singular']));
				$locations['Other Location'] = 'Other Location';
				?>
				<h4 style="margin-top:0">Book For Interview</h4>
				<input type="text" name="data[Booking][date]" id="basic_example_1" placeholder="Select Date" value="" style="background-image: url('<?php echo DOMAIN_NAME_PATH; ?>/css/images/calendar.gif');background-position: right;background-repeat: no-repeat;cursor: pointer;" class="input-large validate[required] b-in" readonly />
				<input type="text" name="data[Booking][start_time]" id="basic_example_2" placeholder="Start Time" value="" style="background-image: url('<?php echo DOMAIN_NAME_PATH; ?>/css/images/clock.png');background-position: right;background-repeat: no-repeat;cursor: pointer;" class="input-large validate[required] b-in" readonly />
				<p style="font-size:12px;">Important! Please include ALL 'off' roads, e.g. Off Admiralty Way, Off Victoria Arobieke Street, etc</p>
				<?php echo $this->Form->input('location', array('type'=>'textarea', 'placeholder'=>'Enter Location', 'class'=>'validate[required] t-area input-large b-in', 'label'=>false, 'div'=>false)); ?>
				<span id="locTextAfter"></span>
				<button class="btn btn-large btn-primary btn-danger" type="submit" onclick="timeGreater();" id="bookingFormButton">Book Interview</button>
				<?php echo $this->Form->end(); ?>
			</div>
			<?php }else { 
				if($booked['Booking']['status'] == 'Pending'){
				?>
			<div class="well">
				<?php
				$date = date("l j F Y", strtotime($booked['Booking']['date']));
				$startTime = date("g:i a",strtotime($booked['Booking']['start_time']));
				$interviewTImeS = strtotime($booked['Booking']['date'].' '.$booked['Booking']['start_time']);
				$curDateS = strtotime(date("Y-m-d h:i:s")).'<br/>';
				$timeDiffS = $interviewTImeS - $curDateS;
				$dayOneS = (24*60*60);
				?>
				<?php 
				if(strtotime($booked['Booking']['date']) < strtotime(date('Y-m-d'))){
					echo "<p style='color:red;font-size: 13px;text-align: justify;'>Booking Schedule Expired! Please update the booking status from your 'My Account' section.</p>";
				}
				?>
				<script>
				function openEditLOc(){
					$("#locWrap").fadeOut("slow", function() {
						$(this).hide();
						$("#locFieldWrap").fadeIn("slow", function() {
							$(this).show();
						});
					});
				}
				function saveEditLOc(){
					loc = $("#locTextBox").val();
					if(loc != ''){
						var dataString = "location=" + loc;
						var id = "<?php echo $booked['Booking']['id']; ?>";
						$.ajax({
							type: "POST",
							url: "<?php echo DOMAIN_NAME_PATH.'Staffs/changeLoc/'; ?>"+id,
							data: dataString,
							success: function(html){
								if(html){
									$("#locFieldWrap").fadeOut("slow", function() {
										$(this).hide();
										$("#locError").fadeOut("slow", function() {
											$(this).remove();
										});
										$("#locWrap").fadeIn("slow", function() {
											$("#locTextWrap").html(html);
											$(this).show();
										});
									});
								}
							}
						});
					} else {
						$('#locFieldWrap').append($('<p style="color:red;" id="locError">Please Insert Location</p>').fadeIn('slow'));
					}
				}
				</script>
				<h4 style="margin-top:0;font-size:16px;">BOOKED FOR INTERVIEW</h4>
				<?php echo '<p id="locWrap">Date<b>: '.$date.'</b><br/>Time: <b>'.$startTime.'</b><br/>Location: <span id="locTextWrap" style="font-weight:bold;">'.$booked['Booking']['location'].'</span> <i class="icon-edit" onclick="openEditLOc();" style="cursor:pointer;margin-top: 3px;"></i></p>';
				echo '<p id="locFieldWrap" style="display:none;"><textarea id="locTextBox" rows="6" cols="30" class="t-area input-large b-in" placeholder="Enter Location" name="editLoca">'.$booked['Booking']['location'].'</textarea><a href="javascript:void(0);" class="btn btn-small btn-primary btn-warning" onclick="saveEditLOc();">Save</a></p>';
				if($timeDiffS > $dayOneS){
					unset($bookingStatus['Cancelled']);
					echo $this->Form->create('Booking', array('id'=>'BookingCancelForm', 'action'=>'cancel/'.$booked['Booking']['id']));
						echo $this->Form->input('status', array('type' => 'hidden', 'value'=>'Cancelled'));
						echo $this->Form->input('friendly_url', array('type' => 'hidden', 'value'=>$staff['Staff']['friendly_url']));
						?><a href="#modalConfirm" role="button" data-toggle="modal" class="btn btn-large btn-primary btn-danger" id="bookingCancel" onclick="ConfirmBox('BookingCancelForm');">Cancel Interview</a><?php 
					echo $this->Form->end();
				}
				?>
			</div>
					<?php 
					} else if($booked['Booking']['status'] == 'Selected'){
						echo '<div class="well"><h4 style="margin-top:0;font-size:16px;">You already selected this domestic staff!</h4></div>';
					} else if($booked['Booking']['status'] == 'Hired'){
						echo '<div class="well"><h4 style="margin-top:0;font-size:16px;">You already hired this domestic staff!</h4></div>';
					}
				}
			} else {
				if(count($hired) != 0){
					echo '<div class="well"><h4 style="margin-top:0;font-size:16px;">An employer already hired this domestic staff!</h4></div>';
				}
				else if(count($selected)!= 0){
					echo '<div class="well"><h4 style="margin-top:0;font-size:16px;">An employer already selected this domestic staff!</h4></div>';
				} 
			}
			?>
			<?php } ?>
			<!-- section for bookimg module -->
		</div>
		<div class="span4 pull-right">
			<h4 class="hack-2">About Chef</h4>
			<p><?php echo $staff['Staff']['profile_description']; ?></p>
			<h4 class="hack-2">Spoken Languages</h4>
			<ul class="nav nav-pills">
				<?php
				if(count($spokenLanguages)>0){
					foreach($spokenLanguages as $spokenLanguage){
				?>
				<li class="disabled"><?php echo $spokenLanguage; ?></li>
				<?php 
					}
				} else {
					echo '<li style="list-style: none;">Spoken Language Not Selected!</li>';
				}
				?>
			</ul>
			<!-- <h4 class="hack-2">Preferred Locations</h4>
			<ul class="nav nav-pills">
				<?php
				if(count($preferedLocations)>0){
					foreach($preferedLocations as $preferedLocation){
				?>
				<li class="disabled"><?php echo $preferedLocation; ?></li>
				<?php 
					}
				} else {
					echo '<li>Preferred Location Not Selected!</li>';
				}
				?>
			</ul>
			<h4 class="hack-2">Preferred 'Residing LG Area'</h4>
			<ul class="nav nav-pills">
				<?php
				if(count($preferedLocalities)>0){
					foreach($preferedLocalities as $preferedLocality){
				?>
				<li class="disabled"><?php echo $preferedLocality; ?></li>
				<?php 
					}
				} else {
					echo '<li>Preferred Locality Not Selected!</li>';
				}
				?>
			</ul> -->
			<h4 class="hack-2">Work Experience</h4>
			<p><?php echo $staff['Staff']['work_experience']; ?></p>
		</div>
	</div>
</div>