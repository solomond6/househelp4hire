<div >
<h2><?php  echo __('Location'); ?></h2>
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($location['Location']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo $location['Location']['content']; ?>
			&nbsp;
		</dd>
		<dt></dt>
		<dd>
			<?php echo $this->Html->link(__('Back'), array('action' => 'index')); ?>
			&nbsp;
		</dd>
	</dl>
</div>

