<div class="vettedCandidates index">
	<h2><?php echo __('Vetted Candidates'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('vetting_category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('vetting_company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('staff_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('payment_ref'); ?></th>
			<th><?php echo $this->Paginator->sort('payment_status'); ?></th>
			<th><?php echo $this->Paginator->sort('created_at'); ?></th>
			<th><?php echo $this->Paginator->sort('modified_at'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($vettedCandidates as $vettedCandidate): ?>
	<tr>
		<td><?php echo h($vettedCandidate['VettedCandidate']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($vettedCandidate['VettingCategory']['name'], array('controller' => 'vetting_categories', 'action' => 'view', $vettedCandidate['VettingCategory']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vettedCandidate['VettingCompany']['name'], array('controller' => 'vetting_companies', 'action' => 'view', $vettedCandidate['VettingCompany']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vettedCandidate['Staff']['name'], array('controller' => 'staffs', 'action' => 'view', $vettedCandidate['Staff']['id'])); ?>
		</td>
		<td><?php echo h($vettedCandidate['VettedCandidate']['status']); ?>&nbsp;</td>
		<td><?php echo h($vettedCandidate['VettedCandidate']['payment_ref']); ?>&nbsp;</td>
		<td><?php echo h($vettedCandidate['VettedCandidate']['payment_status']); ?>&nbsp;</td>
		<td><?php echo h($vettedCandidate['VettedCandidate']['created_at']); ?>&nbsp;</td>
		<td><?php echo h($vettedCandidate['VettedCandidate']['modified_at']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $vettedCandidate['VettedCandidate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vettedCandidate['VettedCandidate']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vettedCandidate['VettedCandidate']['id']), null, __('Are you sure you want to delete # %s?', $vettedCandidate['VettedCandidate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Vetted Candidate'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Vetting Categories'), array('controller' => 'vetting_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Category'), array('controller' => 'vetting_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetting Companies'), array('controller' => 'vetting_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Company'), array('controller' => 'vetting_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
	</ul>
</div>
