     <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE-->
                <h3 class="page-title">Vetted Candidates</h3>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="fa fa-eye"></i> Vetted Candidates</h4>
                        <span class="tools">
                            <a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
                        </span>
                    </div>
                    <div class="widget-body">
						<table cellpadding="0" cellspacing="0" class="table table-striped">
						<tr>
								<th><?php echo $this->Paginator->sort('id'); ?></th>
								<th><?php echo $this->Paginator->sort('vetting_category_id'); ?></th>
								<th><?php echo $this->Paginator->sort('staff_id'); ?></th>
								<th><?php echo $this->Paginator->sort('submited_by'); ?></th>
								<th><?php echo $this->Paginator->sort('status'); ?></th>
								<th><?php echo $this->Paginator->sort('payment_ref'); ?></th>
								<th><?php echo $this->Paginator->sort('payment_status'); ?></th>
								<th><?php echo $this->Paginator->sort('created_at'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
						<?php
						foreach ($vettedCandidates as $vettedCandidate): ?>
						<tr>
							<td><?php echo h($vettedCandidate['VettedCandidate']['id']); ?>&nbsp;</td>
							<td>
								<?php echo $this->Html->link($vettedCandidate['VettingCategory']['name'], array('controller' => 'vetting_categories', 'action' => 'view', $vettedCandidate['VettingCategory']['id'])); ?>
							</td>
							<td>
								<?php echo $this->Html->link($vettedCandidate['Staff']['name'], array('controller' => 'staffs', 'action' => 'view', $vettedCandidate['Staff']['id'])); ?>
							</td>
							<td>
								<?php echo $this->Html->link($vettedCandidate['User']['name'], array('controller' => 'users', 'action' => 'view', $vettedCandidate['User']['id'])); ?>
							</td>
							<td><?php if($vettedCandidate['VettedCandidate']['status'] == '0'){
											echo '<button class="btn btn-sm btn-warning">Pending</button>';
										}elseif($vettedCandidate['VettedCandidate']['status'] == '1'){
											echo '<button class="btn btn-sm btn-success">Approved</button>';
										}elseif($vettedCandidate['VettedCandidate']['payment_status'] == '1'){
											echo '<button class="btn btn-sm btn-danger">Declined</button>';
										} ?>&nbsp;</td>
							<td><?php echo h($vettedCandidate['VettedCandidate']['payment_ref']); ?>&nbsp;</td>
							<td><?php if($vettedCandidate['VettedCandidate']['payment_status'] == '0'){
											echo '<button class="btn btn-sm btn-danger">Failed</button>';
										}elseif($vettedCandidate['VettedCandidate']['payment_status'] == '1'){
											echo '<button class="btn btn-sm btn-success">Success</button>';
										}
								?>
							&nbsp;</td>
							<td><?php echo h($vettedCandidate['VettedCandidate']['created_at']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link(__('Update'), array('action' => 'edit', $vettedCandidate['VettedCandidate']['id']), array('class' => 'btn btn-primary','escape' => FALSE)); ?>
							</td>
						</tr>
						<?php endforeach; ?>
						</table>
						<p>
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
						?>	</p>

						<div class="paging">
						<?php
							echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
							echo $this->Paginator->numbers(array('separator' => ''));
							echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
						?>
						</div>
					</div>
			</div>
		</div>
	</div>