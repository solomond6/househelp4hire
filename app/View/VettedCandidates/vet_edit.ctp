  <!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE-->
            <h3 class="page-title">Vet Management</h3>
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="fa fa-eye"></i> Update Candidate</h4>
                </div>
                <div class="widget-body">
                    <div class="row-fluid">
                    <?php echo $this->Form->create('VettedCandidate'); ?>
                    <?php $options = array('0'=>'Pending', '1'=>'Approve', '2'=>'Declined'); ?>
					<?php
						echo $this->Form->input('id');
						echo $this->Form->input('vetting_category_id', array('disabled'=>'disabled'));
						echo $this->Form->input('vetting_company_id', array('disabled'=>'disabled'));
						echo $this->Form->input('staff_id', array('disabled'=>'disabled'));
						echo $this->Form->input('staff_id', array('type'=>'hidden'));
						echo $this->Form->input('status', array('options'=>$options));
						echo $this->Form->input('comments');
					?>
					<?php echo $this->Form->end(__('Submit')); ?>
				</div>
			</div>
		</div>
	</div>
</div>
