<div class="vettedCandidates form">
<?php echo $this->Form->create('VettedCandidate'); ?>
	<fieldset>
		<legend><?php echo __('Edit Vetted Candidate'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('vetting_category_id');
		echo $this->Form->input('vetting_company_id');
		echo $this->Form->input('staff_id');
		echo $this->Form->input('status');
		echo $this->Form->input('payment_ref');
		echo $this->Form->input('payment_status');
		echo $this->Form->input('created_at');
		echo $this->Form->input('modified_at');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VettedCandidate.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('VettedCandidate.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Vetted Candidates'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Vetting Categories'), array('controller' => 'vetting_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Category'), array('controller' => 'vetting_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetting Companies'), array('controller' => 'vetting_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Company'), array('controller' => 'vetting_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
	</ul>
</div>
