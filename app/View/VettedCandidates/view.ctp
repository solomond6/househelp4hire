<div class="vettedCandidates view">
<h2><?php  echo __('Vetted Candidate'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vettedCandidate['VettedCandidate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vetting Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vettedCandidate['VettingCategory']['name'], array('controller' => 'vetting_categories', 'action' => 'view', $vettedCandidate['VettingCategory']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vetting Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vettedCandidate['VettingCompany']['name'], array('controller' => 'vetting_companies', 'action' => 'view', $vettedCandidate['VettingCompany']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Staff'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vettedCandidate['Staff']['name'], array('controller' => 'staffs', 'action' => 'view', $vettedCandidate['Staff']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($vettedCandidate['VettedCandidate']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Payment Ref'); ?></dt>
		<dd>
			<?php echo h($vettedCandidate['VettedCandidate']['payment_ref']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Payment Status'); ?></dt>
		<dd>
			<?php echo h($vettedCandidate['VettedCandidate']['payment_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created At'); ?></dt>
		<dd>
			<?php echo h($vettedCandidate['VettedCandidate']['created_at']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified At'); ?></dt>
		<dd>
			<?php echo h($vettedCandidate['VettedCandidate']['modified_at']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vetted Candidate'), array('action' => 'edit', $vettedCandidate['VettedCandidate']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vetted Candidate'), array('action' => 'delete', $vettedCandidate['VettedCandidate']['id']), null, __('Are you sure you want to delete # %s?', $vettedCandidate['VettedCandidate']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetted Candidates'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetted Candidate'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetting Categories'), array('controller' => 'vetting_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Category'), array('controller' => 'vetting_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vetting Companies'), array('controller' => 'vetting_companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vetting Company'), array('controller' => 'vetting_companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
	</ul>
</div>
