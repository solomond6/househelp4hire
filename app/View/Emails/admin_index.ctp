 <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Newsletter Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE widget-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-eye"></i>Manage Newsletter
                                    <?php echo $this->Form->create('Email'); echo $this->Form->input('ids', array('id'=>'collectedValue', 'type'=>'hidden')); ?><input type="submit" class="btn btn-danger" value="Delete All" style="margin-left: 200px;margin-top: -32px;"><?php echo $this->Form->end(); ?>
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <table class="table table-striped example1 dt-responsive" style="width: 100%;" id="example">
                                        <thead>
                                        <tr>
                                          <th style="width:8px;">
                                          <input type="checkbox" name="chkbox" id="checkboxAll" class="group-checkable" onclick="checkAllSelected();" /></th>
											<th><?php echo $this->Paginator->sort('id'); ?></th>
											<th><?php echo $this->Paginator->sort('email'); ?></th>
											<th><?php echo $this->Paginator->sort('name'); ?></th>
											<th class="actions"><?php echo __('Actions'); ?></th>
                                        </tr>
                                     </thead>
                                        <tbody>
                                        <?php
	foreach ($emails as $email): ?>
	<tr>
		<td><input type="checkbox" name="chkbox" class="chkboxVal" value="<?php echo $email['Email']['id']; ?>" /></td>
		<td><?php echo h($email['Email']['id']); ?>&nbsp;</td>
		<td><?php echo h($email['Email']['email']); ?>&nbsp;</td>
		<td><?php echo h($email['Email']['name']); ?>&nbsp;</td>
		<td class="actions">
		<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('action' => 'edit', $email['Email']['id']),array('class' => 'btn btn-success','escape' => FALSE)); ?>
			<a href="#conDialog" role="button"  class="btn btn-danger" data-toggle="modal" data-attr="<?php echo $email['Email']['id']?>" id="deleteConfirm"><i class="fa fa-trash-o" ></i> Delete</a>&nbsp;
		</td>
	</tr>
<?php endforeach; ?>
</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	
	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __(' previous '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE widget-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
                    <div id="conDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel1">Are you sure ?</h3>
            </div>
            
            <div class="modal-footer">
                <button class="btn btn-danger" onclick="javascript:Delete();">Yes</button>
                <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">No</button>
            </div>
        </div>
<script>
     var record_id="";
     jQuery(document).ready(function(){
     		jQuery("#deleteConfirm").click(function(){
     			var id=jQuery(this).attr("data-attr");
     			record_id=id;
     		})
     })
	
	function Delete()
	{
		console.log("emails/delete/"+record_id);
		document.location.href="emails/delete/"+record_id;
	}

	function checkAllSelected(){   
		if($("#checkboxAll").is(':checked')){
			$(".chkboxVal").attr("checked", true); 
			$('input:checkbox.chkboxVal').each(function () {
			   var sThisVal = (this.checked ? $(this).val() : "");
			   if($('#collectedValue').val() == ''){
				   $('#collectedValue').val(sThisVal);
			   } else {
				   $('#collectedValue').val($('#collectedValue').val()+','+sThisVal);
			   }
			});
		} else {
			$(".chkboxVal").attr("checked", false); 
			$('#collectedValue').val('');
		}
	}
	function SingleSelect(){
		$('#collectedValue').val('');
		$("#checkboxAll").attr("checked", true); 
		$('input:checkbox.chkboxVal').each(function () {
		   var sThisVal = (this.checked ? $(this).val() : "");
		   if(this.checked){
			   if($('#collectedValue').val() == ''){
				   $('#collectedValue').val(sThisVal);
			   } else {
				   $('#collectedValue').val($('#collectedValue').val()+','+sThisVal);
			   }
		   } else {
			    $("#checkboxAll").attr("checked", false); 
		   }
		});
	}
   $(document).ready(function() {
	$('input:checkbox.chkboxVal').click(function () {
		SingleSelect();
	});
	checkAllSelected();
   });
</script>