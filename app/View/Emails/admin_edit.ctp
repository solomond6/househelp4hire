<script type="text/javascript">

	$(document).ready(function() {
		$("#EmailAdminEditForm").validationEngine({scroll:true})
	});

</script>
<!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE-->
                            <h3 class="page-title">Newsletter Management</h3>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!--error msg begin-->
                            <!--
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <div class="alert alert-error span12">
                                        <button class="close" data-dismiss="alert">×</button>
                                        <strong>Error !</strong> The daily cronjob has failed.
                                    </div>
                                </div>
                            </div>
                            -->
                            <!--error msg end-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="fa fa-edit"></i> Edit E-mail</h4>
                                </div>
                                <div class="widget-body form">
                                <?php echo $this->Form->create('Email'); ?>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <div class="form-horizontal">
                                               <?php
													echo $this->Form->input('id');
													echo $this->Form->input('email', array('class' => 'span12 validate[required,custom[email]]'));
													echo $this->Form->input('name', array('class' => 'span12'));
												?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->