<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>HouseHelp4Hire</title>
	<link rel="shortcut icon" type="image/ico" href="<?php echo DOMAIN_NAME_PATH.'img/favicon.png';?>" />
	<?php echo $this->Html->css(array('style-landing', 'validationEngine.jquery')); ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<?php echo $this->Html->script(array('jquery.validationEngine', 'jquery.validationEngine-en'));	?>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#flashMessage").fadeOut(50000);
			$("#authMessage").fadeOut(50000);
			$('#overlay').click(function(){
				$('#overlay').fadeOut('fast');
			});
		});
	</script>
	<script type="text/javascript">
	   $(document).ready(function() {
		$("#EmailLandingForm").validationEngine({scroll:false, promptPosition: "topRight"})
	   });
	</script>
</head>
<body>
	<div id="wrapper">
		<?php echo $this->Session->flash(); ?>
        <div id="maindiv">
            <div class="header">
            	<div class="logo">
                <a href="javascript:void(0);"><img src="<?php echo DOMAIN_NAME_PATH; ?>img/logo2.png" width="219" height="163" alt="" border="0"/></a>
                </div>
            	<div class="com_soon">
                <img src="<?php echo DOMAIN_NAME_PATH; ?>img/comingsoon.jpg" width="526" height="108" alt="" border="0"/>
                </div>
            </div>
            <div class="clear"></div>
            <div class="banner">
            	<div class="video_box">
					<?php
					if($siteSetting['Setting']['youtube_video'] != null){
						$video = $siteSetting['Setting']['youtube_video']; 
						$pattern = "/height=\"[0-9]*\"/";
						$siteSetting['Setting']['youtube_video'] = preg_replace($pattern, "height='295'",  $siteSetting['Setting']['youtube_video']);
						$pattern = "/width=\"[0-9]*\"/";
						$siteSetting['Setting']['youtube_video'] = preg_replace($pattern, "width='390'", $siteSetting['Setting']['youtube_video']);
						if($siteSetting['Setting']['youtube_video'] != $video){
							echo $siteSetting['Setting']['youtube_video'];
						}else{
							//echo '<img src="'.DOMAIN_NAME_PATH.'img/tv.png" alt="" />';
						}
					} else {
						//echo '<img src="'.DOMAIN_NAME_PATH.'img/tv.png" alt="" />';
					}
					?>
                </div>
            </div>
            <div class="clear"></div>
            <div class="signin">
				<?php echo $this->Html->script(array('oauthpopup')); ?>
				<script type="text/javascript">
				$(document).ready(function(){
					$('#facebook').click(function(e){
						$.oauthpopup({
							path: '<?php echo DOMAIN_NAME_PATH; ?>FacebookDetails/subscribe',
							width:600,
							height:300,
							callback: function(){
								window.location.reload();
							}
						});
						e.preventDefault();
					});
				});
				</script>
				<?php echo $this->Form->create('Email', array('id'=>'EmailLandingForm')); ?>
                <ul>
                    <li><?php echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'placeholder'=>'Enter first name', 'class'=>'validate[required]')); ?></li>
                    <li><?php echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'placeholder'=>'Enter last name', 'class'=>'validate[required]')); ?></li>
                    <li><?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'placeholder'=>'Enter email address', 'class'=>'validate[required,custom[email]]')); ?></li>
                    <li><input name="" type="submit" value="Notify Me!"/></li>
                    <li><?php echo $this->Html->image('f_connect.gif',array('div' => false, 'id'=>'facebook','style'=>'cursor:pointer;margin:0 0 10px 0', 'width'=>'126', 'height'=>'29', 'border'=>'0')); ?></li>
                </ul>
				<?php echo $this->Form->end(); ?>
            </div>
            <div class="clear"></div>
        <div id="footer">
            	<div>
                <!-- <img src="<?php echo DOMAIN_NAME_PATH; ?>img/f.gif" width="38" height="35" alt="" border="0"/>
                <img src="<?php echo DOMAIN_NAME_PATH; ?>img/t.gif" width="38" height="35" alt="" border="0"/> -->
				<a href="<?php echo $siteSetting['Setting']['facebook_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/f.png" alt="" height="40" /></a>
				<a href="<?php echo $siteSetting['Setting']['twitter_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/t.png" alt="" height="40" /></a>
				<!-- <a href="<?php echo $siteSetting['Setting']['linkedin_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/l.png" alt="" height="40" /></a> -->
				<a href="<?php echo $siteSetting['Setting']['pinterest_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/p.png" alt="" height="40" /></a>
				<a href="mailto: <?php echo $siteSetting['Setting']['contact_email']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/e.png" alt="" height="40" /></a>
				<a href="<?php echo $siteSetting['Setting']['rss_link']; ?>" target="_blank" oncontextmenu="return false" ondragstart="return false" onselectstart="return false"><img style="padding:0 10px 0 0" src="<?php echo DOMAIN_NAME_PATH; ?>img/r.png" alt="" height="40" /></a>
                </div>
                <div>
                Copyright <?php echo date("Y") ?><img src="<?php echo DOMAIN_NAME_PATH; ?>img/4-Hire.png" width="54" height="28" alt="" border="0" align="absmiddle"/> All Rights Reserved               
            </div>
          </div>
        </div>
    </div>
</body>
</html>
