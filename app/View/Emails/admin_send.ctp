<div>
	<fieldset>
		<legend><?php echo __('Send Newsletter to Subscribers'); ?></legend>
		<?php 
		echo $this->Form->create('send', array('enctype' => 'multipart/form-data'));
		echo $this->Form->input('recever', array('type'=>'hidden','value'=>'1'));
		echo $this->Form->input('mailer_id', array('type'=>'select', 'options'=>$Newsletter, 'label'=>'Select any newsletter'));
		echo $this->Form->end(__('Send', true));
		?>
	</fieldset>
	<fieldset>
		<legend><?php echo __('Send Newsletter to Clients'); ?></legend>
		<?php 
		echo $this->Form->create('send', array('enctype' => 'multipart/form-data'));
		echo $this->Form->input('recever', array('type'=>'hidden','value'=>'2'));
		echo $this->Form->input('mailer_id', array('type'=>'select', 'options'=>$Newsletter, 'label'=>'Select any newsletter'));
		echo $this->Form->end(__('Send', true));
		?>
	</fieldset>
	<fieldset>
		<legend><?php echo __('Send Newsletter to All'); ?></legend>
		<?php 
		echo $this->Form->create('send', array('enctype' => 'multipart/form-data'));
		echo $this->Form->input('recever', array('type'=>'hidden','value'=>'3'));
		echo $this->Form->input('mailer_id', array('type'=>'select', 'options'=>$Newsletter, 'label'=>'Select any newsletter'));
		echo $this->Form->end(__('Send', true));
		?>
	</fieldset>
</div>
