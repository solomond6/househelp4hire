	<div class="outermain">
	<?php echo $this->Session->flash(); ?>
		<div class="main">
			<div class="content">
				<div class="leftbox">
					<div class="shoebox">
						<img src="<?php echo DOMAIN_NAME_PATH; ?>img/launch/shoe1.png" alt="img" border="0" />
						<img src="<?php echo DOMAIN_NAME_PATH; ?>img/launch/shoe2.png" alt="img" border="0" />
						<img src="<?php echo DOMAIN_NAME_PATH; ?>img/launch/shoe3.png" alt="img" border="0" />
					</div>
				</div>
				<div class="midbox">
					<div class="logo">
						<a href="#"><img src="<?php echo DOMAIN_NAME_PATH; ?>img/launch/logo.png" alt="logo" border="0" /></a>
					</div>
					<div class="midtext">
						<ul>
							<li>Ephemeral</li>
							<li>Unique</li>
							<li>Exclusive</li>
						</ul>
					</div>
					<div class="midtext1" style="font-family:Arial;font-size:30px;"><?php echo $settingMain['Setting']['landing_page_success_massage']; ?></div>
					<div class="midtext2"></div>
					<div style="float:left;width:100%;text-align:center;margin-top:20px;">
						<div style="margin:0 auto; width:240px;">
							<div style="float:left;">
							<div id="fb-root"></div>
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=207277086086544";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>
								<div class="fb-like" data-href="http://sixsnowflakes.com" data-width="450" data-layout="button_count" data-action="recommend" data-show-faces="false" data-send="false"></div>
							</div>
							<div style="float:left;margin-left:10px;">
								<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://sixsnowflakes.com" data-via="pbsourav">Tweet</a>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
					<div class="midtext3" style="font-family:Arial;margin-top:30px;font-size:35px;">You curate the line!<br/>We make the styles exclusively for you!</div>
				</div>
				 <div class="rightbox">
					<div class="bagbox">
						<img src="<?php echo DOMAIN_NAME_PATH; ?>img/launch/bag1.png" alt="img" border="0" />
						<img src="<?php echo DOMAIN_NAME_PATH; ?>img/launch/bag2.png" alt="img" border="0" />
						<img src="<?php echo DOMAIN_NAME_PATH; ?>img/launch/bag3.png" alt="img" border="0" />
					</div>
				</div> 
			</div>
		</div>
	</div>