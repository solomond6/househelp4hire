$(document).ready(function () {
    $("#btnSend").click(function () {
        if(forgotPasswordValidate())
        forgotPassword();
    })
})

function forgotPassword()
{
    var param = {};
    var email = $("#txtEmail").val();
    param.email = $.trim(email);
    param.mode = "forgotPassword";
    $.post("functionality.php",param,function(data){
        
    })
}

function forgotPasswordValidate()
{
    var result = true;
    var email = $("#txtEmail").val();
    $("#txtEmail").css("border-color", "");
    
    if (email == "") {
        $("#txtEmail").attr("placeholder", "Email is Mandatory");
        $("#txtEmail").css("border-color", "red");
        $("#txtEmail").focus();
        result = false;
    }
    else if (!validateEmail(email)) {
    $("#txtEmail").val("");
        $("#txtEmail").attr("placeholder", "Enter correct Email");
        $("#txtEmail").css("border-color", "red");
        $("#txtEmail").focus();
        result = false;
    }
    return result;
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($email)) {
        return false;
    }
    else {
        return true;
    }
}