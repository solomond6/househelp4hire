        
        <?php zilla_content_end(); ?>
		<!-- END #content -->
		</div>
		
		<?php zilla_footer_before(); ?>
		<!-- BEGIN #footer -->
		<div id="footer">
	    <?php zilla_footer_start(); ?>
		    
		    <?php 
    		    $footer_feature_posts = zilla_get_option('footer_feature_posts');
                get_template_part('content', 'footer-feature');
			?>
			
		    <div class="footer-widgets">
		    	<div class="inner clearfix">
					<div class="col"><?php dynamic_sidebar( 'footer1' ); ?></div>
					<div class="col middle"><?php dynamic_sidebar( 'footer2' ); ?></div>
					<div class="col last"><?php dynamic_sidebar( 'footer3' ); ?></div>
				</div>
		    </div>
		    
		    <div class="footer-bottom inner" style="color:#333333;text-align:center;font-size: 16px;line-height: 20px;font-family: 'verdanaregular';text-transform: none;">
				Copyright <span id="copyright_date"></span> <img width="50" onselectstart="return false" ondragstart="return false" oncontextmenu="return false" alt="" src="<?php echo DOMAIN_NAME_PATH; ?>img/4-Hire.png" style="padding:0 10px 0 0; vertical-align:-5px;">. All Rights Reserved &nbsp;&nbsp; <img alt="" src="http://househelp4hire.com/img/interswitch_logo.png" style="
				height: auto;  max-width: 100%;  vertical-align: middle;  border: 0;">
			</div>
		
		<?php zilla_footer_end(); ?>
		<!-- END #footer -->
		</div>
		<?php zilla_footer_after(); ?>
		
	<!-- END #container -->
	</div> 
		
	<!-- Theme Hook -->
	<?php wp_footer(); ?>
	<?php zilla_body_end(); ?>
	
<!--END body-->
	<script>
		function setCopyRightDate()
		{
			var d = new Date();
			var n = d.getFullYear();
			jQuery("#copyright_date").text(n);
		}
		setCopyRightDate();
</script>
</body>
<!--END html-->
</html>