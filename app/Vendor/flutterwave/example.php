<?php
require_once 'vendor/autoload.php';

use Flutterwave\Card;
use Flutterwave\Flutterwave;
use Flutterwave\AuthModel;
use Flutterwave\Currencies;
use Flutterwave\Countries;
use Flutterwave\FlutterEncrypt;
//use Flutterwave\ApiRequest;
// $merchantKey = "tk_fLRnODPQzv";
// $apiKey = "tk_QoGhkH36uy8rEIW9LtZN";
// $env = "staging"; //this can be production when ready for deployment
// Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);

// $result = Banks::allBanks();
//$result is an instance of ApiResponse class which has
//methods like getResponseData(), getStatusCode(), getResponseCode(), isSuccessfulResponse()

$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
$env = "staging"; //can be staging or production
Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);

$card = [
  "card_no" => "5840 4061 8755 3286",
  "cvv" => "116",
  "expiry_month" => "09",
  "expiry_year" => "18",
  "pin" =>"1111",
  "card_type" => "" //optional parameter. only needed if card was issued by diamond card
];
$custId = "76464"; //your users customer id
$currency = Currencies::NAIRA; //currency to charge the card
$authModel = AuthModel::PIN; //can be BVN, NOAUTH, PIN, etc
$narration = "narration for this transaction";
$responseUrl = "http://localhost/flutterwave/example.php"; //callback url
$country = Countries::NIGERIA;
$amount =1000;
$response = Card::charge($card, $amount, $custId, $currency, $country, $authModel, $narration, $responseUrl);

//Important note
//when using VBVSECURECODE as the AuthModel you will need to decrypt responsehtml field in the response data
//of a successful request and load it in an iFrame or redirect your user to the auth url parameter
//your key here is your apiKey from flutterwave dashboard
echo json_encode($response->getResponseData());
// $plain = FlutterEncrypt::decrypt3Des($responsehtml, $key);
// var_dump($plain);
?>