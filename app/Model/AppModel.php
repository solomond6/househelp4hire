<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {	
		/*******************************************************************
		* Function: generateList()
		* Params : $options - model options for data retrieve
		* $key - key field
		* $format - output data format for value
		*/
		function generateList($options, $key, $format)
		{
			// Set recursive
			if (!empty($options['recursive']))
			{
				$this->recursive = $options['recursive'];
			}
			else
			{
				$this->recursive = -1;
			}
			// Retrieve all data
			$data = $this->find('all', $options);
			if (!empty($data))
			{
				// Clear output array
				$list = array();
				// Get all fields
				$fields = $options['fields'];
				// Extract key model & field
				list($key_model, $key_field) = explode('.', $key);
				// Read all items
				foreach($data as $item)
				{
				// Temp var for replacing field values
				$output = $format;
				
				// Read all field names
				foreach($fields as $field)
				{
				// Extract model & field from field's name
				list($m, $f) = explode('.', $field);
				// Get value for field from $data
				$value = $item[$m][$f];
				// Make string for replace
				$field = '%' . $field . '%';
				// Replace %field% with $value
				$output = str_replace($field, $value, $output);
				}
				// Get value for key
				$key_value = $item[$key_model][$key_field];
				// Make output array item
				$list[$key_value] = $output;
				}
				// Return list
				return $list;
		} else {
			return false;
		}
	}

	function getLastQuery()
	{
	    $dbo = $this->getDatasource();
	    $logs = $dbo->getLog();
	    $lastLog = end($logs['log']);
	    return $lastLog['query'];
	}
}
