<?php
App::uses('AppModel', 'Model');
/**
 * VettedCandidate Model
 *
 * @property VettingCategory $VettingCategory
 * @property VettingCompany $VettingCompany
 * @property Staff $Staff
 */
class VettedCandidate extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'VettingCategory' => array(
			'className' => 'VettingCategory',
			'foreignKey' => 'vetting_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'VettingCompany' => array(
			'className' => 'VettingCompany',
			'foreignKey' => 'vetting_company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
