<?php
App::uses('AppModel', 'Model');
/**
 * Rating Model
 *
 * @property User $User
 * @property RatingConfig $RatingConfig
 */
class Rating extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'value';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RatingConfig' => array(
			'className' => 'RatingConfig',
			'foreignKey' => 'rating_config_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
