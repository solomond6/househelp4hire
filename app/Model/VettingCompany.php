<?php
App::uses('AppModel', 'Model');
/**
 * VettingCompany Model
 *
 * @property VettingCategory $VettingCategory
 */
class VettingCompany extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'VettingCategory' => array(
			'className' => 'VettingCategory',
			'foreignKey' => 'vetting_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
