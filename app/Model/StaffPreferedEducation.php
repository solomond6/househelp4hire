<?php
class StaffPreferedEducation extends AppModel {
	public $name = 'StaffPreferedEducation';
	public $primaryKey = 'id';
	var $belongsTo = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Education' => array(
			'className' => 'Education',
			'foreignKey' => 'education_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>