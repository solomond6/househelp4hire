<?php
class StaffPreferedLocality extends AppModel {
	public $name = 'StaffPreferedLocality';
	public $primaryKey = 'id';
	var $belongsTo = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Location' => array(
			'className' => 'Location',
			'foreignKey' => 'locality_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>