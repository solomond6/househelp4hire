<?php
App::uses('AppModel', 'Model');
/**
 * RatingConfig Model
 *
 */
class RatingConfig extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';

}
