<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	//Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
	
	Router::connect('/', array('controller' => 'users', 'action' => 'home', 'admin' => false));
	Router::connect('/admin', array('controller' => 'Staffs', 'action' => 'index', 'prefix' => 'admin', 'admin' => true));
	//Router::connect('/', array('controller' => 'Emails', 'action' => 'landing', 'admin' => false));
	// Router::connect('/', array('controller' => 'staffs', 'action' => 'index', 'admin' => false));
	Router::connect('/my-account', array('controller' => 'Users', 'action' => 'dashboard', 'admin' => false));
	Router::connect('/my-account', array('controller' => 'users', 'action' => 'dashboard', 'admin' => false));
	Router::connect('/login-register', array('controller' => 'users', 'action' => 'login', 'admin' => false));
	Router::connect('/Staffs/loadmore/*', array('controller' => 'Staffs', 'action' => 'loadmore', 'admin' => false));
	Router::connect('/Staffs/loadmore/*', array('controller' => 'staffs', 'action' => 'loadmore', 'admin' => false));
	Router::connect('/domestic-staffs', array('controller' => 'Staffs', 'action' => 'index', 'admin' => false));
	Router::connect('/domestic-staffs', array('controller' => 'staffs', 'action' => 'index', 'admin' => false));
	Router::connect('/domestic-staffs/*', array('controller' => 'staffs', 'action' => 'index', 'admin' => false));
	//Router::connect('/:friendlyurl', array('controller' => 'Staffs', 'action' => 'view', 'admin' => false), array('pass' => array('friendlyurl'),'friendlyurl' => '[^\/]*'));
	//Router::connect('/*', array('controller' => 'Staffs', 'action' => 'view'));
	Router::connect('/:friendlyurl', array('controller' => 'Staffs', 'action' => 'view', 'admin' => false), array('pass' => array('friendlyurl'),'friendlyurl' => '[^\/]*'));

/* Paypal IPN plugin */
Router::connect('/paypal_ipn/process', array('plugin' => 'paypal_ipn', 'controller' => 'instant_payment_notifications', 'action' => 'process'));
/* Optional Route, but nice for administration */
Router::connect('/paypal_ipn/:action/*', array('admin' => 'true', 'plugin' => 'paypal_ipn', 'controller' => 'instant_payment_notifications', 'action' => 'index'));
//Router::connect('/termsofservice', array('controller' => 'pages', 'action' => 'display', 'termsofservice'));
/* End Paypal IPN plugin */
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
