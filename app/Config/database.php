<?php
class DATABASE_CONFIG {
	
	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => 'root',
		'database' => 'hh4h',
		'prefix' => 'hh_',
		'encoding' => 'utf8',
	);
	/*
	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'hh4h.db.12425539.hostedresource.com',
		'login' => 'hh4h',
		'password' => 'Hh4h123@',
		'database' => 'hh4h',
		'prefix' => 'hh_',
		'encoding' => 'utf8',
	);
	*/
	/*public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'nexapp_dev',
		'password' => 'MBmNhDybayXQ',
		'database' => 'nexapp_hh4h',
		'prefix' => 'hh_',
		'encoding' => 'utf8',
	);*/

	/*public $test = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'user',
		'password' => 'password',
		'database' => 'test_database_name',
		'prefix' => '',
		//'encoding' => 'utf8',
	);*/
}
