<?php
class RecaptchaComponent extends Component {
	function beforeValidate(&$model) {
        $model->validate['recaptcha_response_field'] = array(
            'checkRecaptcha' => array(
                'rule' => array('checkRecaptcha', 'recaptcha_challenge_field'),
                'message' => 'You did not enter the words correctly. Please try again.',
            ),
        );
    } 
	function startup(&$controller) {
		if (isset($controller->request->data['recaptcha_challenge_field']) && isset($controller->request->data['recaptcha_response_field'])) {
			$modelClass = $controller->modelClass;
			$controller->request->data[$modelClass]['recaptcha_challenge_field'] = $controller->request->data['recaptcha_challenge_field'];
			$controller->request->data[$modelClass]['recaptcha_response_field'] = $controller->request->data['recaptcha_response_field'];
			$controller->$modelClass->Behaviors->attach('Recaptcha.Validation');
		}
	}
}
