<?php
App::uses('AppController', 'Controller');
/**
 * TestImages Controller
 *
 * @property TestImage $TestImage
 */
class TestImagesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TestImage->recursive = 0;
		$this->set('testImages', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->TestImage->id = $id;
		if (!$this->TestImage->exists()) {
			throw new NotFoundException(__('Invalid test image'));
		}
		$this->set('testImage', $this->TestImage->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TestImage->create();
			if ($this->TestImage->save($this->request->data)) {
				$this->Session->setFlash(__('The test image has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The test image could not be saved. Please, try again.'));
			}
		}
		$tests = $this->TestImage->Test->find('list');
		$this->set(compact('tests'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->TestImage->id = $id;
		if (!$this->TestImage->exists()) {
			throw new NotFoundException(__('Invalid test image'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TestImage->save($this->request->data)) {
				$this->Session->setFlash(__('The test image has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The test image could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->TestImage->read(null, $id);
		}
		$tests = $this->TestImage->Test->find('list');
		$this->set(compact('tests'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TestImage->id = $id;
		if (!$this->TestImage->exists()) {
			throw new NotFoundException(__('Invalid test image'));
		}
		if ($this->TestImage->delete()) {
			$this->Session->setFlash(__('Test image deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Test image was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
