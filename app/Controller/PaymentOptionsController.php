<?php
class PaymentOptionsController extends AppController {

	var $name = 'PaymentOptions';

	function admin_index() {
		$this->PaymentOption->recursive = 0;
		$this->paginate = array('order'=>array('type'=>'ASC'));
		$this->set('paymentOptions', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid payment option', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('paymentOption', $this->PaymentOption->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			if($this->data['PaymentOption']['validity'] == ''){
				$this->data['PaymentOption']['validity'] = null;
			}
			if($this->data['PaymentOption']['quantity'] == ''){
				$this->data['PaymentOption']['quantity'] = null;
			}
			$this->PaymentOption->create();
			if ($this->PaymentOption->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The payment option has been saved successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The payment option could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid payment option', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if($this->data['PaymentOption']['validity'] == ''){
				$this->data['PaymentOption']['validity'] = null;
			}
			if($this->data['PaymentOption']['quantity'] == ''){
				$this->data['PaymentOption']['quantity'] = null;
			}
			if ($this->PaymentOption->save($this->data)) {
				$this->Session->setFlash(sprintf(__('The payment option has been updated successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The payment option could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->PaymentOption->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for payment option', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->PaymentOption->delete($id)) {
			$this->Session->setFlash(sprintf(__('The payment option has been deleted successfully.', true)), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Payment option was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>