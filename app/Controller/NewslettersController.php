<?php
App::uses('AppController', 'Controller');
/**
 * Newsletters Controller
 *
 * @property Newsletter $Newsletter
 */
class NewslettersController extends AppController {
	var $uses = array('Newsletter', 'Mailer', 'User', 'Email');
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Newsletter->recursive = 0;
		$this->set('newsletters', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Newsletter->id = $id;
		if (!$this->Newsletter->exists()) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		$this->set('newsletter', $this->Newsletter->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Newsletter->create();
			if ($this->Newsletter->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('Newsletter Added Successfully.', true), 'Newsletter'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The newsletter could not be saved. Please, try again.'));
			}
		}
		$mailers = $this->Newsletter->Mailer->find('list');
		$this->set(compact('mailers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Newsletter->id = $id;
		if (!$this->Newsletter->exists()) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Newsletter->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('Newsletter Updated Successfully.', true), 'Newsletter'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The newsletter could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Newsletter->read(null, $id);
		}
		$mailers = $this->Newsletter->Mailer->find('list');
		$this->set(compact('mailers'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Newsletter->id = $id;
		if (!$this->Newsletter->exists()) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		if ($this->Newsletter->delete()) {
			$this->Session->setFlash(sprintf(__('Newsletter Deleted Successfully.', true), 'Newsletter'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Newsletter was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function admin_send() {
		if(!empty($this->request->data)){
			if($this->request->data['send']['mode'] == 1){
				$userEmailArr = array();
				if(isset($this->request->data['send']['file']['name']) && $this->request->data['send']['file']['name'] != null){
					if($this->request->data['send']['file']['type'] == 'application/csv' || $this->request->data['send']['file']['type'] == 'application/vnd.ms-excel' || $this->request->data['send']['file']['type'] == 'application/octet-stream'|| $this->request->data['send']['file']['type'] == 'application/x-msexcel'){
						App::import('Vendor', 'reader');
						$excel = new Spreadsheet_Excel_Reader();
						$excel->read($this->request->data['send']['file']['tmp_name']);   
						$results = $excel->sheets[0]['cells'];
						if(count($results)>0){
							foreach($results as $result){
								$userEmailArr[] = $result[2];
							}
							if (filter_var($userEmailArr[0], FILTER_VALIDATE_EMAIL)) {
							} else {
								unset($userEmailArr[0]);
							}
						} else {
							$this->Session->setFlash(__('Upload csv file containing no value in its first coloumn (coloumn A).', true));
						}
					} else {
						$this->Session->setFlash(__('Please upload a xls file.', true));
					}
				} else {
					$this->Session->setFlash(__('Please upload a csv file.', true));
				}
				$userEmail = array_unique($userEmailArr);
				$userArray = $userEmail;
			}
			if($this->request->data['send']['mode'] == 2){
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'conditions'=>array('User.newslater_subscripyion' => 'Y'), 'order'=>array('User.id'=>'desc')));
			}
			if($this->request->data['send']['mode'] == 3){
				$idArr = $this->request->data['send']['user_id'];
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'conditions'=>array('User.id' => $idArr), 'order'=>array('User.id'=>'desc')));
			}
			if($this->request->data['send']['mode'] == 4){
				$numberOfUser = $this->request->data['send']['number_of_user'];
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'limit'=>$numberOfUser, 'order'=>'rand()'));
			}
			if($this->request->data['send']['mode'] == 5){
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'order'=>array('User.id'=>'desc')));
			}
			if($this->request->data['send']['mode'] == 6){
				$userArray = $this->Email->find('all', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'desc')));
			}
			if($this->request->data['send']['mode'] == 7){
				$userArray = $this->Email->find('all', array('fields'=>'Email.email', 'conditions'=>array('Email.id'=>$this->request->data['send']['email_id']), 'order'=>array('Email.id'=>'desc')));
			}
			$siteSetting = $this->viewVars['siteSetting'];
			$this->Newsletter->recursive = 0;
			$mailer = $this->Newsletter->find('first', array('conditions'=>array('Newsletter.id'=>$this->request->data['send']['newsletters_id'])));
			$html = $mailer['Mailer']['html_content'];
			$logoPath = DOMAIN_NAME_PATH.'img/site_logo/'.$siteSetting['Setting']['site_logo'];
			$subject = $mailer['Newsletter']['name'];
			$html = str_replace("##logo_path##", $logoPath, $html);
			$html = str_replace("##noreply_email##", $siteSetting['Setting']['noreply_email'], $html);
			$html = str_replace("##subject##", $subject, $html);
			if(count($userArray) > 0){
				$mail_From = $siteSetting['Setting']['noreply_email'];
				$mail_CC = null;
				foreach($userArray as $user){
					if(isset($user['User']['email'])){
						$mail_To = $user['User']['email'];
						if($user['User']['first_name'] != '' && $user['User']['last_name'] != ''){
							$userName = $user['User']['first_name'].' '.$user['User']['last_name'];
						} else {
							$userName = $user['User']['email'];
						}
					} else if(isset($user['Email']['email'])){
						$mail_To = $user['Email']['email'];
					} else {
						$userName = $user;
						$mail_To = $user;
					}
					$string = $mail_To;
					$encryptKey = 'ab12333way2code';
					//$convertedString = Security::cipher($string, $encryptKey);
					$convertedString = bin2hex(Security::cipher(serialize($string),Configure::read('Security.salt')));
					$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>Click here</a>";
					$mail_body = str_replace("##unsubscribe##", $link, $html);
					$extra_content = "<p style='font-size:11px;text-align:center;'>
										<a href='".$siteSetting['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$siteSetting['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
										<a href='".$siteSetting['Setting']['pinterest_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/p.png' alt='pinterest' height='48' /></a>
										<a href='mailto: ".$siteSetting['Setting']['contact_email']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/e.png' alt='email' height='48' /></a>
										<a href='".$siteSetting['Setting']['rss_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/r.png' alt='rss' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>";
					$mail_body = str_replace("##extra_content##", $extra_content, $html);
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
				}
				$this->Session->setFlash(sprintf(__('Newsletter has been sent Successfully.', true), 'Newsletter'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'send'));
			}
		}
		$users = $this->User->find('list', array('fields'=>'User.email', 'order'=>array('User.id'=>'desc')));
		$emails = $this->Email->find('list', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'desc')));
		$newsletters = $this->Newsletter->find('list', array('order'=>array('Newsletter.id'=>'desc')));
		$this->set(compact('users', 'newsletters', 'emails'));
	}
}
