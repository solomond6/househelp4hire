<?php
App::uses('AppController', 'Controller');
/**
 * RatingConfigs Controller
 *
 * @property RatingConfig $RatingConfig
 */
class RatingConfigsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RatingConfig->recursive = 0;
		$this->set('ratingConfigs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->RatingConfig->id = $id;
		if (!$this->RatingConfig->exists()) {
			throw new NotFoundException(__('Invalid rating config'));
		}
		$this->set('ratingConfig', $this->RatingConfig->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RatingConfig->create();
			if ($this->RatingConfig->save($this->request->data)) {
				$this->Session->setFlash(__('The rating config has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rating config could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->RatingConfig->id = $id;
		if (!$this->RatingConfig->exists()) {
			throw new NotFoundException(__('Invalid rating config'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RatingConfig->save($this->request->data)) {
				$this->Session->setFlash(__('The rating config has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rating config could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RatingConfig->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RatingConfig->id = $id;
		if (!$this->RatingConfig->exists()) {
			throw new NotFoundException(__('Invalid rating config'));
		}
		if ($this->RatingConfig->delete()) {
			$this->Session->setFlash(__('Rating config deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Rating config was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RatingConfig->recursive = 0;
		$this->set('ratingConfigs', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->RatingConfig->id = $id;
		if (!$this->RatingConfig->exists()) {
			throw new NotFoundException(__('Invalid rating config'));
		}
		$this->set('ratingConfig', $this->RatingConfig->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RatingConfig->create();
			if ($this->RatingConfig->save($this->request->data)) {
				$this->Session->setFlash(__('The rating config has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rating config could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->RatingConfig->id = $id;
		if (!$this->RatingConfig->exists()) {
			throw new NotFoundException(__('Invalid rating config'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->RatingConfig->save($this->request->data)) {
				$this->Session->setFlash(__('The rating config has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rating config could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->RatingConfig->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->RatingConfig->id = $id;
		if (!$this->RatingConfig->exists()) {
			throw new NotFoundException(__('Invalid rating config'));
		}
		if ($this->RatingConfig->delete()) {
			$this->Session->setFlash(__('Rating config deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Rating config was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
