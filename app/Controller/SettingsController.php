<?php
class SettingsController extends AppController {

	var $name = 'Settings';
	var $uses = array('Setting', 'User');

	/**** admin code starts ****/

	function admin_index() 
	{
		$id = '1';
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid page', true));
			$this->redirect(array('action' => 'edit',1));
		}
		if (!empty($this->request->data)) {
			$account = $this->Setting->find('first',array('conditions'=>array('Setting.id'=>1)));
			if(isset($this->request->data['Setting']['site_logo']['name']) && $this->request->data['Setting']['site_logo']['name'] != ''){
				if($this->request->data['Setting']['site_logo']['type'] == 'image/png' || $this->request->data['Setting']['site_logo']['type'] == 'image/PNG' || $this->request->data['Setting']['site_logo']['type'] == 'image/jpg' || $this->request->data['Setting']['site_logo']['type'] == 'image/JPG' || $this->request->data['Setting']['site_logo']['type'] == 'image/jpeg' || $this->request->data['Setting']['site_logo']['type'] == 'image/JPEG'){
					if($account['Setting']['site_logo'] != ''){
						unlink(SITE_LOGO.$account['Setting']['site_logo']);
					}
					move_uploaded_file($this->request->data['Setting']['site_logo']['tmp_name'], SITE_LOGO.$this->request->data['Setting']['site_logo']['name']);
					$this->request->data['Setting']['site_logo'] = $this->request->data['Setting']['site_logo']['name'];
				}
				else
				{
					$this->request->data['Setting']['site_logo'] = $account['Setting']['site_logo'];
					$fileTypeError = true;
				}
			}
			else{
				$this->request->data['Setting']['site_logo'] = $account['Setting']['site_logo'];
			}
			if(isset($this->request->data['Setting']['brochure']['name']) && $this->request->data['Setting']['brochure']['name'] != ''){
				//if($this->request->data['Setting']['brochure']['type'] == 'image/png'){
					if($account['Setting']['brochure'] != ''){
						unlink(PDF_UP.$account['Setting']['brochure']);
					}
					move_uploaded_file($this->request->data['Setting']['brochure']['tmp_name'], PDF_UP.$this->request->data['Setting']['brochure']['name']);
					$this->request->data['Setting']['brochure'] = $this->request->data['Setting']['brochure']['name'];
				//}
				//else
				//{
				//	$this->request->data['Setting']['brochure'] = $account['Setting']['brochure'];
				//	$fileTypeError2 = true;
				//}
			}
			else{
				$this->request->data['Setting']['brochure'] = $account['Setting']['brochure'];
			}
			if ($this->Setting->save($this->request->data)) {
				if(isset($fileTypeError) && $fileTypeError == true){
					$this->Session->setFlash(__('Please upload png or jpg image.', true));
				}
				else{
					$this->Session->setFlash(sprintf(__('Settings updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
				}
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Settings could not be updated. Please, try again.', true));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Setting->read(null, $id);
		}
	}

	function admin_meta() 
	{
		if(empty($this->request->data))
		{
			$this->request->data=$this->Setting->read(null, 1);
		}
		else if(isset($this->request->data) && !empty($this->request->data))
		{			
			//pr($this->request->data); //exit;
			if($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The title and meta has been updated', true), 'default', array('class' => 'success'));
				$this->redirect(array('action'=>'meta'));
			}
			else
			{
				$this->Session->setFlash(__('The title and meta were not updated', true));
				$this->redirect(array('action'=>'meta'));
			}
		}
	}
	
	/**** admin code ends ****/

}
?>