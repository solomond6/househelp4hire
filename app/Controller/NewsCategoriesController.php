<?php
class NewsCategoriesController extends AppController {

	public $name = 'NewsCategories';
	public $uses = array('NewsCategory', 'News');
	public $helpers = array('Html', 'Form');
	public $components = array(
		'Session', 
	);
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	function admin_index($search = null) 
	{
		$this->NewsCategory->recursive=0;	
		$this->paginate=array('order' => array('NewsCategory.id' => 'ASC'));
		$this->set('newsCategories', $this->paginate());
	}
	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid News Category.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('newsCategory', $this->NewsCategory->read(null, $id));
	}
	function admin_add() {
		$this->NewsCategory->recursive=0;
		if(isset($this->request->data) && !empty($this->request->data)){
			if(empty($this->request->data['NewsCategory']['name'])){
				$this->Session->setFlash(__('Please enter a category name for the News', true));
			}
			if($this->NewsCategory->save($this->request->data)){
				$this->Session->setFlash(sprintf(__('News Category has been saved successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		}
	}
	function admin_edit($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for News Category', true));
			$this->redirect(array('action'=>'index'));
		}
		if(isset($this->request->data) && !empty($this->request->data)){			
			if($this->NewsCategory->save($this->request->data)){
				$this->Session->setFlash(sprintf(__('News Category has been saved successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		} else {
			$this->request->data=$this->NewsCategory->read(null, $id);
		}
	}
    function admin_deactivate($id = null)
	{
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid News Category', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->NewsCategory->id=$id;
			$this->NewsCategory->saveField('status',0);
			
			$this->Session->setFlash(sprintf(__('News Category deactivated successfully!', true)), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

	function admin_activate($id = null)
	{
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid News Category', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->NewsCategory->id=$id;
			$this->NewsCategory->saveField('status',1);	
			$this->Session->setFlash(sprintf(__('News Category activated successfully!', true)), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}
    function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid News Category', true));
			$this->redirect(array('action'=>'index'));
		}
		$news = $this->NewsCategory->read(null, $id);
		$this->News->deleteAll(array('news_category_id'=>$id));
		if ($this->NewsCategory->delete($id)) {
			$this->Session->setFlash(__('News Category deleted successfully', true), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('News Category could not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>