<?php
App::uses('AppController', 'Controller');
/**
 * Locations Controller
 *
 * @property Location $Location
 */
class EducationsController extends AppController {
	public $uses = array('Education');

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Education->recursive = 0;
		$this->set('educations', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Education->id = $id;
		if (!$this->Education->exists()) {
			$this->Session->setFlash(__('Invalid Education'));
		}
		$this->set('education', $this->Education->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Education->create();
			if ($this->Education->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The education has been saved successfully!', true), 'Education'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The education could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Education->id = $id;
		if (!$this->Education->exists()) {
			$this->Session->setFlash(__('Invalid Education'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Education->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The education has been updated successfully!', true), 'Education'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The education could not be updated. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Education->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			$this->Session->setFlash(__('Invalid Education'));
		}
		$this->Education->id = $id;
		if (!$this->Education->exists()) {
			$this->Session->setFlash(__('Invalid Education'));
		}
		if ($this->Education->delete()) {
			$this->Session->setFlash(sprintf(__('The Education has been deleted successfully!', true), 'Education'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Education was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function admin_import_export($pass = null) {
		$model = $this->modelClass;
		$this->backup($model);
		$this->set('modelName', $model);
	}
}
