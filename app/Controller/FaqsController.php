<?php
class FaqsController extends AppController {

	public $name = 'Faqs';
	public $uses = array('Faq');
	public $helpers = array('Html', 'Form');
	public $components = array(
		'Session', 
	);
	public function beforeFilter() {
		parent::beforeFilter();
	}
	function index() 
	{
		$this->Faq->recursive=0;	
		$this->paginate=array('conditions' => array('Faq.status' => 1), 'order' => array('Faq.id' => 'ASC'));
		$this->set('faqs', $this->paginate());
	}
	function admin_index($search = null) 
	{
		$this->Faq->recursive=0;	
		$this->paginate=array('order' => array('Faq.id' => 'ASC'));
		$this->set('Faqs', $this->paginate());
	}
	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid FAQ.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('faq', $this->Faq->read(null, $id));
	}
	function admin_add() {
		if(isset($this->request->data) && !empty($this->request->data)){
			if($this->Faq->save($this->request->data)){
				$this->Session->setFlash(sprintf(__('FAQ has been saved successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		}
	}
	function admin_edit($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Id', true));
			$this->redirect(array('action'=>'index'));
		}
		if(isset($this->request->data) && !empty($this->request->data)){			
			if($this->Faq->save($this->request->data)){
				$this->Session->setFlash(sprintf(__('FAQ has been saved successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		} else {
			$this->request->data=$this->Faq->read(null, $id);
		}
	}
    function admin_deactivate($id = null)
	{
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid Id', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->Faq->id=$id;
			$this->Faq->saveField('status',0);
			
			$this->Session->setFlash(sprintf(__('FAQ has been deactivated successfully!', true)), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

	function admin_activate($id = null)
	{
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid Id', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->Faq->id=$id;
			$this->Faq->saveField('status',1);	
			$this->Session->setFlash(sprintf(__('FAQ has been activated successfully!', true)), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}
    function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Faq->delete($id)) {
			$this->Session->setFlash(__('FAQ has been deleted successfully', true), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('FAQ could not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>