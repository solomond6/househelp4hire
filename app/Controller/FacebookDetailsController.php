<?php
/* -----------------------------------------------------------------------------------------
   IdiotMinds - http://idiotminds.com
   -----------------------------------------------------------------------------------------
*/
//App::uses('Controller', 'Controller');
App::import('Vendor', 'Facebook',array('file'=>'Facebook'.DS.'facebook.php'));	


class FacebookDetailsController extends AppController {
	public $components = array('Session');
	public $name = 'FacebookDetails';
	public $uses = array('FacebookDetail', 'User', 'Setting', 'Email');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
            'index',
            'login',
			'facebookConnect',
			'subscribe',
			'subscribeUsingFacebook'
            );
	}

	public function index(){
		$this->layout=false;	  	
	}
	
	function login()	{
		Configure::load('facebook');
		$appId=Configure::read('Facebook.appId');
		$app_secret=Configure::read('Facebook.secret');
		$facebook = new Facebook(array(
				'appId'		=>  $appId,
				'secret'	=> $app_secret,
				));
		$loginUrl = $facebook->getLoginUrl(array(
			'scope'			=> 'email,read_stream, publish_stream, user_birthday, user_location, user_work_history, user_hometown, user_photos',
			'redirect_uri'	=> DOMAIN_NAME_PATH.'FacebookDetails/facebookConnect',
			'display'=>'popup'
			));
		$this->redirect($loginUrl);
   	}
	
	function facebookConnect()	{
		$this->layout = false;
	    Configure::load('facebook');
	    $appId=Configure::read('Facebook.appId');
	    $app_secret=Configure::read('Facebook.secret');	   
	   	 $facebook = new Facebook(array(
		'appId'		=>  $appId,
		'secret'	=> $app_secret,
		));	
	    $user = $facebook->getUser();		
		if($user){
	     	try{
					$me = $facebook->api('/me');
					//pr($me);
					/*$friends = $facebook->api('me/friends?limit=5000');
					$friendIds = array();
					foreach ($friends['data'] as $friend) {
						$friendIds[] = $friend['id'];
					}

					// get info of all the friends without using any access token
					$friendIds = implode(',', $friendIds);
					$fql = "SELECT uid, first_name, last_name, email FROM user WHERE uid IN ($friendIds)";
					$friendsNew = $facebook->api(array(
					  'method' => 'fql.query',
					  'query'  => $fql,
					  // don't use an access token
					));
					// we should now have a list of friend infos with their email addresses
					pr($friendsNew);*/
					//exit;
					$params=array('next' => DOMAIN_NAME_PATH.'Users/logout');
					$logout =$facebook->getLogoutUrl($params);
					$this->User->recursive = 0;
					$userList = $this->User->find('first', array('fields' => array('User.email', 'User.id'), 'conditions' => array('User.email' => $me['email'])));
					
					if(!empty($userList)){
						$my_user_id = $userList['User']['id'];
					} else {
						$user = array();
						$org_password = $this->create_coupon();
						$user['User']['email'] = $me['email'];
						$user['User']['password'] = Security::hash(Configure::read('Security.salt') . $org_password);
						$user['User']['first_name'] = $me['first_name'];
						$user['User']['last_name'] = $me['last_name'];
						$user['User']['name'] = $me['first_name'].' '.$me['last_name'];
						$user['User']['user_type'] = 2;
						$user['User']['registration_date'] = date("Y-m-d");
						//$user['User']['dob'] = date("Y-m-d", strtotime($me['birthday']));
						$user['User']['status'] = 1;
						$user['User']['is_verified'] = 1;
						$this->User->create();
						$this->User->save($user);
						$my_user_id = $this->User->getLastInsertId();
						$uId = 'EM'.$this->User->id;
						$idMinLentgth = 5;
						$idLength = strlen($this->User->id);
						$lengtDiff = $idMinLentgth-$idLength;
						if($lengtDiff > 0){
							$midStr = '';
							for($iter = 0; $iter < $lengtDiff; $iter++){
								$midStr.='0';
							}
							$uId = 'EM'.$midStr.$this->User->id;
						}
						$this->User->savefield('emp-uid', $uId);
						/*Mail To User*/
						$siteSetting = $this->viewVars['settingMain'];
						$mail_To= $user['User']['email'];
						$mail_From = $siteSetting['Setting']['noreply_email'];
						$mail_CC = '';
						$mail_subject="Registration complete using your facebook account on HouseHelp4Hire";
						$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
								 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
								  <tr>
									<td width='77%' align='left' valign='top' bgcolor='#F1F1F2'>
									  <div style='width:97%; height:auto; border:1px solid #041A54; -moz-border:10px; border-radius:10px; background:#ED217C; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Dear ".$user['User']['first_name'].' '.$user['User']['last_name'].",</span><br/>
										  <span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
									  </div>
									  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
										<table width='100%' border='0' cellspacing='5' cellpadding='5'>
										  <tr>
											<td align='left' valign='top'>
											  <p><strong>Your registration had been placed successfully on HH4H. Login details are bellow...</strong></p>
											  <p>Login Email: ".$user['User']['email']."</p>
											  <p>Password: ".$org_password."</p>
											</td>
										  </tr>
										</table>
									  </div>
									  <div style='width:97%; height:auto; border:1px solid #041A54; -moz-border:10px; border-radius:10px; background:#0AB1F0; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
									  <span style='font-size:12px'>Regards Admin [".trim($siteSetting['Setting']['noreply_email'])."],</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
									  </div>
									</td>
								   </tr>
								 </table>
							   </div>"; 
						$mail_Body = $content; 
						$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
						/*Mail To User*/
					}
					
					$facebookCp['FacebookDetail']['user_id'] = $my_user_id;
					$facebookCp['FacebookDetail']['fb_id'] = $me['id'];
					$facebookCp['FacebookDetail']['fb_first_name'] = $me['first_name'] != '' ? $me['first_name'] : '';
					$facebookCp['FacebookDetail']['fb_last_name'] = $me['last_name'] != '' ? $me['last_name'] : '';
					$facebookCp['FacebookDetail']['fb_link'] = $me['link'] != '' ? $me['link'] : '';
					$facebookCp['FacebookDetail']['fb_username'] = $me['username'] != '' ? $me['username'] : '';
					$facebookCp['FacebookDetail']['fb_email'] = $me['email'] != '' ? $me['email'] : '';
					$facebookCp['FacebookDetail']['fb_birthday'] = $me['birthday'] != '' ? date("Y-m-d", strtotime($me['birthday'])) : '';
					$facebookCp['FacebookDetail']['fb_hometown'] = !empty($me['hometown']) ? serialize($me['hometown']) : serialize(array());
					$facebookCp['FacebookDetail']['fb_location'] = !empty($me['location']) ? serialize($me['location']) : serialize(array());
					$facebookCp['FacebookDetail']['fb_bio'] = $me['bio'] != '' ? $me['bio'] : '' ;
					$facebookCp['FacebookDetail']['fb_work'] = !empty($me['work']) ? serialize($me['work']) : serialize(array());
					$facebookCp['FacebookDetail']['fb_education'] = !empty($me['education']) ? serialize($me['education']) : serialize(array());
					$facebookCp['FacebookDetail']['fb_gender'] = $me['gender'] != '' ? $me['gender'] : '';
					$facebookCp['FacebookDetail']['fb_locale'] = $me['locale'] != '' ? $me['locale'] : '';
					$facebookCp['FacebookDetail']['fb_languages'] = !empty($me['languages']) ? serialize($me['languages']) : serialize(array());
					$facebookCp['FacebookDetail']['fb_verified'] = $me['verified'] != '' ? $me['verified'] : '1';					
					$this->FacebookDetail->recursive = 0;
					$fbList = $this->FacebookDetail->find('first', array('fields' => array('FacebookDetail.id'), 'conditions' => array('FacebookDetail.fb_email' => $me['email'])));
					if(!empty($fbList)) {
						$facebookCp->FacebookDetail->id = $fbList['FacebookDetail']['id'];
						$facebookCp['FacebookDetail']['id'] = $fbList['FacebookDetail']['id'];						
					} else {
						$this->FacebookDetail->create();
					}
					/*echo "<hr>";
					pr($facebookCp);
					die("OLD");*/
					$this->FacebookDetail->save($facebookCp);
					$this->User->recursive = 0;
					$userSession = $this->User->find('first', array('conditions' => array('User.email' => $me['email'])));
					$this->Session->write('Auth.User.id', $my_user_id);
					$this->Session->write('Auth.User.user_type', $userSession['User']['user_type']);
					$this->Session->write('Auth.User.first_name', $userSession['User']['first_name']);
					$this->Session->write('Auth.User.last_name', $userSession['User']['last_name']);
					$this->Session->write('Auth.User.email', $userSession['User']['email']);
					$this->Session->write('Auth.User.status', $userSession['User']['status']);
					$this->Session->write('User',$me);
					//$this->Session->write('logout',$logout);
			} catch(FacebookApiException $e){
				error_log($e);
				$user = NULL;
			}		
		} else {
			echo '<script>windowName.close();</script>';
			//$this->Session->setFlash('Sorry.Please try again');   
			//$this->redirect(DOMAIN_NAME_PATH);
	   }
   }
   
   function facebook_logout(){
		$this->Session->delete('User');
		$this->Session->delete('logout');   
		$this->redirect(array('action'=>'index'));
   }

	function subscribe(){
		Configure::load('facebook');
		$appId=Configure::read('Facebook.appId');
		$app_secret=Configure::read('Facebook.secret');
		$facebook = new Facebook(array(
				'appId'		=>  $appId,
				'secret'	=> $app_secret,
				));
		$loginUrl = $facebook->getLoginUrl(array(
			'scope'			=> 'email,read_stream, publish_stream, user_birthday, user_location, user_work_history, user_hometown, user_photos',
			'redirect_uri'	=> DOMAIN_NAME_PATH.'FacebookDetails/subscribeUsingFacebook',
			'display'=>'popup'
			));
		$this->redirect($loginUrl);
   	}

	function subscribeUsingFacebook()	{
		$this->layout = false;
	    Configure::load('facebook');
	    $appId=Configure::read('Facebook.appId');
	    $app_secret=Configure::read('Facebook.secret');	   
	   	 $facebook = new Facebook(array(
		'appId'		=>  $appId,
		'secret'	=> $app_secret,
		));	
	    $user = $facebook->getUser();		
		if($user){
	     	try{
					$me = $facebook->api('/me');
					$params=array('next' => DOMAIN_NAME_PATH.'Users/logout');
					$logout = $facebook->getLogoutUrl($params);
					$this->Email->recursive = 0;
					$exist_data = $this->Email->find('list', array('conditions' => array('Email.email' => $me['email'])));
					
					if(empty($exist_data)){
						$this->request->data['Email']['email'] = $me['email'];
						$this->request->data['Email']['name'] = $me['first_name'].' '.$me['last_name'];
						$this->Email->create();
						if ($this->Email->save($this->request->data)) {
							$this->loadModel('Setting');
							$this->Setting->recursive = 0;
							$settingMain = $this->Setting->read(null, 1);
							/*for mail*/
							$mail_To= $this->request->data['Email']['email'];
							$mail_From = $settingMain['Setting']['noreply_email'];
							$mail_CC = '';
							$mail_subject="Welcome to HouseHelp4Hire, Nigeria�s no.1 source for domestic staff!";
							$string = $this->request->data['Email']['email'];
							$convertedString = bin2hex(Security::cipher(serialize($string),Configure::read('Security.salt')));
							$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>click here</a>";
							$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
											 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$settingMain['Setting']['site_logo']."' alt='HH4H'></p>
											 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
											  <tr>
												<td width='77%' align='left' valign='top' bgcolor=''>
												  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
												  <span style='font-size:12px'>Dear ".$this->request->data["Email"]["email"]."</span><br/><span style='font-size:15px;font-weight:bold;'>Welcome to HouseHelp4Hire</span>
												  </div>
												  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
													<table width='100%' border='0' cellspacing='5' cellpadding='5'>
													  <tr>
														<td align='left' valign='top'>
														  <p>Thank you for signing up with us. You'll be the first to receive all the latest info, including new available staff, news and more</p>
														</td>
													  </tr>
													</table>
												  </div>
												  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
												  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
												  </div>
													<p style='font-sixe:10px;'>To unsubscribe please ".$link.".</p>
												</td>
											   </tr>
											 </table>
											 <p style='font-size:11px;text-align:center;'>
												<a href='".$settingMain['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
												<a href='".$settingMain['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
											 </p>
											 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
										   </div>"; 
							$mail_Body =$content; 
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
							/*for mail*/
							$this->Session->setFlash(sprintf(__('Newsletter Service Subscribed Successfully!', true), 'User'), 'default', array('class' => 'success'));
						}
					} else {
						$this->Session->setFlash('Sorry! You Already Subscribed...');
					}
			} catch(FacebookApiException $e){
				error_log($e);
				$user = NULL;
			}		
		} else {
		}
		echo '<script type="text/javascript">
				window.opener.location.reload();
				self.close();
			</script>';
		exit;
   }
}
