<?php
class PaymentsController extends AppController {

	var $name = 'Payments';

	function admin_index() {
		$this->Payment->recursive = 0;
		$this->paginate = array(
        'order' => array(
            'Payment.id' => 'desc'
        ));
		$this->set('payments', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Payment.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('payment', $this->Payment->read(null, $id));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Payment.', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Payment->delete($id)) {
			$this->Session->setFlash(sprintf(__('Payment deleted successfully.', true), 'Payment'), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Payment was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

	function admin_transactions() {
		$this->Payment->recursive = 0;
		$this->paginate = array(
        'order' => array(
            'Payment.id' => 'desc'
        ));
		$this->set('payments', $this->paginate());
	}
	function admin_requery($id)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Payment.', true));
			$this->redirect(array('action' => 'index'));
		}
		 $payments=$this->Payment->read(null, $id);
		
		 $min = 100000;
         $max = 99999999999;
         $hashkey = 'A419A2ABB86F1CEFA30637D2E70551ED4E01EC364EF363D9375EDE57611C10E0C685316B15E327772E14C0DC89049F27CF750A48671073D2E6C6D891A80859C1';
                    $gttran_id = $payments['Payment']['gtpay_tranx_id'];
                    $amount=$payments['Payment']['paid_amount'];
                    $gtpay_kobo_amount = $amount * 100;
					$merchant_id="2937";
                    $gttran_id_s = (string)$gttran_id;
                    $gtpay_kobo_amount_s = (string)$gtpay_kobo_amount;
                    $tranx_hashstring = $merchant_id.$gttran_id_s.  $hashkey;
					$tranx_hash = hash('sha512',$tranx_hashstring, false);
					
					//call json Service
					$url="https://ibank.gtbank.com/GTPayService/gettransactionstatus.json?mertid=".$merchant_id."&amount=".$gtpay_kobo_amount_s ."&tranxid=".$gttran_id_s."&hash=".$tranx_hash;
					$json = file_get_contents($url);
					$entity= json_decode($json, TRUE);
					$response_code=$entity["ResponseCode"];
					$response_desc=$entity["ResponseDescription"];
					//if($response_code=="00")
						$this->Payment->saveField('gtpay_tranx_status_code', $response_code);
  					//else
  					//{
						$this->Session->setFlash(__($response_desc));
						$this->redirect($this->referer());
					//}
  						
	}
	
	function admin_update($id=null,$qty=null)
	{
		
		$this->Payment->recursive = 0;
		$conditions = array("Payment.id" => $id);
		$this->Payment->id = $id;
		$this->Payment->saveField('quantity_viewed', $qty);
		echo "1";
		exit();
		//$this->set('payment', $this->Payment->read(null, $id));
	}
	/*function add() {
		if (!empty($this->data)) {
			$this->ResumeExpertiseArea->create();
			if ($this->ResumeExpertiseArea->save($this->data)) {
				$this->Session->setFlash(__('The resume expertise area has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The resume expertise area could not be saved. Please, try again.', true));
			}
		}
		$members = $this->ResumeExpertiseArea->Member->find('list');
		$jobCategories = $this->ResumeExpertiseArea->JobCategory->find('list');
		$jobSubcategories = $this->ResumeExpertiseArea->JobSubcategory->find('list');
		$industryExperiences = $this->ResumeExpertiseArea->IndustryExperience->find('list');
		$this->set(compact('members', 'jobCategories', 'jobSubcategories', 'industryExperiences'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid resume expertise area', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ResumeExpertiseArea->save($this->data)) {
				$this->Session->setFlash(__('The resume expertise area has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The resume expertise area could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ResumeExpertiseArea->read(null, $id);
		}
		$members = $this->ResumeExpertiseArea->Member->find('list');
		$jobCategories = $this->ResumeExpertiseArea->JobCategory->find('list');
		$jobSubcategories = $this->ResumeExpertiseArea->JobSubcategory->find('list');
		$industryExperiences = $this->ResumeExpertiseArea->IndustryExperience->find('list');
		$this->set(compact('members', 'jobCategories', 'jobSubcategories', 'industryExperiences'));
	}*/
}
?>