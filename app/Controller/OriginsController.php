<?php
App::uses('AppController', 'Controller');
/**
 * Origins Controller
 *
 * @property Origin $Origin
 */
class OriginsController extends AppController {
	public $uses = array('Origin', 'Page');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Origin->recursive = 0;
		$this->set('Origins', $this->paginate());
		$this->loadmodel('Page');
		$this->Page->recursive = 0;
		$Originpage=$this->Page->find('first',array('conditions'=>array('Page.id' => 8)));
		$this->set('Originpage',$Originpage);
	}
	public function admin_index() {
		$this->Origin->recursive = 0;
		$this->set('Origins', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Origin->id = $id;
		if (!$this->Origin->exists()) {
			throw new NotFoundException(__('Invalid Origin'));
		}
		$this->set('Origindetails', $this->Origin->read(null, $id));
		$this->Origin->recursive = 0;
		$this->set('Origins', $this->paginate());
		$this->set(compact('id'));
	}
	public function admin_view($id = null) {
		$this->Origin->id = $id;
		if (!$this->Origin->exists()) {
			throw new NotFoundException(__('Invalid Origin'));
		}
		$this->set('Origin', $this->Origin->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Origin->create();
			if ($this->Origin->save($this->request->data)) {
				$this->Session->setFlash(__('The Origin has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Origin could not be saved. Please, try again.'));
			}
		}
	}
	public function admin_add() {
		if ($this->request->is('post')) {
			//pr($this->request->data);die;
			$this->Origin->create();
			if ($this->Origin->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The Origin has been saved successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Origin could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Origin->id = $id;
		if (!$this->Origin->exists()) {
			throw new NotFoundException(__('Invalid Origin'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Origin->save($this->request->data)) {
				$this->Session->setFlash(__('The Origin has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Origin could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Origin->read(null, $id);
		}
	}
	public function admin_edit($id = null) {
		$this->Origin->id = $id;
		if (!$this->Origin->exists()) {
			throw new NotFoundException(__('Invalid Origin'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Origin->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The Origin has been updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Origin could not be updated. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Origin->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Origin->id = $id;
		if (!$this->Origin->exists()) {
			throw new NotFoundException(__('Invalid Origin'));
		}
		if ($this->Origin->delete()) {
			$this->Session->setFlash(__('Origin deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Origin was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Origin->id = $id;
		if (!$this->Origin->exists()) {
			throw new NotFoundException(__('Invalid Origin'));
		}
		if ($this->Origin->delete()) {
				$this->Session->setFlash(sprintf(__('The Origin has been deleted successfully!', true), 'Page'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Origin was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function admin_import_export($pass = null) {
		$model = $this->modelClass;
		$this->backup($model);
		$this->set('modelName', $model);
	}
}
