<?php
App::uses('AppController', 'Controller');
/**
 * Staffs Controller
 *
 * @property Staff $Staff
 */
class StaffsController extends AppController {
	public $uses = array('Staff','Location','Category','Language', 'Location', 'Education', 'Booking', 'StaffPreferedLocation', 'StaffSpokenLanguage', 'StaffPreferedEducation', 'Locality', 'StaffPreferedLocality', 'Email', 'Mailer', 'PaymentOption', 'Payment');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index', 'loadmore', 'firemail');
	}
	public $components = array(
	  'Session', 
	  'Cardsave'
	 );
/**
 * index method
 *
 * @return void
 */

	public function firemail() {
		$subscribers = $this->Email->find('list', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'DESC')));
		if(count($subscribers)>0){
			$before = date('Y-m-d', strtotime('-7 days'));
			$this->Staff->recursive = 0;
			$staffs = $this->Staff->find('all', array('fields'=>'Category.name,Staff.name,Staff.friendly_url,Staff.image1', 'conditions' => array('Staff.reg_date >=' => $before)));

			if(count($staffs)>0){
				$contents = '';
				foreach($staffs as $staff){
					$contents.= '<div  style="margin: 3px; margin: 6px; float: left; padding: 8px; background: #FA7D01" onMouseOver="this.style.background=#fdc53c" onMouseOut="this.style.background=#FA7D01">
						<div style="margin-bottom:5px">
							<img src="'.PAGE_IMAGES_URL.$staff['Staff']['image1'].'" width="180" height="200" alt="" border="0" />
						</div>
						<div style="color: #fff">
							<p style="font-size: 16px; padding:3px 0; margin: 0;"><strong>'.$staff['Staff']['name'].'</strong></p>
							<p style="padding: 3px 0; margin: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><strong>Category</strong>: '.$staff['Category']['name'].'</p>
							<p style="padding: 3px 0; margin: 0;"><a href="'.DOMAIN_NAME_PATH.$staff['Staff']['friendly_url'].'" target="_blank" style="background-color: #faa732; background-image: linear-gradient(to bottom, #fbb450, #f89406); background-repeat: repeat-x; border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; border-image: none; border-radius: 4px; border-style: solid; border-width: 1px; box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05); display: inline-block; font-size: 14px; line-height: 20px; margin-bottom: 0; padding: 4px 12px; text-align: center; text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75); vertical-align: 4px; text-decoration:none; color: #ffffff;">View Profile</a></p>
						</div>
					</div>';
				}
				$siteSetting = $this->viewVars['siteSetting'];
				$this->Mailer->recursive = 0;
				$mailer = $this->Mailer->find('first', array('conditions'=>array('Mailer.id'=>2)));
				$html = $mailer['Mailer']['html_content'];
				$logoPath = DOMAIN_NAME_PATH.'img/site_logo/'.$siteSetting['Setting']['site_logo'];
				$mail_From = $siteSetting['Setting']['noreply_email'];
				$mail_CC = null;
				$subject = $mailer['Mailer']['name'];
				$html = str_replace("##logo_path##", $logoPath, $html);
				$html = str_replace("##noreply_email##", $siteSetting['Setting']['noreply_email'], $html);
				$html = str_replace("##subject##", $subject, $html);
				$html = str_replace("##content##", $contents, $html);
				foreach($subscribers as $subscriber){
					$mail_To = $subscriber;
					$convertedString = bin2hex(Security::cipher(serialize($mail_To),Configure::read('Security.salt')));
					$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>Click here</a>";
					$html = str_replace("##unsubscribe##", $link, $html);
					$mail_body = str_replace("##user##", $mail_To, $html);
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
					$mail_body = str_replace($link, "##unsubscribe##", $html);
				}
			}
		}
		exit;
	}
	public function index($url=null) {
		$page = 1;
		$this->Staff->recursive = 1;
		//$this->set('staffs', $this->paginate());
		$conditions = array('Staff.status'=>1);
		$category = array();
		if($url != null){
			$category = $this->Category->find('first', array('fields'=>'Category.id,Category.name,Category.friendly_url', 'conditions' => array('Category.friendly_url' => $url)));
			$conditions = array('Staff.status'=>1, 'Category.friendly_url'=>$url);
		}
		if ($this->request->is('post')) {
			if(isset($this->request->data['Staff']['search'])){
				$conditions = array('Staff.status'=>1, 'or'=>array('Category.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.mini_biography LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.work_experience LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.profile_description LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Origin.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Religion.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%'));
				//$this->set('headerKeyWord', $this->request->data['Staff']['search']);
				//unset($this->request->data['Staff']['search']);
				$headerKeyWord = $this->request->data['Staff']['search'];
				unset($this->request->data['Staff']);
				$this->request->data['Staff']['search'] = $headerKeyWord;
			} else {
				$staffId = array();
				if($this->request->data['Staff']['spoken_language'] != ''){
					$langStaffRow = array_unique($this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.staff_id', 'conditions'=>array('StaffSpokenLanguage.language_id'=>$this->request->data['Staff']['spoken_language']))));
					$staffId = array_merge($staffId, $langStaffRow);
				}
				if($this->request->data['Staff']['prefered_locality'] != ''){
					$locStaffRow = array_unique($this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.staff_id', 'conditions'=>array('StaffPreferedLocality.locality_id'=>$this->request->data['Staff']['prefered_locality']))));
					$staffId = array_merge($staffId, $locStaffRow);
				}
				if($this->request->data['Staff']['education'] != ''){
					$eduStaffRow = array_unique($this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.staff_id', 'conditions'=>array('StaffPreferedEducation.education_id'=>$this->request->data['Staff']['education']))));
					$staffId = array_merge($staffId, $eduStaffRow);
				}
				$staffId = array_unique($staffId);
				if($this->request->data['Staff']['experience'] == 1){
					$minExp = 0;
					$maxExp = 1;
				} else if($this->request->data['Staff']['experience'] == 2){
					$minExp = 1;
					$maxExp = 3;
				} else if($this->request->data['Staff']['experience'] == 3){
					$minExp = 3;
					$maxExp = 5;
				} else if($this->request->data['Staff']['experience'] == 4){
					$minExp = 5;
					$maxExp = 10;
				} else if($this->request->data['Staff']['experience'] == 5){
					$minExp = 10;
					$maxExp = 100;
				} else {
					$minExp = 0;
					$maxExp = 0;
				}
				if($this->request->data['Staff']['age'] == 1){
					$minAge = 18;
					$maxAge = 25;
				} else if($this->request->data['Staff']['age'] == 2){
					$minAge = 25;
					$maxAge = 30;
				} else if($this->request->data['Staff']['age'] == 3){
					$minAge = 30;
					$maxAge = 35;
				} else if($this->request->data['Staff']['age'] == 4){
					$minAge = 35;
					$maxAge = 40;
				} else if($this->request->data['Staff']['age'] == 5){
					$minAge = 40;
					$maxAge = 60;
				} else {
					$minAge = 0;
					$maxAge = 0;
				}
				$conditions = array('Staff.category_id'=>$this->request->data['Staff']['category_id'], 'Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.english_speaking'=>$this->request->data['Staff']['english_speaking'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.id'=>$staffId, 'Staff.status'=>1);
				//pr($conditions);die;
				if($this->request->data['Staff']['category_id'] == ''){
					//unset($conditions['OR']['Job.job_title LIKE']);
					unset($conditions['Staff.category_id']);
				}
				if($this->request->data['Staff']['experience'] == ''){
					unset($conditions['Staff.experience >=']);
					unset($conditions['Staff.experience <=']);
				}
				if($this->request->data['Staff']['age'] == ''){
					unset($conditions['Staff.age >=']);
					unset($conditions['Staff.age <=']);
				}
				if($this->request->data['Staff']['sex'] == ''){
					unset($conditions['Staff.sex']);
				}
				if($this->request->data['Staff']['accommodation'] == ''){
					unset($conditions['Staff.accommodation']);
				}
				if($this->request->data['Staff']['origin_id'] == ''){
					unset($conditions['Staff.origin_id']);
				}
				if($this->request->data['Staff']['english_speaking'] == ''){
					unset($conditions['Staff.english_speaking']);
				}
				if($this->request->data['Staff']['marital_status'] == ''){
					unset($conditions['Staff.marital_status']);
				}
				if($this->request->data['Staff']['religion_id'] == ''){
					unset($conditions['Staff.religion_id']);
				}
				/*if($this->request->data['Staff']['base_salary'] == ''){
					unset($conditions['Staff.base_salary >=']);
				}
				if($this->request->data['Staff']['max_salary'] == ''){
					unset($conditions['Staff.max_salary <=']);
				}*/
				if((isset($langStaffRow) && count($langStaffRow) == 0) || (isset($locStaffRow) && count($locStaffRow) == 0) || (isset($eduStaffRow) && count($eduStaffRow) == 0)){
					$conditions['Staff.id'] = 0;
				} else if((!isset($langStaffRow)) && (!isset($locStaffRow)) && (!isset($eduStaffRow))){
					if(count($staffId) == 0){
						unset($conditions['Staff.id']);
					}
				}
			}
		}
		$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>3, 'page'=>$page));
		//pr($staffs);die;
		$this->set('staffs', $staffs);
		$this->set('category', $category);
		$countries = $this->Staff->Country->find('list',array('order'=>'Country.name'));
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list');
		$localities = $this->Locality->find('list');		
		$educations = $this->Education->find('list');
		$religions = $this->Staff->Religion->find('list');
		$experiences = array_unique($this->Staff->find('list', array('fields'=>'Staff.experience', 'order'=>array('Staff.experience'=>'ASC'))));
		$experience = array();
		if(count($experiences)>0){
			foreach($experiences as $experienceList){
				$yearPrint = ' Year';
				if($experienceList > 1){
					$yearPrint = ' Years';
				}
				$experience[$experienceList] = $experienceList.$yearPrint;
			}
		}
		$ages = array_unique($this->Staff->find('list', array('fields'=>'Staff.age', 'order'=>array('Staff.age'=>'ASC'))));
		$age = array();
		if(count($experiences)>0){
			foreach($ages as $ageList){
				$yearPrintAge = ' Year';
				if($ageList > 1){
					$yearPrintAge = ' Years';
				}
				$age[$ageList] = $ageList.$yearPrintAge;
			}
		}
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations', 'localities', 'religions', 'experience', 'age', 'educations'));
	}
	public function loadmore($url = null) {
		$this->layout = false;
		$page = $_POST['page'];
		$this->Staff->recursive = 1;
		$conditions = array('Staff.status'=>1);
		if($url != null){
			$conditions = array('Staff.status'=>1, 'Category.friendly_url'=>$url);
		}
		if($_POST['search'] != 'false' && $_POST['searchType'] != 'false'){
			$arrayStringArray = unserialize($_POST['search']);
			$this->request->data = $arrayStringArray;
			if(isset($this->request->data['Staff']['search'])){
				$conditions = array('Staff.status'=>1, 'or'=>array('Category.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.mini_biography LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.work_experience LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.profile_description LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Origin.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Religion.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%'));
			} else {
				$staffId = array();
				if($this->request->data['Staff']['spoken_language'] != ''){
					$langStaffRow = array_unique($this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.staff_id', 'conditions'=>array('StaffSpokenLanguage.language_id'=>$this->request->data['Staff']['spoken_language']))));
					$staffId = array_merge($staffId, $langStaffRow);
				}
				if($this->request->data['Staff']['prefered_locality'] != ''){
					$locStaffRow = array_unique($this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.staff_id', 'conditions'=>array('StaffPreferedLocality.locality_id'=>$this->request->data['Staff']['prefered_locality']))));
					$staffId = array_merge($staffId, $locStaffRow);
				}
				if($this->request->data['Staff']['education'] != ''){
					$eduStaffRow = array_unique($this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.staff_id', 'conditions'=>array('StaffPreferedEducation.education_id'=>$this->request->data['Staff']['education']))));
					$staffId = array_merge($staffId, $eduStaffRow);
				}
				$staffId = array_unique($staffId);
				if($this->request->data['Staff']['experience'] == 1){
					$minExp = 0;
					$maxExp = 1;
				} else if($this->request->data['Staff']['experience'] == 2){
					$minExp = 1;
					$maxExp = 3;
				} else if($this->request->data['Staff']['experience'] == 3){
					$minExp = 3;
					$maxExp = 5;
				} else if($this->request->data['Staff']['experience'] == 4){
					$minExp = 5;
					$maxExp = 10;
				} else if($this->request->data['Staff']['experience'] == 5){
					$minExp = 10;
					$maxExp = 100;
				} else {
					$minExp = 0;
					$maxExp = 0;
				}
				if($this->request->data['Staff']['age'] == 1){
					$minAge = 18;
					$maxAge = 25;
				} else if($this->request->data['Staff']['age'] == 2){
					$minAge = 25;
					$maxAge = 30;
				} else if($this->request->data['Staff']['age'] == 3){
					$minAge = 30;
					$maxAge = 35;
				} else if($this->request->data['Staff']['age'] == 4){
					$minAge = 35;
					$maxAge = 40;
				} else if($this->request->data['Staff']['age'] == 5){
					$minAge = 40;
					$maxAge = 100;
				} else {
					$minAge = 0;
					$maxAge = 0;
				}
				$conditions = array('Staff.category_id'=>$this->request->data['Staff']['category_id'], 'Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.english_speaking'=>$this->request->data['Staff']['english_speaking'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.id'=>$staffId, 'Staff.status'=>1);
				//pr($conditions);die;
				if($this->request->data['Staff']['category_id'] == ''){
					//unset($conditions['OR']['Job.job_title LIKE']);
					unset($conditions['Staff.category_id']);
				}
				if($this->request->data['Staff']['experience'] == ''){
					unset($conditions['Staff.experience >=']);
					unset($conditions['Staff.experience <=']);
				}
				if($this->request->data['Staff']['age'] == ''){
					unset($conditions['Staff.age >=']);
					unset($conditions['Staff.age <=']);
				}
				if($this->request->data['Staff']['sex'] == ''){
					unset($conditions['Staff.sex']);
				}
				if($this->request->data['Staff']['accommodation'] == ''){
					unset($conditions['Staff.accommodation']);
				}
				if($this->request->data['Staff']['origin_id'] == ''){
					unset($conditions['Staff.origin_id']);
				}
				if($this->request->data['Staff']['english_speaking'] == ''){
					unset($conditions['Staff.english_speaking']);
				}
				if($this->request->data['Staff']['marital_status'] == ''){
					unset($conditions['Staff.marital_status']);
				}
				if($this->request->data['Staff']['religion_id'] == ''){
					unset($conditions['Staff.religion_id']);
				}
				/*if($this->request->data['Staff']['base_salary'] == ''){
					unset($conditions['Staff.base_salary >=']);
				}
				if($this->request->data['Staff']['max_salary'] == ''){
					unset($conditions['Staff.max_salary <=']);
				}*/
				if((isset($langStaffRow) && count($langStaffRow) == 0) || (isset($locStaffRow) && count($locStaffRow) == 0) || (isset($eduStaffRow) && count($eduStaffRow) == 0)){
					$conditions['Staff.id'] = 0;
				} else if((!isset($langStaffRow)) && (!isset($locStaffRow)) && (!isset($eduStaffRow))){
					if(count($staffId) == 0){
						unset($conditions['Staff.id']);
					}
				}
			}
		}
		$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>3, 'page'=>$page));
		if(count($staffs) > 0){$this->set('staffs', $staffs);} else {exit;}
	}
	public function admin_index() {
		$conditions = array();		
		if(isset($this->request->data['Staff']['key']) && $this->request->data['Staff']['key'] != null) {
			$conditions = array('or'=>array('Staff.email LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.staff-uid LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Category.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%'));
		}
		$this->Staff->recursive = 0;
        $this->paginate=array('conditions'=>$conditions);
		$this->set('staffs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($url = null) {
		$this->Staff->recursive = 0;
		$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $url)));
		if (!$chk) {
			$this->Session->setFlash(__('Invalid staff'));
			$this->redirect(array('action' => 'index'));
		}
		$id = $chk['Staff']['id'];
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(__('Invalid staff'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Staff->recursive = 1;
		$staff = $this->Staff->read(null, $id);
		$this->Category->recursive = 0;
		$categoriesRelated = $this->Category->find('list',array('fielda'=>'Category.friendly_url', 'conditions'=>array('Category.id !='=>$staff['Staff']['category_id'])));
		$this->Staff->recursive = 0;
		$relatedStaffs = $this->Staff->find('all',array('fields'=>'Staff.id, Staff.friendly_url, Staff.name, Staff.image1, Staff.image2, Staff.experience, Staff.age, Origin.id, Origin.name', 'conditions'=>array('Staff.id !='=>$staff['Staff']['id'], 'Category.id'=>$staff['Staff']['category_id']), 'order'=>'rand()', 'limit'=>5));
		$this->Language->recursive = 0;
		/*$spokenLanguages = array();
		if($staff['Staff']['spoken_languages'] != null){
			$pLang = explode(',',$staff['Staff']['spoken_languages']);
			$spokenLanguages = $this->Language->find('list',array('conditions'=>array('Language.id'=>$pLang)));
		}*/
		$preferedLocations = array();
		if(count($staff['StaffPreferedLocation'])>0){
			foreach($staff['StaffPreferedLocation'] as $locArr){
				$idLoc[] = $locArr['location_id'];
			}
			$preferedLocations = $this->Location->find('list', array('conditions'=>array('Location.id'=>$idLoc)));
		}
		$preferedLocalities = array();
		if(count($staff['StaffPreferedLocality'])>0){
			foreach($staff['StaffPreferedLocality'] as $locaArr){
				$idLoca[] = $locaArr['locality_id'];
			}
			$preferedLocalities = $this->Locality->find('list', array('conditions'=>array('Locality.id'=>$idLoca)));
		}
		$spokenLanguages = array();
		if(count($staff['StaffSpokenLanguage'])>0){
			foreach($staff['StaffSpokenLanguage'] as $langArr){
				$idLang[] = $langArr['language_id'];
			}
			$spokenLanguages = $this->Language->find('list', array('conditions'=>array('Language.id'=>$idLang)));
		}
		if(count($staff['StaffPreferedEducation'])>0){
			foreach($staff['StaffPreferedEducation'] as $eduArr){
				$idEdu[] = $eduArr['education_id'];
			}
			$educations = $this->Education->find('list', array('conditions'=>array('Education.id'=>$idEdu)));
			$PreferedEducation = implode(', ', $educations);
			$this->set('PreferedEducation', $PreferedEducation);
		}
		$this->Location->recursive = 0;
		$locations = $this->Location->find('list', array('fields'=>'Location.name,Location.name'));
		$this->Booking->recursive = -1;
		$booked = $this->Booking->find('first', array('conditions'=>array('Booking.status'=>array('Pending', 'Selected'), 'Booking.staff_id'=>$id, 'Booking.user_id'=>$this->Session->read('Auth.User.id'))));
		$selected = $this->Booking->find('first', array('conditions'=>array('Booking.status'=>array('Selected'), 'Booking.staff_id'=>$id, 'Booking.user_id !='=>$this->Session->read('Auth.User.id'))));
		$hired = $this->Booking->find('first', array('conditions'=>array('Booking.status'=>array('Hired'), 'Booking.staff_id'=>$id, 'Booking.user_id !='=>$this->Session->read('Auth.User.id'))));
		$this->set('staff', $staff);
		$this->set('categoriesRelated', $categoriesRelated);
		$this->set('relatedStaffs', $relatedStaffs);
		$this->set('spokenLanguages', $spokenLanguages);
		$this->set('preferedLocations', $preferedLocations);
		$this->set('preferedLocalities', $preferedLocalities);
		$this->set('locations', $locations);
		$this->set('booked', $booked);
		$this->set('selected', $selected);
		$this->set('hired', $hired);
	}
	public function contacts($staffId){
		$this->Payment->recursive = 0;
		$payment = $this->Payment->find('first',array('conditions'=>array('Payment.user_id'=>$this->Session->read('Auth.User.id')), 'order'=>array('Payment.id'=>'DESC')));
		$flag = false;
		$expiryDateChk = false;
		if(!empty($payment)){
			if($payment['Payment']['payment_status'] == 'Y' || $payment['Payment']['payment_status'] == 'E'){
				if($payment['Payment']['expiry_date'] == null){
					$expiryDateChk = true;
				} else if($payment['Payment']['expiry_date'] >= date('Y-m-d')){
					$expiryDateChk = true;
				}
				if($expiryDateChk == true){
					if($payment['Payment']['quantity'] == null){
						$flag = true;
					} else if($payment['Payment']['quantity'] > $payment['Payment']['quantity_viewed']){
						$flag = true;
					} else if($payment['Payment']['quantity'] == $payment['Payment']['quantity_viewed']){
						$profileChk = explode(',', $payment['Payment']['viewed_profile_contacts']);
						if (in_array($staffId, $profileChk)) {
							$flag = true;
						}
					}
				}
				if($payment['Payment']['payment_status'] == 'Y'){
					if($flag == false){
						$this->Payment->id = $payment['Payment']['id'];
						$this->Payment->saveField('payment_status', 'E');
					}
				}
			}
		}
		$html = '<div class="contactDispWrap">';
		if($flag == true){
			$this->Payment->id = $payment['Payment']['id'];
			if($payment['Payment']['viewed_profile_contacts'] != null){
				$profileArr = explode(',', $payment['Payment']['viewed_profile_contacts']);
				if (!in_array($staffId, $profileArr)) {
					array_push($profileArr, $staffId);
					$profString = implode(',', $profileArr);
					$this->Payment->saveField('viewed_profile_contacts', $profString);
					$this->Payment->saveField('quantity_viewed', $payment['Payment']['quantity_viewed']+1);
				}
			} else {
				$this->Payment->saveField('viewed_profile_contacts', $staffId);
				$this->Payment->saveField('quantity_viewed', $payment['Payment']['quantity_viewed']+1);
			}
			$this->Staff->recursive = 0;
			$staff = $this->Staff->find('first',array('fields'=>'Staff.email, Staff.mobile_number, Staff.contact_number', 'conditions'=>array('Staff.id'=>$staffId)));
			$html.= '<p>Email Address: '.($staff['Staff']['email']!='' ? $staff['Staff']['email'] : 'NA').'</p>';
			$html.= '<p>Mobile Number: '.($staff['Staff']['mobile_number']!='' ? $staff['Staff']['mobile_number'] : 'NA').'</p>';
			$html.= '<p>Other Number: '.($staff['Staff']['contact_number']!='' ? $staff['Staff']['contact_number'] : 'NA').'</p>';
		} else {
			$this->PaymentOption->recursive = 0;
			$options = $this->PaymentOption->find('all', array('conditions'=>array('PaymentOption.type'=>1)));
			$html.= '<p>Please subscribe an option to view contact details.</p>';
			$i = 0;
			if(count($options) > 0){
				$html.= '<table class="table table-striped"><thead>
					<tr>       
						<th>Option </th>
						<th>Qquantity</th>
						<th>Validity</th>
						<th>Unit Price</th>
						<th>Price</th>
						<th>&nbsp;</th>
					</tr>
				</thead><tbody>';
				foreach($options as $option){
					++$i;
					$optionId = $option['PaymentOption']['id'];
					$optionName = $option['PaymentOption']['name'];
					$price = $option['PaymentOption']['amount'].' GBP';
					$quantity = $option['PaymentOption']['quantity']!=null ? $option['PaymentOption']['quantity'].' Profile' : 'Unlimited';
					$validity = $option['PaymentOption']['validity']!=null ? $option['PaymentOption']['validity'].' Days' : 'Unlimited';
					$unitprice = $option['PaymentOption']['quantity']!=null ? (number_format($option['PaymentOption']['amount']/$option['PaymentOption']['quantity'], 2)).' GBP / Contact' : 'NA';
					$param = "'".$option['PaymentOption']['id']."'";
					$html.= '<tr>       
					  <td>'.$optionName.'</td>
					  <td>'.$quantity.'</td>
					  <td>'.$validity.'</td>
					  <td>'.$unitprice.'</td>
					  <td>'.$price.'</td>
					  <td><a href="#inisiatePay" onclick="putOptionVal('.$param.');" role="button" data-toggle="modal" data-dismiss="modal" aria-hidden="true" class="btn btn-success btn-primary btn-lg">Pay</a></td>
					</tr>';
				}
			}
			$html.= '</tbody></table>';
			$html.= '<script>
				function putOptionVal(optionId){
					$("#StaffPaymentOptionId").val(optionId);
				}
			</script>';
		}
		$html.= '</div>';
		echo $html;
		exit;
	}
	public function payment(){
		$this->request->data['Payment']['payment_option_id'] = $this->request->data['Staff']['payment_option_id'];
		$this->request->data['Payment']['address'] = $this->Session->read('Auth.User.address');
		$this->request->data['Payment']['city'] = $this->Session->read('Auth.User.city');
		$this->request->data['Payment']['state'] = $this->Session->read('Auth.User.state');
		$this->request->data['Payment']['postal_code'] = $this->Session->read('Auth.User.postal_code');
		$this->request->data['Payment']['country_id'] = $this->Session->read('Auth.User.country_id');
		$this->request->data['Payment']['email'] = $this->Session->read('Auth.User.email');
		$this->request->data['Payment']['phone'] = $this->Session->read('Auth.User.phone');
		$this->request->data['Payment']['user_id'] = $this->Session->read('Auth.User.id');
		$this->PaymentOption->recursive = 0;
		$option = $this->PaymentOption->find('first', array('conditions'=>array('PaymentOption.id'=>$this->request->data['Payment']['payment_option_id'])));
		$this->request->data['Payment']['paid_amount'] = $option['PaymentOption']['amount'];
		$this->Payment->create();
		if ($this->Payment->save($this->request->data)) {
			$lastCreatedOrderId = $this->Payment->id;
			$this->Cardsave->process($this->request->data, $lastCreatedOrderId);
		} else {
			$this->Session->setFlash(__('Payment Failed!'));
			$this->redirect($this->referer());
		}
	}
	public function Secure(){
		$this->Cardsave->carsaveSecure();
	}
	public function SecureIFrame(){
		$this->Cardsave->carsaveIframe();
	}
	public function SecureProcess() {
		$arrayResponse = $this->Cardsave->carsaveSecureProcess();
		$order = array();
		$this->Payment->recursive = 0;
		if($arrayResponse['szCrossReference'] != null){
			$this->Payment->id = $_SESSION['CardSave_Direct_OrderID'];
			$this->Payment->saveField('payment_status','Y');
			$this->Payment->saveField('txn-id',$arrayResponse['szCrossReference']);
			$this->Payment->saveField('payment_date',date('Y-m-d H:i:s'));
			$this->set('szCrossReference', $arrayResponse['szCrossReference']);
			$order = $this->Payment->find('first', array('conditions'=>array('Payment.id'=>$_SESSION['CardSave_Direct_OrderID'])));
			if($order['PaymentOption']['validity'] != null || $order['PaymentOption']['validity'] != ''){
				$this->Payment->saveField('expiry_date', date('Y-m-d H:i:s',strtotime('+'.$order['PaymentOption']['validity'].' days')));
			}
			if($order['PaymentOption']['quantity'] != null || $order['PaymentOption']['quantity'] != ''){
				$this->Payment->saveField('quantity', $order['PaymentOption']['quantity']);
			}
			$this->Payment->saveField('validity', $order['PaymentOption']['validity']);
			$siteSetting = $this->viewVars['siteSetting'];
			/*mail start*/
			$orderLink = DOMAIN_NAME_PATH.'my-account';
			//$mailer = $this->Mailer->read(null, 9);
			$logoPath = SITE_LOGO_URL.$siteSetting['Setting']['site_logo'];
			$mail_From = $siteSetting['Setting']['noreply_email'];
			$mail_To = $this->Session->read('Auth.User.email');
			$mail_CC = null;
			//$subject = $mailer['Mailer']['name'];
			//$template = $mailer['Mailer']['html_content'];
			$subject = 'Payment Placed on HouseHelp4Hire';
			$template = '<div style="width: 700px; font-family: Arial, Helvetica, sans-serif;">
			<p align="center"><img src="##logo##" alt="TechLux" border="0" /></p>
			<table style="width: 100%;" border="0" cellspacing="10" cellpadding="10">
			<tbody>
			<tr>
			<td align="left" valign="top" bgcolor="#F1F1F2" width="77%">
			<div style="width: 97%; height: auto; border: 1px solid #01A279; -moz-border: 10px; border-radius: 10px; background: #01A279; margin: 0 0 10px 0; padding: 10px; color: #ffffff;"><span style="font-size: 12px;">Dear ##user##,</span><br /><span style="font-size: 15px; font-weight: bold;">##mail_subject##</span></div>
			<div style="border: 1px solid #b8b8b8; border-radius: 10px; -moz-border-radius: 10px; padding: 10px; margin: 0 0 10px 0; width: 635px; background: #ffffff;">
			<table style="width: 100%;" border="0" cellspacing="5" cellpadding="5">
			<tbody>
			<tr>
			<td align="left" valign="top">
			<p><strong>Your order had been placed successfully on TechLux. Order details are below...</strong></p>
			<div>##mail_content##</div>
			<p>Should you have any questions relating to this process? Please do not hesitate to contact our Agency Support ##support_email##.</p>
			<p>We look forward to a successful and profitable working relationship.</p>
			</td>
			</tr>
			</tbody>
			</table>
			</div>
			<div style="width: 97%; height: auto; border: 1px solid #01A279; -moz-border: 10px; border-radius: 10px; background: #01A279; margin: 0 0 10px 0; padding: 10px; color: #ffffff;"><span style="font-size: 12px;">Regards, Admin [##contact_email##],</span><br /><span style="font-size: 15px; font-weight: bold;">HouseHelp4Hire</span></div>
			</td>
			</tr>
			</tbody>
			</table>
			</div>';
			$nameBuyer = $this->Session->read('Auth.User.name');
			$mail_content = "<p>Order ID: ".$order['Payment']['id']."<br/>
							Transaction ID: ".$arrayResponse['szCrossReference']."<br/>
							Paid Amount: ".number_format($order['Payment']['paid_amount'], 2)."<br/>
							Payment Date: ".date("F j, Y, g:i a", strtotime($order['Payment']['payment_date']))."<br/>
							You can review your order: <a href='".$orderLink."'>".$orderLink."</a></p>";
			$template = str_replace("##logo##", $logoPath, $template);
			$template = str_replace("##user##", $nameBuyer, $template);
			$template = str_replace("##mail_subject##", $subject, $template);
			$template = str_replace("##support_email##", $siteSetting['Setting']['support_email'], $template);
			$template = str_replace("##contact_email##", $siteSetting['Setting']['contact_email'], $template);
			$template = str_replace("##mail_content##", $mail_content, $template);
			//$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $template);
			/*mail end*/
		}
		$this->set('order', $order);
		$this->set('Response', $arrayResponse['message']);
		$this->set('szCrossReference', $arrayResponse['szCrossReference']);
	}
	public function changeLoc($id){
		$location = $_POST['location'];
		$this->Booking->id=$id;
		$this->Booking->saveField('location', $location);
		echo $location;
		exit;
	}
	public function admin_view($id = null) {
		$this->Staff->recursive = 1;
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(__('Invalid staff.'));
		}
		$staff = $this->Staff->read(null, $id);
		//pr($staff['StaffPreferedLocation']);
		//pr($staff['StaffSpokenLanguage']);
		if(count($staff['StaffPreferedLocation'])>0){
			foreach($staff['StaffPreferedLocation'] as $locArr){
				$idLoc[] = $locArr['location_id'];
			}
			$locations = $this->Location->find('list', array('conditions'=>array('Location.id'=>$idLoc)));
			$PreferedLocation = implode(',', $locations);
			$this->set('PreferedLocation', $PreferedLocation);
		}
		if(count($staff['StaffPreferedLocality'])>0){
			foreach($staff['StaffPreferedLocality'] as $locArr){
				$idLoca[] = $locArr['locality_id'];
			}
			$localities = $this->Locality->find('list', array('conditions'=>array('Locality.id'=>$idLoca)));
			$PreferedLocality = implode(',', $localities);
			$this->set('PreferedLocality', $PreferedLocality);
		}
		if(count($staff['StaffSpokenLanguage'])>0){
			foreach($staff['StaffSpokenLanguage'] as $langArr){
				$idLang[] = $langArr['language_id'];
			}
			$languages = $this->Language->find('list', array('conditions'=>array('Language.id'=>$idLang)));
			$PreferedLanguage = implode(',', $languages);
			$this->set('PreferedLanguage', $PreferedLanguage);
		}
		if(count($staff['StaffPreferedEducation'])>0){
			foreach($staff['StaffPreferedEducation'] as $eduArr){
				$idEdu[] = $eduArr['education_id'];
			}
			$educations = $this->Education->find('list', array('conditions'=>array('Education.id'=>$idEdu)));
			$PreferedEducation = implode(',', $educations);
			$this->set('PreferedEducation', $PreferedEducation);
		}
		$this->set('staff', $staff);
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$exist_data = $this->Staff->find('list', array('conditions' => array('Staff.email' => $this->request->data['Staff']['email'])));
			if(empty($exist_data)){
				$CategoryCode = $this->Category->field('Category_code', array('Category.id'=>$this->request->data['Staff']['category_id']));
				$friendlyurl = $this->request->data['Staff']['name'];
				foreach($this->notAllowedWord AS $arrNAWord)
				{
					$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
				}
				$friendlyurl = str_replace(' ', '-', $friendlyurl);
				$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $friendlyurl)));
				$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				$duplicate = false;
				if(!empty($chk)){
					$this->request->data['Staff']['friendly_url'] = '';
					$duplicate = true;
				}
				if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != '')
				{
					if($this->request->data['Staff']['image1']['type'] == 'image/png' || $this->request->data['Staff']['image1']['type'] == 'image/PNG' || $this->request->data['Staff']['image1']['type'] == 'image/jpg' || $this->request->data['Staff']['image1']['type'] == 'image/JPG' || $this->request->data['Staff']['image1']['type'] == 'image/jpeg' || $this->request->data['Staff']['image1']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Staff']['image1']['name'];

						move_uploaded_file($this->request->data['Staff']['image1']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Staff']['image1'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					$this->Session->setFlash(__('The staff could not be saved.Please upload image1'));
				}
				if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != '')
				{
					if($this->request->data['Staff']['image2']['type'] == 'image/png' || $this->request->data['Staff']['image2']['type'] == 'image/PNG' || $this->request->data['Staff']['image2']['type'] == 'image/jpg' || $this->request->data['Staff']['image2']['type'] == 'image/JPG' || $this->request->data['Staff']['image2']['type'] == 'image/jpeg' || $this->request->data['Staff']['image2']['type'] == 'image/JPEG')
					{
						$filename2=rand(). $this->request->data['Staff']['image2']['name'];
						move_uploaded_file($this->request->data['Staff']['image2']['tmp_name'], PAGE_IMAGES.$filename2);
						$this->request->data['Staff']['image2'] = $filename2;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					$this->Session->setFlash(__('The staff could not be saved.Please upload image2'));
				}
				$preferedLocationArr = $this->request->data['Staff']['prefered_location'];
				unset($this->request->data['Staff']['prefered_location']);
				$spokenLanguageArr = $this->request->data['Staff']['spoken_languages'];
				unset($this->request->data['Staff']['spoken_languages']);
				$preferedLocalityArr = $this->request->data['Staff']['prefered_locality'];
				unset($this->request->data['Staff']['prefered_locality']);
				$preferedEducationArr = $this->request->data['Staff']['prefered_education'];
				unset($this->request->data['Staff']['prefered_education']);
				$this->request->data['Staff']['status']=1;
				$this->request->data['Staff']['reg_date']=date('Y-m-d');
				$this->Staff->create();
				if ($this->Staff->save($this->request->data)) {
					$uId = $CategoryCode.$this->Staff->id;
					$idMinLentgth = 5;
					$idLength = strlen($this->Staff->id);
					$lengtDiff = $idMinLentgth-$idLength;
					if($lengtDiff > 0){
						$midStr = '';
						for($iter = 0; $iter < $lengtDiff; $iter++){
							$midStr.='0';
						}
						$uId = $CategoryCode.$midStr.$this->Staff->id;
					}
					$this->Staff->savefield('staff-uid', $uId);
					if($duplicate){
						$friendlyurl .= '-'.$this->Staff->id;
						$this->Staff->savefield('friendly_url', $friendlyurl);
					}
					$id = $this->Staff->id;
					if($preferedLocationArr != ''){
						if(count($preferedLocationArr)>0){
							$checking = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
							if(count($checking)>0){
								//$this->StaffPreferedLocation->id = $checking;
								$this->StaffPreferedLocation->delete($checking);
							}
							$locationRow['StaffPreferedLocation']['staff_id'] = $id;
							foreach($preferedLocationArr as $preferedLocation){
								$locationRow['StaffPreferedLocation']['location_id'] = $preferedLocation;
								$this->StaffPreferedLocation->create();
								$this->StaffPreferedLocation->save($locationRow);
							}
						}
					}
					if($preferedLocalityArr != ''){
						if(count($preferedLocalityArr)>0){
							$checking1 = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
							if(count($checking1)>0){
								$this->StaffPreferedLocality->delete($checking1);
							}
							$localityRow['StaffPreferedLocality']['staff_id'] = $id;
							foreach($preferedLocalityArr as $preferedLocality){
								$localityRow['StaffPreferedLocality']['locality_id'] = $preferedLocality;
								$this->StaffPreferedLocality->create();
								$this->StaffPreferedLocality->save($localityRow);
							}
						}
					}
					if($spokenLanguageArr != ''){
						if(count($spokenLanguageArr)>0){
							$checking2 = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
							if(count($checking2)>0){
								$this->StaffSpokenLanguage->delete($checking2);
							}
							$languageRow['StaffSpokenLanguage']['staff_id'] = $id;
							foreach($spokenLanguageArr as $spokenLanguage){
								$languageRow['StaffSpokenLanguage']['language_id'] = $spokenLanguage;
								$this->StaffSpokenLanguage->create();
								$this->StaffSpokenLanguage->save($languageRow);
							}
						}
					}
					if($preferedEducationArr != ''){
						if(count($preferedEducationArr)>0){
							$checking3 = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
							if(count($checking3)>0){
								$this->StaffPreferedEducation->delete($checking3);
							}
							$educationRow['StaffPreferedEducation']['staff_id'] = $id;
							foreach($preferedEducationArr as $education){
								$educationRow['StaffPreferedEducation']['education_id'] = $education;
								$this->StaffPreferedEducation->create();
								$this->StaffPreferedEducation->save($educationRow);
							}
						}
					}
					$subscribers = $this->Email->find('list', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'DESC')));
					if(count($subscribers)>0){
						$siteSetting = $this->viewVars['siteSetting'];
						$this->Mailer->recursive = 0;
						$mailer = $this->Mailer->find('first', array('conditions'=>array('Mailer.id'=>1)));
						$html = $mailer['Mailer']['html_content'];
						$logoPath = DOMAIN_NAME_PATH.'img/site_logo/'.$siteSetting['Setting']['site_logo'];
						$mail_From = $siteSetting['Setting']['noreply_email'];
						$mail_CC = null;
						$subject = $mailer['Mailer']['name'];
						$html = str_replace("##logo_path##", $logoPath, $html);
						$html = str_replace("##noreply_email##", $siteSetting['Setting']['noreply_email'], $html);
						$html = str_replace("##subject##", $subject, $html);
						$html = str_replace("##staff_name##", $this->request->data['Staff']['name'], $html);
						$html = str_replace("##experience##", $this->request->data['Staff']['experience'], $html);
						$html = str_replace("##age##", $this->request->data['Staff']['age'], $html);
						$html = str_replace("##mini_biography##", $this->request->data['Staff']['mini_biography'], $html);
						$html = str_replace("##staff_url##", DOMAIN_NAME_PATH.$friendlyurl, $html);
						$html = str_replace("##staff_image##", PAGE_IMAGES_URL.$filename, $html);
						foreach($subscribers as $subscriber){
							$mail_To = $subscriber;
							$convertedString = bin2hex(Security::cipher(serialize($mail_To),Configure::read('Security.salt')));
							$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>Click here</a>";
							$mail_body = str_replace("##unsubscribe##", $link, $html);
							$mail_body = str_replace("##user##", $mail_To, $html);
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
						}
					}
					$this->Session->setFlash(sprintf(__('The staff has been added successfully!', true), 'Page'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staff could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('This email has already been registered.', true)));
			}
		}
		$countries = $this->Staff->Country->find('list',array('order'=>'Country.name'));
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list', array('conditions'=>array('Location.service_availability'=>'Yes')));
		$localities = $this->Locality->find('list');
		$religions = $this->Staff->Religion->find('list');
		$educations = $this->Education->find('list');
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations','religions','localities', 'educations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(sprintf(__('Invalid staff', true)));
		}
		$staffs=$this->Staff->read(null, $id);
		$oldpic1=$staffs['Staff']['image1'];
		$oldpic2=$staffs['Staff']['image2'];
		if ($this->request->is('post') || $this->request->is('put')) {
			$exist_data = $this->Staff->find('list', array('conditions' => array('Staff.email' => $this->request->data['Staff']['email'], 'Staff.id !=' => $id)));
			if(empty($exist_data)){
				$CategoryCode = $this->Category->field('Category_code', array('Category.id'=>$this->request->data['Staff']['category_id']));
				$friendlyurl = $this->request->data['Staff']['name'];
				foreach($this->notAllowedWord AS $arrNAWord)
				{
					$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
				}
				$friendlyurl = str_replace(' ', '-', $friendlyurl);
				$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $friendlyurl, 'Staff.id !='=>$id)));
				$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				if(!empty($chk)){
					$this->request->data['Staff']['friendly_url'] = $friendlyurl.'-'.$id;
				} else {
					$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				}
				if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != '')
				{
					if($this->request->data['Staff']['image1']['type'] == 'image/png' || $this->request->data['Staff']['image1']['type'] == 'image/PNG' || $this->request->data['Staff']['image1']['type'] == 'image/jpg' || $this->request->data['Staff']['image1']['type'] == 'image/JPG' || $this->request->data['Staff']['image1']['type'] == 'image/jpeg' || $this->request->data['Staff']['image1']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Staff']['image1']['name'];

						move_uploaded_file($this->request->data['Staff']['image1']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Staff']['image1'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image1']);
				}
				if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != '')
				{
					if($this->request->data['Staff']['image2']['type'] == 'image/png' || $this->request->data['Staff']['image2']['type'] == 'image/PNG' || $this->request->data['Staff']['image2']['type'] == 'image/jpg' || $this->request->data['Staff']['image2']['type'] == 'image/JPG' || $this->request->data['Staff']['image2']['type'] == 'image/jpeg' || $this->request->data['Staff']['image2']['type'] == 'image/JPEG')
					{
						$filename2=rand(). $this->request->data['Staff']['image2']['name'];
						move_uploaded_file($this->request->data['Staff']['image2']['tmp_name'], PAGE_IMAGES.$filename2);
						$this->request->data['Staff']['image2'] = $filename2;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image2']);
				}
				$preferedLocationArr = $this->request->data['Staff']['prefered_location'];
				unset($this->request->data['Staff']['prefered_location']);
				$spokenLanguageArr = $this->request->data['Staff']['spoken_languages'];
				unset($this->request->data['Staff']['spoken_languages']);
				$preferedLocalityArr = $this->request->data['Staff']['prefered_locality'];
				unset($this->request->data['Staff']['prefered_locality']);
				$preferedEducationArr = $this->request->data['Staff']['prefered_education'];
				unset($this->request->data['Staff']['prefered_education']);
				$uId = $CategoryCode.$this->request->data['Staff']['id'];
				$idMinLentgth = 5;
				$idLength = strlen($this->request->data['Staff']['id']);
				$lengtDiff = $idMinLentgth-$idLength;
				if($lengtDiff > 0){
					$midStr = '';
					for($iter = 0; $iter < $lengtDiff; $iter++){
						$midStr.='0';
					}
					$uId = $CategoryCode.$midStr.$this->request->data['Staff']['id'];
				}
				$this->request->data['Staff']['staff-uid'] = $uId;
				//pr($this->request->data);die;
				if ($this->Staff->save($this->request->data)) {
					if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic1);
					}
					if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic2);
					}
					if($preferedLocationArr != ''){
						if(count($preferedLocationArr)>0){
							$checking = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
							if(count($checking)>0){
								$this->StaffPreferedLocation->delete($checking);
							}
							$locationRow['StaffPreferedLocation']['staff_id'] = $id;
							foreach($preferedLocationArr as $preferedLocation){
								$locationRow['StaffPreferedLocation']['location_id'] = $preferedLocation;
								$this->StaffPreferedLocation->create();
								$this->StaffPreferedLocation->save($locationRow);
							}
						}
					}
					if($preferedLocalityArr != ''){
						if(count($preferedLocalityArr)>0){
							$checking1 = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
							if(count($checking1)>0){
								$this->StaffPreferedLocality->delete($checking1);
							}
							$localityRow['StaffPreferedLocality']['staff_id'] = $id;
							foreach($preferedLocalityArr as $preferedLocality){
								$localityRow['StaffPreferedLocality']['locality_id'] = $preferedLocality;
								$this->StaffPreferedLocality->create();
								$this->StaffPreferedLocality->save($localityRow);
							}
						}
					}
					if($spokenLanguageArr != ''){
						if(count($spokenLanguageArr)>0){
							$checking2 = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
							if(count($checking2)>0){
								$this->StaffSpokenLanguage->delete($checking2);
							}
							$languageRow['StaffSpokenLanguage']['staff_id'] = $id;
							foreach($spokenLanguageArr as $spokenLanguage){
								$languageRow['StaffSpokenLanguage']['language_id'] = $spokenLanguage;
								$this->StaffSpokenLanguage->create();
								$this->StaffSpokenLanguage->save($languageRow);
							}
						}
					}
					if($preferedEducationArr != ''){
						if(count($preferedEducationArr)>0){
							$checking3 = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
							if(count($checking3)>0){
								$this->StaffPreferedEducation->delete($checking3);
							}
							$educationRow['StaffPreferedEducation']['staff_id'] = $id;
							foreach($preferedEducationArr as $education){
								$educationRow['StaffPreferedEducation']['education_id'] = $education;
								$this->StaffPreferedEducation->create();
								$this->StaffPreferedEducation->save($educationRow);
							}
						}
					}
					$this->Session->setFlash(sprintf(__('The staff has been updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staff could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('This email has already been registered.', true)));
			}
		} else {
			$this->request->data = $this->Staff->read(null, $id);
		}
		$countries = $this->Staff->Country->find('list');
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list', array('conditions'=>array('Location.service_availability'=>'Yes')));
		$localities = $this->Locality->find('list');
		$religions = $this->Staff->Religion->find('list');
		$educations = $this->Education->find('list');
		$ploc=array();
		$locDisp = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.location_id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
		if(count($locDisp)>0)
		{
			$ploc = array_values($locDisp);
		}
		$ploca=array();
		$locaDisp = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.locality_id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
		if(count($locaDisp)>0)
		{
			$ploca = array_values($locaDisp);
		}
		$pLang=array();
		$langDisp = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.language_id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
		if(count($langDisp)>0)
		{
			$pLang = array_values($langDisp);
		}
		$pEdu=array();
		$eduDisp = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.education_id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
		if(count($eduDisp)>0)
		{
			$pEdu = array_values($eduDisp);
		}
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations','localities','ploc','ploca','pLang','pEdu','staffs','religions','educations'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			throw new NotFoundException(__('Invalid staff'));
		}
		if ($this->Staff->delete()) {
			$this->Session->setFlash(__('Staff deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Staff was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			throw new NotFoundException(__('Invalid staff'));
		}
		$staffs=$this->Staff->read(null, $id);
		$oldpic1=$staffs['Staff']['image1'];
		$oldpic2=$staffs['Staff']['image2'];
		if ($this->Staff->delete()) {
			unlink(PAGE_IMAGES.$oldpic1);
			unlink(PAGE_IMAGES.$oldpic2);
			$this->Session->setFlash(sprintf(__('The staff has been deleted successfully!', true), 'Page'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Staff was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
