<?php
class PagesController extends AppController {

	var $name = 'Pages';
	var $uses = array('Page', 'Job', 'Member','Email');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
			'view',
			'termsofservice',
			'privacypolicy',
			'aboutus',
			'howitworks',
			'contactus'
            );
         
	}

	function index() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
		$this->Job->recursive = 0;
		$jobLatest = $this->Job->find('all', array('conditions' => array('Job.status'=>'Y'), 'order'=>array('Job.date_of_post'=>'DESC'), 'limit'=>'10'));
		$jobFeatured = $this->Job->find('all', array('conditions' => array('Job.status'=>'Y', 'Job.featured'=>'Y', 'Member.featured_company'=>'Y'), 'order'=>array('Job.id'=>'DESC'), 'limit'=>'10'));
		$this->Member->recursive = 0;
		$companyFeatured = $this->Member->find('all', array('fields'=>'Member.company_name, Member.company_logo, Member.company_website, Member.id', 'conditions' => array('Member.featured_company'=>'Y', 'Member.company_logo !='=>null), 'order'=>array('Member.id'=>'DESC'), 'limit'=>'20'));
		//pr($companyFeatured);exit;
		//echo Security::hash(Configure::read('Security.salt') . '1234');
		$this->set(compact('jobLatest', 'jobFeatured', 'companyFeatured'));
	}

	function admin_index() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid page', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('page', $this->Page->read(null, $id));
	}
	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid page', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('page', $this->Page->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Page->create();
			if ($this->Page->save($this->data)) {
				$this->Session->setFlash(__('The page has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid page', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Page->save($this->data)) {
				$this->Session->setFlash(__('The page has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Page->read(null, $id);
		}
	}
	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid page', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$account = $this->Page->find('first',array('conditions'=>array('Page.id'=>$id)));
			if(isset($this->request->data['Page']['image']['name']) && $this->request->data['Page']['image']['name'] != ''){
				if($this->request->data['Page']['image']['type'] == 'image/png' || $this->request->data['Page']['image']['type'] == 'image/PNG' || $this->request->data['Page']['image']['type'] == 'image/jpg' || $this->request->data['Page']['image']['type'] == 'image/JPG' || $this->request->data['Page']['image']['type'] == 'image/jpeg' || $this->request->data['Page']['image']['type'] == 'image/JPEG'){
					if($account['Page']['image'] != ''){
						unlink(PAGE_IMAGES.$account['Page']['image']);
					}
					$filename=rand().basename($this->request->data['Page']['image']['name']);
					move_uploaded_file($this->request->data['Page']['image']['tmp_name'], PAGE_IMAGES.$filename);
					$this->request->data['Page']['image'] =$filename;
				}
				else
				{
					$this->request->data['Page']['image'] = $account['Page']['image'];
					$fileTypeError = true;
				}
			}
			else{
				$this->request->data['Page']['image'] = $account['Page']['image'];
			}
			if ($this->Page->save($this->data)) {
				if(isset($fileTypeError) && $fileTypeError == true){
					$this->Session->setFlash(__('Please upload png or jpg image.', true));
				}
				else{
					$this->Session->setFlash(sprintf(__('Page updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
				}
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be updated. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Page->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for page', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Page->delete($id)) {
			$this->Session->setFlash(__('Page deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Page was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

	function termsofservice() {
		
		
	}
	
	function privacypolicy() {
		
		
	}
	function aboutus() {
		
		
	}
	function howitworks() {
		
	}
	function contactus(){
		if(!empty($this->request->data)){
		$logo="logo.png";
		$mail_To= "hello@househelp4hire.com";
		$mail_From = "no-reply@Househelp4hire.com";
		$mail_CC = '';
		$mail_subject="HouseHelp4Hire-Contact Request";
		$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
		<p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$logo."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;'>
										  <span style='font-size:12px'>Hi From - ".$this->data['Contact']['name'].",</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p><strong>Message Details :</strong></p>
												  <p>
												  	 Name :&nbsp;&nbsp;".$this->data['Contact']['name']."<br/>
													 Email:&nbsp;&nbsp;".$this->data['Contact']['email']."<br/>
													 Phone:&nbsp;&nbsp;".$this->data['Contact']['phone']."<br/>
													 Message:&nbsp;&nbsp;".$this->data['Contact']['message']."<br/> 
													 </p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
							</div>"; 
							$mail_Body = $content;
							
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
							$this->data = null;
							$this->Session->setFlash(sprintf(__("Thanks! We'll be in touch soon!", true), 'Page'), 'default', array('class' => 'success', 'contact'));
			}
			else{
				  $this->data = null;
				  $this->set('page', $this->Page->read(null, 1));
				  
				}
				
				
	}
}
?>