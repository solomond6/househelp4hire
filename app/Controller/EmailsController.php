<?php
App::uses('AppController', 'Controller');
/**
 * Emails Controller
 *
 * @property Email $Email
 */
class EmailsController extends AppController {
	var $uses = array('Email', 'Mailer', 'User');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
			'landing',
            'add',
			'delete',
			'index'
            );
	}
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		if(!empty($this->request->data)){
			if($this->request->data['Email']['ids'] != ''){
				$idArr = explode(',', $this->request->data['Email']['ids']);
				foreach($idArr as $id){
					$this->request->data['Email']['ids'];
					$this->Email->id = $id;
					$this->Email->delete();
				}
				$this->Session->setFlash(sprintf(__('Selected rows are deleted Successfully.', true), 'User'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Please select minimum one checkbox!'));
				$this->redirect(array('action' => 'index'));
			}
		}
		$this->Email->recursive = 0;
		$orderby = array('Email.id'=>'desc');	
		$this->paginate=array('order'=>$orderby);
		$this->set('emails', $this->paginate());
	}

	public function index() {
		$this->redirect(array('action' => 'landing'));
	}

/**
 * add method
 *
 * @return void
 */
	public function landing() {
		$this->layout=false;
		if ($this->request->is('post')) {
			$exist_data = $this->Email->find('list', array('conditions' => array('Email.email' => $this->request->data['Email']['email'])));
			if(empty($exist_data)){
				$this->request->data['Email']['name'] = $this->request->data['Email']['first_name'].' '.$this->request->data['Email']['last_name'];
				$this->Email->create();
				if ($this->Email->save($this->request->data)) {
					$this->loadModel('Setting');
					$this->Setting->recursive = 0;
					$settingMain = $this->Setting->read(null, 1);
					/*for mail*/
					$mail_To= $this->request->data['Email']['email'];
					$mail_From = $settingMain['Setting']['noreply_email'];
					$mail_CC = '';
					$mail_subject="Welcome to HouseHelp4Hire, Nigeria�s no.1 source for domestic staff!";
					//$this->encryption_decryption($this->request->data['Email']['email']);
					$string = $this->request->data['Email']['email'];
					$convertedString = bin2hex(Security::cipher(serialize($string),Configure::read('Security.salt')));
					//echo $convertedString; die;
					$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>click here</a>";
					$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$settingMain['Setting']['site_logo']."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Dear ".$this->request->data["Email"]["email"]."</span><br/><span style='font-size:15px;font-weight:bold;'>Welcome to HouseHelp4Hire</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p>Thank you for signing up with us. You'll be the first to receive all the latest info, including new available staff, news and more</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
											<p style='font-sixe:10px;'>To unsubscribe please ".$link.".</p>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$settingMain['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$settingMain['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
					$mail_Body =$content; 
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
					/*for mail*/
					$this->Session->setFlash(sprintf(__('Newsletter Service Subscribed Successfully!', true), 'User'), 'default', array('class' => 'success'));
					//$this->redirect($this->referer());
				} else {
					$this->Session->setFlash(__('Newsletter service not be subscribed. Please, try again!'));
				}
			}else {
				$this->Session->setFlash(sprintf(__("You've already subscribed to our newsletter", true)));
			}	
		}
	}

	public function add() {
		if ($this->request->is('post')) {
			$exist_data = $this->Email->find('list', array('conditions' => array('Email.email' => $this->request->data['Email']['email'])));
			if(empty($exist_data)){
				$this->Email->create();
				if ($this->Email->save($this->request->data)) {
					$this->loadModel('Setting');
					$this->Setting->recursive = 0;
					$settingMain = $this->Setting->read(null, 1);
					/*for mail*/
					$mail_To= $this->request->data['Email']['email'];
					$mail_From = $settingMain['Setting']['noreply_email'];
					$mail_CC = '';
					$mail_subject="Welcome to HouseHelp4Hire, Nigeria�s no.1 source for domestic staff!";
					//$this->encryption_decryption($this->request->data['Email']['email']);
					$string = $this->request->data['Email']['email'];
					$convertedString = bin2hex(Security::cipher(serialize($string),Configure::read('Security.salt')));
					//echo $convertedString; die;
					$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>click here</a>";
					$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$settingMain['Setting']['site_logo']."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Dear ".$this->request->data["Email"]["email"]."</span><br/><span style='font-size:15px;font-weight:bold;'>Welcome to HouseHelp4Hire</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p>Thank you for signing up with us. You'll be the first to receive all the latest info, including new available staff, news and more</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
											<p style='font-sixe:10px;'>To unsubscribe please ".$link.".</p>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$settingMain['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$settingMain['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
					$mail_Body =$content; 
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
					/*for mail*/
					//$this->Session->setFlash(sprintf(__('Newsletter Service Subscribed Successfully!', true), 'User'), 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash(__('Newsletter service not be subscribed. Please, try again!'));
				}
			}else {
				$this->Session->setFlash(sprintf(__("You've already subscribed to our newsletter", true)));
			}	
		}
		$this->redirect($this->referer());
		$this->layout=false;
		$this->layout='launch';
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Email->id = $id;
		if (!$this->Email->exists()) {
			throw new NotFoundException(__('Invalid email'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Email->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('Subscriber updated Successfully.', true), 'User'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Email->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null, $flag = null) {
		$string = $id;
		$convertedString =  unserialize(Security::cipher(pack("H*",$string),Configure::read('Security.salt')));
		$exist_data = $this->Email->find('first', array('conditions' => array('Email.email' => $convertedString)));
		if(count($exist_data)>0){
			$this->Email->id = $exist_data['Email']['id'];
			if ($this->Email->delete()) {
				$this->Session->setFlash(sprintf(__('Newsletter Service Unubscribed Successfully!', true), 'User'), 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash(__('Failed to unsubscribe newsletter service!'));
			}
		} else {
			$this->Session->setFlash(__('Invalid Email Address'));
		}
		if($flag == null){
			$this->redirect(array('action' => 'index', 'controller'=>'Users'));
		} else {
			$this->redirect($this->referer());
		}
	}

	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			$this->Session->setFlash(__('Invalid Subscriber'));
		}
		$this->Email->id = $id;
		if (!$this->Email->exists()) {
			$this->Session->setFlash(__('Invalid Subscriber'));
		}
		if ($this->Email->delete()) {
			$this->Session->setFlash(sprintf(__('Subscriber deleted Successfully.', true), 'User'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Subscriber was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function unsubscribe($emailEncoded = null) {
		if($emailEncoded)
		{
			$this->User->recursive = -1;
			$exist_data = $this->User->find('first', array('conditions' => array('User.email' =>base64_decode($emailEncoded))));
			$this->User->id = $exist_data['User']['id'];
			$this->User->saveField('newsletter_subscribe', 0);
			$exist_email = $this->Email->find('list', array('conditions' => array('Email.email' =>base64_decode($emailEncoded))));
			if($exist_email)
			{
				foreach($exist_email as $k=>$v)
				{
					$this->Email->delete($k);
				}
			}
			$this->Session->setFlash(sprintf(__('You have been unsubscribed Successfully.', true), 'User'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index','controller'=>'users'));

		}
		else
		{
			$this->Session->setFlash(sprintf(__('Invlid unsubscription process. Please try again later', true)));
			$this->redirect(array('action' => 'index','controller'=>'users'));
		}
	}

	public function admin_import_export($pass = null) {
		$model = $this->modelClass;
		$this->backup($model);
		$this->set('modelName', $model);
	}
}
