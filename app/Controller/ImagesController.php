<?php 
class ImagesController extends AppController {
    var $name = 'Images';
    var $uses = array('Image');
	public $helpers = array('Html', 'Form', 'Js', 'Number');
	public $components = array(
		'Session', 
	);
	public function beforeFilter() {
		parent::beforeFilter();
	}
    function admin_index() {
		$this->layout = 'popup_window';
        $this->set(
            'images',
            $this->Image->readFolder(APP.WEBROOT_DIR.DS.'uploads')
        );
    }

    function admin_upload() {
        if (!empty($this->data)) {
			$p_name = str_replace(' ', '_', $this->data['Image']['image']['name']);
			$extention = strrchr($p_name,".");
			$fileName = rand().'_'.$p_name;
			$this->validImageFormats=array('.jpg','.jpeg','.png','.gif');
			if(in_array($extention, $this->validImageFormats))
			{
				if(move_uploaded_file($this->data['Image']['image']['tmp_name'], WWW_ROOT.'uploads'.DS.$fileName)) {
					$this->Session->setFlash(sprintf(__('The image was successfully uploaded.', true)), 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash('There was an error with the uploaded file.');
				}
			} else {
				$this->Session->setFlash('Please uploade having .jpg, .jpeg, .png, .gif files.');
			}
        }
		$this->redirect(array('controller' => 'images', 'action' => 'index'));
    }

	function index() {
		$this->layout = 'popup_window';
        $this->set(
            'images',
            $this->Image->readFolder(APP.WEBROOT_DIR.DS.'uploads')
        );
    }

    function upload() {
        if (!empty($this->data)) {
			$p_name = str_replace(' ', '_', $this->data['Image']['image']['name']);
			$extention = strrchr($p_name,".");
			$fileName = rand().'_'.$p_name;
			$this->validImageFormats=array('.jpg','.jpeg','.png','.gif');
			if(in_array($extention, $this->validImageFormats))
			{
				if(move_uploaded_file($this->data['Image']['image']['tmp_name'], WWW_ROOT.'uploads'.DS.$fileName)) {
					$this->Session->setFlash(sprintf(__('The image was successfully uploaded.', true)), 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash('There was an error with the uploaded file.');
				}
			} else {
				$this->Session->setFlash('Please uploade having .jpg, .jpeg, .png, .gif files.');
			}
        }
		$this->redirect(array('controller' => 'images', 'action' => 'index'));
    }
}
?>