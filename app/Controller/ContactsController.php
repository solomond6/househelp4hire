<?php
App::uses('AppController', 'Controller');
/**
 * Contacts Controller
 *
 * @property Contact $Contact
 */
class ContactsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Contact->recursive = 0;
		$this->set('contacts', $this->paginate());
	}

	public function admin_index() {
		if(!empty($this->request->data)){
			if($this->request->data['Contact']['ids'] != ''){
				$idArr = explode(',', $this->request->data['Contact']['ids']);
				foreach($idArr as $id){
					$this->request->data['Contact']['ids'];
					$this->Contact->id = $id;
					$this->Contact->delete();
				}
				$this->Session->setFlash(sprintf(__('Selected rows are deleted Successfully.', true), 'User'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Please select minimum one checkbox!'));
				$this->redirect(array('action' => 'index'));
			}
		}
		$this->Contact->recursive = 0;
		$this->paginate = array('order'=>array('Contact.id'=>'DESC'));
		$this->set('contacts', $this->paginate());
		$unreadmsg=$this->Contact->find('all',array('conditions'=>array('Contact.read'=>'0')));
		$totalunreadmsg=count($unreadmsg);
		$this->set(compact('totalunreadmsg'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		else
		{
			$this->Contact->id=$id;
			$this->Contact->saveField('read', 1);
		}
		$this->set('contact', $this->Contact->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->request->data["Contact"]["date"]=date('Y-m-d H:i:s');
			$this->Contact->create();
			if ($this->Contact->save($this->request->data)) {
				$this->loadModel('Setting');
				$this->Setting->recursive = 0;
				$settingMain = $this->Setting->read(null, 1);
				/*for mail*/
					$mail_To= $settingMain['Setting']['contact_email'];
					//$mail_To= 'pathbreakerz.koushik@gmail.com';
					$mail_From = $settingMain['Setting']['noreply_email'];
					$mail_CC = '';
					$mail_subject="Somebody Contact You";
					$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor='#F1F1F2'>
										  <div style='width:97%; height:auto; border:1px solid #041A54; -moz-border:10px; border-radius:10px; background:#041A54; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Dear Admin</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p><strong>Details are below...</strong></p>
												  <p>
												   Name : ".$this->request->data["Contact"]["name"]."<br/>
												   Email : ".$this->request->data["Contact"]["email"]."<br/>
												   Phone : ".$this->request->data["Contact"]["phone"]."<br/>
												   Message : ".$this->request->data["Contact"]["message"]."<br/>
												   About Contact Person : ".$this->request->data["Contact"]["about_user"]."<br/>
												  </p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #041A54; -moz-border:10px; border-radius:10px; background:#041A54; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'></span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$settingMain['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$settingMain['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
				$mail_Body =$content; 
				$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
				/*for mail*/
				$this->Session->setFlash(sprintf(__('Message has been sent successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index','controller'=>'users'));
			} else {
				$this->Session->setFlash(__('Message could not be sent. Please, try again.'));
			}
		}
		/*<a href='".$settingMain['Setting']['pinterest_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/p.png' alt='pinterest' height='48' /></a>
		<a href='mailto: ".$settingMain['Setting']['contact_email']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/e.png' alt='email' height='48' /></a>
		<a href='".$settingMain['Setting']['rss_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/r.png' alt='rss' height='48' /></a>*/
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		if ($this->Contact->delete()) {
			$this->Session->setFlash(sprintf(__('Contact message deleted successfully!', true), 'Page'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contact message was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
