<?php
class CouponsController extends AppController {

	var $name = 'Coupons';
	public $uses = array('Coupon');
	public function applyCoupon($couponCode)
	{
		$current = date('Y-m-d');
		$options = $this->Coupon->find('all', array('conditions'=>array('Coupon.code'=>$couponCode,'and'=>array('Coupon.valid_till >= '=> $current))));
		
		if(count($options) > 0){
			$discount_percent=0;
		foreach($options as $option){
				$discount = $option['Coupon']['discount'];
			}
			echo $discount;
		}
		else
		{
			echo "-1";	
		}
		
		exit();
	}

	
	function admin_manage($search = null) {
		if($this->Session->read('Auth.User.user_type')!=1){
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		$uid = $this->Session->read('Auth.User.id');
		//$WHERE = 'User.id != "'.$uid.'" AND User.user_type ="3"';	
		$this->Coupon->recursive=1;
		//$this->strCondition=$WHERE;		
        $this->paginate=array('order' => array('Coupon.id' => 'DESC'));
		$this->set('coupons', $this->paginate());
	}
	
	function admin_addcoupon() {
		if($this->Session->read('Auth.User.user_type')!=1)
		{
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		if (!empty($this->data)) {
			$dup_chk = $this->Coupon->find('count', array('conditions' => array('Coupon.code' => $this->data['Coupon']['code']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This code is not available.', true)));
			} else {
				$this->request->data['Coupon']['code'] = $this->request->data['Coupon']['code'];
				$this->request->data['Coupon']['discount'] = $this->request->data['Coupon']['discount'];
			
				$this->Coupon->create();
				if ($this->Coupon->save($this->data)) {
					
				$this->Session->setFlash(sprintf(__('Coupon has been added successfully.', true), 'Coupon'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'manage'));
				} else {
					$this->Session->setFlash(__('Coupon could not be saved. Please, try again.', true));
				}
			}
		}
	
	}

	function admin_deletecoupon($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for admin', true));
			$this->redirect(array('action'=>'indexsubadmin'));
		}
		
		if ($this->Coupon->delete($id)) {
			$this->Session->setFlash(__('Coupon deleted successfully', true), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'manage'));
		}
		$this->Session->setFlash(__('Coupon was not deleted', true));
		$this->redirect(array('action' => 'manage'));
	}
	
	function admin_editcoupon($id=null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid admin.', true));
			$this->redirect(array('action' => 'indexsubadmin'));
		}
		if($this->Session->read('Auth.User.user_type')!=1)
		{
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		if(!empty($this->data))
		{
			
			$dup_chk = $this->Coupon->find('count', array('conditions' => array('Coupon.code' => $this->data['Coupon']['code'],'Coupon.id !='=>$this->request->data['Coupon']['id']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This code is not available.', true)));
			} else {
			
				
				if(empty($this->request->data['Coupon']['code']))
				{
					unset($this->request->data['Coupon']['code']);
				}
				if(empty($this->request->data['Coupon']['discount']))
				{
					unset($this->request->data['Coupon']['discount']);
				}
				
				
				if ($this->Coupon->save($this->data)) {
				$this->Session->setFlash(sprintf(__('Coupon has been updated successfully.', true), 'Coupon'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'manage'));
				} else {
					$this->Session->setFlash(__('Coupon could not be updated. Please, try again.', true));
				}
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Coupon->read(null, $id);
			
		}
	
	}
}
?>