<?php
class HomeproductComponent extends Object {
	function startup( &$controller ) {
	  $this->Controller =& $controller;
	}
	function initialize(Controller $controller) { }
    function productListing() {
      error_reporting(0);
	  App::import('Model', array('User', 'Setting', 'Run', 'Collection', 'Group', 'Category', 'OrderDetail', 'Order', 'Product'));
	  $model=new Product();
	  $run=new Run();
		//http://book.cakephp.org/2.0/en/controllers/components.html
		$now = date("Y-m-d H:i:s");
		$run->recursive = 0;
		$currentRun = $run->find('first', array('conditions'=>array('Run.status'=>'Y', 'Run.last_date_of_voting >'=>$now), 'order'=>array('Run.last_date_of_voting'=>'ASC')));
		$currentRunId = $currentRun['Run']['id'];
		//$currentRunName = $currentRun['Run']['name'];
		//$currentRunDescription = $currentRun['Run']['description'];
		if(count($currentRun) > 0){
			$expTime = $currentRun['Run']['last_date_of_voting'];
			$remainTime = strtotime($expTime) - strtotime($now);
		}
		else{
			$remainTime = ('0000-00-00 00:00:00');
		}
		$model->unbindModel(array('hasMany' => array('OrderDetail', 'Order'), 'belongsTo' => array('Run', 'Category', 'User')));
		$model->recursive = 2;
		$productsVote = $model->find('all', array('fields'=>'Product.id,Product.name,Product.collection_id,Product.group_id', 'conditions'=>array('Product.run_id'=>$currentRunId, 'Product.status'=>'Y'), 'order'=>array('Product.collection_id'=>'ASC', 'Product.group_id'=>'ASC', 'Product.id'=>'ASC')));

		$currentRunSale = $run->find('first', array('conditions'=>array('Run.status'=>'Y', 'Run.last_date_of_voting <'=>$now, 'Run.last_date_of_selling >'=>$now), 'order'=>array('Run.last_date_of_selling'=>'ASC')));
		$currentRunIdSale = $currentRunSale['Run']['id'];
		//$currentRunNameSale = $currentRunSale['Run']['name'];
		//$currentRunDescriptionSale = $currentRunSale['Run']['description'];
		if(count($currentRunSale) > 0){
			$expTimeSale = $currentRunSale['Run']['last_date_of_selling'];
			$remainTimeSale = strtotime($expTimeSale) - strtotime($now);
		}
		else{
			$remainTimeSale = ('0000-00-00 00:00:00');
		}
		$productsSale = $model->find('all', array('fields'=>'Product.id,Product.name,Product.collection_id,Product.group_id', 'conditions'=>array('Product.run_id'=>$currentRunIdSale, 'Product.status'=>'Y'), 'order'=>array('Product.collection_id'=>'ASC', 'Product.group_id'=>'ASC', 'Product.id'=>'ASC')));
		return(array($remainTime, $productsVote, $currentRunName, $currentRunDescription, $remainTimeSale, $productsSale, $currentRunNameSale, $currentRunDescriptionSale));
    }
	function beforeRender(Controller $controller){}
	function shutdown(Controller $controller){}
}
?>