<?php 
class CardsaveComponent extends Object{
	public $WebAddress = null;
	public $MerchantID = null;
	public $Password = null;

	function startup( &$controller ) {
	  $this->Controller =& $controller;
	}

	function initialize(Controller $controller) { }

	function __construct(){
		//CardSave Direct PHP XML Payment
		//Copyright (C) 2013 Modacs Limited trading as CardSave.
		//Support: ecomm@cardsave.net

		//Created:		26/02/2013
		//Created By:	Alistair Richardson, CardSave Online
		//Modified:		24/10/2013
		//Modified By: 	Mark Godley, CardSave Online
		//Version:		2.0
			
		//Terms of Use:
		//This file and its enclosed scripts may be modified without limitation for the benefit of CardSave customers, CardSave Approved Partners and CardSave Approved Developers only.
		//This file and its enclosed scripts must not be modified in any way to allow it to work with any other gateway/payment system other than that which is provided by CardSave.

		//Disclaimer: This code is provided on an "as is" basis. It is the responsibility of the merchant/merchants web developer to test its implementation and function.

		//You will need to edit this file and set the various variable values BEFORE you upload the files to your server.

		//WEBSITE ADDRESS
		//EXAMPLE: https://www.mysite.com
		//INCLUDE: the folder which the files are situated on the server
		//DO NOT INCLUDE: a trailing / at the end
		$this->WebAddress = DOMAIN_NAME_PATH.'Staffs';

		//ECOM MERCHANT ID
		//EXAMPLE: Cardsa-1234567
		//NOTE: Your ECOM Merchant ID was supplied to you via email when your account went live.
		//		If you need to check your test or live Merchant IDs, login to the CardSave Merchant Mangement System
		//		and select "Gateway Account Admin". You can also change the gateway password from this page.
		//		Your Merchant ID will NOT begin with the word "Merchant".
		$this->MerchantID = 'Techlu-6015024';

		//ECOM MERCHANT PASSWORD
		//EXAMPLE: Mypassword1234
		//NOTE: Your ECOM Merchant Password was set when you first logged into the CardSave Merchant Management System.
		//		If you need to check your test or live Merchant IDs, login to the CardSave Merchant Mangement System
		//		and select "Gateway Account Admin". You can also change the gateway password from this page.
		//		Your Merchant Password will NOT contain any symbols, only letters and numbers.
		$this->Password = '5P08KVT9J4';
	}

	function hit($paymentInfo,$function){
		$cardsave = new Cardsave();
	}

	public function process($values, $orderId) {
		//XML Headers used in cURL - remember to change the function after thepaymentgateway.net in SOAPAction when changing the XML to call a different function
		$headers = array(
					'SOAPAction:https://www.thepaymentgateway.net/CardDetailsTransaction',
					'Content-Type: text/xml; charset = utf-8',
					'Connection: close'
				);

		$Amount = $values['Payment']['paid_amount']*100; //Amount must be passed as an integer in pence
		$CurrencyCode = "826"; //826 = GBP

		$OrderID = $orderId;
		$OrderDescription = "Payment " . $OrderID; //Order Description for this new transaction

		$CardName = $this->stripGWInvalidChars($values['Staff']['name_on_card']);
		$CardNumber = $values['Staff']['card_number'];
		$ExpMonth = $values['Staff']['card_expiry_month'];
		$ExpYear = $values['Staff']['card_expiry_year'];
		$CV2 = $values['Staff']['cvv'];
		$IssueNumber = null;

		$Address1 = $this->stripGWInvalidChars($values['Payment']['address']);
		$Address2 = null;
		$Address3 = null;
		$Address4 = null;
		$City = $this->stripGWInvalidChars($values['Payment']['city']);
		$State = $this->stripGWInvalidChars($values['Payment']['state']);
		$Postcode = $this->stripGWInvalidChars($values['Payment']['postal_code']);
		$Country = $this->stripGWInvalidChars($values['Payment']['country_id']);
		$EmailAddress = $this->stripGWInvalidChars($values['Payment']['email']);
		$PhoneNumber = $this->stripGWInvalidChars($values['Payment']['phone']);

		$IPAddress = $_SERVER['REMOTE_ADDR'];

		//XML to send to the Gateway. Clean it up and make sure it doesnt exceed the allowed limits
		$xml = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:xsd="http://www.w3.org/2001/XMLSchema"
		xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<CardDetailsTransaction xmlns="https://www.thepaymentgateway.net/">
		<PaymentMessage>
		<MerchantAuthentication MerchantID="'. trim($this->MerchantID) .'" Password="'. trim($this->Password) .'" />
		<TransactionDetails Amount="'. $Amount .'" CurrencyCode="'. $CurrencyCode .'">
		<MessageDetails TransactionType="SALE" />
		<OrderID>'. $this->clean($OrderID, 50) .'</OrderID>
		<OrderDescription>'. $this->clean($OrderDescription, 256) . '</OrderDescription>
		<TransactionControl>
		<EchoCardType>TRUE</EchoCardType>
		<EchoAVSCheckResult>TRUE</EchoAVSCheckResult>
		<EchoCV2CheckResult>TRUE</EchoCV2CheckResult>
		<EchoAmountReceived>TRUE</EchoAmountReceived>
		<DuplicateDelay>20</DuplicateDelay>
		<CustomVariables>
		<GenericVariable Name="MyInputVariable" Value="Ping" />
		</CustomVariables>
		</TransactionControl>
		</TransactionDetails>
		<CardDetails>
		<CardName>'. $this->clean($CardName, 100) .'</CardName>
		<CardNumber>'. $CardNumber .'</CardNumber>
		<StartDate Month="" Year="" />
		<ExpiryDate Month="'. $ExpMonth .'" Year="'. $ExpYear .'" />
		<CV2>'. $CV2 .'</CV2>
		<IssueNumber>'. $IssueNumber .'</IssueNumber>
		</CardDetails>
		<CustomerDetails>
		<BillingAddress>
		<Address1>'. $this->clean($Address1, 100) .'</Address1>
		<Address2>'. $this->clean($Address2, 50) .'</Address2>
		<Address3>'. $this->clean($Address3, 50) .'</Address3>
		<Address4>'. $this->clean($Address4, 50) .'</Address4>
		<City>'. $this->clean($City, 50) .'</City>
		<State>'. $this->clean($State, 50) .'</State>
		<PostCode>'. $this->clean($Postcode, 50) .'</PostCode>
		<CountryCode>'. $Country .'</CountryCode>
		</BillingAddress>
		<EmailAddress>'. $this->clean($EmailAddress, 100) .'</EmailAddress>
		<PhoneNumber>'. $this->clean($PhoneNumber, 30) .'</PhoneNumber>
		<CustomerIPAddress>'. $IPAddress .'</CustomerIPAddress>
		</CustomerDetails>
		<PassOutData>Some data to be passed out</PassOutData>
		</PaymentMessage>
		</CardDetailsTransaction>
		</soap:Body>
		</soap:Envelope>';

		$gwId = 1;
		$domain = "cardsaveonlinepayments.com";
		$port = "4430";
		$transattempt = 1;
		$soapSuccess = false;

		//It will attempt each of the gateway servers (gw1, gw2 & gw3) 3 times each before totally failing
		while(!$soapSuccess && $gwId <= 3 && $transattempt <= 3) {		
			
			//builds the URL to post to (rather than it being hard coded - means we can loop through all 3 gateway servers)
			$url = 'https://gw'.$gwId.'.'.$domain.':'.$port.'/';
			
			//initialise cURL
			$curl = curl_init();
			
			//set the options
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers); 
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			
			//Execute cURL request
			//$ret = returned XML
			$ret = curl_exec($curl);
			//$err = returned error number
			$err = curl_errno($curl);
			//retHead = returned XML header
			$retHead = curl_getinfo($curl);
			
			//close cURL connection
			curl_close($curl);
			$curl = null;
			
			//if no error returned
			if($err == 0) {
				//Get the status code
				$StatusCode = $this->GetXMLValue("StatusCode", $ret, "[0-9]+");
				
				if(is_numeric($StatusCode)) {
					//request was processed correctly
					
					if( $StatusCode != 30 ) {
						//set success flag so it will not run the request again.
						$soapSuccess = true;
						
						//grab some of the most commonly used information from the response
						$szMessage = $this->GetXMLValue("Message", $ret, ".+");
						$szAuthCode = $this->GetXMLValue("AuthCode", $ret, ".+");
						$szCrossReference = $this->GetCrossReference($ret);
						$szAddressNumericCheckResult = $this->GetXMLValue("AddressNumericCheckResult", $ret, ".+");
						$szPostCodeCheckResult = $this->GetXMLValue("PostCodeCheckResult", $ret, ".+");
						$szCV2CheckResult = $this->GetXMLValue("CV2CheckResult", $ret, ".+");
						$szThreeDSecureAuthenticationCheckResult = $this->GetXMLValue("ThreeDSecureAuthenticationCheckResult", $ret, ".+");				
										
						switch ($StatusCode) {				
							case 0:
								// transaction authorised
								$Response = "Transaction Sucessful: " . $szMessage;
								$Response .= "<BR>OrderID: " . $OrderID;
								$Response .= "<BR>AuthCode: " . $szAuthCode;
								$Response .= "<BR>CrossReference: " . $szCrossReference;
								break;			
							case 3:
								//3D Secure Auth required
								//Gather required variables
								$PaREQ = $this->GetXMLValue("PaREQ", $ret, ".+");								
								$ACSURL = $this->GetXMLValue("ACSURL", $ret, ".+");						
								$FormAction = $this->WebAddress . "/Secure";								
								$TermURL = $this->WebAddress . "/SecureIFrame";								
								$Process3DSURL = $this->WebAddress . "/SecureProcess";
								
								//Set CardSave_Direct_OrderID Session variable, can be used to update order after 3DS.
								$_SESSION['CardSave_Direct_OrderID'] = $OrderID;
								
								$res =  '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">' .
								'<html><head>' .
								'<script type="text/javascript"> function OnLoadEvent() { document.form.submit(); }</script>' .
								'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . 
								'<title>3D-Secure Redirect</title>' .             
								'</head>' . 
								'<body OnLoad="OnLoadEvent();">' .
								'<form name="form" action="' . $FormAction . '" method="POST" >' .
								'<input type="hidden" name="PaReq" value="' . $PaREQ . '"/>' .
								'<input type="hidden" name="MD" value="' . $szCrossReference . '"/>' .
								'<input type="hidden" name="TermUrl" value="' . $TermURL . '"/>' .
								'<input type="hidden" name="ACSURL" value="' . $ACSURL . '"/>' .
								'<input type="hidden" name="Process3DSURL" value="' . $Process3DSURL . '"/>' .
								'<noscript>' .
								'<center><p>Please click button below to Authenticate your card</p><input type="submit" value="Go"/></p></center>' .
								'</noscript>' .
								'</form></body></html>';
								echo $res;
								break;
							case 4:
								//Card Referred - treat as a decline
								$Response = "Card Referred";
								break;
							case 5:
								//Card declined
								$Response = "Your payment was not successful.";
							
								if ($szAddressNumericCheckResult == "FAILED") {
									$Response .= "<br>Billing address check failed - Please check your billing address.";
								}
								
								if ($szPostCodeCheckResult == "FAILED") {
									$Response .= "<br>Billing postcode check failed - Please check your billing postcode.";
								}
								
								if ($szCV2CheckResult == "FAILED") {
									$Response .= "<br>The CV2 number you entered is incorrect.";
								}
								
								if ($szThreeDSecureAuthenticationCheckResult == "FAILED") {
									$Response .= "<br>Your bank declined the transaction due to Verified by Visa / MasterCard SecureCode.";
								}
								
								if ($szMessage == "Card declined" || $szMessage == "Card referred") {
									$Response .= "<br>Your bank declined the payment.";
								}
								break;
							case 20:
								//duplicate transaction - check PreviousTransactionResult for PreviousStatusCode & PreviousMessage
								$soapPreviousTransactionResult = null;
								$PreviousTransactionResult = null;
								if (preg_match('#<PreviousTransactionResult>(.+)</PreviousTransactionResult>#iU', $ret, $soapPreviousTransactionResult)) {
									$PreviousTransactionResult = $soapPreviousTransactionResult[1];
									
									$PreviousMessage = $this->GetXMLValue("Message", $PreviousTransactionResult, ".+");
									$PreviousStatusCode = $this->GetXMLValue("StatusCode", $PreviousTransactionResult, ".+");
								}
							
								// need to look at the previous status code to see if the transaction was successful
								if ($PreviousStatusCode == 0) {
									$szMessage = $PreviousMessage;
									// transaction authorised
									$Response = "Transaction Sucessful: " . $szMessage;
									$Response .= "<BR>OrderID: " . $_SESSION['CardSave_Direct_OrderID'];
									$Response .= "<BR>CrossReference: " . $szCrossReference;
								} else {
									$szMessage = $PreviousMessage;
									// transaction not authorised
									$Response = "Your payment was not successful.";
								}
								break;
							default:
								$Response = "Unknown response: " . $szMessage;
								$Response .= "<BR>Returned XML: <xmp>" . $ret . "</xmp>";
								break;				
						}
						
					}
					else {
						// status code is 30 - error occured
						// get the reason from the xml
						$szMessageDetail = $this->GetXMLValue("Detail", $ret, ".+");
						
						//run the function to get the cause of the error
						$Response = "Error occurred: ";
						$Response .= $this->getErrorFromGateway($szMessageDetail);
					}
				}
			}
			
			//increment the transaction attempt if <=2
			if($transattempt <=2) {
				$transattempt++;
			} else {
				//reset transaction attempt to 1 & incremend $gwID (to use next numeric gateway number (eg. use gw2 rather than gw1 now))
				$transattempt = 1;
				$gwId++;
			}			
		}

		//echo the response to the user
		//echo $Response;
		exit;
	}

	public function carsaveSecure(){
		/*GET POST VARIABLES AND SET SESSION VARIABLES*/
		$_SESSION['CardSave_Direct_ACSURL'] = $_POST['ACSURL'];
		$_SESSION['CardSave_Direct_PaREQ'] = $_POST['PaReq'];
		$_SESSION['CardSave_Direct_MD'] = $_POST['MD'];
		$_SESSION['CardSave_Direct_TermURL'] = $_POST['TermUrl'];
		$_SESSION['CardSave_Direct_Process3DSURL'] = $_POST['Process3DSURL'];
	}

	public function carsaveIframe(){
		?>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			</head>
			<body>
				<?php
				//Initial 3DS - Post customer to ACSURL
				if (!isset($_POST['PaRes']) && isset($_SESSION['CardSave_Direct_ACSURL'])) {
				?>
					<form id="cardsave-direct-3d-secure" action="<?php echo $_SESSION['CardSave_Direct_ACSURL']; ?>" method="POST" >
						<input type="hidden" name="PaReq" value="<?php echo $_SESSION['CardSave_Direct_PaREQ']; ?>"/>
						<input type="hidden" name="MD" value="<?php echo $_SESSION['CardSave_Direct_MD']; ?>"/>
						<input type="hidden" name="TermUrl" value="<?php echo $_SESSION['CardSave_Direct_TermURL']; ?>"/>
						<noscript>
							<center><p>Please click button below to Authenticate your card</p><input type="submit" value="Go"/></p></center>
						</noscript>
					</form>
					<script type="text/javascript">
					document.getElementById('cardsave-direct-3d-secure').submit();
					</script>
				<?php
				//Return from ACSURL - Post PaRES & MD to 3DS Processing page
				} else if (isset($_POST['PaRes'])){
				?>
					<form id="cardsave-direct-3d-secure" action="<?php echo $_SESSION['CardSave_Direct_Process3DSURL']; ?>" method="POST" target="_parent">
						<input type="hidden" name="PaRes" value="<?php echo str_replace(' ', '+', htmlspecialchars($_POST['PaRes'])); ?>" />
						<input type="hidden" name="MD" value="<?php echo str_replace(' ', '+', htmlspecialchars($_POST['MD'])); ?>" />
						<noscript>
							<center><p>Please click button below to Authenticate your card</p><input type="submit" value="Go"/></p></center>
						</noscript>
					</form>
					<script type="text/javascript">
					document.getElementById('cardsave-direct-3d-secure').submit();
					</script>
				<?php } ?>
			</body>
		</html>
		<?php
		exit;
	}

	public function carsaveSecureProcess() {
		$szCrossReference = null;
		//XML Headers used in cURL - remember to change the function after thepaymentgateway.net in SOAPAction when changing the XML to call a different function
		$headers = array(
					'SOAPAction:https://www.thepaymentgateway.net/ThreeDSecureAuthentication',
					'Content-Type: text/xml; charset = utf-8',
					'Connection: close'
				);

		$PaRES = $_POST['PaRes'];
		$MD = $_POST['MD'];

		//XML to send to the Gateway - again clean it up just in chase
		$xml = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:xsd="http://www.w3.org/2001/XMLSchema"
		xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<ThreeDSecureAuthentication xmlns="https://www.thepaymentgateway.net/">
		<ThreeDSecureMessage>
		<MerchantAuthentication MerchantID="'. trim($this->MerchantID) .'" Password="'. trim($this->Password) .'" />
		<ThreeDSecureInputData CrossReference="'. $MD .'">
		<PaRES>'. $PaRES .'</PaRES>
		</ThreeDSecureInputData>
		<PassOutData>Some data to be passed out</PassOutData>
		</ThreeDSecureMessage>
		</ThreeDSecureAuthentication>
		</soap:Body>
		</soap:Envelope>';

		$gwId = 1;
		$domain = "cardsaveonlinepayments.com";
		$port = "4430";
		$transattempt = 1;
		$soapSuccess = false;

		//It will attempt each of the gateway servers (gw1, gw2 & gw3) 3 times each before totally failing
		while(!$soapSuccess && $gwId <= 3 && $transattempt <= 3) {		
			
			//builds the URL to post to (rather than it being hard coded - means we can loop through all 3 gateway servers)
			$url = 'https://gw'.$gwId.'.'.$domain.':'.$port.'/';
			
			//initialise cURL
			$curl = curl_init();
			
			//set the options
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers); 
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			
			//Execute cURL request
			//$ret = returned XML
			$ret = curl_exec($curl);
			//$err = returned error number
			$err = curl_errno($curl);
			//retHead = returned XML header
			$retHead = curl_getinfo($curl);
			
			//close cURL connection
			curl_close($curl);
			$curl = null;
			
			//if no error returned
			if($err == 0) {		
				//Get the status code
				$StatusCode = $this->GetXMLValue("StatusCode", $ret, "[0-9]+");
				
				if(is_numeric($StatusCode)) {		
					//request was processed correctly
					if( $StatusCode != 30 ) {
						//set success flag so it will not run the request again.
						$soapSuccess = true;
						
						//collect some of the most commonly used information from the response
						$szMessage = $this->GetXMLValue("Message", $ret, ".+");
						$szAuthCode = $this->GetXMLValue("AuthCode", $ret, ".+");
						$szCrossReference = $this->GetCrossReference($ret);
						$szAddressNumericCheckResult = $this->GetXMLValue("AddressNumericCheckResult", $ret, ".+");
						$szPostCodeCheckResult = $this->GetXMLValue("PostCodeCheckResult", $ret, ".+");
						$szCV2CheckResult = $this->GetXMLValue("CV2CheckResult", $ret, ".+");
						$szThreeDSecureAuthenticationCheckResult = $this->GetXMLValue("ThreeDSecureAuthenticationCheckResult", $ret, ".+");
						
						switch ($StatusCode) {				
							case 0:
								// transaction authorised
								$Response = "Transaction Sucessful: " . $szMessage;
								$Response .= "<BR>OrderID: " . $_SESSION['CardSave_Direct_OrderID'];
								$Response .= "<BR>AuthCode: " . $szAuthCode;
								$Response .= "<BR>CrossReference: " . $szCrossReference;
								break;					
							case 4:
								//Card Referred - treat as a decline
								$Response = "Card Referred";
								break;
							case 5:
								//Card declined
								$Response = "Your payment was not successful.";
							
								if ($szAddressNumericCheckResult == "FAILED") {
									$Response .= "<br>Billing address check failed - Please check your billing address.";
								}
								
								if ($szPostCodeCheckResult == "FAILED") {
									$Response .= "<br>Billing postcode check failed - Please check your billing postcode.";
								}
								
								if ($szCV2CheckResult == "FAILED") {
									$Response .= "<br>The CV2 number you entered is incorrect.";
								}
								
								if ($szThreeDSecureAuthenticationCheckResult == "FAILED") {
									$Response .= "<br>Your bank declined the transaction due to Verified by Visa / MasterCard SecureCode.";
								}
								
								if ($szMessage == "Card declined" || $szMessage == "Card referred") {
									$Response .= "<br>Your bank declined the payment.";
								}
								break;
							case 20:
								//duplicate transaction - check PreviousTransactionResult for PreviousStatusCode & PreviousMessage
								$soapPreviousTransactionResult = null;
								$PreviousTransactionResult = null;
								if (preg_match('#<PreviousTransactionResult>(.+)</PreviousTransactionResult>#iU', $ret, $soapPreviousTransactionResult)) {
									$PreviousTransactionResult = $soapPreviousTransactionResult[1];
									
									$PreviousMessage = $this->GetXMLValue("Message", $PreviousTransactionResult, ".+");
									$PreviousStatusCode = $this->GetXMLValue("StatusCode", $PreviousTransactionResult, ".+");
								}
							
								// need to look at the previous status code to see if the transaction was successful
								if ($PreviousStatusCode == 0) {
									$szMessage = $PreviousMessage;
									// transaction authorised
									$Response = "Transaction Sucessful: " . $szMessage;
									$Response .= "<BR>OrderID: " . $_SESSION['CardSave_Direct_OrderID'];
									$Response .= "<BR>CrossReference: " . $szCrossReference;
								} else {
									$szMessage = $PreviousMessage;
									// transaction not authorised
									$Response = "Your payment was not successful.";
								}
								break;
							default:
								$Response = "An error has occurred: " . $szMessage;
								break;					
						}
					}
					else {
					// status code is 30 - error occured
						$Response = "An error has occurred: " . $szMessage;
					}
				}
			}
			
			//increment the transaction attempt if <=2
			if($transattempt <=2) {
				$transattempt++;
			} else {
				//reset transaction attempt to 1 & incremend $gwID (to use next numeric gateway number (eg. use gw2 rather than gw1 now))
				$transattempt = 1;
				$gwId++;
			}			
		}

		//show final response to user
		$arrayResponse['message'] = $Response;
		$arrayResponse['szCrossReference'] = $szCrossReference;
		return $arrayResponse;
	}

	//Functions to pull result from XML
	function GetXMLValue($XMLElement, $XML, $pattern) {
		$soapArray = null;
		$ToReturn = null;
		if (preg_match('#<'.$XMLElement.'>('.$pattern.')</'.$XMLElement.'>#iU', $XML, $soapArray)) {
			$ToReturn = $soapArray[1];
		} else {
			$ToReturn = $XMLElement . " Not Found";
		}
		
		return $ToReturn;
	}

	function GetCrossReference($XML) {
		$soapArray = null;
		$ToReturn = null;
		if (preg_match('#<TransactionOutputData CrossReference="(.+)">#iU', $XML, $soapArray)) {
			$ToReturn = $soapArray[1];
		} else {
			$ToReturn = "No Data Found";
		}
		
		return $ToReturn;
	}

	function stripGWInvalidChars($strToCheck) {
		$toReplace = array("<","&");
		$replaceWith = array("","&amp;");
		$cleanString = str_replace($toReplace, $replaceWith, $strToCheck);
		return $cleanString;
	}

	// remove/convert restricted characters to html equivalent and ensure values don�t go over the amount allowed
	function clean($string, $numberLimit) {
		// remove restricted characters
		$toReplace = array("#","\\",">","<", "\"", "[", "]");
		$string = str_replace($toReplace, "", $string);

		// remove html special chars and turn into html equivalent value
		$string = htmlspecialchars($string);
		
		// now ensure it doesnt exceed the allowed amount
		$string = substr($string, 0, $numberLimit);
			
		//return clean string
		return $string;
	}

	//get the error from the gateway and show a friendly message to the user i.e invalid card number
	function getErrorFromGateway($szMessageDetail){
		if ($szMessageDetail == "Invalid card type"){
			// invalid card type/number
			$szMessageError = "Invalid card type/number.";
		}
		else if ($szMessageDetail == "Passed variable (PaymentMessage.CardDetails.CV2) has an invalid value"){
			// invalid CV2
			$szMessageError = "Invalid CV2 number.";
		}
		else if ($szMessageDetail == "Required variable (PaymentMessage.CardDetails.CardNumber) is missing"){
			// no card number entered
			$szMessageError = "No card number entered.";
		}
		else if ($szMessageDetail == "Passed variable (PaymentMessage.CardDetails.CardNumber) has an invalid value"){
			// invalid card number - normally caused by exceeding the length
			$szMessageError = "Invalid card number.";
		}
		else if ($szMessageDetail == "Required variable (PaymentMessage.CardDetails.ExpiryDate.Month) is missing"){
			// missing expiry month
			$szMessageError = "Expiry month missing.";
		}
		else if ($szMessageDetail == "Required variable (PaymentMessage.CardDetails.ExpiryDate.Year) is missing"){
			// missing expiry year
			$szMessageError = "Expiry year missing.";
		}
		else if ($szMessageDetail == "Passed variable (PaymentMessage.CardDetails.ExpiryDate.Year) has an invalid value"){
			// invalid expiry year -  - normally caused by exceeding the length
			$szMessageError = "Invalid expiry year.";
		}
		else if ($szMessageDetail == "Passed variable (PaymentMessage.CardDetails.ExpiryDate.Month) has an invalid value"){
			// invalid expiry month -  - normally caused by exceeding the length
			$szMessageError = "Invalid expiry month.";
		}
		else if ($szMessageDetail == "Passed variable (PaymentMessage.CardDetails.IssueNumber) has an invalid value"){
			// invalid issue number - normally caused by exceeding the length
			$szMessageError = "Invalid issue number.";
		}
		else {
			// other error - return what the gateway advised
			$szMessageError = $szMessageDetail;
		}
		
		//return the error from gateway
		return $szMessageError;
	}

	function beforeRedirect(Controller $controller){}
	function beforeRender(Controller $controller){}
	function shutdown(Controller $controller){}
}
?>