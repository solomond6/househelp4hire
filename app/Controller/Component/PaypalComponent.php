<?php 
App::import('Vendor','paypal' ,array('file'=>'Paypal.php'));
class PaypalComponent extends Object{
	function startup( &$controller ) {
	  $this->Controller =& $controller;
	}
	function initialize(Controller $controller) { }
	function hit($paymentInfo,$function){
		$paypal = new Paypal();
		if ($function=="DoDirectPayment")
			return $paypal->DoDirectPayment($paymentInfo);
			elseif ($function=="SetExpressCheckout")
			return $paypal->SetExpressCheckout($paymentInfo);
			elseif ($function=="GetExpressCheckoutDetails")
			return $paypal->GetExpressCheckoutDetails($paymentInfo);
			elseif ($function=="DoExpressCheckoutPayment")
			return $paypal->DoExpressCheckoutPayment($paymentInfo);
			else
			return "Function Does Not Exist!";
	}
	function beforeRender(Controller $controller){}
	function shutdown(Controller $controller){}
}
?>