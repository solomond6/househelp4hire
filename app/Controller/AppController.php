<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	// public $components = array('Session', 'Cookie', 'Auth');
	public $components = array(
		'Session',
		'RequestHandler',
		'Cookie',
		//'DebugKit.Toolbar',
		'Auth' => array(
            'autoRedirect' => false,
	        'loginAction' => array(
	        	'admin' => false,
	        	'agent' => false,
	        	'jobseeker' => false,
	        	'vet' => false,
	            'controller' => 'Users',
	            'action' => 'login'
	        ),
			'loginRedirect' => array(
				'controller' => 'Users',
				'action' => 'dashboard',
				'admin' => true,
	        	'agent' => true,
	        	'jobseeker' => true,
	        	'vet' => true
			),
			'logoutRedirect' => array(
	        	'admin' => false,
	        	'agent' => false,
	        	'jobseeker' => false,
	        	'vet' => false,
				'controller' => 'Users',
				'action' => 'home'
			),
	        'authError' => 'Please login to continue.',
	        //'flash' => array('element' => 'flash/default', 'key' => 'auth', 'params' => array('class' => 'error', 'title' => 'Authentication Error')),
	        'authorize' => 'Controller',
	        'authenticate' => array(
	            'Form' => array(
	            	'model' => 'User',
	            	'scope' => array('User.status' => 1),
	                'fields' => array(
	                	'username' => 'email',
	                	'password' => 'password'
	                	),
	            )
	        ),
	    )
	);
	public $helpers = array('Html', 'Form', 'Session');
	
	public $validImageFormats = array(".jpg", ".gif", ".png", ".jpeg");
	
	public $notAllowedWord = array('"', "'", "!", "@", "#", "$", "%", "^", "&", "*", "/");
	
	public $user_types = array('2' => 'Photographer (Pro)', '3' => 'Photographer (Hobbyist)', '4' => 'Corporate Buyer', '5' => 'Digital Artist', '6' => 'Interior Designer', '7' => 'Home Decorator', '8' => 'Other');
	
	public $order_status = array('Pending' => 'Pending', 'Paid' => 'Paid', 'Completed' => 'Completed');
	
	public $bookingStatusChk = array('Interviewed'=>'Interviewed','Cancelled'=>'Cancelled','Pending'=>'Pending','Absent'=>'Absent','Selected'=>'Selected');
	
	public function beforeFilter(){
		$this->loadmodel('Setting');
		$this->Setting->recursive = 0;
		$siteSetting = $this->Setting->find('first',array('conditions'=>array('Setting.id' => 1)));
		$this->set('siteSetting', $siteSetting);
		
		if (!$this->Auth->loggedIn()) {
			$this->Auth->authError = false;
		}

		 if(isset($this->request->prefix)) {
            switch ($this->request->prefix) {
            	// case 'admin':
            	// 	$this->layout = 'admin_layout';
            	// 	$admin = true;
            	// 	break;
            	// case 'agent':
            	// 	$this->layout = 'agent_layout';
            	// 	$agent = true;
            	// 	break;
            	// case 'jobseeker':
            	// 	$this->layout = 'default';
            	// 	$jobseeker = true;
            	// 	break;
            }
        }
		// if(isset($this->params['prefix']) &&  ($this->params['prefix'] == 'admin'))
		// {
		// 	$this->layout = 'admin_default';
		// 	$this->Auth->LoginError = 'Username and password mismatch';
		// 	$this->Auth->authorize = array('Controller');
		// 	$this->Auth->authenticate = array(
		// 		'Form' => array(
		// 			'model' => 'User',
		// 			'scope' => array('User.status' => 1),
		// 			'fields' => array(
		// 				'username' => 'email',
		// 				'password' => 'password'
		// 			)
		// 		)
		// 	);
		// 	$this->Auth->autoRedirect = true;
		// 	$this->Auth->loginAction = array('controller' => 'Users', 'action' => 'login', 'admin' => true, 'prefix' => 'admin');
		// 	$this->Auth->loginRedirect = array('controller' => 'Users', 'action' => 'profile', 'admin' => true, 'prefix' => 'admin');				
		// 	$this->Auth->logoutRedirect = array('controller' => 'Users', 'action' => 'login', 'admin' => true, 'prefix' => 'admin');
		// 	if($this->Session->read('Auth.User.id')=='1')
		// 	{
		// 		$this->Auth->allow('*');
		// 	}
		// 	else
		// 	{
		// 		$this->Auth->allow(array('admin_login', 'admin_logout'));
		// 	}
		// }else if(isset($this->params['prefix']) &&  ($this->params['prefix'] == 'agent'))
		// {

		// 	$this->layout = 'agent_default';
		// 	// exit;
		// 	$this->Auth->LoginError = 'Username and password mismatch';
		// 	$this->Auth->authorize = array('Controller');
		// 	$this->Auth->authenticate = array(
		// 		'Form' => array(
		// 			'model' => 'User',
		// 			'scope' => array('User.status' => 1, 'User.user_type' => 4, 'User.is_verified'=>1),
		// 			'fields' => array(
		// 				'username' => 'email',
		// 				'password' => 'password'
		// 			)
		// 		)
		// 	);
		// 	$this->Auth->autoRedirect = true;
		// 	$this->Auth->loginAction = array('controller' => 'Users', 'action' => 'login', 'agent' => true, 'prefix' => 'agent');
		// 	$this->Auth->loginRedirect = array('controller' => 'Staffs', 'action' => 'index', 'agent' => true, 'prefix' => 'agent');				
		// 	$this->Auth->logoutRedirect = array('controller' => 'Users', 'action' => 'login', 'agent' => true, 'prefix' => 'agent');

		// 	if($this->Session->read('Auth.User.id')=='1')
		// 	{
		// 		$this->Auth->allow('*');
		// 	}
		// 	else
		// 	{
		// 		$this->Auth->allow(array('agent_login', 'agent_logout'));
		// 	}
		// } else {
		// 	$this->layout = 'default';	
		// 	//Security::setHash('md5');
		// 	//$this->Auth->LoginError = 'Username and password mismatch!';
		// 	$this->Auth->authorize = array('Controller');
		// 	$this->Auth->autoRedirect = true;
		// 	$this->Auth->authError = "You are not authorized to access that location.";
		// 	$this->Auth->loginRedirect = array('controller' => $this->params['controller'], 'action' => $this->params['action'], 'admin' => false);
		// 	$this->Auth->logoutRedirect = array('controller' => 'Users', 'action' => 'index', 'admin' => false);
		// 	if($this->Session->read('Auth.User.id')=='1')
		// 	{
		// 		$this->Auth->allow('*');
		// 	} else {
		// 		$this->Auth->allow('setMenuSession');
		// 	}
		// 	$this->Auth->authenticate = array(
		// 		'Form' => array(
		// 			'model' => 'User',
		// 			'scope' => array('User.status' => 1, 'User.user_type' => 2, 'User.is_verified'=>1),
		// 			'fields' => array(
		// 				'username' => 'email',
		// 				'password' => 'password'
		// 			)
		// 		)
		// 	);
		// 	$this->Auth->loginAction = array('controller' => 'Users', 'action' => 'login', 'admin' => false);
		// 	$this->loadModel('Page');
		// 	$this->Page->recursive = 0;
		// 	$contactpage=$this->Page->find('first',array('conditions'=>array('Page.id' => 5)));
		// 	$this->set('terms', $this->Page->read(null, 7));
		// 	$this->loadModel('Faq');
		// 	$faqs=$this->Faq->find('list',array('fields'=>'Faq.question, Faq.solution', 'conditions'=>array('Faq.status' => 1)));
		// 	$this->loadModel('User');
		// 	$locations = $this->User->Location->find('list');
		// 	$this->set(compact('contactpage', 'terms', 'faqs', 'locations'));
		// 	$this->set('payCms', $this->Page->read(null, 10));
		// }
		$bookingStatus = array('Interviewed'=>'Interviewed','Selected'=>'Selected','Rejected'=>'Rejected','Cancelled'=>'Cancelled','Pending'=>'Pending','Absent'=>'Absent');
		$this->set(compact('bookingStatus'));
	}
	function beforeRender () {  
		//$this->_setErrorLayout();
		//$this->response->disableCache();
		if($this->params['action'] != 'index' && strtolower($this->params['controller']) != 'staffs'){ 
			$this->response->disableCache();
		}
	}
	
	// function isAuthorized(){		
	// 	if(isset($this->params['prefix']) &&  ($this->params['prefix'] == 'admin'))
	// 	{
	// 		if($this->Session->read('Auth.User.is_admin') !='1') {
	// 			$this->redirect(array('controller' => 'users', 'action' => 'index', 'admin' => false));
	// 		}
	// 	}
	// 	return true;
	// }
	public function isAuthorized($user) {
 		# Accept if Admin
        // if($user['admin']){
        //     return true;
        // }

        # Check if current prefix is admin or physician and authenticate user
        if(isset($this->request->prefix)) {
            switch ($this->request->prefix){
            	case 'admin':
                	if(!$user['admin']){
        				$this->Auth->authError = 'Sorry, you do not have permission to access the Administrators\'s area';
                	}else{
                		$this->layout = 'admin_default';
                	}
                    return $user['admin'];
                    break;
                case 'vet':
                	if(!$user['vet']){
        				$this->Auth->authError = 'Sorry, you do not have permission to access the Administrators\'s area';
                	}else{
                		$this->layout = 'vet_default';
                	}
                    return $user['vet'];
                    break;
                case 'agent':
                	if(!$user['agent']){
        				$this->Auth->authError = 'Sorry, you do not have permission to access the Agent\'s area';
                	}else{
                		$this->layout = 'agent_default';
                	}
                    return $user['agent'];
                    break;
                case 'jobseeker':
                	if(!$user['jobseeker']){
        				$this->Auth->authError = 'Sorry, you do not have permission to access the Jobseeker\'s area';
                	}else{
                		$this->layout = 'default';
                	}
                    return $user['jobseeker'];
                    break;
            }
        }else{
        	$this->layout = 'default';
            return true;
        }

        $this->Auth->authError = 'Sorry, you do not have permission to access the Admin area';
        return false;
    }

	public function emailCheck($email){
		$emailPat = '/^([a-z\d])(([-a-z\d._])*([a-z\d]))*\@'.
						'([a-z\d])(([a-z\d-])*([a-z\d]))+'.
						'(\.([a-z\d])([-a-z\d_-])?([a-z\d])+)+$/i';
		$isMatches = preg_match($emailPat, $email);
		if($isMatches == 0)
		{
			return 'Email address seems incorrect (check @ and .\'s).';
		}
	}
	public function Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body){
		
		$mail_Headers = "MIME-Version: 1.0\r\n";
		$mail_Headers .= "Content-type: text/html; charset=UTF-8\r\n";
		$mail_Headers .= "From: ${mail_From}\r\n";
		if($mail_CC != '')
		{
			$mail_Headers .= "Cc: ${mail_CC}\r\n";
		}

		/*
        $senderDomain=explode('@',$mail_To);
		
		if( strtolower($senderDomain[1])=="hotmail.com" || strtolower($senderDomain[1])=="mail.com" )
		{
			
			$params = array();
			$params['mail_Body'] = $mail_Body;
			$params['mail_From'] = $mail_From;
			$params['mail_To'] = $mail_To;
			$params['mail_subject'] = $mail_subject;
			$params['mail_CC'] = $mail_CC;
			
			$poststr = html_build_query($params);
		
			$post_data="mail_From=".$mail_From."&mail_To=".$mail_To."&mail_subject=".$mail_subject."&mail_Body=".rawurlencode($mail_Body)."&mail_CC=".$mail_CC;
			$ch = curl_init();                    // initiate curl
			$url = "http://idevtek.com/hh4h-mail.php"; // where you want to post data
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); // define what you want to post
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
			$output = curl_exec ($ch); // execute
			 
			curl_close ($ch); // close curl handle
			return true;
		
		}
		*/
		if(mail($mail_To, $mail_subject, $mail_Body, $mail_Headers))
		{
			return true;
		}
		else
		{
			return false;
		}
		return true;
		/* New one */
		/*
		$headers = "From: " . $mail_From . "\r\n";
		$headers = "Reply-to: hello@Househelp4hire.com\r\n";
		if($mail_CC != '')
		{
			$headers .= "CC: " . $mail_CC . "\r\n";
		}
		
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if (mail($mail_To, $mail_subject, $mail_Body, $headers,'-fhello@Househelp4hire.com')) {
		
			return true;
		}
		else
		{
			return false;
		}
		return true;
		*/

	}
	public function create_coupon($length=10){
	   $phrase1 = "";
	   $chars1 = array (
				  "1","2","3","4","5","6","7","8","9","0",
				  "a","A","b","B","c","C","d","D","e","E","f","F","g","G","h","H","i","I","j","J",
				  "k","K","l","L","m","M","n","N","o","O","p","P","q","Q","r","R","s","S","t","T",
				  "u","U","v","V","w","W","x","X","y","Y","z","Z"
				  );

	   $count1 = count ($chars1) - 1;
	   for ($i = 0; $i < $length; $i++)
		  $phrase1 .= $chars1[rand (0, $count1)];

	   return $phrase1;
	}
	public function get_promo_code() {
		$this->loadModel('Promo');
		$data = $this->Promo->find('first', array('conditions' => array('Promo.status' => 1, 'Promo.created <=' => date('Y-m-d'), 'Promo.expired >=' => date('Y-m-d')), 'order' => 'Promo.id DESC'));
		return $data;
	}
	public function file_download($filename, $id = null) {
		$path = UPLOADED_IMAGES.'org'.DS;
		$downloded_filename = $filename;
		if($id!=''){
			$downloded_filename = $id.'_'.$filename;
		}
		$filename = $path.$filename;
		if (file_exists($filename)) {
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Type: application/force-download");
		header("Content-Type: application/zip");
		header("Content-Type: application/pdf");
		header("Content-Type: application/octet-stream");
		header("Content-Type: image/png");
		header("Content-Type: image/gif");
		header("Content-Type: image/jpg");
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Type: application/vnd.ms-powerpoint");
		header("Content-type: application/x-msexcel");
		header("Content-type: application/msword");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=" . $downloded_filename . ";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . filesize($filename));
		readfile($filename) or die('Errors');
		exit(0);
		}
	}
	public function encryption_decryption($string){
		$encryptKey = 'ab12333way2code';
		$convertedString = Security::cipher($string, $encryptKey);
		return $convertedString;
	}
	public function setMenuSession(){
		if(isset($_POST['linkId']) && $_POST['linkId']!=null){
			$this->Session->write('setMenuSession', $_POST['linkId']);
			echo 'Success';
		}
		exit;
	}
	public function backup($model,$options=null) {
		//pr($this->Project->schema());
		$this->loadModel($model);
		if(isset($this->params['pass'][0]) && $this->params['pass'][0]=='export'){
			$date = date('Y-m-d_H-i-s');
			$fileName = $model.'_'.$date;
			$this->layout=false;
			$this->autoRender=false;
			$this->$model->recursive = -1;
			if(isset($options) && !empty($options))
			{
				$rows = $this->$model->find('all',$options);
			}
			else
			{
				$rows = $this->$model->find('all');
			}
			
			$fields = array_keys($this->$model->getColumnTypes());

			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);
			date_default_timezone_set('Europe/London');

			if (PHP_SAPI == 'cli')
				die('This example should only be run from a Web Browser');

			/** Include PHPExcel */
			//require_once '../Classes/PHPExcel.php';
			App::import('Vendor', 'PHPExcel');

			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();

			// Set document properties
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
										 ->setLastModifiedBy("RealtorProp")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Test result file");
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0);
			$colCountz = 1;
			foreach($fields as $fieldKey => $fieldValue){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow("$fieldKey", "$colCountz", "$fieldValue");
			}
			$colCountz++;
			foreach($rows as $rowArr){
				$finalRows = array_values($rowArr[$model]);
				foreach($finalRows as $rowKey => $rowValue){
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow("$rowKey", "$colCountz", "$rowValue");
				}
				$colCountz++;
			}
			// Miscellaneous glyphs, UTF-8
			//$objPHPExcel->setActiveSheetIndex(0)
						//->setCellValue('A4', 'Miscellaneous glyphs')
						//->setCellValue('A5', '�����������������');

			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Simple');


			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);


			// Redirect output to a client�s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}
		if(!empty($this->request->data)){
			if(isset($this->request->data[$model]['file']['name']) && $this->request->data[$model]['file']['name'] != null){
				if($this->request->data[$model]['file']['type'] == 'application/csv' || $this->request->data[$model]['file']['type'] == 'application/vnd.ms-excel' || $this->request->data[$model]['file']['type'] == 'application/octet-stream'|| $this->request->data[$model]['file']['type'] == 'application/x-msexcel'){
					App::import('Vendor', 'reader');
					$excel = new Spreadsheet_Excel_Reader();
					$excel->read($this->request->data[$model]['file']['tmp_name']);   
					$results = $excel->sheets[0]['cells'];
					if(count($results > 0)){
						$filedArr = $results[1];
						unset($results[1]);
						$valueArr = array_values($results);
						unset($this->request->data[$model]['file']);
						foreach($valueArr as $valueRow){
							foreach($filedArr as $fieldNameKey => $fieldName){
								$finalValue = null;
								if(isset($valueRow[$fieldNameKey])){
									$finalValue = $valueRow[$fieldNameKey];
								}
								$this->request->data[$model][$fieldName] = $finalValue;
							}
							//pr($this->request->data);
							$this->$model->create();
							$this->$model->save($this->request->data[$model]);
						}
					}
				} else {
					$this->Session->setFlash(__('Please upload an EXCEL file.', true));
				}
			} else {
				$this->Session->setFlash(__('Please upload an EXCEL file.', true));
			}
		}
	}
}