<?php
require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'flutterwave' . DS . 'vendor' . DS . 'autoload.php');
use Flutterwave\Card;
use Flutterwave\Flutterwave;
use Flutterwave\AuthModel;
use Flutterwave\Currencies;
use Flutterwave\Countries;
use Flutterwave\FlutterEncrypt;
App::uses('AppController', 'Controller');
/**
 * Staffs Controller
 *
 * @property Staff $Staff
 */
class StaffsController extends AppController {
	public $uses = array('Staff','Location','Category','Language', 'Location', 'Education', 'Booking', 'StaffPreferedLocation', 'StaffSpokenLanguage', 'StaffPreferedEducation', 'Locality', 'StaffPreferedLocality', 'Email', 'Mailer', 'PaymentOption', 'Payment','Viewcontacts','Coupon','User','Agent','StaffCategory','Setting','VettedCandidate');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('loadmore', 'firemail','view','saveContacts','validateOtp','flutterwave','validateOtp2','vetPaymentCandidate', 'vettinCompanies','vetPayment');
	}
	public $helpers = array('SocialShare.SocialShare');
	public $components = array( 
	  'Session', 
	  'Cardsave'
	 );
/**
 * index method
 *
 * @return void
 */

	public function firemail() {
		$subscribers = $this->Email->find('list', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'DESC')));
		if(count($subscribers)>0){
			$before = date('Y-m-d', strtotime('-7 days'));
			$this->Staff->recursive = 0;
			$staffs = $this->Staff->find('all', array('fields'=>'Category.name,Staff.name,Staff.friendly_url,Staff.image1', 'conditions' => array('Staff.reg_date >=' => $before)));

			if(count($staffs)>0){
				$contents = '';
				foreach($staffs as $staff){
					$contents.= '<div  style="margin: 3px; margin: 6px; float: left; padding: 8px; background: #FA7D01" onMouseOver="this.style.background=#fdc53c" onMouseOut="this.style.background=#FA7D01">
						<div style="margin-bottom:5px">
							<img src="'.PAGE_IMAGES_URL.$staff['Staff']['image1'].'" width="180" height="200" alt="" border="0" />
						</div>
						<div style="color: #fff">
							<p style="font-size: 16px; padding:3px 0; margin: 0;"><strong>'.$staff['Staff']['name'].'</strong></p>
							<p style="padding: 3px 0; margin: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><strong>Category</strong>: '.$staff['Category']['name'].'</p>
							<p style="padding: 3px 0; margin: 0;"><a href="'.DOMAIN_NAME_PATH.$staff['Staff']['friendly_url'].'" target="_blank" style="background-color: #faa732; background-image: linear-gradient(to bottom, #fbb450, #f89406); background-repeat: repeat-x; border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);-moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; border-image: none; border-radius: 4px; border-style: solid; border-width: 1px; box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05); display: inline-block; font-size: 14px; line-height: 20px; margin-bottom: 0; padding: 4px 12px; text-align: center; text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75); vertical-align: 4px; text-decoration:none; color: #ffffff;">View Profile</a></p>
						</div>
					</div>';
				}
				$siteSetting = $this->viewVars['siteSetting'];
				$this->Mailer->recursive = 0;
				$mailer = $this->Mailer->find('first', array('conditions'=>array('Mailer.id'=>2)));
				$html = $mailer['Mailer']['html_content'];
				$logoPath = DOMAIN_NAME_PATH.'img/site_logo/'.$siteSetting['Setting']['site_logo'];
				$mail_From = $siteSetting['Setting']['noreply_email'];
				$mail_CC = null;
				$subject = $mailer['Mailer']['name'];
				$html = str_replace("##logo_path##", $logoPath, $html);
				$html = str_replace("##noreply_email##", $siteSetting['Setting']['noreply_email'], $html);
				$html = str_replace("##subject##", $subject, $html);
				$html = str_replace("##content##", $contents, $html);
				foreach($subscribers as $subscriber){
					$mail_To = $subscriber;
					$convertedString = bin2hex(Security::cipher(serialize($mail_To),Configure::read('Security.salt')));
					$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>Click here</a>";
					$html = str_replace("##unsubscribe##", $link, $html);
					$mail_body = str_replace("##user##", $mail_To, $html);
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
					$mail_body = str_replace($link, "##unsubscribe##", $html);
				}
			}
		}
		exit;
	}

	public function index($url=null) {
		$page = 1;
		$this->Staff->recursive = 1;
		//$this->set('staffs', $this->paginate());
		$conditions = array('Staff.status'=>1);
		$category = array();
		if($url != null){
			$category = $this->Category->find('first', array('fields'=>'Category.id,Category.name,Category.friendly_url', 'conditions' => array('Category.friendly_url' => $url)));
			
			
			if(count($category)>0)
			{
				$category_id=$category['Category']['id'];
				$subquery=new stdClass();
				$subquery->type="expression";
				$subquery->value="`Staff`.id IN (select distinct staff_id from hh_staff_categories where category_id=".$category_id.") and `Staff`.status=1";
				
				$conditions = array($subquery);
			
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));
			}
			else
			{
				$conditions = array('Staff.status'=>1);				
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));
			}			
			
		}
		if($this->request->is('post')){
			if(isset($this->request->data['Staff']['search'])){
				$status = $this->request->data['Staff']['status'] ? $this->request->data['Staff']['status'] : 1;
				$matchingCategory=$this->Category->find('first',array('conditions'=>array('Category.name Like'=>'%'.$this->request->data['Staff']['search'].'%')));
			
				if(count($matchingCategory)>0){
					$category_id=$matchingCategory['Category']['id'];
					$subquery=new stdClass();
					$subquery->type="expression";
					$subquery->value="`Staff`.id IN (select distinct staff_id from hh_staff_categories where category_id=".$category_id.") and `Staff`.status='$status'";
					
					$conditions = array($subquery);
					$conditions[] = array('Staff.status'=>$status);
					/*
					$conditions[] = array('Staff.status'=>1, 'or'=>array( 'Staff.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.mini_biography LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.work_experience LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.profile_description LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Origin.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Religion.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%'));
					*/
					$headerKeyWord = $this->request->data['Staff']['search'];
					unset($this->request->data['Staff']);
					$this->request->data['Staff']['search'] = $headerKeyWord;
					//$staffs = $this->Staff->find('all', array('conditions'=>array('or'=>$conditions), 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));
					
					$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));
				}
				else{
					$conditions = array('Staff.status'=>$status, 'or'=>array( 'Staff.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.mini_biography LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.work_experience LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.profile_description LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Origin.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Religion.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%'));
					$headerKeyWord = $this->request->data['Staff']['search'];
					unset($this->request->data['Staff']);
					$this->request->data['Staff']['search'] = $headerKeyWord;
					$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8,  'order'=>array('Staff.id'=>'DESC'),'page'=>$page));
				}
					//$this->set('headerKeyWord', $this->request->data['Staff']['search']);
					//unset($this->request->data['Staff']['search']);
			} 
			else 
			{
				$staffId = array();
				$listCollection= array();
				$status = $this->request->data['Staff']['status'];
				$this->Session->write('qStatus', $status);
				if($this->request->data['Staff']['spoken_language'] != ''){
					$langStaffRow = array_unique($this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.staff_id', 'conditions'=>array('StaffSpokenLanguage.language_id'=>$this->request->data['Staff']['spoken_language']))));
					$staffId = array_merge($staffId, $langStaffRow);
					$listCollection[]=$langStaffRow;
				}
				if($this->request->data['Staff']['prefered_locality'] != ''){
					$locStaffRow = array_unique($this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.staff_id', 'conditions'=>array('StaffPreferedLocality.locality_id'=>$this->request->data['Staff']['prefered_locality']))));
					$staffId = array_merge($staffId, $locStaffRow);
					$listCollection[]=$locStaffRow;
				}
				if($this->request->data['Staff']['education'] != ''){
					$eduStaffRow = array_unique($this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.staff_id', 'conditions'=>array('StaffPreferedEducation.education_id'=>$this->request->data['Staff']['education']))));
					$staffId = array_merge($staffId, $eduStaffRow);
					$listCollection[]=$eduStaffRow;
				}
				
				
				$staffId = array_unique($staffId);
				if($this->request->data['Staff']['experience'] == 1){
					$minExp = 0;
					$maxExp = 1;
				} else if($this->request->data['Staff']['experience'] == 2){
					$minExp = 1;
					$maxExp = 3;
				} else if($this->request->data['Staff']['experience'] == 3){
					$minExp = 3;
					$maxExp = 5;
				} else if($this->request->data['Staff']['experience'] == 4){
					$minExp = 5;
					$maxExp = 10;
				} else if($this->request->data['Staff']['experience'] == 5){
					$minExp = 10;
					$maxExp = 100;
				} else {
					$minExp = 0;
					$maxExp = 0;
				}
				if($this->request->data['Staff']['age'] == 1){
					$minAge = 18;
					$maxAge = 25;
				} else if($this->request->data['Staff']['age'] == 2){
					$minAge = 25;
					$maxAge = 30;
				} else if($this->request->data['Staff']['age'] == 3){
					$minAge = 30;
					$maxAge = 35;
				} else if($this->request->data['Staff']['age'] == 4){
					$minAge = 35;
					$maxAge = 40;
				} else if($this->request->data['Staff']['age'] == 5){
					$minAge = 40;
					$maxAge = 60;
				} else {
					$minAge = 0;
					$maxAge = 0;
				}
				if($this->request->data['Staff']['category_id'] != ''){
					$categoryStaffRow = array_unique($this->StaffCategory->find('list', array('fields'=>'StaffCategory.staff_id', 'conditions'=>array('StaffCategory.category_id'=>$this->request->data['Staff']['category_id']))));
					$staffId = array_merge($staffId, $categoryStaffRow);
					$listCollection[]=$categoryStaffRow;
				}
				if(count($listCollection)>=2)
				{
					$staffId = call_user_func_array('array_intersect',$listCollection);
					//print_r($staffId);
				}
				
				if(!empty($this->request->data['Staff']['immigration_status']))
					$immigration_status=$this->request->data['Staff']['immigration_status'];
					else
					$immigration_status='';
										
				if(!empty($this->request->data['Staff']['nationality']))
					$nationality=$this->request->data['Staff']['nationality'];
					else
					$nationality='';
					
				if(!empty($this->request->data['Staff']['english_speaking']))
					$english_speaking=$this->request->data['Staff']['english_speaking'];
					else
					$english_speaking='';
					
				if($immigration_status!='' && $nationality!='' && $english_speaking!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.english_speaking'=>$english_speaking, 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality, 'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
					else if($immigration_status!='' && $nationality!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'],'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality, 'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
					else if($immigration_status!='' && $english_speaking!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'],'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'],'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
					else if($english_speaking!='' && $nationality!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'],'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality, 'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
					else if($immigration_status!='' && $nationality=='' && $english_speaking==''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
				    else if($immigration_status=='' && $nationality!='' && $english_speaking==''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality,'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
					else if($immigration_status=='' && $nationality=='' && $english_speaking!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge,'Staff.english_speaking'=>$english_speaking, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality,'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
					else{
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'],'Staff.id'=>$staffId, 'Staff.status'=>$status);
					}
			
				//print_r($conditions);die;
				if($this->request->data['Staff']['category_id'] == ''){
					//unset($conditions['OR']['Job.job_title LIKE']);
					unset($conditions['Staff.category_id']);
				}
				if($this->request->data['Staff']['experience'] == ''){
					unset($conditions['Staff.experience >=']);
					unset($conditions['Staff.experience <=']);
				}
				if($this->request->data['Staff']['age'] == ''){
					unset($conditions['Staff.age >=']);
					unset($conditions['Staff.age <=']);
				}
				if($this->request->data['Staff']['sex'] == ''){
					unset($conditions['Staff.sex']);
				}
				if($this->request->data['Staff']['accommodation'] == ''){
					unset($conditions['Staff.accommodation']);
				}
				if($this->request->data['Staff']['origin_id'] == ''){
					unset($conditions['Staff.origin_id']);
				}
				if($english_speaking == ''){
					unset($conditions['Staff.english_speaking']);
				}
				if($this->request->data['Staff']['marital_status'] == ''){
					unset($conditions['Staff.marital_status']);
				}
				if($this->request->data['Staff']['religion_id'] == ''){
					unset($conditions['Staff.religion_id']);
				}
				/*if($this->request->data['Staff']['base_salary'] == ''){
					unset($conditions['Staff.base_salary >=']);
				}
				if($this->request->data['Staff']['max_salary'] == ''){
					unset($conditions['Staff.max_salary <=']);
				}*/
				if((isset($langStaffRow) && count($langStaffRow) == 0) || (isset($locStaffRow) && count($locStaffRow) == 0) || (isset($eduStaffRow) && count($eduStaffRow) == 0)){
					$conditions['Staff.id'] = 0;
				} else if((!isset($langStaffRow)) && (!isset($locStaffRow)) && (!isset($eduStaffRow))){
					if(count($staffId) == 0){
						unset($conditions['Staff.id']);
					}
				}
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'),'page'=>$page));
			}
		}
		
		//pr($staffs);die;
		$this->set('staffs', $staffs);
		$this->set('category', $category);
		$countries = $this->Staff->Country->find('list',array('order'=>'Country.name'));
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list');
		$localities = $this->Locality->find('list');		
		$educations = $this->Education->find('list');
		$religions = $this->Staff->Religion->find('list');
		$experiences = array_unique($this->Staff->find('list', array('fields'=>'Staff.experience', 'order'=>array('Staff.experience'=>'ASC'))));
		
		$experience = array();
		if(count($experiences)>0){
			foreach($experiences as $experienceList){
				$yearPrint = ' Year';
				if($experienceList > 1){
					$yearPrint = ' Years';
				}
				$experience[$experienceList] = $experienceList.$yearPrint;
			}
		}
		$ages = array_unique($this->Staff->find('list', array('fields'=>'Staff.age', 'order'=>array('Staff.age'=>'ASC'))));
		$age = array();
		if(count($experiences)>0){
			foreach($ages as $ageList){
				$yearPrintAge = ' Year';
				if($ageList > 1){
					$yearPrintAge = ' Years';
				}
				$age[$ageList] = $ageList.$yearPrintAge;
			}
		}
		
		$immigration_list[""] = "Select";
		$immigration_list["N/A"] = "N/A";
  		$immigration_list["Valid Resident Card"] = "Valid Resident Card";
  		$immigration_list["Resident Card Required"] = "Resident Card Required";
  		$this->set('immigration_list', $immigration_list);
  		
  		$nationality_list[""] = "Select Nationality";
  		$nationality_list["Benin"] = "Benin";
  		$nationality_list["Burkina Faso"] = "Burkina Faso";
  		
  		$nationality_list["Cape Verde"] = "Cape Verde";
  		$nationality_list["The Gambia"] = "The Gambia";
  		
  		$nationality_list["Ghana"] = "Ghana";
  		$nationality_list["Guinea"] = "Guinea";
  		$nationality_list["Guinea-Bissau"] = "Guinea-Bissau";
  		$nationality_list["Ivory Coast"] = "Ivory Coast";
  		$nationality_list["Liberia"] = "Liberia";
  		$nationality_list["Mali"] = "Mali";
  		$nationality_list["Mali Federation"] = "Mali Federation";
  		$nationality_list["Mauritania"] = "Mauritania";
  		$nationality_list["Niger"] = "Niger";
  		$nationality_list["Nigeria"] = "Nigeria";
  		$nationality_list["Senegal"] = "Senegal";
  		$nationality_list["Sierra Leone"] = "Sierra Leone";
  		$nationality_list["Togo"] = "Togo";
  		
  		$this->set('nationality_list', $nationality_list);
		
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations', 'localities', 'religions', 'experience', 'age', 'educations'));
	}
	
	public function loadmore($url = null) {
		$this->layout = false;
		$page = $_POST['page'];
		$this->Staff->recursive = 1;
		$qStatus = $this->Session->read('qStatus');
		$conditions = array('Staff.status'=>$qStatus);
		if($url != null){
			$category = $this->Category->find('first', array('fields'=>'Category.id,Category.name,Category.friendly_url', 'conditions' => array('Category.friendly_url' => $url)));
			
			if(count($category)>0)
			{
				$category_id=$category['Category']['id'];
				$subquery=new stdClass();
				$subquery->type="expression";
				$subquery->value="`Staff`.id IN (select distinct staff_id from hh_staff_categories where category_id=".$category_id.") and `Staff`.status='$qStatus'";
				
				$conditions = array($subquery);
				
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));
			}
			else
			{
				$conditions = array('Staff.status'=>$qStatus);
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));
			}
			
		}
		if($_POST['search'] != 'false' && $_POST['searchType'] != 'false'){
			$arrayStringArray = unserialize(base64_decode($_POST['search']));
			
			$this->request->data = $arrayStringArray;
			if(isset($this->request->data['Staff']['search'])){
				$matchingCategory=$this->Category->find('first',array('conditions'=>array('Category.name Like'=>'%'.$this->request->data['Staff']['search'].'%')));
			
			if(count($matchingCategory)>0)
			{
				$category_id=$matchingCategory['Category']['id'];
				$subquery=new stdClass();
				$subquery->type="expression";
				$subquery->value="`Staff`.id IN (select distinct staff_id from hh_staff_categories where category_id=".$category_id.") and `Staff`.status='$qStatus'";
				
				$conditions = array($subquery);
				$conditions[] = array('Staff.status'=>$qStatus);
				/*
				$conditions[] = array('Staff.status'=>1, 'or'=>array('Staff.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.mini_biography LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.work_experience LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.profile_description LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Origin.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Religion.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%'));
				*/
				//$staffs = $this->Staff->find('all', array('conditions'=>array('or'=>$conditions), 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));	
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));	
			}
			else
			{
				$conditions = array('Staff.status'=>$qStatus, 'or'=>array('Staff.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.mini_biography LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.work_experience LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Staff.profile_description LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Origin.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%', 'Religion.name LIKE'=>'%'.$this->request->data['Staff']['search'].'%'));
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8, 'order'=>array('Staff.id'=>'DESC'), 'page'=>$page));	
			}
				
			} 
			else {
				$staffId = array();
				$listCollection= array();
				
				if($this->request->data['Staff']['spoken_language'] != ''){
					$langStaffRow = array_unique($this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.staff_id', 'conditions'=>array('StaffSpokenLanguage.language_id'=>$this->request->data['Staff']['spoken_language']))));
					$staffId = array_merge($staffId, $langStaffRow);
					$listCollection[]=$langStaffRow;
				}
				if($this->request->data['Staff']['prefered_locality'] != ''){
					$locStaffRow = array_unique($this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.staff_id', 'conditions'=>array('StaffPreferedLocality.locality_id'=>$this->request->data['Staff']['prefered_locality']))));
					$staffId = array_merge($staffId, $locStaffRow);
					$listCollection[]=$locStaffRow;
				}
				if($this->request->data['Staff']['education'] != ''){
					$eduStaffRow = array_unique($this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.staff_id', 'conditions'=>array('StaffPreferedEducation.education_id'=>$this->request->data['Staff']['education']))));
					$staffId = array_merge($staffId, $eduStaffRow);
					$listCollection[]=$eduStaffRow;
				}
				$staffId = array_unique($staffId);
				if($this->request->data['Staff']['experience'] == 1){
					$minExp = 0;
					$maxExp = 1;
				} else if($this->request->data['Staff']['experience'] == 2){
					$minExp = 1;
					$maxExp = 3;
				} else if($this->request->data['Staff']['experience'] == 3){
					$minExp = 3;
					$maxExp = 5;
				} else if($this->request->data['Staff']['experience'] == 4){
					$minExp = 5;
					$maxExp = 10;
				} else if($this->request->data['Staff']['experience'] == 5){
					$minExp = 10;
					$maxExp = 100;
				} else {
					$minExp = 0;
					$maxExp = 0;
				}
				if($this->request->data['Staff']['age'] == 1){
					$minAge = 18;
					$maxAge = 25;
				} else if($this->request->data['Staff']['age'] == 2){
					$minAge = 25;
					$maxAge = 30;
				} else if($this->request->data['Staff']['age'] == 3){
					$minAge = 30;
					$maxAge = 35;
				} else if($this->request->data['Staff']['age'] == 4){
					$minAge = 35;
					$maxAge = 40;
				} else if($this->request->data['Staff']['age'] == 5){
					$minAge = 40;
					$maxAge = 100;
				} else {
					$minAge = 0;
					$maxAge = 0;
				}
			    if($this->request->data['Staff']['category_id'] != ''){
					$categoryStaffRow = array_unique($this->StaffCategory->find('list', array('fields'=>'StaffCategory.staff_id', 'conditions'=>array('StaffCategory.category_id'=>$this->request->data['Staff']['category_id']))));
					$staffId = array_merge($staffId, $categoryStaffRow);
					$listCollection[]=$categoryStaffRow;
				}
				if(count($listCollection)>=2)
				{
					$staffId = call_user_func_array('array_intersect',$listCollection);
				}
				
				if(!empty($this->request->data['Staff']['immigration_status']))
					$immigration_status=$this->request->data['Staff']['immigration_status'];
					else
					$immigration_status='';
										
				if(!empty($this->request->data['Staff']['nationality']))
					$nationality=$this->request->data['Staff']['nationality'];
					else
					$nationality='';
					
				if(!empty($this->request->data['Staff']['english_speaking']))
					$english_speaking=$this->request->data['Staff']['english_speaking'];
					else
					$english_speaking='';
			
				if($immigration_status!='' && $nationality!='' && $english_speaking!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.english_speaking'=>$english_speaking, 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality, 'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
					else if($immigration_status!='' && $nationality!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'],'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality, 'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
					else if($immigration_status!='' && $english_speaking!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'],'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'],'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
					else if($english_speaking!='' && $nationality!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'],'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality, 'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
					else if($immigration_status!='' && $nationality=='' && $english_speaking==''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.immigration_status'=>$immigration_status,'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
				    else if($immigration_status=='' && $nationality!='' && $english_speaking==''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality,'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
					else if($immigration_status=='' && $nationality=='' && $english_speaking!=''){
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge,'Staff.english_speaking'=>$english_speaking, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'], 'Staff.nationality'=>$nationality,'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
					else{
						$conditions = array('Staff.experience >='=>$minExp, 'Staff.experience <='=>$maxExp, 'Staff.age >='=>$minAge, 'Staff.age <='=>$maxAge, 'Staff.sex'=>$this->request->data['Staff']['sex'], 'Staff.origin_id'=>$this->request->data['Staff']['origin_id'], 'Staff.religion_id'=>$this->request->data['Staff']['religion_id'], 'Staff.marital_status'=>$this->request->data['Staff']['marital_status'], 'Staff.accommodation'=>$this->request->data['Staff']['accommodation'],'Staff.id'=>$staffId, 'Staff.status'=>$qStatus);
					}
				//pr($conditions);die;
				if($this->request->data['Staff']['category_id'] == ''){
					//unset($conditions['OR']['Job.job_title LIKE']);
					unset($conditions['Staff.category_id']);
				}
				if($this->request->data['Staff']['experience'] == ''){
					unset($conditions['Staff.experience >=']);
					unset($conditions['Staff.experience <=']);
				}
				if($this->request->data['Staff']['age'] == ''){
					unset($conditions['Staff.age >=']);
					unset($conditions['Staff.age <=']);
				}
				if($this->request->data['Staff']['sex'] == ''){
					unset($conditions['Staff.sex']);
				}
				if($this->request->data['Staff']['accommodation'] == ''){
					unset($conditions['Staff.accommodation']);
				}
				if($this->request->data['Staff']['origin_id'] == ''){
					unset($conditions['Staff.origin_id']);
				}
				if($english_speaking == ''){
					unset($conditions['Staff.english_speaking']);
				}
				if($this->request->data['Staff']['marital_status'] == ''){
					unset($conditions['Staff.marital_status']);
				}
				if($this->request->data['Staff']['religion_id'] == ''){
					unset($conditions['Staff.religion_id']);
				}
				/*if($this->request->data['Staff']['base_salary'] == ''){
					unset($conditions['Staff.base_salary >=']);
				}
				if($this->request->data['Staff']['max_salary'] == ''){
					unset($conditions['Staff.max_salary <=']);
				}*/
				if((isset($langStaffRow) && count($langStaffRow) == 0) || (isset($locStaffRow) && count($locStaffRow) == 0) || (isset($eduStaffRow) && count($eduStaffRow) == 0)){
					$conditions['Staff.id'] = 0;
				} else if((!isset($langStaffRow)) && (!isset($locStaffRow)) && (!isset($eduStaffRow))){
					if(count($staffId) == 0){
						unset($conditions['Staff.id']);
					}
				}
				
				
				$staffs = $this->Staff->find('all', array('conditions'=>$conditions, 'limit'=>8,  'order'=>array('Staff.id'=>'DESC'),'page'=>$page));
			}
		}
		
		if(count($staffs) > 0){$this->set('staffs', $staffs);} else {exit;}
	}

	public function admin_index() {
		$conditions = array();	
		$orderby = array('Staff.id'=>'desc');	
		if(isset($this->request->data['Staff']['key']) && $this->request->data['Staff']['key'] != null) {
			
			$matchingCategory=$this->Category->find('first',array('conditions'=>array('Category.name Like'=>'%'.trim($this->request->data['Staff']['key']).'%')));
			$andCondition="";
			
			if(count($matchingCategory)>0)
			{
				$category_id=$matchingCategory['Category']['id'];
				$subquery=new stdClass();
				$subquery->type="expression";
				$subquery->value="`Staff`.id IN (select distinct staff_id from hh_staff_categories where category_id=".$category_id.")";
				
				$conditions = array($subquery);
				//commented on 25-Feb-2016
				/*
				$conditions[] = array('or'=>array('Staff.email LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.staff-uid LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%','Staff.reg_date >='=>date(trim($this->request->data['Staff']['key']))));
				*/
				$this->Staff->recursive = 0;
				$this->Session->write('conditions',$conditions);
        		//$this->paginate=array('conditions'=>array("or"=>$conditions),'order'=>$orderby);
        		$this->paginate=array('conditions'=>$conditions,'order'=>$orderby);
        		$this->set('staffs', $this->paginate());
			}
			else
			{
				
				if ($this->validateDate(trim($this->request->data['Staff']['key']))) {
					$conditions = array('or'=>array('Staff.email LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.staff-uid LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%','Staff.reg_date >='=>date(trim($this->request->data['Staff']['key']))));
				}
				else
				{
					$conditions = array('or'=>array('Staff.email LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.staff-uid LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%'));
				}
				
				$this->Staff->recursive = 0;
				$this->Session->write('conditions',$conditions);
        		$this->paginate=array('conditions'=>$conditions,'order'=>$orderby);
        		$this->set('staffs', $this->paginate());
			}
			
		}
		else
		{
			$orderby = array('Staff.id'=>'desc');
			$page = $this->request->params['named']['page'];
			if($this->Session->check('conditions') && !empty($page) && intval($page)>=2){
      			$conditions = $this->Session->read('conditions');
      			$this->paginate=array('conditions'=>$conditions,'order'=>$orderby);
    		}
    		else{
    			$this->Session->delete('conditions');
				$this->paginate = array('order'=>$orderby);	
			}
			
			$this->set('staffs', $this->paginate());
		}
		
		
	}

	public function agent_index() {
		$conditions = array();	
		$orderby = array('Staff.id'=>'desc');
		$user_id = $this->Session->read('Auth.User.id');
		// $this->paginate = array(
		//     'conditions' => array('agent_id', $user_id)
		// );
		// $this->set('staffs', $this->paginate());
		// var_dump($user_id);exit;
		if(isset($this->request->data['Staff']['key']) && $this->request->data['Staff']['key'] != null) {
			
			$matchingCategory=$this->Category->find('first',array('conditions'=>array('Category.name Like'=>'%'.trim($this->request->data['Staff']['key']).'%')));
			$andCondition="";
			
			// var_dump($matchingCategory);exit;
			if(count($matchingCategory)>0)
			{
				$category_id=$matchingCategory['Category']['id'];
				$subquery=new stdClass();
				$subquery->type="expression";
				$subquery->value="`Staff`.id IN (select distinct staff_id from hh_staff_categories where category_id=".$category_id.")";
				
				$conditions = array($subquery);
				//commented on 25-Feb-2016
				/*
				$conditions[] = array('or'=>array('Staff.email LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.staff-uid LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%','Staff.reg_date >='=>date(trim($this->request->data['Staff']['key']))));
				*/
				$this->Staff->recursive = 0;
				$this->Session->write('conditions',$conditions);
        		//$this->paginate=array('conditions'=>array("or"=>$conditions),'order'=>$orderby);
        		$this->paginate=array('conditions'=>$conditions, 'conditions' => array('agent_id', $user_id), 'order'=>$orderby);
        		$this->set('staffs', $this->paginate());
			}
			else
			{
				if ($this->validateDate(trim($this->request->data['Staff']['key']))) {
					$conditions = array('or'=>array('Staff.email LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.staff-uid LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%','Staff.reg_date >='=>date(trim($this->request->data['Staff']['key']))));
				}
				else
				{
					$conditions = array('or'=>array('Staff.email LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.staff-uid LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%', 'Staff.name LIKE'=>'%'.trim($this->request->data['Staff']['key']).'%'));
				}
				$this->Staff->recursive = 0;
				$this->Session->write('conditions',$conditions);
        		$this->paginate=array('conditions'=>$conditions, 'conditions' => array('agent_id', $user_id), 'order'=>$orderby);
        		$this->set('staffs', $this->paginate());
			}
		}
		else
		{
			$orderby = array('Staff.id'=>'desc');
			
			$page = $this->request->params['named']['page'];
			if($this->Session->check('conditions') && !empty($page) && intval($page)>=2){
      			$conditions = $this->Session->read('conditions');
      			$this->paginate=array('conditions'=>$conditions, 'conditions' => array('agent_id', $user_id), 'order'=>$orderby);
    		}
    		else{
    			$this->Session->delete('conditions');
				$this->paginate = array('conditions' => array('agent_id', $user_id), 'order'=>$orderby);	
			}

			$this->set('staffs', $this->paginate());
		}	
	}

	public function agent_add() {
		if ($this->request->is('post')) {
			$exist_data = $this->Staff->find('list', array('conditions' => array('Staff.contact_number' => $this->request->data['Staff']['contact_number'])));
			if(empty($exist_data)){				
				$friendlyurl = $this->request->data['Staff']['name'];
				foreach($this->notAllowedWord AS $arrNAWord)
				{
					$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
				}
				$friendlyurl = str_replace(' ', '-', $friendlyurl);
				$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $friendlyurl)));
				$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				$duplicate = false;
				if(!empty($chk)){
					$this->request->data['Staff']['friendly_url'] = '';
					$duplicate = true;
				}
				if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != '')
				{
					if($this->request->data['Staff']['image1']['type'] == 'image/png' || $this->request->data['Staff']['image1']['type'] == 'image/PNG' || $this->request->data['Staff']['image1']['type'] == 'image/jpg' || $this->request->data['Staff']['image1']['type'] == 'image/JPG' || $this->request->data['Staff']['image1']['type'] == 'image/jpeg' || $this->request->data['Staff']['image1']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Staff']['image1']['name'];

						move_uploaded_file($this->request->data['Staff']['image1']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Staff']['image1'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					$this->Session->setFlash(__('The staff could not be saved.Please upload image1'));
				}
				if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != ''){
					if($this->request->data['Staff']['image2']['type'] == 'image/png' || $this->request->data['Staff']['image2']['type'] == 'image/PNG' || $this->request->data['Staff']['image2']['type'] == 'image/jpg' || $this->request->data['Staff']['image2']['type'] == 'image/JPG' || $this->request->data['Staff']['image2']['type'] == 'image/jpeg' || $this->request->data['Staff']['image2']['type'] == 'image/JPEG')
					{
						$filename2=rand(). $this->request->data['Staff']['image2']['name'];
						move_uploaded_file($this->request->data['Staff']['image2']['tmp_name'], PAGE_IMAGES.$filename2);
						$this->request->data['Staff']['image2'] = $filename2;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					$this->Session->setFlash(__('The staff could not be saved.Please upload image2'));
				}
				$preferedLocationArr = $this->request->data['Staff']['prefered_location'];
				unset($this->request->data['Staff']['prefered_location']);
				$spokenLanguageArr = $this->request->data['Staff']['spoken_languages'];
				unset($this->request->data['Staff']['spoken_languages']);
				$preferedLocalityArr = $this->request->data['Staff']['prefered_locality'];
				unset($this->request->data['Staff']['prefered_locality']);
				$preferedEducationArr = $this->request->data['Staff']['prefered_education'];
				unset($this->request->data['Staff']['prefered_education']);
				$this->request->data['Staff']['agent_id']=$this->Session->read('Auth.User.id');
				$this->request->data['Staff']['status']=0;
				$this->request->data['Staff']['hired']=0;
				$this->request->data['Staff']['reg_date']=date('Y-m-d');
				$staffCategoryArr = $this->request->data['Staff']['category_id'];
				unset($this->request->data['Staff']['category_id']);
				
				$CategoryCode="AS"; //assorted
				
				$this->Staff->create();
				if ($this->Staff->save($this->request->data)) {
					$uId = $CategoryCode.$this->Staff->id;
					$idMinLentgth = 5;
					$idLength = strlen($this->Staff->id);
					$lengtDiff = $idMinLentgth-$idLength;
					if($lengtDiff > 0){
						$midStr = '';
						for($iter = 0; $iter < $lengtDiff; $iter++){
							$midStr.='0';
						}
						$uId = $CategoryCode.$midStr.$this->Staff->id;
					}
					$this->Staff->savefield('staff-uid', $uId);
					if($duplicate){
						$friendlyurl .= '-'.$this->Staff->id;
						$this->Staff->savefield('friendly_url', $friendlyurl);
					}
					$id = $this->Staff->id;
					if($preferedLocationArr != ''){
						if(count($preferedLocationArr)>0){
							$checking = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
							if(count($checking)>0){
								//$this->StaffPreferedLocation->id = $checking;
								$this->StaffPreferedLocation->delete($checking);
							}
							$locationRow['StaffPreferedLocation']['staff_id'] = $id;
							foreach($preferedLocationArr as $preferedLocation){
								$locationRow['StaffPreferedLocation']['location_id'] = $preferedLocation;
								$this->StaffPreferedLocation->create();
								$this->StaffPreferedLocation->save($locationRow);
							}
						}
					}
					if($preferedLocalityArr != ''){
						if(count($preferedLocalityArr)>0){
							$checking1 = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
							if(count($checking1)>0){
								$this->StaffPreferedLocality->delete($checking1);
							}
							$localityRow['StaffPreferedLocality']['staff_id'] = $id;
							foreach($preferedLocalityArr as $preferedLocality){
								$localityRow['StaffPreferedLocality']['locality_id'] = $preferedLocality;
								$this->StaffPreferedLocality->create();
								$this->StaffPreferedLocality->save($localityRow);
							}
						}
					}
					if($spokenLanguageArr != ''){
						if(count($spokenLanguageArr)>0){
							$checking2 = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
							if(count($checking2)>0){
								$this->StaffSpokenLanguage->delete($checking2);
							}
							$languageRow['StaffSpokenLanguage']['staff_id'] = $id;
							foreach($spokenLanguageArr as $spokenLanguage){
								$languageRow['StaffSpokenLanguage']['language_id'] = $spokenLanguage;
								$this->StaffSpokenLanguage->create();
								$this->StaffSpokenLanguage->save($languageRow);
							}
						}
					}
					if($preferedEducationArr != ''){
						if(count($preferedEducationArr)>0){
							$checking3 = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
							if(count($checking3)>0){
								$this->StaffPreferedEducation->delete($checking3);
							}
							$educationRow['StaffPreferedEducation']['staff_id'] = $id;
							foreach($preferedEducationArr as $education){
								$educationRow['StaffPreferedEducation']['education_id'] = $education;
								$this->StaffPreferedEducation->create();
								$this->StaffPreferedEducation->save($educationRow);
							}
						}
					}
					if($staffCategoryArr != ''){
						if(count($staffCategoryArr)>0){
							$checking4 = $this->StaffCategory->find('list', array('fields'=>'id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
							if(count($checking4)>0){
								$this->StaffCategory->delete($checking4);
							}
							$categoryRow['StaffCategory']['staff_id'] = $id;
							foreach($staffCategoryArr as $category){
								$categoryRow['StaffCategory']['category_id'] = $category;
								$this->StaffCategory->create();
								$this->StaffCategory->save($categoryRow);
							}
						}
					}
					/*
					$subscribers = $this->Email->find('list', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'DESC')));
					
					if(count($subscribers)>0){
						$siteSetting = $this->viewVars['siteSetting'];
						$this->Mailer->recursive = 0;
						$mailer = $this->Mailer->find('first', array('conditions'=>array('Mailer.id'=>1)));
						$html = $mailer['Mailer']['html_content'];
						$logoPath = DOMAIN_NAME_PATH.'img/site_logo/'.$siteSetting['Setting']['site_logo'];
						$mail_From = $siteSetting['Setting']['noreply_email'];
						$mail_CC = null;
						$subject = $mailer['Mailer']['name'];
						$html = str_replace("##logo_path##", $logoPath, $html);
						$html = str_replace("##noreply_email##", $siteSetting['Setting']['noreply_email'], $html);
						$html = str_replace("##subject##", $subject, $html);
						$html = str_replace("##staff_name##", $this->request->data['Staff']['name'], $html);
						$html = str_replace("##experience##", $this->request->data['Staff']['experience'], $html);
						$html = str_replace("##age##", $this->request->data['Staff']['age'], $html);
						$html = str_replace("##mini_biography##", $this->request->data['Staff']['mini_biography'], $html);
						$html = str_replace("##staff_url##", DOMAIN_NAME_PATH.$friendlyurl, $html);
						$html = str_replace("##staff_image##", PAGE_IMAGES_URL.$filename, $html);
						foreach($subscribers as $subscriber){
							$mail_To = $subscriber;
							$convertedString = bin2hex(Security::cipher(serialize($mail_To),Configure::read('Security.salt')));
							$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>Click here</a>";
							$mail_body = str_replace("##unsubscribe##", $link, $html);
							$mail_body = str_replace("##user##", $mail_To, $html);
							//$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
						}
					}
					*/
					$this->Session->setFlash(sprintf(__('The staff has been added successfully!', true), 'Page'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staff could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('This phone has already been registered.', true)));
			}
		}
		$countries = $this->Staff->Country->find('list',array('order'=>'Country.name'));
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list', array('conditions'=>array('Location.service_availability'=>'Yes')));
		$localities = $this->Locality->find('list');
		$religions = $this->Staff->Religion->find('list');
		$educations = $this->Education->find('list');
		
		$agents_list=array();
		$agents=$this->Agent->find('list', array('fields'=>'Agent.name', 'order'=>array('Agent.id'=>'ASC')));
		
		$plans_list["0"]="Direct";
		foreach($agents as $row=>$value) {
    		$plans_list["{$row}"] = "{$value}";
        }
  		$this->set('plans_list', $plans_list);
  		
  		$immigration_list["N/A"] = "N/A";
  		$immigration_list["Valid Resident Card"] = "Valid Resident Card";
  		$immigration_list["Resident Card Required"] = "Resident Card Required";
  		$this->set('immigration_list', $immigration_list);
  		
  		$nationality_list["Benin"] = "Benin";
  		$nationality_list["Burkina Faso"] = "Burkina Faso";
  		
  		$nationality_list["Cape Verde"] = "Cape Verde";
  		$nationality_list["The Gambia"] = "The Gambia";
  		
  		$nationality_list["Ghana"] = "Ghana";
  		$nationality_list["Guinea"] = "Guinea";
  		$nationality_list["Guinea-Bissau"] = "Guinea-Bissau";
  		$nationality_list["Ivory Coast"] = "Ivory Coast";
  		$nationality_list["Liberia"] = "Liberia";
  		$nationality_list["Mali"] = "Mali";
  		$nationality_list["Mali Federation"] = "Mali Federation";
  		$nationality_list["Mauritania"] = "Mauritania";
  		$nationality_list["Niger"] = "Niger";
  		$nationality_list["Nigeria"] = "Nigeria";
  		$nationality_list["Senegal"] = "Senegal";
  		$nationality_list["Sierra Leone"] = "Sierra Leone";
  		$nationality_list["Togo"] = "Togo";
  		
  		$this->set('nationality_list', $nationality_list);
  		
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations','religions','localities', 'educations'));
	}

	public function agent_view($id = null) {
		$this->Staff->recursive = 1;
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(__('Invalid staff.'));
		}
		$staff = $this->Staff->read(null, $id);
		//pr($staff['StaffPreferedLocation']);
		//pr($staff['StaffSpokenLanguage']);
		if(count($staff['StaffPreferedLocation'])>0){
			foreach($staff['StaffPreferedLocation'] as $locArr){
				$idLoc[] = $locArr['location_id'];
			}
			$locations = $this->Location->find('list', array('conditions'=>array('Location.id'=>$idLoc)));
			$PreferedLocation = implode(',', $locations);
			$this->set('PreferedLocation', $PreferedLocation);
		}
		if(count($staff['StaffPreferedLocality'])>0){
			foreach($staff['StaffPreferedLocality'] as $locArr){
				$idLoca[] = $locArr['locality_id'];
			}
			$localities = $this->Locality->find('list', array('conditions'=>array('Locality.id'=>$idLoca)));
			$PreferedLocality = implode(',', $localities);
			$this->set('PreferedLocality', $PreferedLocality);
		}
		if(count($staff['StaffSpokenLanguage'])>0){
			foreach($staff['StaffSpokenLanguage'] as $langArr){
				$idLang[] = $langArr['language_id'];
			}
			$languages = $this->Language->find('list', array('conditions'=>array('Language.id'=>$idLang)));
			$PreferedLanguage = implode(',', $languages);
			$this->set('PreferedLanguage', $PreferedLanguage);
		}
		if(count($staff['StaffPreferedEducation'])>0){
			foreach($staff['StaffPreferedEducation'] as $eduArr){
				$idEdu[] = $eduArr['education_id'];
			}
			$educations = $this->Education->find('list', array('conditions'=>array('Education.id'=>$idEdu)));
			$PreferedEducation = implode(',', $educations);
			$this->set('PreferedEducation', $PreferedEducation);
			
			
		}
		if(count($staff['StaffCategory'])>0){
			foreach($staff['StaffCategory'] as $catArr){
				$idCat[] = $catArr['category_id'];
			}
			$categories = $this->Category->find('list', array('conditions'=>array('Category.id'=>$idCat)));
			$PreferedCategory = implode(',', $categories);
			$this->set('PreferedCategory', $PreferedCategory);
			
			
		}
		$this->set('staff', $staff);
	}

	public function agent_edit($id = null) {
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(sprintf(__('Invalid staff', true)));
		}
		$staffs=$this->Staff->read(null, $id);
		$oldpic1=$staffs['Staff']['image1'];
		$oldpic2=$staffs['Staff']['image2'];
		if ($this->request->is('post') || $this->request->is('put')) {
			$exist_data = $this->Staff->find('list', array('conditions' => array('Staff.contact_number' => $this->request->data['Staff']['contact_number'], 'Staff.id !=' => $id)));
			if(empty($exist_data)){
				$CategoryCode = $this->Category->field('Category_code', array('Category.id'=>$this->request->data['Staff']['category_id']));
				$friendlyurl = $this->request->data['Staff']['name'];
				foreach($this->notAllowedWord AS $arrNAWord)
				{
					$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
				}
				$friendlyurl = str_replace(' ', '-', $friendlyurl);
				$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $friendlyurl, 'Staff.id !='=>$id)));
				$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				if(!empty($chk)){
					$this->request->data['Staff']['friendly_url'] = $friendlyurl.'-'.$id;
				} else {
					$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				}
				if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != '')
				{
					if($this->request->data['Staff']['image1']['type'] == 'image/png' || $this->request->data['Staff']['image1']['type'] == 'image/PNG' || $this->request->data['Staff']['image1']['type'] == 'image/jpg' || $this->request->data['Staff']['image1']['type'] == 'image/JPG' || $this->request->data['Staff']['image1']['type'] == 'image/jpeg' || $this->request->data['Staff']['image1']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Staff']['image1']['name'];

						move_uploaded_file($this->request->data['Staff']['image1']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Staff']['image1'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image1']);
				}
				if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != '')
				{
					if($this->request->data['Staff']['image2']['type'] == 'image/png' || $this->request->data['Staff']['image2']['type'] == 'image/PNG' || $this->request->data['Staff']['image2']['type'] == 'image/jpg' || $this->request->data['Staff']['image2']['type'] == 'image/JPG' || $this->request->data['Staff']['image2']['type'] == 'image/jpeg' || $this->request->data['Staff']['image2']['type'] == 'image/JPEG')
					{
						$filename2=rand(). $this->request->data['Staff']['image2']['name'];
						move_uploaded_file($this->request->data['Staff']['image2']['tmp_name'], PAGE_IMAGES.$filename2);
						$this->request->data['Staff']['image2'] = $filename2;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image2']);
				}
				$preferedLocationArr = $this->request->data['Staff']['prefered_location'];
				unset($this->request->data['Staff']['prefered_location']);
				$spokenLanguageArr = $this->request->data['Staff']['spoken_languages'];
				unset($this->request->data['Staff']['spoken_languages']);
				$preferedLocalityArr = $this->request->data['Staff']['prefered_locality'];
				unset($this->request->data['Staff']['prefered_locality']);
				$preferedEducationArr = $this->request->data['Staff']['prefered_education'];
				unset($this->request->data['Staff']['prefered_education']);
				
				$uId = $CategoryCode.$this->request->data['Staff']['id'];
				$idMinLentgth = 5;
				$idLength = strlen($this->request->data['Staff']['id']);
				$lengtDiff = $idMinLentgth-$idLength;
				if($lengtDiff > 0){
					$midStr = '';
					for($iter = 0; $iter < $lengtDiff; $iter++){
						$midStr.='0';
					}
					$uId = $CategoryCode.$midStr.$this->request->data['Staff']['id'];
				}
				$staffCategoryArr = $this->request->data['Staff']['category_id'];
				unset($this->request->data['Staff']['category_id']);
				
				$this->request->data['Staff']['staff-uid'] = $uId;
				if($this->request->data['Staff']['hired']=="1")
				{
					$this->request->data['Staff']['hired_date']=date('Y-m-d',strtotime('+3 days'));
				}
					
				//pr($this->request->data);die;
				if ($this->Staff->save($this->request->data)) {
					if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic1);
					}
					if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic2);
					}
					if($preferedLocationArr != ''){
						if(count($preferedLocationArr)>0){
							$checking = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
							if(count($checking)>0){
								$this->StaffPreferedLocation->delete($checking);
							}
							$locationRow['StaffPreferedLocation']['staff_id'] = $id;
							foreach($preferedLocationArr as $preferedLocation){
								$locationRow['StaffPreferedLocation']['location_id'] = $preferedLocation;
								$this->StaffPreferedLocation->create();
								$this->StaffPreferedLocation->save($locationRow);
							}
						}
					}
					if($preferedLocalityArr != ''){
						if(count($preferedLocalityArr)>0){
							$checking1 = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
							if(count($checking1)>0){
								$this->StaffPreferedLocality->delete($checking1);
							}
							$localityRow['StaffPreferedLocality']['staff_id'] = $id;
							foreach($preferedLocalityArr as $preferedLocality){
								$localityRow['StaffPreferedLocality']['locality_id'] = $preferedLocality;
								$this->StaffPreferedLocality->create();
								$this->StaffPreferedLocality->save($localityRow);
							}
						}
					}
					if($spokenLanguageArr != ''){
						if(count($spokenLanguageArr)>0){
							$checking2 = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
							if(count($checking2)>0){
								$this->StaffSpokenLanguage->delete($checking2);
							}
							$languageRow['StaffSpokenLanguage']['staff_id'] = $id;
							foreach($spokenLanguageArr as $spokenLanguage){
								$languageRow['StaffSpokenLanguage']['language_id'] = $spokenLanguage;
								$this->StaffSpokenLanguage->create();
								$this->StaffSpokenLanguage->save($languageRow);
							}
						}
					}
					if($preferedEducationArr != ''){
						if(count($preferedEducationArr)>0){
							$checking3 = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
							if(count($checking3)>0){
								$this->StaffPreferedEducation->delete($checking3);
							}
							$educationRow['StaffPreferedEducation']['staff_id'] = $id;
							foreach($preferedEducationArr as $education){
								$educationRow['StaffPreferedEducation']['education_id'] = $education;
								$this->StaffPreferedEducation->create();
								$this->StaffPreferedEducation->save($educationRow);
							}
						}
					}
					if($staffCategoryArr != ''){
						if(count($staffCategoryArr)>0){
							$checking4 = $this->StaffCategory->find('list', array('fields'=>'StaffCategory.id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
							if(count($checking4)>0){
								$this->StaffCategory->delete($checking4);
							}
							$categoryRow['StaffCategory']['staff_id'] = $id;
							foreach($staffCategoryArr as $category){
								$categoryRow['StaffCategory']['category_id'] = $category;
								$this->StaffCategory->create();
								$this->StaffCategory->save($categoryRow);
							}
						}
					}
					$this->Session->setFlash(sprintf(__('The staff has been updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staff could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('This phone has already been registered.', true)));
			}
		} else {
			$this->request->data = $this->Staff->read(null, $id);
		}
		$countries = $this->Staff->Country->find('list');
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list', array('conditions'=>array('Location.service_availability'=>'Yes')));
		$localities = $this->Locality->find('list');
		$religions = $this->Staff->Religion->find('list');
		$educations = $this->Education->find('list');
		$ploc=array();
		$locDisp = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.location_id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
		if(count($locDisp)>0)
		{
			$ploc = array_values($locDisp);
		}
		$ploca=array();
		$locaDisp = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.locality_id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
		if(count($locaDisp)>0)
		{
			$ploca = array_values($locaDisp);
		}
		$pLang=array();
		$langDisp = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.language_id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
		if(count($langDisp)>0)
		{
			$pLang = array_values($langDisp);
		}
		$pEdu=array();
		$eduDisp = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.education_id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
		if(count($eduDisp)>0)
		{
			$pEdu = array_values($eduDisp);
		}
		
		$pCat=array();
		$catDisp = $this->StaffCategory->find('list', array('fields'=>'StaffCategory.category_id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
		if(count($catDisp)>0)
		{
			$pCat = array_values($catDisp);
		}
		
		$agents_list=array();
		$agents=$this->Agent->find('list', array('fields'=>'Agent.name', 'order'=>array('Agent.id'=>'ASC')));
		
		$plans_list["0"]="Direct";
		foreach($agents as $row=>$value) {
    		$plans_list["{$row}"] = "{$value}";
        }
  		$this->set('plans_list', $plans_list);
  		
  		$immigration_list["N/A"] = "N/A";
  		$immigration_list["Valid Resident Card"] = "Valid Resident Card";
  		$immigration_list["Resident Card Required"] = "Resident Card Required";
  		$this->set('immigration_list', $immigration_list);
  		
  		$nationality_list["Benin"] = "Benin";
  		$nationality_list["Burkina Faso"] = "Burkina Faso";
  		
  		$nationality_list["Cape Verde"] = "Cape Verde";
  		$nationality_list["The Gambia"] = "The Gambia";
  		
  		$nationality_list["Ghana"] = "Ghana";
  		$nationality_list["Guinea"] = "Guinea";
  		$nationality_list["Guinea-Bissau"] = "Guinea-Bissau";
  		$nationality_list["Ivory Coast"] = "Ivory Coast";
  		$nationality_list["Liberia"] = "Liberia";
  		$nationality_list["Mali"] = "Mali";
  		$nationality_list["Mali Federation"] = "Mali Federation";
  		$nationality_list["Mauritania"] = "Mauritania";
  		$nationality_list["Niger"] = "Niger";
  		$nationality_list["Nigeria"] = "Nigeria";
  		$nationality_list["Senegal"] = "Senegal";
  		$nationality_list["Sierra Leone"] = "Sierra Leone";
  		$nationality_list["Togo"] = "Togo";
  		
  		$this->set('nationality_list', $nationality_list);
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations','localities','ploc','ploca','pLang','pEdu','staffs','religions','educations','pCat'));
	}

	public function agent_shortlisted(){
		$agent_id = $this->Session->read('Auth.User.id');
		$shortlisted = $this->Viewcontacts->query("SELECT a.name, a.friendly_url, b.*, c.name FROM hh_staffs a LEFT JOIN hh_viewcontacts b ON a.id = b.staff_id LEFT JOIN hh_users c ON c.id = b.user_id where b.agent_id = '$agent_id' and b.shortlisted ='1'");
		$this->set('shortlisted', $shortlisted);
	}

	public function agent_contacted(){
		$agent_id = $this->Session->read('Auth.User.id');
		$shortlisted = $this->Viewcontacts->query("SELECT a.name, a.friendly_url, b.*, c.name FROM hh_staffs a LEFT JOIN hh_viewcontacts b ON a.id = b.staff_id LEFT JOIN hh_users c ON c.id = b.user_id where b.agent_id = '$agent_id' and b.shortlisted ='0'");
		$this->set('shortlisted', $shortlisted);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($url = null) {
		$this->Staff->recursive = 0;
		$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $url)));
		if (!$chk) {
			$this->Session->setFlash(__('Invalid staff'));
			$this->redirect(array('action' => 'index'));
		}
		$id = $chk['Staff']['id'];
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(__('Invalid staff'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Staff->recursive = 1;
		$staff = $this->Staff->read(null, $id);
		$this->Category->recursive = 0;
		$categoriesRelated = $this->Category->find('list',array('fields'=>'Category.friendly_url'));
		$this->Staff->recursive = 0;
		
		$subquery=new stdClass();
		$subquery->type="expression";
		$subquery->value="`Staff`.id !=".$this->Staff->id." and `Staff`.id IN ( select distinct staff_id from hh_staff_categories where category_id in (select distinct category_id from hh_staff_categories where staff_id=".$this->Staff->id.")) and `Staff`.status=1";
				
		$conditions = array($subquery);
		
		$relatedStaffs = $this->Staff->find('all',array('fields'=>'Staff.id, Staff.friendly_url, Staff.name, Staff.image1, Staff.image2, Staff.experience, Staff.age, Origin.id, Origin.name,Staff.hired', 'conditions'=>$conditions, 'order'=>'rand()', 'limit'=>5));
		$this->Language->recursive = 0;
		/*$spokenLanguages = array();
		if($staff['Staff']['spoken_languages'] != null){
			$pLang = explode(',',$staff['Staff']['spoken_languages']);
			$spokenLanguages = $this->Language->find('list',array('conditions'=>array('Language.id'=>$pLang)));
		}*/
		$preferedLocations = array();
		if(count($staff['StaffPreferedLocation'])>0){
			foreach($staff['StaffPreferedLocation'] as $locArr){
				$idLoc[] = $locArr['location_id'];
			}
			$preferedLocations = $this->Location->find('list', array('conditions'=>array('Location.id'=>$idLoc)));
		}
		$preferedLocalities = array();
		if(count($staff['StaffPreferedLocality'])>0){
			foreach($staff['StaffPreferedLocality'] as $locaArr){
				$idLoca[] = $locaArr['locality_id'];
			}
			$preferedLocalities = $this->Locality->find('list', array('conditions'=>array('Locality.id'=>$idLoca)));
		}
		$spokenLanguages = array();
		if(count($staff['StaffSpokenLanguage'])>0){
			foreach($staff['StaffSpokenLanguage'] as $langArr){
				$idLang[] = $langArr['language_id'];
			}
			$spokenLanguages = $this->Language->find('list', array('conditions'=>array('Language.id'=>$idLang)));
		}
		if(count($staff['StaffPreferedEducation'])>0){
			foreach($staff['StaffPreferedEducation'] as $eduArr){
				$idEdu[] = $eduArr['education_id'];
			}
			$educations = $this->Education->find('list', array('conditions'=>array('Education.id'=>$idEdu)));
			$PreferedEducation = implode(', ', $educations);
			$this->set('PreferedEducation', $PreferedEducation);
		}
		if(count($staff['StaffCategory'])>0){
			foreach($staff['StaffCategory'] as $catArr){
				$idCat[] = $catArr['category_id'];
			}
			$categories = $this->Category->find('list', array('conditions'=>array('Category.id'=>$idCat)));
			$CategoryList = implode(', ', $categories);
			$this->set('CategoryList', $CategoryList);
		}
		$this->Location->recursive = 0;
		$locations = $this->Location->find('list', array('fields'=>'Location.name,Location.name'));
		$this->Booking->recursive = -1;
		/*
		$booked = $this->Booking->find('first', array('conditions'=>array('Booking.status'=>array('Pending', 'Selected'), 'Booking.staff_id'=>$id, 'Booking.user_id'=>$this->Session->read('Auth.User.id'))));
		$selected = $this->Booking->find('first', array('conditions'=>array('Booking.status'=>array('Selected'), 'Booking.staff_id'=>$id, 'Booking.user_id !='=>$this->Session->read('Auth.User.id'))));
		$hired = $this->Booking->find('first', array('conditions'=>array('Booking.status'=>array('Hired'), 'Booking.staff_id'=>$id, 'Booking.user_id !='=>$this->Session->read('Auth.User.id'))));
		*/
		$hired = $this->Booking->find('first', array('conditions'=>array('Booking.status'=>array('Hired'), 'Booking.staff_id'=>$id)));

		$this->set('staff', $staff);
		$this->set('categoriesRelated', $categoriesRelated);
		$this->set('relatedStaffs', $relatedStaffs);
		$this->set('spokenLanguages', $spokenLanguages);
		$this->set('preferedLocations', $preferedLocations);
		$this->set('preferedLocalities', $preferedLocalities);
		$this->set('locations', $locations);
		/*
		$this->set('booked', $booked);
		$this->set('selected', $selected);
		*/
		$this->set('hired', $hired);
	}

	// public function shortlists($staffId){
	// 	$user_id=$this->Session->read('Auth.User.id');
	// 	$contactView = $this->Viewcontacts->query("select id from hh_viewcontacts where user_id = '$user_id' and staff_id ='$staffId'");
	// 	$shortlisted = $this->Viewcontacts->query("select id from hh_viewcontacts where user_id = '$user_id' and staff_id ='$staffId' and shortlisted='1'");

	// 	$staff = $this->Staff->find('first',array('fields'=>'Staff.name', 'conditions'=>array('Staff.id'=>$staffId)));
		
	// 	if(count($shortlisted) == 0){
	// 		if(count($contactView) == 1){
	// 			$this->Viewcontacts->query("UPDATE hh_viewcontacts SET shortlisted = '1' where user_id = '$user_id' and staff_id ='$staffId'");
	// 			$this->Staff->recursive = 0;
	// 			$html.='<p><h3>You have shortlisted: <br/>'.$staff['Staff']['name'].'</h3></p>';
	// 			$html.='</div>';
	// 			echo $html;
	// 			exit;
	// 			$this->Viewcontacts->query("insert into hh_viewcontacts (user_id,staff_id,created_date) values('$user_id','$staffId',NOW())");
	// 		}else{
	// 			$this->Staff->recursive = 0;
	// 			$html.='<p><h3>You have shortlisted: <br/>'.$staff['Staff']['name'].'</h3></p>';
	// 			$html.='</div>';
	// 			echo $html;
	// 			exit;
	// 		}
	// 	}else{
	// 		$html.='<p><h3>You have already shortlisted: <br/>'.$staff['Staff']['name'].'</h3></p>';
	// 		$html.='</div>';
	// 		echo $html;
	// 		exit;
	// 	}
	// }

	public function shortlists($staffId){
		$this->Payment->recursive = 0;
		
		$payment = $this->Payment->find('first',array('conditions'=>array('Payment.user_id'=>$this->Session->read('Auth.User.id')), 'order'=>array('Payment.id'=>'DESC')));

		$agent = $this->Staff->find('first',array('fields'=>'Staff.agent_id', 'conditions'=>array('Staff.id'=>$staffId)));
		$agent_id = $agent['Staff']['agent_id'];

		$flag = false;

		$expiryDateChk = false;

		$html = '<div class="contactDispWrap">';
		$user_id=$this->Session->read('Auth.User.id');
		
		$contactView = $this->Viewcontacts->query("select id from hh_viewcontacts where user_id = '$user_id' and staff_id ='$staffId'");
		$shortlisted = $this->Viewcontacts->query("select id from hh_viewcontacts where user_id = '$user_id' and staff_id ='$staffId' and shortlisted='1'");

		$staff = $this->Staff->find('first',array('fields'=>'Staff.name', 'conditions'=>array('Staff.id'=>$staffId)));

		if(count($shortlisted) == 0){
			if(count($contactView) == 1){
				$this->Viewcontacts->query("UPDATE hh_viewcontacts SET shortlisted = '1', agent_id ='$agent_id' where user_id = '$user_id' and staff_id ='$staffId'");
				$this->Staff->recursive = 0;
				$html.='<p><h3>You have shortlisted: <br/>'.$staff['Staff']['name'].'</h3></p>';
				$html.='</div>';
				echo $html;
				exit;
			}else{
				$this->Viewcontacts->query("insert into hh_viewcontacts (user_id,staff_id,agent_id, shortlisted, created_date) values('$user_id','$staffId','$agent_id','1',NOW())");
				$this->Staff->recursive = 0;
				$html.='<p><h3>You have shortlisted: <br/>'.$staff['Staff']['name'].'</h3></p>';
				$html.='</div>';
				echo $html;
				exit;
			}
		}else{
			$html.='<p><h3>You have already shortlisted: <br/>'.$staff['Staff']['name'].'</h3></p>';
			$html.='</div>';
			echo $html;
			exit;
		}
		echo $html;
		exit;
	}

	public function contacts($staffId){
		$this->Payment->recursive = 0;
		$user_id = $this->Session->read('Auth.User.id');
		$payment = $this->Payment->find('first',array('conditions'=>array('Payment.user_id'=>$this->Session->read('Auth.User.id')), 'order'=>array('Payment.id'=>'DESC')));

		$agent = $this->Staff->find('first',array('fields'=>'Staff.agent_id', 'conditions'=>array('Staff.id'=>$staffId)));
		$agent_id = $agent['Staff']['agent_id'];

		$vetCandidatePend = $this->User->query("select user_id from hh_vetted_candidates where user_id = '$user_id' and staff_id='$staffId' and payment_status='1' and status='0'");

		$vetCandidateApp = $this->User->query("select user_id from hh_vetted_candidates where user_id = '$user_id' and staff_id='$staffId' and payment_status='1' and status='1'");

		$flag = false;
		$expiryDateChk = false;

		if(!empty($payment)){
			if($payment['Payment']['payment_status'] == 'Y' || $payment['Payment']['payment_status'] == 'E'){
				if($payment['Payment']['expiry_date'] == null){
					$expiryDateChk = true;
				}else if($payment['Payment']['expiry_date'] >= date('Y-m-d')){
					$expiryDateChk = true;
				}

				if($expiryDateChk == true){
					if($payment['Payment']['quantity'] == null){
						$flag = true;
					} else if($payment['Payment']['quantity'] > $payment['Payment']['quantity_viewed']){
						$flag = true;
					} else if($payment['Payment']['quantity'] == $payment['Payment']['quantity_viewed']){
						$profileChk = explode(',', $payment['Payment']['viewed_profile_contacts']);
						if (in_array($staffId, $profileChk)) {
							$flag = true;
						}
					}
				}
				if($payment['Payment']['payment_status'] == 'Y'){
					if($flag == false){
						$this->Payment->id = $payment['Payment']['id'];
						$this->Payment->saveField('payment_status', 'E');
					}
				}
			}
		}
		
		if($flag == true){
			$html = '<div class="contactDispWrap">';
			$this->Payment->id = $payment['Payment']['id'];
			if($payment['Payment']['viewed_profile_contacts'] != null){
			
				$profileArr = explode(',', $payment['Payment']['viewed_profile_contacts']);
				
				if (!in_array($staffId, $profileArr)) {
					array_push($profileArr, $staffId);
					$profString = implode(',', $profileArr);
					$this->Payment->saveField('viewed_profile_contacts', $profString);
					$this->Payment->saveField('quantity_viewed', $payment['Payment']['quantity_viewed']+1);
				}
                /*
				else
				{
					$this->Payment->saveField('quantity_viewed', $payment['Payment']['quantity_viewed']+1);
				}
                */
			} 
			else {
				
				$this->Payment->saveField('viewed_profile_contacts', $staffId);
				$this->Payment->saveField('quantity_viewed', $payment['Payment']['quantity_viewed']+1);
			}

			$user_id=$this->Session->read('Auth.User.id');
			
			$contactView = $this->Viewcontacts->query("select id from hh_viewcontacts where user_id = '$user_id' and staff_id ='$staffId'");
			if(count($contactView) == 0){
				$this->Viewcontacts->query("insert into hh_viewcontacts (user_id,staff_id,agent_id,created_date) values('$user_id','$staffId','$agent_id',NOW())");
			}
			
			$this->Staff->recursive = 0;
			$staff = $this->Staff->find('first',array('fields'=>'Staff.name,Staff.email, Staff.mobile_number, Staff.contact_number', 'conditions'=>array('Staff.id'=>$staffId)));
			$html.= '<p>Name: '.($staff['Staff']['name']!='' ? $staff['Staff']['name'] : 'NA').'</p>';
			$html.= '<p>Email Address: '.($staff['Staff']['email']!='' ? $staff['Staff']['email'] : 'NA').'</p>';
			$html.= '<p>Mobile Number: '.($staff['Staff']['mobile_number']!='' ? $staff['Staff']['mobile_number'] : 'NA').'</p>';
			$html.= '<p class="payment_complete">Other Number: '.($staff['Staff']['contact_number']!='' ? $staff['Staff']['contact_number'] : 'NA').'</p>';
			if($vetCandidatePend){
				$html.=' <input type="button" data-attr='.$staffId.' class="btn btn-warning" value="You have submited a pending verification for '.($staff['Staff']['name']!='' ? $staff['Staff']['name'] : 'NA').'" data-toggle="modal" data-target="#">';
			}elseif($vetCandidateApp){
				$html.=' <input type="button" data-attr='.$staffId.' class="btn btn-success" value="'.($staff['Staff']['name']!='' ? $staff['Staff']['name'] : 'NA').' has been verified" data-toggle="modal" data-target="#">';
			}else{
				$html.=' <a href =staffs/vetPaymentCandidate/'.$staffId.' class="btn btn-danger" value="Click to Have '.($staff['Staff']['name']!='' ? $staff['Staff']['name'] : 'NA').' Verified">Click to Have '.($staff['Staff']['name']!='' ? $staff['Staff']['name'] : 'NA').' VVerified</a>';
			}
			
			$html.= '</div>';
		} else {
			$this->PaymentOption->recursive = 0;
			$options = $this->PaymentOption->find('all', array('conditions'=>array('PaymentOption.type'=>1)));
			//$html.= '<p>Please subscribe to an option to view the contact details.</p>';
			$i = 0;
			if(count($options) > 0){
				/*
				$html.= '<table class="table table-striped"><thead>
					<tr>       
						<th>Option </th>
						<th>Quantity</th>
						<th>Validity</th>
						<th>Unit Price</th>
						<th>Price</th>
						<th>&nbsp;</th>
					</tr>
				</thead><tbody>';
				*/
				
				foreach($options as $option){
					++$i;
					$staffProperties="";
					$optionId = $option['PaymentOption']['id'];
					$optionName = $option['PaymentOption']['name'];
					$price = $option['PaymentOption']['amount'];
					$quantity = $option['PaymentOption']['quantity']!=null ? 'Call up to '.$option['PaymentOption']['quantity'].' Candidates' : 'Unlimited';
					$validity = $option['PaymentOption']['validity']!=null ? $option['PaymentOption']['validity'].' Days' : 'Unlimited';
					$unitprice = $option['PaymentOption']['quantity']!=null ? '&#8358;'.(number_format($option['PaymentOption']['amount']/$option['PaymentOption']['quantity'], 2,".",",")).'/ Contact' : 'NA';
					$param = "'".$option['PaymentOption']['id']."'";
					$staffProperties.='"'.$optionName.'|'.$quantity.'|'.$validity.'|'.$unitprice.'|'.number_format($price,2,".",",").'"';
					
					if(count($options)=="1")
						$class_name="col-sm-12 col-md-12";
					if(count($options)=="2")
						$class_name="col-sm-6 col-md-6";
					if(count($options)=="3")
						$class_name ="col-sm-4 col-md-4";
					
					if($i==2) {
						$html.='<div class="'.$class_name.' sp-box">';
					}
					else
					{
						$html.='<div class="'.$class_name.' fp-box">';
					}
                        $html.='<h4>'.$optionName.'</h4>';
                        $html.='<span class="white-circle">Pay <br/> &#8358; '.number_format($price,2,".",",").'</span>';
                        $html.='<p>Valid For</p>';
                        $html.='<h5><i class="fa fa-calendar"></i> '.$validity.'</h5>';
                        //$html.='<p>View Details</p>';
                        $html.='<h5 style="height: 46px;"><i class="fa fa-user"></i> '.$quantity.'</h5>';
                        $html.=' <input type="button" data-attr='.$staffProperties.' onclick="putOptionVal('.$param.');getSummary(this)" class="btn btn-default" value="Select Plan" data-toggle="modal" data-target="#ConfirmPlans">';
                   $html.='</div>';
   				}
			}
			//$html.= '</tbody></table>';
			$html.= '<script>
				function putOptionVal(optionId){
					jQuery("#StaffPaymentOptionId").val(optionId);
				}
			</script>';
		}
		echo $html;
		exit;
	}

	public function vetPaymentCandidate($staff_id=null){
		$this->Staff->recursive = 0;
		$staff = $this->Staff->find('first',array('fields'=>'Staff.id, Staff.staff-uid, Staff.name, Staff.email, Staff.mobile_number, Staff.contact_number, Staff.image1, Staff.image2', 'conditions'=>array('Staff.id'=>$staff_id)));
		$vetCategories = $this->User->query("select * from hh_vetting_categories");
		$this->set('staff', $staff);
		$this->set('vetCategories', $vetCategories);
		// var_dump($staff);exit;
	}

	public function vettinCompanies($id=null){
		$query = $this->User->query("SELECT * FROM hh_vetting_companies WHERE vetting_category_id = '$id'");
		echo json_encode($query);
		exit;
	}

	public function payment(){
		$this->request->data['Payment']['payment_option_id'] = $this->request->data['Staff']['payment_option_id'];
		$this->request->data['Payment']['address'] = $this->Session->read('Auth.User.address');
		$this->request->data['Payment']['city'] = $this->Session->read('Auth.User.city');
		$this->request->data['Payment']['state'] = $this->Session->read('Auth.User.state');
		$this->request->data['Payment']['postal_code'] = $this->Session->read('Auth.User.postal_code');
		$this->request->data['Payment']['country_id'] = $this->Session->read('Auth.User.country_id');
		$this->request->data['Payment']['email'] = $this->Session->read('Auth.User.email');
		$this->request->data['Payment']['phone'] = $this->Session->read('Auth.User.phone');
		$this->request->data['Payment']['user_id'] = $this->Session->read('Auth.User.id');
		$this->PaymentOption->recursive = 0;
		$option = $this->PaymentOption->find('first', array('conditions'=>array('PaymentOption.id'=>$this->request->data['Payment']['payment_option_id'])));
		$this->request->data['Payment']['paid_amount'] = $option['PaymentOption']['amount'];
		$this->Payment->create();
		if ($this->Payment->save($this->request->data)) {
			$lastCreatedOrderId = $this->Payment->id;
			$this->Cardsave->process($this->request->data, $lastCreatedOrderId);
		} else {
			$this->Session->setFlash(__('Payment Failed!'));
			$this->redirect($this->referer());
		}
	}

	public function vetPayment(){

		$user_id = $this->Session->read('Auth.User.id');
		$staff_id = $this->request->data['staff_id'];
		$comIdcom = $this->request->data['comIdcom'];
		$comCatcom = $this->request->data['comCatcom'];
		$comids = explode(',', $comIdcom);
		$comCats = explode(',', $comCatcom);
		$this->Session->write('staff_id', $staff_id);
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);

		$card = [
		  "card_no" => $this->request->data['card_no'],
		  "cvv" => $this->request->data['cvv'],
		  "expiry_month" => $this->request->data['expiry_month'],
		  "expiry_year" => $this->request->data['expiry_year'],
		  "pin" =>$this->request->data['pin'],
		  "card_type" => "" //optional parameter. only needed if card was issued by diamond card
		];
		$custId = "76464"; //your users customer id
		$currency = Currencies::NAIRA; //currency to charge the card
		$authModel = AuthModel::PIN; //can be BVN, NOAUTH, PIN, etc
		$narration = "Payment for Vetting";
		$responseUrl = ""; //callback url
		$country = Countries::NIGERIA;
		$amount = $this->request->data['amount'];
		$this->Session->write('amount', $amount);
		$response = Card::charge($card, $amount, $custId, $currency, $country, $authModel, $narration, $responseUrl);
		$data = $response->getResponseData();
		$ref = $data['data']['transactionreference'];
		for ($i = 0; $i < count($comids); $i++){
	        $this->VettedCandidate->create();
	        $this->VettedCandidate->save(array(
	            'vetting_category_id' => $comCats[$i],
	            'vetting_company_id'=> $comids[$i],
	            'user_id'=>$user_id,
	            'staff_id'=>$staff_id,
	            'payment_ref' => $ref,
	        ));
	    }
		echo json_encode($response->getResponseData());
		exit;
	}

	public function validateOtp2(){
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
		$ref = $this->request->data['ref'];
		$otp = $this->request->data['otp'];
		$cardType = "";
		$result = Card::validate($ref, $otp, $cardType = "");

		$this->User->recursive = 0;
		$user_id = $this->Session->read('Auth.User.id');
		$user_condition= array("User.id" => $user_id);
		$user_details=$this->User->find('first', array('conditions' => $user_condition));
		$user_email=$user_details['User']['email'];
		$user_name=$user_details['User']['name'];

		$this->Staff->recursive = 0;
		$staff_id = $this->Session->read('staff_id');
		$amount = $this->Session->read('amount');
		$staff = $this->Staff->find('first',array('fields'=>'Staff.name, Staff.email, Staff.mobile_number, Staff.contact_number', 'conditions'=>array('Staff.id'=>$staff_id)));

		if($result->isSuccessfulResponse()){
			$this->User->query("update hh_vetted_candidates set payment_status = 1 where payment_ref = '$ref'");
		  	// $message='Your Transaction was successful!<br/>';
			// $message.='Reason : Vetting For '.$staff['Staff']['name']."<br/>";
			// $message.='Transaction Reference : '.$this->request->data['ref']."<br/>";
			// $this->getEmailContent($user_name,$amount,'successful',$this->request->data['ref'],$user_email);
		  	echo "Successful";
		  	exit;
		}else{
			// $message='Your Transaction Failed!<br/>';
			// $message.='Reason : Vetting For '.$staff['Staff']['name']."<br/>";
			// $message.='Transaction Reference : '.$this->request->data['ref']."<br/>";
			// $this->getEmailContent($user_name,$amount,'successful',$this->request->data['ref'],$user_email);
			echo 'Failed';
			exit;
		}

	}

	public function flutterwave(){
		
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
		$amount = str_replace( ',', '', $this->request->data['amount']);
		$card = [
		  "card_no" => $this->request->data['card_no'],
		  "cvv" => $this->request->data['cvv'],
		  "expiry_month" => $this->request->data['expiry_month'],
		  "expiry_year" => $this->request->data['expiry_year'],
		  "pin" =>$this->request->data['pin'],
		  "card_type" => "" //optional parameter. only needed if card was issued by diamond card
		];
		$custId = "76464"; //your users customer id
		$currency = Currencies::NAIRA; //currency to charge the card
		$authModel = AuthModel::PIN; //can be BVN, NOAUTH, PIN, etc
		$narration = "HouseHelp4Hire Subscrition";
		$responseUrl = DOMAIN_NAME_PATH. 'staffs/flutterwavestatus';; //callback url
		$country = Countries::NIGERIA;
		$amount = $amount;
		$response = Card::charge($card, $amount, $custId, $currency, $country, $authModel, $narration, $responseUrl);
		$data = $response->getResponseData();
		$ref = $data['data']['transactionreference'];
		
        $gtpay_tranx_noti_url =  DOMAIN_NAME_PATH. 'staffs/flutterwavestatus';
      
        $this->request->data['Payment']['gtpay_tranx_id'] =  $ref;
        $this->request->data['Payment']['gtpay_tranx_state'] = 1;

        $this->request->data['Payment']['payment_option_id'] = $this->request->data['payment_option_id'];

        $this->request->data['Payment']['email'] = $this->Session->read('Auth.User.email');
        $this->request->data['Payment']['user_id'] = $this->Session->read('Auth.User.id');
		$this->request->data['Payment']['created_at'] = date('Y-m-d H:i:s');
		
		//check for coupon_code 
		$discount_percent =0;
		if(!empty($this->request->data['Staff']['coupon_code']))
		{
			$coupon_code=$this->request->data['Staff']['coupon_code'];
			$discount_percent=0;
			$current = date('Y-m-d');
			$coupon_options = $this->Coupon->find('all', array('conditions'=>array('Coupon.code'=>$coupon_code,'and'=>array('Coupon.valid_till >= '=> $current))));
			
			if(count($coupon_options) > 0){
			
			foreach($coupon_options as $coupons){
					$discount_percent = $coupons['Coupon']['discount'];
				}		
			}
		}
        $this->PaymentOption->recursive = 0;
        
        $option = $this->PaymentOption->find('first', array('conditions'=>array('PaymentOption.id'=>$this->request->data['Payment']['payment_option_id'])));

        $amount= $option['PaymentOption']['amount'];
      	if($discount_percent>0)
      	{
			$price_after_discount=$amount-(($amount*$discount_percent)/100);
			$amount=$price_after_discount;
		}
		
		$this->request->data['Payment']['paid_amount'] =$amount;
        
        // $gtpay_kobo_amount = $this->request->data['Payment']['paid_amount'] * 100;

        // $gttran_id_s = (string)$gttran_id;
        // $gtpay_kobo_amount_s = (string)$gtpay_kobo_amount;
        // $this->request->data['Payment']['gtpay_tranx_amt'] = $gtpay_kobo_amount_s;
        
        $gt_cust_id= $this->Session->read('Auth.User.id');
        $this->request->data['Payment']['gtpay_cust_id'] = $gt_cust_id;
        
        // $tranx_hashstring=$gtpay_mert_id.$gttran_id_s.$gtpay_kobo_amount_s.$gtpay_tranx_curr.$gt_cust_id.$gtpay_tranx_noti_url.$hashkey;
		
        // $tranx_hash = hash('sha512',$tranx_hashstring, false);
			
        // $this->request->data['Payment']['gtpay_hash'] = $tranx_hash;
		//save quantity and validity
		$validity= $option['PaymentOption']['validity'];
		$quantity= $option['PaymentOption']['quantity'];
		
		if($validity != null || $validity != ''){
		 $this->request->data['Payment']['expiry_date'] = date('Y-m-d H:i:s',strtotime('+'.$validity.' days'));
		}
		if($quantity != null || $quantity != ''){
			$this->request->data['Payment']['quantity'] = $quantity;
		}
		$this->request->data['Payment']['validity']=$validity;
        $this->Payment->create();
        if ($this->Payment->save($this->request->data)) {
            $lastCreatedOrderId = $this->Payment->id;
        } else {
            $this->Session->setFlash(__('Payment Failed!'));
            $this->redirect($this->referer());
        }

        $gtpaydata = $this->request->data['Payment'];
        $gtpaydata['gtpay_cust_name'] = $this->Session->read('Auth.User.name');
        $gtpaydata['gtpay_tranx_noti_url'] = $gtpay_tranx_noti_url;
        // $this->set('gtpaydata', $gtpaydata);
        $this->Payment->saveField('gtpay_tranx_state', 2);

        echo json_encode($response->getResponseData());
		exit;
	}

	public function validateOtp(){
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
		$ref = $this->request->data['ref'];
		$otp = $this->request->data['otp'];
		$cardType = "";
		$result = Card::validate($ref, $otp, $cardType = "");

		$this->Payment->recursive = 0;
		$conditions = array("Payment.gtpay_tranx_id" => $ref);
		$payment = $this->Payment->find('first', array('conditions' => $conditions));
		$paymentid = $payment['Payment']['id'];
		$this->Payment->id = $paymentid;
		
		$user_id=$payment['Payment']['user_id'];
		
		$this->User->recursive = 0;
		$user_condition= array("User.id" => $user_id);
		$user_details=$this->User->find('first', array('conditions' => $user_condition));
		$user_email=$user_details['User']['email'];
		$user_name=$user_details['User']['name'];
		if($result->isSuccessfulResponse()){
			$this->Payment->saveField('gtpay_tranx_state', 'completed');
            $this->Payment->saveField('payment_status', 'Y');
			// $message='Your Transaction was successful!<br/>';
			// $message.='Reason : '.$this->request->data['gtpay_tranx_status_msg']."<br/>";
			// $message.='Transaction Reference : '.$this->request->data['ref']."<br/>";
			// $this->getEmailContent($user_name,$payment['Payment']['paid_amount'],'successful',$this->request->data['ref'],$user_email);
		  	echo "Successful";
		  	exit;
		}else{
			// $message='Your Transaction was not successful!<br/>';
			// $message.='Reason : '.$this->request->data['gtpay_tranx_status_msg']."<br/>";
			// $message.='Transaction Reference : '.$this->request->data['ref'];
			// $message.=$status;
			// $this->getEmailContent($user_name,$payment['Payment']['paid_amount'],'not successful',$this->request->data['gtpay_tranx_id'],$user_email);
			echo 'Failed';
			exit;
		}
	}

    public function gtpayment(){
                    $min = 100000;
                    $max = 99999999999;
                    $hashkey = 'A419A2ABB86F1CEFA30637D2E70551ED4E01EC364EF363D9375EDE57611C10E0C685316B15E327772E14C0DC89049F27CF750A48671073D2E6C6D891A80859C1';//'D3D1D05AFE42AD50818167EAC73C109168A0F108F32645C8B59E897FA930DA44F9230910DAC9E20641823799A107A02068F7BC0F4CC41D2952E249552255710F';
                    $gttran_id = mt_rand($min, $max);
                    $gtpay_mert_id="2937";
                    $gtpay_tranx_curr="566";                   	
                    $gtpay_tranx_noti_url =  DOMAIN_NAME_PATH. 'staffs/gtpaymentstatus';
                  
                    $this->request->data['Payment']['gtpay_tranx_id'] =  $gttran_id;
                    $this->request->data['Payment']['gtpay_tranx_state'] = 1;
	
                    $this->request->data['Payment']['payment_option_id'] = $this->request->data['Staff']['payment_option_id'];
                    $this->request->data['Payment']['email'] = $this->Session->read('Auth.User.email');
                    $this->request->data['Payment']['user_id'] = $this->Session->read('Auth.User.id');
					$this->request->data['Payment']['created_at'] = date('Y-m-d H:i:s');
					
					//check for coupon_code 
					$discount_percent =0;
					if(!empty($this->request->data['Staff']['coupon_code']))
					{
						$coupon_code=$this->request->data['Staff']['coupon_code'];
						$discount_percent=0;
						$current = date('Y-m-d');
						$coupon_options = $this->Coupon->find('all', array('conditions'=>array('Coupon.code'=>$coupon_code,'and'=>array('Coupon.valid_till >= '=> $current))));
						
						if(count($coupon_options) > 0){
						
						foreach($coupon_options as $coupons){
								$discount_percent = $coupons['Coupon']['discount'];
							}		
						}
					}
                    $this->PaymentOption->recursive = 0;
                    
                    $option = $this->PaymentOption->find('first', array('conditions'=>array('PaymentOption.id'=>$this->request->data['Payment']['payment_option_id'])));
                    $amount= $option['PaymentOption']['amount'];
                  	if($discount_percent>0)
                  	{
						$price_after_discount=$amount-(($amount*$discount_percent)/100);
						$amount=$price_after_discount;
					}
					
					$this->request->data['Payment']['paid_amount'] =$amount;
                    $gtpay_kobo_amount = $this->request->data['Payment']['paid_amount'] * 100;

                    $gttran_id_s = (string)$gttran_id;
                    $gtpay_kobo_amount_s = (string)$gtpay_kobo_amount;
                    $this->request->data['Payment']['gtpay_tranx_amt'] = $gtpay_kobo_amount_s;
                    
                    $gt_cust_id= $this->Session->read('Auth.User.id');
                    $this->request->data['Payment']['gtpay_cust_id'] = $gt_cust_id;
                    
                    $tranx_hashstring=$gtpay_mert_id.$gttran_id_s.$gtpay_kobo_amount_s.$gtpay_tranx_curr.$gt_cust_id.$gtpay_tranx_noti_url.$hashkey;
					
                    $tranx_hash = hash('sha512',$tranx_hashstring, false);
  					
                    $this->request->data['Payment']['gtpay_hash'] = $tranx_hash;
					//save quantity and validity
					$validity= $option['PaymentOption']['validity'];
					$quantity= $option['PaymentOption']['quantity'];
					
					if($validity != null || $validity != ''){
					 $this->request->data['Payment']['expiry_date'] = date('Y-m-d H:i:s',strtotime('+'.$validity.' days'));
					}
					if($quantity != null || $quantity != ''){
						$this->request->data['Payment']['quantity'] = $quantity;
					}
					$this->request->data['Payment']['validity']=$validity;
                    $this->Payment->create();
                    if ($this->Payment->save($this->request->data)) {
                        $lastCreatedOrderId = $this->Payment->id;
                    } else {
                        $this->Session->setFlash(__('Payment Failed!'));
                        $this->redirect($this->referer());
                    }

                    $gtpaydata = $this->request->data['Payment'];
                    $gtpaydata['gtpay_cust_name'] = $this->Session->read('Auth.User.name');
                    $gtpaydata['gtpay_tranx_noti_url'] = $gtpay_tranx_noti_url;
                    $this->set('gtpaydata', $gtpaydata);
                    $this->Payment->saveField('gtpay_tranx_state', 2);
				}

public function gtpaymentstatus () {
	
				//TODO 
				// refactoring,  gaurd against 'GET'. empty 'POST' and repeated request.

				$tranxid = $this->request->data['gtpay_tranx_id'];
				$statuscode = $this->request->data['gtpay_tranx_status_code'];
				$message="";
				$status="";
				if(isset($statuscode)){

						if(isset($tranxid)){
							$this->Payment->recursive = 0;
							$conditions = array("Payment.gtpay_tranx_id" => $tranxid);

							$payment = $this->Payment->find('first', array('conditions' => $conditions));
							$paymentid = $payment['Payment']['id'];
							$this->Payment->id = $paymentid;
							
							$user_id=$payment['Payment']['user_id'];
							
							$this->User->recursive = 0;
							$user_condition= array("User.id" => $user_id);
							$user_details=$this->User->find('first', array('conditions' => $user_condition));
							$user_email=$user_details['User']['email'];
							$user_name=$user_details['User']['name'];

						}

						if($statuscode == '00'){
							//successful payment
							
							$this->Payment->saveField('gtpay_verification_hash', $this->request->data['gtpay_full_verification_hash']);
							$this->Payment->saveField('gtpay_tranx_status_msg', $this->request->data['gtpay_tranx_status_msg']);
							$this->Payment->saveField('gtpay_tranx_status_code', $this->request->data['gtpay_tranx_status_code']);
							$this->Payment->saveField('gtpay_tranx_state', 'completed');
                            $this->Payment->saveField('payment_status', 'Y');
							$message='Your Transaction was successful!<br/>';
							$message.='Reason : '.$this->request->data['gtpay_tranx_status_msg']."<br/>";
							$message.='Transaction Reference : '.$this->request->data['gtpay_tranx_id']."<br/>";
							
							$this->getEmailContent($user_name,$payment['Payment']['paid_amount'],'successful',$this->request->data['gtpay_tranx_id'],$user_email);
							//$this->Session->setFlash(__($message));
							$this->Session->setFlash(sprintf(__($message, true), 'Member'), 'default', array('class' => 'payment_success'));
						
							//$this->redirect($this->referer());
                            $redirect_url= $this->Session->read('last_url');
                            $this->redirect($redirect_url);

						} else {

							//TODO
							// Proper error handling and meesage as per the docs
							switch ($statuscode) {
							case "G105":
							// Invalid Transaction id
							
							$status='StatusCode : Invalid Transaction id';
							break;
							case "G000":
							// transaction error
							$status='StatusCode : Transaction error';
							break;
							case "G100":
							// Invalid Merchant
							$status='StatusCode : Invalid Merchant';
							break;
							case "G101":
							// invalid customer id
							$status='StatusCode : Invalid Customer Id';
							break;
							case "G102":
							// Missing transaction amount
							$status='StatusCode : Missing transaction amount';
							break;
							case "G103":
							// Missing transaction amount
							$status='StatusCode : Invalid transaction amount';
							break;
							case "G104":
							$status='StatusCode : Duplicate transaction id';
							break;
							case "G106":
							$status='StatusCode : Invalid transaction notification url';
							break;
							case "G107":
							$status='StatusCode : Missing Transaction hash';
							break;
							case "G108":
							$status='StatusCode : Interface Integration Error';
							break;
							case "G109":
							$status='StatusCode : Invalid gateway';
							break;
						}
							$message='Your Transaction was not successful!<br/>';
							$message.='Reason : '.$this->request->data['gtpay_tranx_status_msg']."<br/>";
							$message.='Transaction Reference : '.$this->request->data['gtpay_tranx_id'];
							$message.=$status;
							
							//$this->Session->setFlash(__($message));
							$this->Session->setFlash(sprintf(__($message, true), 'Member'), 'default', array('class' => 'payment_error'));
							$this->getEmailContent($user_name,$payment['Payment']['paid_amount'],'not successful',$this->request->data['gtpay_tranx_id'],$user_email);
                            //$this->redirect($this->referer());
                            $redirect_url= $this->Session->read('last_url');
                            $this->redirect($redirect_url);
						}
					
				}


 }

public function getEmailContent($user_name,$amount,$status,$transaction_reference,$user_email)
{
	$admin = $this->Setting->read(null,'1');
	$site_logo=$admin['Setting']['site_logo'];
	$fb_link=$admin['Setting']['facebook_link'];
	$tw_link=$admin['Setting']['twitter_link'];
	$support_email=$admin['Setting']['support_email'];
	
	$mail_To= $user_email;
	$mail_From = $admin['Setting']['noreply_email'];
	$mail_subject="HouseHelp4Hire-Your Transaction Details";
	
	$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									<p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$site_logo."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;'>
										  <span style='font-size:12px'>Hi ".$user_name.",</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p><strong>The following are your transaction details:</strong></p>
												  	<p>
													 	Your payment to HouseHelp4Hire for NGN ".$amount." was ".$status.".</p><p> Your payment reference number is ".$transaction_reference."
													 </p>
													 <p>If you're having any technical issues, please contact our support team (".trim($support_email).")</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$fb_link."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$tw_link."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
	$mail_Body = $content;
	$mail_CC='';				
	$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);						
}
public function Secure(){
$this->Cardsave->carsaveSecure();
}
public function SecureIFrame(){
$this->Cardsave->carsaveIframe();
}
public function SecureProcess() {
	$arrayResponse = $this->Cardsave->carsaveSecureProcess();
	$order = array();
	$this->Payment->recursive = 0;
	if($arrayResponse['szCrossReference'] != null){
			$this->Payment->id = $_SESSION['CardSave_Direct_OrderID'];
			$this->Payment->saveField('payment_status','Y');
			$this->Payment->saveField('txn-id',$arrayResponse['szCrossReference']);
			$this->Payment->saveField('payment_date',date('Y-m-d H:i:s'));
			$this->set('szCrossReference', $arrayResponse['szCrossReference']);
			$order = $this->Payment->find('first', array('conditions'=>array('Payment.id'=>$_SESSION['CardSave_Direct_OrderID'])));
			if($order['PaymentOption']['validity'] != null || $order['PaymentOption']['validity'] != ''){
				$this->Payment->saveField('expiry_date', date('Y-m-d H:i:s',strtotime('+'.$order['PaymentOption']['validity'].' days')));
			}
			if($order['PaymentOption']['quantity'] != null || $order['PaymentOption']['quantity'] != ''){
				$this->Payment->saveField('quantity', $order['PaymentOption']['quantity']);
			}
			$this->Payment->saveField('validity', $order['PaymentOption']['validity']);
			$siteSetting = $this->viewVars['siteSetting'];
			/*mail start*/
			$orderLink = DOMAIN_NAME_PATH.'my-account';
			//$mailer = $this->Mailer->read(null, 9);
			$logoPath = SITE_LOGO_URL.$siteSetting['Setting']['site_logo'];
			$mail_From = $siteSetting['Setting']['noreply_email'];
			$mail_To = $this->Session->read('Auth.User.email');
			$mail_CC = null;
			//$subject = $mailer['Mailer']['name'];
			//$template = $mailer['Mailer']['html_content'];
			$subject = 'Payment Placed on HouseHelp4Hire';
			$template = '<div style="width: 700px; font-family: Arial, Helvetica, sans-serif;">
			<p align="center"><img src="##logo##" alt="TechLux" border="0" /></p>
			<table style="width: 100%;" border="0" cellspacing="10" cellpadding="10">
			<tbody>
			<tr>
			<td align="left" valign="top" bgcolor="#F1F1F2" width="77%">
			<div style="width: 97%; height: auto; border: 1px solid #01A279; -moz-border: 10px; border-radius: 10px; background: #01A279; margin: 0 0 10px 0; padding: 10px; color: #ffffff;"><span style="font-size: 12px;">Dear ##user##,</span><br /><span style="font-size: 15px; font-weight: bold;">##mail_subject##</span></div>
			<div style="border: 1px solid #b8b8b8; border-radius: 10px; -moz-border-radius: 10px; padding: 10px; margin: 0 0 10px 0; width: 635px; background: #ffffff;">
			<table style="width: 100%;" border="0" cellspacing="5" cellpadding="5">
			<tbody>
			<tr>
			<td align="left" valign="top">
			<p><strong>Your order had been placed successfully on TechLux. Order details are below...</strong></p>
			<div>##mail_content##</div>
			<p>Should you have any questions relating to this process? Please do not hesitate to contact our Agency Support ##support_email##.</p>
			<p>We look forward to a successful and profitable working relationship.</p>
			</td>
			</tr>
			</tbody>
			</table>
			</div>
			<div style="width: 97%; height: auto; border: 1px solid #01A279; -moz-border: 10px; border-radius: 10px; background: #01A279; margin: 0 0 10px 0; padding: 10px; color: #ffffff;"><span style="font-size: 12px;">Regards, Admin [##contact_email##],</span><br /><span style="font-size: 15px; font-weight: bold;">HouseHelp4Hire</span></div>
			</td>
			</tr>
			</tbody>
			</table>
			</div>';
			$nameBuyer = $this->Session->read('Auth.User.name');
			$mail_content = "<p>Order ID: ".$order['Payment']['id']."<br/>
							Transaction ID: ".$arrayResponse['szCrossReference']."<br/>
							Paid Amount: ".number_format($order['Payment']['paid_amount'], 2)."<br/>
							Payment Date: ".date("F j, Y, g:i a", strtotime($order['Payment']['payment_date']))."<br/>
							You can review your order: <a href='".$orderLink."'>".$orderLink."</a></p>";
			$template = str_replace("##logo##", $logoPath, $template);
			$template = str_replace("##user##", $nameBuyer, $template);
			$template = str_replace("##mail_subject##", $subject, $template);
			$template = str_replace("##support_email##", $siteSetting['Setting']['support_email'], $template);
			$template = str_replace("##contact_email##", $siteSetting['Setting']['contact_email'], $template);
			$template = str_replace("##mail_content##", $mail_content, $template);
			//$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $template);
			/*mail end*/
		}
		$this->set('order', $order);
		$this->set('Response', $arrayResponse['message']);
		$this->set('szCrossReference', $arrayResponse['szCrossReference']);
	}
	public function changeLoc($id){
		$location = $_POST['location'];
		$this->Booking->id=$id;
		$this->Booking->saveField('location', $location);
		echo $location;
		exit;
	}
	public function admin_view($id = null) {
		$this->Staff->recursive = 1;
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(__('Invalid staff.'));
		}
		$staff = $this->Staff->read(null, $id);
		//pr($staff['StaffPreferedLocation']);
		//pr($staff['StaffSpokenLanguage']);
		if(count($staff['StaffPreferedLocation'])>0){
			foreach($staff['StaffPreferedLocation'] as $locArr){
				$idLoc[] = $locArr['location_id'];
			}
			$locations = $this->Location->find('list', array('conditions'=>array('Location.id'=>$idLoc)));
			$PreferedLocation = implode(',', $locations);
			$this->set('PreferedLocation', $PreferedLocation);
		}
		if(count($staff['StaffPreferedLocality'])>0){
			foreach($staff['StaffPreferedLocality'] as $locArr){
				$idLoca[] = $locArr['locality_id'];
			}
			$localities = $this->Locality->find('list', array('conditions'=>array('Locality.id'=>$idLoca)));
			$PreferedLocality = implode(',', $localities);
			$this->set('PreferedLocality', $PreferedLocality);
		}
		if(count($staff['StaffSpokenLanguage'])>0){
			foreach($staff['StaffSpokenLanguage'] as $langArr){
				$idLang[] = $langArr['language_id'];
			}
			$languages = $this->Language->find('list', array('conditions'=>array('Language.id'=>$idLang)));
			$PreferedLanguage = implode(',', $languages);
			$this->set('PreferedLanguage', $PreferedLanguage);
		}
		if(count($staff['StaffPreferedEducation'])>0){
			foreach($staff['StaffPreferedEducation'] as $eduArr){
				$idEdu[] = $eduArr['education_id'];
			}
			$educations = $this->Education->find('list', array('conditions'=>array('Education.id'=>$idEdu)));
			$PreferedEducation = implode(',', $educations);
			$this->set('PreferedEducation', $PreferedEducation);
			
			
		}
		if(count($staff['StaffCategory'])>0){
			foreach($staff['StaffCategory'] as $catArr){
				$idCat[] = $catArr['category_id'];
			}
			$categories = $this->Category->find('list', array('conditions'=>array('Category.id'=>$idCat)));
			$PreferedCategory = implode(',', $categories);
			$this->set('PreferedCategory', $PreferedCategory);
			
			
		}
		$this->set('staff', $staff);
	}

	public function vet_view($id = null) {
		$this->Staff->recursive = 1;
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(__('Invalid staff.'));
		}
		$staff = $this->Staff->read(null, $id);
		//pr($staff['StaffPreferedLocation']);
		//pr($staff['StaffSpokenLanguage']);
		if(count($staff['StaffPreferedLocation'])>0){
			foreach($staff['StaffPreferedLocation'] as $locArr){
				$idLoc[] = $locArr['location_id'];
			}
			$locations = $this->Location->find('list', array('conditions'=>array('Location.id'=>$idLoc)));
			$PreferedLocation = implode(',', $locations);
			$this->set('PreferedLocation', $PreferedLocation);
		}
		if(count($staff['StaffPreferedLocality'])>0){
			foreach($staff['StaffPreferedLocality'] as $locArr){
				$idLoca[] = $locArr['locality_id'];
			}
			$localities = $this->Locality->find('list', array('conditions'=>array('Locality.id'=>$idLoca)));
			$PreferedLocality = implode(',', $localities);
			$this->set('PreferedLocality', $PreferedLocality);
		}
		if(count($staff['StaffSpokenLanguage'])>0){
			foreach($staff['StaffSpokenLanguage'] as $langArr){
				$idLang[] = $langArr['language_id'];
			}
			$languages = $this->Language->find('list', array('conditions'=>array('Language.id'=>$idLang)));
			$PreferedLanguage = implode(',', $languages);
			$this->set('PreferedLanguage', $PreferedLanguage);
		}
		if(count($staff['StaffPreferedEducation'])>0){
			foreach($staff['StaffPreferedEducation'] as $eduArr){
				$idEdu[] = $eduArr['education_id'];
			}
			$educations = $this->Education->find('list', array('conditions'=>array('Education.id'=>$idEdu)));
			$PreferedEducation = implode(',', $educations);
			$this->set('PreferedEducation', $PreferedEducation);
			
			
		}
		if(count($staff['StaffCategory'])>0){
			foreach($staff['StaffCategory'] as $catArr){
				$idCat[] = $catArr['category_id'];
			}
			$categories = $this->Category->find('list', array('conditions'=>array('Category.id'=>$idCat)));
			$PreferedCategory = implode(',', $categories);
			$this->set('PreferedCategory', $PreferedCategory);
			
			
		}
		$this->set('staff', $staff);
	}
	
/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$exist_data = $this->Staff->find('list', array('conditions' => array('Staff.contact_number' => $this->request->data['Staff']['contact_number'])));
			if(empty($exist_data)){
				
				/*
				$CategoryCode = "$this->Category->field('Category_code', array('Category.id'=>$this->request->data['Staff']['category_id']));
				*/
				
				$friendlyurl = $this->request->data['Staff']['name'];
				foreach($this->notAllowedWord AS $arrNAWord)
				{
					$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
				}
				$friendlyurl = str_replace(' ', '-', $friendlyurl);
				$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $friendlyurl)));
				$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				$duplicate = false;
				if(!empty($chk)){
					$this->request->data['Staff']['friendly_url'] = '';
					$duplicate = true;
				}
				if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != '')
				{
					if($this->request->data['Staff']['image1']['type'] == 'image/png' || $this->request->data['Staff']['image1']['type'] == 'image/PNG' || $this->request->data['Staff']['image1']['type'] == 'image/jpg' || $this->request->data['Staff']['image1']['type'] == 'image/JPG' || $this->request->data['Staff']['image1']['type'] == 'image/jpeg' || $this->request->data['Staff']['image1']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Staff']['image1']['name'];

						move_uploaded_file($this->request->data['Staff']['image1']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Staff']['image1'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					$this->Session->setFlash(__('The staff could not be saved.Please upload image1'));
				}
				if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != '')
				{
					if($this->request->data['Staff']['image2']['type'] == 'image/png' || $this->request->data['Staff']['image2']['type'] == 'image/PNG' || $this->request->data['Staff']['image2']['type'] == 'image/jpg' || $this->request->data['Staff']['image2']['type'] == 'image/JPG' || $this->request->data['Staff']['image2']['type'] == 'image/jpeg' || $this->request->data['Staff']['image2']['type'] == 'image/JPEG')
					{
						$filename2=rand(). $this->request->data['Staff']['image2']['name'];
						move_uploaded_file($this->request->data['Staff']['image2']['tmp_name'], PAGE_IMAGES.$filename2);
						$this->request->data['Staff']['image2'] = $filename2;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					$this->Session->setFlash(__('The staff could not be saved.Please upload image2'));
				}
				$preferedLocationArr = $this->request->data['Staff']['prefered_location'];
				unset($this->request->data['Staff']['prefered_location']);
				$spokenLanguageArr = $this->request->data['Staff']['spoken_languages'];
				unset($this->request->data['Staff']['spoken_languages']);
				$preferedLocalityArr = $this->request->data['Staff']['prefered_locality'];
				unset($this->request->data['Staff']['prefered_locality']);
				$preferedEducationArr = $this->request->data['Staff']['prefered_education'];
				unset($this->request->data['Staff']['prefered_education']);
				$this->request->data['Staff']['status']=1;
				$this->request->data['Staff']['hired']=0;
				$this->request->data['Staff']['reg_date']=date('Y-m-d');
				$staffCategoryArr = $this->request->data['Staff']['category_id'];
				unset($this->request->data['Staff']['category_id']);
				
				$CategoryCode="AS"; //assorted
				
				$this->Staff->create();
				if ($this->Staff->save($this->request->data)) {
					$uId = $CategoryCode.$this->Staff->id;
					$idMinLentgth = 5;
					$idLength = strlen($this->Staff->id);
					$lengtDiff = $idMinLentgth-$idLength;
					if($lengtDiff > 0){
						$midStr = '';
						for($iter = 0; $iter < $lengtDiff; $iter++){
							$midStr.='0';
						}
						$uId = $CategoryCode.$midStr.$this->Staff->id;
					}
					$this->Staff->savefield('staff-uid', $uId);
					if($duplicate){
						$friendlyurl .= '-'.$this->Staff->id;
						$this->Staff->savefield('friendly_url', $friendlyurl);
					}
					$id = $this->Staff->id;
					if($preferedLocationArr != ''){
						if(count($preferedLocationArr)>0){
							$checking = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
							if(count($checking)>0){
								//$this->StaffPreferedLocation->id = $checking;
								$this->StaffPreferedLocation->delete($checking);
							}
							$locationRow['StaffPreferedLocation']['staff_id'] = $id;
							foreach($preferedLocationArr as $preferedLocation){
								$locationRow['StaffPreferedLocation']['location_id'] = $preferedLocation;
								$this->StaffPreferedLocation->create();
								$this->StaffPreferedLocation->save($locationRow);
							}
						}
					}
					if($preferedLocalityArr != ''){
						if(count($preferedLocalityArr)>0){
							$checking1 = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
							if(count($checking1)>0){
								$this->StaffPreferedLocality->delete($checking1);
							}
							$localityRow['StaffPreferedLocality']['staff_id'] = $id;
							foreach($preferedLocalityArr as $preferedLocality){
								$localityRow['StaffPreferedLocality']['locality_id'] = $preferedLocality;
								$this->StaffPreferedLocality->create();
								$this->StaffPreferedLocality->save($localityRow);
							}
						}
					}
					if($spokenLanguageArr != ''){
						if(count($spokenLanguageArr)>0){
							$checking2 = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
							if(count($checking2)>0){
								$this->StaffSpokenLanguage->delete($checking2);
							}
							$languageRow['StaffSpokenLanguage']['staff_id'] = $id;
							foreach($spokenLanguageArr as $spokenLanguage){
								$languageRow['StaffSpokenLanguage']['language_id'] = $spokenLanguage;
								$this->StaffSpokenLanguage->create();
								$this->StaffSpokenLanguage->save($languageRow);
							}
						}
					}
					if($preferedEducationArr != ''){
						if(count($preferedEducationArr)>0){
							$checking3 = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
							if(count($checking3)>0){
								$this->StaffPreferedEducation->delete($checking3);
							}
							$educationRow['StaffPreferedEducation']['staff_id'] = $id;
							foreach($preferedEducationArr as $education){
								$educationRow['StaffPreferedEducation']['education_id'] = $education;
								$this->StaffPreferedEducation->create();
								$this->StaffPreferedEducation->save($educationRow);
							}
						}
					}
					if($staffCategoryArr != ''){
						if(count($staffCategoryArr)>0){
							$checking4 = $this->StaffCategory->find('list', array('fields'=>'id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
							if(count($checking4)>0){
								$this->StaffCategory->delete($checking4);
							}
							$categoryRow['StaffCategory']['staff_id'] = $id;
							foreach($staffCategoryArr as $category){
								$categoryRow['StaffCategory']['category_id'] = $category;
								$this->StaffCategory->create();
								$this->StaffCategory->save($categoryRow);
							}
						}
					}
					/*
					$subscribers = $this->Email->find('list', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'DESC')));
					
					if(count($subscribers)>0){
						$siteSetting = $this->viewVars['siteSetting'];
						$this->Mailer->recursive = 0;
						$mailer = $this->Mailer->find('first', array('conditions'=>array('Mailer.id'=>1)));
						$html = $mailer['Mailer']['html_content'];
						$logoPath = DOMAIN_NAME_PATH.'img/site_logo/'.$siteSetting['Setting']['site_logo'];
						$mail_From = $siteSetting['Setting']['noreply_email'];
						$mail_CC = null;
						$subject = $mailer['Mailer']['name'];
						$html = str_replace("##logo_path##", $logoPath, $html);
						$html = str_replace("##noreply_email##", $siteSetting['Setting']['noreply_email'], $html);
						$html = str_replace("##subject##", $subject, $html);
						$html = str_replace("##staff_name##", $this->request->data['Staff']['name'], $html);
						$html = str_replace("##experience##", $this->request->data['Staff']['experience'], $html);
						$html = str_replace("##age##", $this->request->data['Staff']['age'], $html);
						$html = str_replace("##mini_biography##", $this->request->data['Staff']['mini_biography'], $html);
						$html = str_replace("##staff_url##", DOMAIN_NAME_PATH.$friendlyurl, $html);
						$html = str_replace("##staff_image##", PAGE_IMAGES_URL.$filename, $html);
						foreach($subscribers as $subscriber){
							$mail_To = $subscriber;
							$convertedString = bin2hex(Security::cipher(serialize($mail_To),Configure::read('Security.salt')));
							$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>Click here</a>";
							$mail_body = str_replace("##unsubscribe##", $link, $html);
							$mail_body = str_replace("##user##", $mail_To, $html);
							//$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
						}
					}
					*/
					$this->Session->setFlash(sprintf(__('The staff has been added successfully!', true), 'Page'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staff could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('This phone has already been registered.', true)));
			}
		}
		$countries = $this->Staff->Country->find('list',array('order'=>'Country.name'));
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list', array('conditions'=>array('Location.service_availability'=>'Yes')));
		$localities = $this->Locality->find('list');
		$religions = $this->Staff->Religion->find('list');
		$educations = $this->Education->find('list');
		
		$agents_list=array();
		$agents=$this->Agent->find('list', array('fields'=>'Agent.name', 'order'=>array('Agent.id'=>'ASC')));
		
		$plans_list["0"]="Direct";
		foreach($agents as $row=>$value) {
    		$plans_list["{$row}"] = "{$value}";
        }
  		$this->set('plans_list', $plans_list);
  		
  		$immigration_list["N/A"] = "N/A";
  		$immigration_list["Valid Resident Card"] = "Valid Resident Card";
  		$immigration_list["Resident Card Required"] = "Resident Card Required";
  		$this->set('immigration_list', $immigration_list);
  		
  		$nationality_list["Benin"] = "Benin";
  		$nationality_list["Burkina Faso"] = "Burkina Faso";
  		
  		$nationality_list["Cape Verde"] = "Cape Verde";
  		$nationality_list["The Gambia"] = "The Gambia";
  		
  		$nationality_list["Ghana"] = "Ghana";
  		$nationality_list["Guinea"] = "Guinea";
  		$nationality_list["Guinea-Bissau"] = "Guinea-Bissau";
  		$nationality_list["Ivory Coast"] = "Ivory Coast";
  		$nationality_list["Liberia"] = "Liberia";
  		$nationality_list["Mali"] = "Mali";
  		$nationality_list["Mali Federation"] = "Mali Federation";
  		$nationality_list["Mauritania"] = "Mauritania";
  		$nationality_list["Niger"] = "Niger";
  		$nationality_list["Nigeria"] = "Nigeria";
  		$nationality_list["Senegal"] = "Senegal";
  		$nationality_list["Sierra Leone"] = "Sierra Leone";
  		$nationality_list["Togo"] = "Togo";
  		
  		$this->set('nationality_list', $nationality_list);
  		
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations','religions','localities', 'educations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(sprintf(__('Invalid staff', true)));
		}
		$staffs=$this->Staff->read(null, $id);
		$oldpic1=$staffs['Staff']['image1'];
		$oldpic2=$staffs['Staff']['image2'];
		if ($this->request->is('post') || $this->request->is('put')) {
			$exist_data = $this->Staff->find('list', array('conditions' => array('Staff.contact_number' => $this->request->data['Staff']['contact_number'], 'Staff.id !=' => $id)));
			if(empty($exist_data)){
				$CategoryCode = $this->Category->field('Category_code', array('Category.id'=>$this->request->data['Staff']['category_id']));
				$friendlyurl = $this->request->data['Staff']['name'];
				foreach($this->notAllowedWord AS $arrNAWord)
				{
					$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
				}
				$friendlyurl = str_replace(' ', '-', $friendlyurl);
				$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $friendlyurl, 'Staff.id !='=>$id)));
				$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				if(!empty($chk)){
					$this->request->data['Staff']['friendly_url'] = $friendlyurl.'-'.$id;
				} else {
					$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				}
				if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != '')
				{
					if($this->request->data['Staff']['image1']['type'] == 'image/png' || $this->request->data['Staff']['image1']['type'] == 'image/PNG' || $this->request->data['Staff']['image1']['type'] == 'image/jpg' || $this->request->data['Staff']['image1']['type'] == 'image/JPG' || $this->request->data['Staff']['image1']['type'] == 'image/jpeg' || $this->request->data['Staff']['image1']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Staff']['image1']['name'];

						move_uploaded_file($this->request->data['Staff']['image1']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Staff']['image1'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image1']);
				}
				if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != '')
				{
					if($this->request->data['Staff']['image2']['type'] == 'image/png' || $this->request->data['Staff']['image2']['type'] == 'image/PNG' || $this->request->data['Staff']['image2']['type'] == 'image/jpg' || $this->request->data['Staff']['image2']['type'] == 'image/JPG' || $this->request->data['Staff']['image2']['type'] == 'image/jpeg' || $this->request->data['Staff']['image2']['type'] == 'image/JPEG')
					{
						$filename2=rand(). $this->request->data['Staff']['image2']['name'];
						move_uploaded_file($this->request->data['Staff']['image2']['tmp_name'], PAGE_IMAGES.$filename2);
						$this->request->data['Staff']['image2'] = $filename2;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image2']);
				}
				$preferedLocationArr = $this->request->data['Staff']['prefered_location'];
				unset($this->request->data['Staff']['prefered_location']);
				$spokenLanguageArr = $this->request->data['Staff']['spoken_languages'];
				unset($this->request->data['Staff']['spoken_languages']);
				$preferedLocalityArr = $this->request->data['Staff']['prefered_locality'];
				unset($this->request->data['Staff']['prefered_locality']);
				$preferedEducationArr = $this->request->data['Staff']['prefered_education'];
				unset($this->request->data['Staff']['prefered_education']);
				
				$uId = $CategoryCode.$this->request->data['Staff']['id'];
				$idMinLentgth = 5;
				$idLength = strlen($this->request->data['Staff']['id']);
				$lengtDiff = $idMinLentgth-$idLength;
				if($lengtDiff > 0){
					$midStr = '';
					for($iter = 0; $iter < $lengtDiff; $iter++){
						$midStr.='0';
					}
					$uId = $CategoryCode.$midStr.$this->request->data['Staff']['id'];
				}
				$staffCategoryArr = $this->request->data['Staff']['category_id'];
				unset($this->request->data['Staff']['category_id']);
				
				$this->request->data['Staff']['staff-uid'] = $uId;
				if($this->request->data['Staff']['hired']=="1")
				{
					$this->request->data['Staff']['hired_date']=date('Y-m-d',strtotime('+3 days'));
				}
					
				//pr($this->request->data);die;
				if ($this->Staff->save($this->request->data)) {
					if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic1);
					}
					if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic2);
					}
					if($preferedLocationArr != ''){
						if(count($preferedLocationArr)>0){
							$checking = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
							if(count($checking)>0){
								$this->StaffPreferedLocation->delete($checking);
							}
							$locationRow['StaffPreferedLocation']['staff_id'] = $id;
							foreach($preferedLocationArr as $preferedLocation){
								$locationRow['StaffPreferedLocation']['location_id'] = $preferedLocation;
								$this->StaffPreferedLocation->create();
								$this->StaffPreferedLocation->save($locationRow);
							}
						}
					}
					if($preferedLocalityArr != ''){
						if(count($preferedLocalityArr)>0){
							$checking1 = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
							if(count($checking1)>0){
								$this->StaffPreferedLocality->delete($checking1);
							}
							$localityRow['StaffPreferedLocality']['staff_id'] = $id;
							foreach($preferedLocalityArr as $preferedLocality){
								$localityRow['StaffPreferedLocality']['locality_id'] = $preferedLocality;
								$this->StaffPreferedLocality->create();
								$this->StaffPreferedLocality->save($localityRow);
							}
						}
					}
					if($spokenLanguageArr != ''){
						if(count($spokenLanguageArr)>0){
							$checking2 = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
							if(count($checking2)>0){
								$this->StaffSpokenLanguage->delete($checking2);
							}
							$languageRow['StaffSpokenLanguage']['staff_id'] = $id;
							foreach($spokenLanguageArr as $spokenLanguage){
								$languageRow['StaffSpokenLanguage']['language_id'] = $spokenLanguage;
								$this->StaffSpokenLanguage->create();
								$this->StaffSpokenLanguage->save($languageRow);
							}
						}
					}
					if($preferedEducationArr != ''){
						if(count($preferedEducationArr)>0){
							$checking3 = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
							if(count($checking3)>0){
								$this->StaffPreferedEducation->delete($checking3);
							}
							$educationRow['StaffPreferedEducation']['staff_id'] = $id;
							foreach($preferedEducationArr as $education){
								$educationRow['StaffPreferedEducation']['education_id'] = $education;
								$this->StaffPreferedEducation->create();
								$this->StaffPreferedEducation->save($educationRow);
							}
						}
					}
					if($staffCategoryArr != ''){
						if(count($staffCategoryArr)>0){
							$checking4 = $this->StaffCategory->find('list', array('fields'=>'StaffCategory.id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
							if(count($checking4)>0){
								$this->StaffCategory->delete($checking4);
							}
							$categoryRow['StaffCategory']['staff_id'] = $id;
							foreach($staffCategoryArr as $category){
								$categoryRow['StaffCategory']['category_id'] = $category;
								$this->StaffCategory->create();
								$this->StaffCategory->save($categoryRow);
							}
						}
					}
					$this->Session->setFlash(sprintf(__('The staff has been updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The staff could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('This phone has already been registered.', true)));
			}
		} else {
			$this->request->data = $this->Staff->read(null, $id);
		}
		$countries = $this->Staff->Country->find('list');
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list', array('conditions'=>array('Location.service_availability'=>'Yes')));
		$localities = $this->Locality->find('list');
		$religions = $this->Staff->Religion->find('list');
		$educations = $this->Education->find('list');
		$ploc=array();
		$locDisp = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.location_id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
		if(count($locDisp)>0)
		{
			$ploc = array_values($locDisp);
		}
		$ploca=array();
		$locaDisp = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.locality_id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
		if(count($locaDisp)>0)
		{
			$ploca = array_values($locaDisp);
		}
		$pLang=array();
		$langDisp = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.language_id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
		if(count($langDisp)>0)
		{
			$pLang = array_values($langDisp);
		}
		$pEdu=array();
		$eduDisp = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.education_id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
		if(count($eduDisp)>0)
		{
			$pEdu = array_values($eduDisp);
		}
		
		$pCat=array();
		$catDisp = $this->StaffCategory->find('list', array('fields'=>'StaffCategory.category_id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
		if(count($catDisp)>0)
		{
			$pCat = array_values($catDisp);
		}
		
		$agents_list=array();
		$agents=$this->Agent->find('list', array('fields'=>'Agent.name', 'order'=>array('Agent.id'=>'ASC')));
		
		$plans_list["0"]="Direct";
		foreach($agents as $row=>$value) {
    		$plans_list["{$row}"] = "{$value}";
        }
  		$this->set('plans_list', $plans_list);
  		
  		$immigration_list["N/A"] = "N/A";
  		$immigration_list["Valid Resident Card"] = "Valid Resident Card";
  		$immigration_list["Resident Card Required"] = "Resident Card Required";
  		$this->set('immigration_list', $immigration_list);
  		
  		$nationality_list["Benin"] = "Benin";
  		$nationality_list["Burkina Faso"] = "Burkina Faso";
  		
  		$nationality_list["Cape Verde"] = "Cape Verde";
  		$nationality_list["The Gambia"] = "The Gambia";
  		
  		$nationality_list["Ghana"] = "Ghana";
  		$nationality_list["Guinea"] = "Guinea";
  		$nationality_list["Guinea-Bissau"] = "Guinea-Bissau";
  		$nationality_list["Ivory Coast"] = "Ivory Coast";
  		$nationality_list["Liberia"] = "Liberia";
  		$nationality_list["Mali"] = "Mali";
  		$nationality_list["Mali Federation"] = "Mali Federation";
  		$nationality_list["Mauritania"] = "Mauritania";
  		$nationality_list["Niger"] = "Niger";
  		$nationality_list["Nigeria"] = "Nigeria";
  		$nationality_list["Senegal"] = "Senegal";
  		$nationality_list["Sierra Leone"] = "Sierra Leone";
  		$nationality_list["Togo"] = "Togo";
  		
  		$this->set('nationality_list', $nationality_list);
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations','localities','ploc','ploca','pLang','pEdu','staffs','religions','educations','pCat'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			throw new NotFoundException(__('Invalid staff'));
		}
		if ($this->Staff->delete()) {
			$this->Session->setFlash(__('Staff deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Staff was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		/*
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		*/
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			throw new NotFoundException(__('Invalid staff'));
		}
		$staffs=$this->Staff->read(null, $id);
		$oldpic1=$staffs['Staff']['image1'];
		$oldpic2=$staffs['Staff']['image2'];
		if ($this->Staff->delete()) {
			unlink(PAGE_IMAGES.$oldpic1);
			unlink(PAGE_IMAGES.$oldpic2);
			$this->Session->setFlash(sprintf(__('The staff has been deleted successfully!', true), 'Page'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Staff was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
/**
 * send email method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function admin_sendmail() {
		if ($this->request->is('post')) {
			$staff_collection="";
			foreach($this->request->data['Staff']['user_email'] as $email)
					{
			$staff_collection.="'".$email."',";
		}
			$staff_collection=substr($staff_collection,0,-1);
			$data=$this->Staff->query("SELECT * FROM hh_staffs where id in (".$staff_collection.")");
	    	if(!empty($data))									
	    	{
	    		
				$siteSetting = $this->viewVars['siteSetting'];
				$this->Mailer->recursive = 0;
				
				$mail_header = $this->Mailer->find('first', array('conditions'=>array('Mailer.name'=>'Header')));
				$mail_footer = $this->Mailer->find('first', array('conditions'=>array('Mailer.name'=>'Footer')));
				$mailer = $this->Mailer->find('first', array('conditions'=>array('Mailer.id'=>4)));
				
				$html_header = $mail_header['Mailer']['html_content'];
				$html_footer = $mail_footer['Mailer']['html_content'];
				$html_content=$mailer['Mailer']['html_content'];
				
				$logoPath = DOMAIN_NAME_PATH.'img/site_logo/'.$siteSetting['Setting']['site_logo'];
				$mail_From = $siteSetting['Setting']['noreply_email'];
				$mail_CC = null;
				
				$subject = $mailer['Mailer']['name'];
			
				
				$html_header = str_replace("##logo_path##", $logoPath, $html_header);
				$html_footer = str_replace("##noreply_email##", $siteSetting['Setting']['noreply_email'], $html_footer);
				$html_header = str_replace("##subject##", $subject, $html_header);	
				
				$email_content=$html_content;
				
				$counter=0;
    			foreach($data as $key=>$value)  
				{
					$staff=$value['hh_staffs'];
					
					if($counter==0)
					{
						$email_content = str_replace("##staff_name##", $staff['name'], $email_content);
						$email_content = str_replace("##experience##", $staff['experience'], $email_content);
						$email_content = str_replace("##age##", $staff['age'], $email_content);
						$email_content = str_replace("##mini_biography##", $staff['mini_biography'], $email_content);
						$email_content = str_replace("##staff_url##", DOMAIN_NAME_PATH.$staff['friendly_url'], $email_content);
						$email_content = str_replace("##staff_image##", PAGE_IMAGES_URL.$staff['image1'], $email_content);
					}
					else{
						
						$email_content.=$html_content;
						$email_content = str_replace("##staff_name##", $staff['name'], $email_content);
						$email_content = str_replace("##experience##", $staff['experience'], $email_content);
						$email_content = str_replace("##age##", $staff['age'], $email_content);
						$email_content = str_replace("##mini_biography##", $staff['mini_biography'], $email_content);
						$email_content = str_replace("##staff_url##", DOMAIN_NAME_PATH.$staff['friendly_url'], $email_content);
						$email_content = str_replace("##staff_image##", PAGE_IMAGES_URL.$staff['image1'], $email_content);
					}
					
					$counter++;
				}	
				$mail_content=$html_header.$email_content.$html_footer;
				
				//echo $mail_content;
				
				$subscribers = $this->Email->find('list', array('fields'=>'Email.email', 'order'=>array('Email.id'=>'DESC')));
				
				if(count($subscribers)>0){
						
						foreach($subscribers as $subscriber){
							$mail_To = $subscriber;
							$convertedString = bin2hex(Security::cipher(serialize($mail_To),Configure::read('Security.salt')));
							$link = "<a href='".DOMAIN_NAME_PATH.'Emails/delete/'.$convertedString."' target='_blank'>Click here</a>";
							$mail_body = str_replace("##unsubscribe##", $link, $mail_content);
							$mail_body = str_replace("##user##", $mail_To, $mail_body);
							
							//echo $mail_body."<br/>";
							
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
						}
					}
				$this->Session->setFlash(sprintf(__('The email has been sent successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		}
	}

public function admin_monitor() {
		
	$conditions =array('1' =>  'Staff.id IN (SELECT distinct staff_id FROM hh_viewcontacts)');
	$this->Staff->recursive = 0;
	$this->paginate = array(
        'order' => array(
        'Staff.id' => 'desc'
    	),
    	'conditions'=>$conditions
    );
    //$this->paginate=array('conditions'=>$conditions);
	$this->set('staffs', $this->paginate());
}

public function admin_userview() {
		
	$conditions =array('1' =>  '1');
	
	$data=$this->Staff->query("SELECT distinct  s.name,s.image1,s.id as staff_id,u.id as user_id,u.first_name,u.last_name,s.hired_date FROM hh_viewcontacts v INNER JOIN hh_staffs s ON s.id = v.staff_id INNER JOIN hh_users u ON u.id = v.user_id order by u.id desc");
	if(!empty($data))									
	{
		//$this->Staff->recursive = 0;
		//$this->paginate=array('conditions'=>$conditions);
		$this->set('staffs', $data);
	}
	/*
	$conditions =array('1' =>  'Staff.id IN (SELECT distinct staff_id FROM hh_viewcontacts)');
	$this->Staff->recursive = 0;
    $this->paginate=array('conditions'=>$conditions);
	$this->set('staffs', $this->paginate());
	*/
}


public function admin_hired($id=null) {
		
	
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			throw new NotFoundException(__('Invalid staff'));
		}
		$hired_date = date('Y-m-d', strtotime('+3 days'));
		$this->Staff->hired=1;
		$this->Staff->hired_date=$hired_date;
		$this->Staff->query("update hh_staffs set hired=1,hired_date='$hired_date' where id=".$id);
		
		$this->Session->setFlash(sprintf(__('The staff set to Hired!', true), 'Page'), 'default', array('class' => 'success'));
		$this->redirect(array('action' => 'monitor'));

}
public function admin_available($id=null) {
		
	
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			throw new NotFoundException(__('Invalid staff'));
		}
		
		$this->Staff->query("update hh_staffs set hired=0 where id=".$id);
		
		$this->Session->setFlash(sprintf(__('The staff set to Available!', true), 'Page'), 'default', array('class' => 'success'));
		$this->redirect(array('action' => 'monitor'));
		

}
public function admin_viewstaff($id=null) {
		$this->Staff->id = $id;
		$conditions = array('Staff.agent_id'=>$id);
		$this->Staff->recursive = 0;
        $this->paginate=array('conditions'=>$conditions);
		$this->set('staffs', $this->paginate());
	}
public function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') == $date;
}
}