<?php

class LinkedinDetailsController extends AppController {
	var $components = array('LinkedIn.LinkedIn');
	public $uses = array('User', 'Setting', 'Email');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
            'connect',
			'authorize',
			'accept'
        );
	}

	public function accept() {
		// Check if connected to LinkedIn
		if ($this->LinkedIn->isConnected()){
			$person = $this->LinkedIn->call('people/~', [
						'id',
						'picture-url',
						'first-name', 'last-name', 'summary', 'specialties', 'associations',
						'honors', 'interests', 'twitter-accounts',
						'positions' => ['title', 'summary', 'is-current', 'company'],
						'educations',
						'certifications',
						'skills' => ['id', 'skill', 'proficiency', 'years'],
						'recommendations-received',
						'email-address',
						'phone-numbers',
					]);
			$exist_data = $this->User->find('list', array('conditions' => array('User.email' => $person['person']['email-address'])));
			if(empty($exist_data)){
				$user = array();
				$org_password = $this->create_coupon();
				$user['User']['email'] = $person['person']['email-address'];
				$user['User']['password'] = Security::hash(Configure::read('Security.salt') . $org_password);
				$user['User']['first_name'] = $person['person']['first-name'];
				$user['User']['last_name'] = $person['person']['last-name'];
				$user['User']['name'] = $person['person']['first-name'].' '.$person['person']['last-name'];
				$user['User']['user_type'] = 2;
				$user['User']['registration_date'] = date("Y-m-d");
				//$user['User']['dob'] = date("Y-m-d", strtotime($me['birthday']));
				$user['User']['status'] = 1;
				$user['User']['is_verified'] = 1;
				$this->User->create();
				$this->User->save($user);
				$my_user_id = $this->User->getLastInsertId();
				$uId = 'EM'.$this->User->id;
				$idMinLentgth = 5;
				$idLength = strlen($this->User->id);
				$lengtDiff = $idMinLentgth-$idLength;
				if($lengtDiff > 0){
					$midStr = '';
					for($iter = 0; $iter < $lengtDiff; $iter++){
						$midStr.='0';
					}
					$uId = 'EM'.$midStr.$this->User->id;
				}
				$this->User->savefield('emp-uid', $uId);
				/*Mail To User*/
				// $siteSetting = $this->viewVars['settingMain'];
				// $mail_To= $user['User']['email'];
				// $mail_From = $siteSetting['Setting']['noreply_email'];
				// $mail_CC = '';
				// $mail_subject="Registration complete using your facebook account on HouseHelp4Hire";
				// $content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
				// 		 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
				// 		  <tr>
				// 			<td width='77%' align='left' valign='top' bgcolor='#F1F1F2'>
				// 			  <div style='width:97%; height:auto; border:1px solid #041A54; -moz-border:10px; border-radius:10px; background:#ED217C; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
				// 				  <span style='font-size:12px'>Dear ".$user['User']['first_name'].' '.$user['User']['last_name'].",</span><br/>
				// 				  <span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
				// 			  </div>
				// 			  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
				// 				<table width='100%' border='0' cellspacing='5' cellpadding='5'>
				// 				  <tr>
				// 					<td align='left' valign='top'>
				// 					  <p><strong>Your registration had been placed successfully on HH4H. Login details are bellow...</strong></p>
				// 					  <p>Login Email: ".$user['User']['email']."</p>
				// 					  <p>Password: ".$org_password."</p>
				// 					</td>
				// 				  </tr>
				// 				</table>
				// 			  </div>
				// 			  <div style='width:97%; height:auto; border:1px solid #041A54; -moz-border:10px; border-radius:10px; background:#0AB1F0; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
				// 			  <span style='font-size:12px'>Regards Admin [".trim($siteSetting['Setting']['noreply_email'])."],</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
				// 			  </div>
				// 			</td>
				// 		   </tr>
				// 		 </table>
				// 	   </div>"; 
				// $mail_Body = $content; 
				//$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
				$this->User->recursive = 0;
				$userSession = $this->User->find('first', array('conditions' => array('User.email' => $person['person']['email-address'])));
				$this->Session->write('Auth.User.id', $userSession['User']['id']);
				$this->Session->write('Auth.User.user_type', $userSession['User']['user_type']);
				$this->Session->write('Auth.User.first_name', $userSession['User']['first_name']);
				$this->Session->write('Auth.User.last_name', $userSession['User']['last_name']);
				$this->Session->write('Auth.User.email', $userSession['User']['email']);
				$this->Session->write('Auth.User.status', $userSession['User']['status']);
				$this->Session->write('User',$person);
				$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
			}else{
				$userSession = $this->User->find('first', array('conditions' => array('User.email' => $person['person']['email-address'])));
				$this->Session->write('Auth.User.id', $userSession['User']['id']);
				$this->Session->write('Auth.User.user_type', $userSession['User']['user_type']);
				$this->Session->write('Auth.User.first_name', $userSession['User']['first_name']);
				$this->Session->write('Auth.User.last_name', $userSession['User']['last_name']);
				$this->Session->write('Auth.User.email', $userSession['User']['email']);
				$this->Session->write('Auth.User.status', $userSession['User']['status']);
				$this->Session->write('User',$person);
				$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
			}
		}else{
			$this->redirect(array('controller'=>'Users', 'action' => 'home'));
			echo('Not connected');
		}
	}

	// This route will redirect to LinkedIn's login page, collect a request token and
	// then redirect back to the route provided
	public function connect() {
		$this->LinkedIn->connect(['controller'=>'LinkedinDetails', 'action' => 'authorize']);
	}

	// Here we convert the request token into a usable access token and redirect
	public function authorize() {
		$this->LinkedIn->authorize(['controller'=>'LinkedinDetails','action' => 'accept']);
		exit;
	}
}