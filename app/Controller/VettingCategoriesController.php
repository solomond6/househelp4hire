<?php
App::uses('AppController', 'Controller');
/**
 * VettingCategories Controller
 *
 * @property VettingCategory $VettingCategory
 */
class VettingCategoriesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->VettingCategory->recursive = 0;
		$this->set('vettingCategories', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->VettingCategory->id = $id;
		if (!$this->VettingCategory->exists()) {
			throw new NotFoundException(__('Invalid vetting category'));
		}
		$this->set('vettingCategory', $this->VettingCategory->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->VettingCategory->create();
			if ($this->VettingCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting category has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->VettingCategory->id = $id;
		if (!$this->VettingCategory->exists()) {
			throw new NotFoundException(__('Invalid vetting category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VettingCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting category has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting category could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VettingCategory->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->VettingCategory->id = $id;
		if (!$this->VettingCategory->exists()) {
			throw new NotFoundException(__('Invalid vetting category'));
		}
		if ($this->VettingCategory->delete()) {
			$this->Session->setFlash(__('Vetting category deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vetting category was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VettingCategory->recursive = 0;
		$this->set('vettingCategories', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->VettingCategory->id = $id;
		if (!$this->VettingCategory->exists()) {
			throw new NotFoundException(__('Invalid vetting category'));
		}
		$this->set('vettingCategory', $this->VettingCategory->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->VettingCategory->create();
			if ($this->VettingCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting category has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->VettingCategory->id = $id;
		if (!$this->VettingCategory->exists()) {
			throw new NotFoundException(__('Invalid vetting category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VettingCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting category has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting category could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VettingCategory->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->VettingCategory->id = $id;
		if (!$this->VettingCategory->exists()) {
			throw new NotFoundException(__('Invalid vetting category'));
		}
		if ($this->VettingCategory->delete()) {
			$this->Session->setFlash(__('Vetting category deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vetting category was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
