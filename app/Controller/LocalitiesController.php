<?php
App::uses('AppController', 'Controller');
App::uses('Locality', 'Model');
/**
 * Localities Controller
 *
 * @property Locality $Locality
 */
class LocalitiesController extends AppController {

public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
            'fetchlocalitiessidebar' );
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Locality->recursive = 0;
		$this->set('localities', $this->paginate());
	}
	public function admin_index() {
		$this->Locality->recursive = 0;
		$conditions = array();
		if(isset($this->request->data['Locality']['name']) && $this->request->data['Locality']['name'] != null) {
			$conditions = array('Locality.name LIKE'=>'%'.trim($this->request->data['Locality']['name']).'%');
		}
		$this->paginate=array('conditions'=>$conditions, 'order' => array('Locality.name' => 'ASC'));
		$this->set('localities', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Locality->id = $id;
		if (!$this->Locality->exists()) {
			throw new NotFoundException(__('Invalid locality'));
		}
		$this->set('locality', $this->Locality->read(null, $id));
	}
	public function admin_getlocalities() {
		$location=$_POST['location_id'];
		$localities = $this->Locality->find('list',array('conditions'=>array('Locality.location_id'=>$location),'order'=>array('Locality.id'=>'Asc')));
		$html='<option value="">select locality</option>';
		if($localities)
		{
			foreach($localities as $k=>$v)
			{
				$html.='<option value="'.$k.'">'.$v.'</option>';
			}
		}
		echo $html;
		exit;
	}
	public function getlocalities() {
		$location=$_POST['location_id'];
		$localities = $this->Locality->find('list',array('conditions'=>array('Locality.location_id'=>$location),'order'=>array('Locality.id'=>'Asc')));
		$html='<option value="">select locality</option>';
		if($localities)
		{
			foreach($localities as $k=>$v)
			{
				$html.='<option value="'.$k.'">'.$v.'</option>';
			}
		}
		echo $html;
		exit;
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Locality->create();
			if ($this->Locality->save($this->request->data)) {
				$this->Session->setFlash(__('The locality has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The locality could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Locality->Location->find('list');
		$this->set(compact('locations'));
	}
	public function admin_add() {
		if ($this->request->is('post')) {
			if($this->request->data['Locality']['name']!='' && $this->request->data['Locality']['location_id'] != null)
			{
				$this->Locality->create();
				if ($this->Locality->save($this->request->data)) {
					$this->Session->setFlash(sprintf(__('locality has been added successfully.', true), 'User'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The locality could not be saved. Please, try again.'));
				}
			}
			else if($this->request->data['Locality']['name']=='')
			{
				$this->Session->setFlash(__('The locality could not be saved. Please, enter name.'));
			}
		}
		$locations = $this->Locality->Location->find('list');
		$this->set(compact('locations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Locality->id = $id;
		if (!$this->Locality->exists()) {
			throw new NotFoundException(__('Invalid locality'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Locality->save($this->request->data)) {
				$this->Session->setFlash(__('The locality has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The locality could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Locality->read(null, $id);
		}
		$locations = $this->Locality->Location->find('list');
		$this->set(compact('locations'));
	}
	public function admin_edit($id = null) {
		$this->Locality->id = $id;
		if (!$this->Locality->exists()) {
			throw new NotFoundException(__('Invalid locality'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if($this->request->data['Locality']['name']!='' && $this->request->data['Locality']['location_id'] != null)
			{
				$this->Locality->create();
				if ($this->Locality->save($this->request->data)) {
					$this->Session->setFlash(sprintf(__('locality has been updated successfully.', true), 'User'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The locality could not be updated. Please, try again.'));
				}
			}
			else if($this->request->data['Locality']['name']=='')
			{
				$this->Session->setFlash(__('The locality could not be updated. Please, enter name.'));
			}
		} else {
			$this->request->data = $this->Locality->getLocality($id);
		}
		$locations = $this->Locality->Location->find('list');
		$this->set(compact('locations'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Locality->id = $id;
		if (!$this->Locality->exists()) {
			throw new NotFoundException(__('Invalid locality'));
		}
		if ($this->Locality->delete()) {
			$this->Session->setFlash(__('Locality deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Locality was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Locality->id = $id;
		if (!$this->Locality->exists()) {
			throw new NotFoundException(__('Invalid locality'));
		}
		if ($this->Locality->delete()) {
			$this->Session->setFlash(sprintf(__('locality has been deleted successfully.', true), 'User'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Locality was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function fetchlocalitiessidebar() {
		$location=$_POST['locations'];
		$localities = $this->Locality->find('list',array('conditions'=>array('Locality.location_id'=>$location),'order'=>array('Locality.name'=>'Asc')));
		$html='';
		if($localities)
		{
			$html.='<div class="searchoptionshead"><span >Localities</span><img src="'.DOMAIN_NAME_PATH.'img/dropdown_bottom.png" alt="Bedrooms"></div>
			<div class="clear"></div>
			<div style="max-height: 150px;overflow: auto;width: 89%;margin-left: 10px;">';
			foreach($localities as $k=>$v)
			{
				$html.='<p><input name="projctlocality" type="checkbox" value="'.$k.'" id="projctlocality'.$k.'" onclick= "filterlocalities('.$k.')"/>'.$v.'</p>';
			}
			$html.='<div class="clear"></div></div>';
		}
		echo $html;
		exit;
	}
	public function admin_import_export($pass = null) {
		$model = $this->modelClass;
		$this->backup($model);
		$this->set('modelName', $model);
	}
}
