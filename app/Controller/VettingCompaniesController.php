<?php
App::uses('AppController', 'Controller');
/**
 * VettingCompanies Controller
 *
 * @property VettingCompany $VettingCompany
 */
class VettingCompaniesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->VettingCompany->recursive = 0;
		$this->set('vettingCompanies', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->VettingCompany->id = $id;
		if (!$this->VettingCompany->exists()) {
			throw new NotFoundException(__('Invalid vetting company'));
		}
		$this->set('vettingCompany', $this->VettingCompany->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->VettingCompany->create();
			if ($this->VettingCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting company has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting company could not be saved. Please, try again.'));
			}
		}
		$vettingCategories = $this->VettingCompany->VettingCategory->find('list');
		$this->set(compact('vettingCategories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->VettingCompany->id = $id;
		if (!$this->VettingCompany->exists()) {
			throw new NotFoundException(__('Invalid vetting company'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VettingCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting company has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting company could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VettingCompany->read(null, $id);
		}
		$vettingCategories = $this->VettingCompany->VettingCategory->find('list');
		$this->set(compact('vettingCategories'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->VettingCompany->id = $id;
		if (!$this->VettingCompany->exists()) {
			throw new NotFoundException(__('Invalid vetting company'));
		}
		if ($this->VettingCompany->delete()) {
			$this->Session->setFlash(__('Vetting company deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vetting company was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VettingCompany->recursive = 0;
		$this->set('vettingCompanies', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->VettingCompany->id = $id;
		if (!$this->VettingCompany->exists()) {
			throw new NotFoundException(__('Invalid vetting company'));
		}
		$this->set('vettingCompany', $this->VettingCompany->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->VettingCompany->create();
			if ($this->VettingCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting company has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting company could not be saved. Please, try again.'));
			}
		}
		$vettingCategories = $this->VettingCompany->VettingCategory->find('list');
		$this->set(compact('vettingCategories'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->VettingCompany->id = $id;
		if (!$this->VettingCompany->exists()) {
			throw new NotFoundException(__('Invalid vetting company'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VettingCompany->save($this->request->data)) {
				$this->Session->setFlash(__('The vetting company has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetting company could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VettingCompany->read(null, $id);
		}
		$vettingCategories = $this->VettingCompany->VettingCategory->find('list');
		$this->set(compact('vettingCategories'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->VettingCompany->id = $id;
		if (!$this->VettingCompany->exists()) {
			throw new NotFoundException(__('Invalid vetting company'));
		}
		if ($this->VettingCompany->delete()) {
			$this->Session->setFlash(__('Vetting company deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vetting company was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
