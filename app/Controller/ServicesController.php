<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 */
class ServicesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Service->recursive = 0;
		$news = $this->Service->find('all', array('irder'=>array('id'=>'desc')));
		$this->set('service', $service);
		//$this->set('events', $this->paginate());
	}
	
	public function admin_index() {
		$this->Service->recursive = 0;
		$this->set('services', $this->paginate());
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->News->id = $id;
		if (!$this->News->exists()) {
			$this->Session->setFlash(__('Invalid News'));
		}
		$this->set('news', $this->News->read(null, $id));
	}

	public function admin_view($id = null) {
		$this->Service->id = $id;
		if (!$this->Service->exists()) {
			$this->Session->setFlash(__('Invalid Service'));
		}
		$this->set('service', $this->Service->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if(empty($this->request->data['Service']['image']['name']) && empty($this->request->data['Service']['image']['tmp_name'])) {
				$this->request->data['Service']['image'] = null;
			} else {
				if(is_uploaded_file($this->request->data['Service']['image']['tmp_name'])) {
					$p_name = $this->request->data['Service']['image']['name'];
					foreach($this->notAllowedWord AS $arrNAWord){
						$p_name = trim(strtolower(str_replace($arrNAWord, '', $p_name)));
					}
					$p_name = str_replace(' ', '_', $p_name);
					$extention = strrchr($p_name,".");
					$fileName = rand().'_'.$p_name;
					$filePath = NEWS_IMAGES;
					if(in_array($extention, $this->validImageFormats)) {
						if(move_uploaded_file($this->request->data['Service']['image']['tmp_name'], $filePath.$fileName)){
							$this->request->data['Service']['image'] = $fileName;
						}
					} else {
						$this->Session->setFlash('Please upload images only having .jpg, .gif, .png, .jpeg extensions.');
					}
				}
			}
			$this->request->data['Service']['date'] = date('Y-m-d H:i:s');
			$this->Service->create();
			if ($this->Service->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('Service has been saved successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Service could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Service->id = $id;
		if (!$this->Service->exists()) {
			$this->Session->setFlash(__('Invalid Service'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if(is_uploaded_file($this->request->data['Service']['image']['tmp_name'])) 
			{
				$p_name = $this->request->data['Service']['image']['name'];
				foreach($this->notAllowedWord AS $arrNAWord){
					$p_name = trim(strtolower(str_replace($arrNAWord, '', $p_name)));
				}
				$p_name = str_replace(' ', '_', $p_name);
				$extention = strrchr($p_name,".");
				$fileName = rand().'_'.$p_name;
				$filePath = NEWS_IMAGES;
				if(in_array($extention, $this->validImageFormats))
				{
					if(move_uploaded_file($this->request->data['Service']['image']['tmp_name'], $filePath.$fileName)){
						$this->request->data['Service']['image'] = $fileName;
						if($this->request->data['Service']['prev_image']){
							unlink($filePath.$this->request->data['Service']['prev_image']);
						}
					} else {
						$this->request->data['Service']['image'] = $this->request->data['Service']['prev_image'];
					}
				} else {
					$this->Session->setFlash('Please upload images only having .jpg, .gif, .png, .jpeg extensions.');
					$this->request->data['Service']['image'] = $this->request->data['Service']['prev_image'];
				}
			} else {
				$this->request->data['Service']['image'] = $this->request->data['Service']['prev_image'];
			}
			if($this->Service->save($this->request->data)){
				$this->Session->setFlash(sprintf(__('Service has been uploaded successfully.', true)), 'default', array('class' => 'success'));
				$this->redirect(array('action'=>'index'));
			}
		} else {
			$this->request->data = $this->Service->read(null, $id);
		}
	}

	function admin_deactivate($id = null){
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid Service', true)));
			$this->redirect(array('action' => 'index'));
		}
		else{
			$this->Service->id=$id;
			$this->Service->saveField('status','N');
			$this->Session->setFlash(sprintf(__('Service deactivated successfully!', true), 'Service'), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'Service', 'action' => 'index'));
		}
	}

	function admin_activate($id = null)	{
		if (!$id) {
			$this->Session->setFlash(sprintf(__('Invalid Service', true)));
			$this->redirect(array('action' => 'index'));
		}else{
			$this->Service->id=$id;
			$this->Service->saveField('status','Y');	
			$this->Session->setFlash(sprintf(__('Service activated successfully!', true), 'Service'), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'Service', 'action' => 'index'));
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			$this->Session->setFlash(sprintf(__('Invalid Service', true)));
		}
		$this->Service->id = $id;
		if (!$this->Service->exists()) {
			$this->Session->setFlash(sprintf(__('Invalid Service', true)));
		}
		$row = $this->Service->read(null, $id);
		unlink(NEWS_IMAGES.$row['Service']['image']);
		if ($this->Service->delete()) {
			$this->Session->setFlash(sprintf(__('Service deleted successfully!', true), 'Service'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Service could not be deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
