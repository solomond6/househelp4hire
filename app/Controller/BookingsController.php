<?php
App::uses('AppController', 'Controller');
/**
 * Bookings Controller
 *
 * @property Booking $Booking
 */
class BookingsController extends AppController {
public $uses = array('Booking', 'User', 'Staff');
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Booking->recursive = 0;
		$this->paginate = array
		(
			'Booking' => array
			(
				'order' => array
				(
					'Booking.id' =>'desc',
				),
			)
		);
		$this->set('bookings', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$maxBooking = $this->Booking->find('list',array('conditions'=>array('Booking.status'=>'Pending', 'Booking.user_id'=>$this->request->data['Booking']['user_id'])));
			if(count($maxBooking) < 3){
				$alreadyBooked = $this->Booking->find('first',array('conditions'=>array('Booking.status'=>'Pending', 'Booking.user_id'=>$this->request->data['Booking']['user_id'], 'Booking.staff_id'=>$this->request->data['Booking']['staff_id'])));
				if(count($alreadyBooked) == 0){
					$staffAvailability = $this->Booking->find('first',array('conditions'=>array('Booking.status'=>'Pending', 'Booking.staff_id'=>$this->request->data['Booking']['staff_id'], 'Booking.date'=>$this->request->data['Booking']['date'])));
					if(count($staffAvailability) == 0){
						$interviewerAvailability = $this->Booking->find('first',array('conditions'=>array('Booking.status'=>'Pending', 'Booking.user_id'=>$this->request->data['Booking']['user_id'], 'Booking.date'=>$this->request->data['Booking']['date'])));
						//if(count($interviewerAvailability) == 0){
							$this->request->data['Booking']['start_time'] = date('H:i:s', strtotime($this->request->data['Booking']['start_time']));
							$this->Booking->create();
							if ($this->Booking->save($this->request->data)) {
								$admin = $this->Setting->read(null,1);
								$mail_To= $this->$admin['Setting']['contact_email'];
								$mail_From = $this->Session->read('Auth.User.email');
								$mail_CC = '';
								$mail_subject="HouseHelp4Hire New Booking";
								$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
										 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
										 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
										  <tr>
											<td width='77%' align='left' valign='top' bgcolor=''>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Dear Admin,</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
											  </div>
											  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
												<table width='100%' border='0' cellspacing='5' cellpadding='5'>
												  <tr>
													<td align='left' valign='top'>
													  <p><strong>New Booking details are below...</strong></p>
													  <p>
														 Interviewer:&nbsp;&nbsp;".$this->Session->read('Auth.User.name')."<br/>
														 Interviewer's Unique ID:&nbsp;&nbsp;".$this->Session->read('Auth.User.emp-uid')."<br/>
														 Interviewee:&nbsp;&nbsp;".$this->request->data['Booking']['staff_name']."<br/>
														 Interviewee's Unique ID:&nbsp;&nbsp;".$this->request->data['Booking']['staff-uid']."<br/>
														 Category:&nbsp;&nbsp;".$this->request->data['Booking']['staff_category']."<br/>
														 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($this->request->data['Booking']['date']))." ".date("g:i a",strtotime($this->request->data['Booking']['start_time']))."<br/>
														 Location:&nbsp;&nbsp;".$this->request->data['Booking']['location']."<br/>
														 </p>
													</td>
												  </tr>
												</table>
											  </div>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
											  </div>
											</td>
										   </tr>
										 </table>
										 <p style='font-size:11px;text-align:center;'>
											<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
											<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
										 </p>
										 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
									   </div>";
								$mail_Body = $content; 
								$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
								$mail_To = $this->Session->read('Auth.User.email');
								$mail_From = $admin['Setting']['noreply_email'];
								$mail_CC = '';
								$mail_subject="HouseHelp4Hire New Booking";
								$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
										 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
										 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
										  <tr>
											<td width='77%' align='left' valign='top' bgcolor=''>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Dear ".$this->Session->read('Auth.User.name').",</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
											  </div>
											  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
												<table width='100%' border='0' cellspacing='5' cellpadding='5'>
												  <tr>
													<td align='left' valign='top'>
													  <p><strong>Your Booking details are below...</strong></p>
													  <p>
														 Interviewer:&nbsp;&nbsp;".$this->Session->read('Auth.User.name')."<br/>
														 Interviewer's Unique ID:&nbsp;&nbsp;".$this->Session->read('Auth.User.emp-uid')."<br/>
														 Interviewee:&nbsp;&nbsp;".$this->request->data['Booking']['staff_name']."<br/>
														 Interviewee's Unique ID:&nbsp;&nbsp;".$this->request->data['Booking']['staff-uid']."<br/>
														 Category:&nbsp;&nbsp;".$this->request->data['Booking']['staff_category']."<br/>
														 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($this->request->data['Booking']['date']))." ".date("g:i a",strtotime($this->request->data['Booking']['start_time']))."<br/>
														 Location:&nbsp;&nbsp;".$this->request->data['Booking']['location']."<br/>
														 <p>Please note there is a 24hr window with which you can cancel this booking, <a href='".DOMAIN_NAME_PATH."Users/dashboard'>click here</a> to cancel.</p>
													</td>
												  </tr>
												</table>
											  </div>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
											  </div>
											</td>
										   </tr>
										 </table>
										 <p style='font-size:11px;text-align:center;'>
											<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
											<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
										 </p>
										 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
									   </div>"; 
								$mail_Body = $content; 
								$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
								$mail_To = $this->request->data['Booking']['staff_email'];
								$mail_From = $admin['Setting']['noreply_email'];
								$mail_CC = '';
								$mail_subject="HouseHelp4Hire New Booking";
								$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
										 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
										 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
										  <tr>
											<td width='77%' align='left' valign='top' bgcolor=''>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Dear ".$this->request->data['Booking']['staff_name'].",</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
											  </div>
											  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
												<table width='100%' border='0' cellspacing='5' cellpadding='5'>
												  <tr>
													<td align='left' valign='top'>
													  <p><strong>Your Booking details are below...</strong></p>
													  <p>
														 Interviewer:&nbsp;&nbsp;".$this->Session->read('Auth.User.name')."<br/>
														 Interviewer's Unique ID:&nbsp;&nbsp;".$this->Session->read('Auth.User.emp-uid')."<br/>
														 Interviewee:&nbsp;&nbsp;".$this->request->data['Booking']['staff_name']."<br/>
														 Interviewee's Unique ID:&nbsp;&nbsp;".$this->request->data['Booking']['staff-uid']."<br/>
														 Category:&nbsp;&nbsp;".$this->request->data['Booking']['staff_category']."<br/>
														 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($this->request->data['Booking']['date']))." ".date("g:i a",strtotime($this->request->data['Booking']['start_time']))."<br/>
														 Location:&nbsp;&nbsp;".$this->request->data['Booking']['location']."<br/>
														 <p>We look forward to a successful and profitable working relationship.</p>
													</td>
												  </tr>
												</table>
											  </div>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
											  </div>
											</td>
										   </tr>
										 </table>
										 <p style='font-size:11px;text-align:center;'>
											<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
											<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
										 </p>
										 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
									   </div>"; 
								$mail_Body = $content; 
								$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
								$this->Session->setFlash(sprintf(__('Great News! A confirmation of this booking shall be sent to your email. ', true), 'Booking'), 'default', array('class' => 'success'));
							} else {
								$this->Session->setFlash(__('Your interview booking for this staff could not be saved. Please, try again.'));
							}
						//} else {
						//	$this->Session->setFlash(__('You can take only one interview in a single day.'));
						//}
					} else {
						$this->Session->setFlash(__('This staff is not available for booking on this date.'));
					}
				} else {
					$this->Session->setFlash(__('You already booked this staff for interview.'));
				}
			} else {
				$this->Session->setFlash(__('You can only have a maximum number of three staff booked at anytime. You can cancel bookings from your account within 24hrs, to make room for additional interviews.'));
			}
			$this->redirect(DOMAIN_NAME_PATH.$this->request->data['Booking']['friendly_url']);
		} else {
			$this->Session->setFlash(__('invalid Access'));
			$this->redirect(array('controller'=>'Staffs', 'action' => 'index'));
		}
		/*<a href='".$admin['Setting']['pinterest_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/p.png' alt='pinterest' height='48' /></a>
		<a href='mailto: ".$admin['Setting']['contact_email']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/e.png' alt='email' height='48' /></a>
		<a href='".$admin['Setting']['rss_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/r.png' alt='rss' height='48' /></a>*/
	}
	public function admin_add() {
		if ($this->request->is('post')) {
			$exist_data = $this->User->find('first', array('conditions' => array('User.id' =>$this->request->data['Booking']['user_id'] )));
			if($exist_data['User']['interview_booked']>=3)
			{
				$this->Session->setFlash(__('booking failed.This employer already has the maximum no. of booking.'));
				$this->redirect(array('action' => 'index'));
			}
			$check_schedule = $this->Booking->find('first', array('conditions' => array('Booking.staff_id' =>$this->request->data['Booking']['staff_id'] ,'Booking.date' =>$this->request->data['Booking']['date'])));
			if($check_schedule)
			{
				$this->Session->setFlash(__('booking failed.This Stuff has been booked already for this date.'));
				$this->redirect(array('action' => 'index'));
			}
			$this->Booking->create();
			if ($this->Booking->save($this->request->data)) {
				$this->User->id = $exist_data['User']['id'];
				$totalbooked=$exist_data['User']['interview_booked']+1;
				$this->User->saveField('interview_booked', $totalbooked);
				$this->Session->setFlash(sprintf(__('The booking has been completed successfully!', true), 'User'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The booking could not be saved. Please, try again.'));
			}
		}
		$staffs = $this->Booking->Staff->find('list',array('conditions'=>array('Staff.status'=>1)));
		$users = $this->Booking->User->find('list',array('conditions'=>array('User.user_type'=>2,'User.status'=>1,'User.interview_booked <'=>3)));
		$this->set(compact('staffs', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function feedback($id = null) {
		$this->Booking->id = $this->request->data['Booking']['id'];
		if (!$this->Booking->exists()) {
			$this->Session->setFlash(__('Invalid booking.'));
			$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
		}
		if ($this->Booking->save($this->request->data)) {
			$this->Session->setFlash(sprintf(__('The feedback sent successfully!', true), 'Booking'), 'default', array('class' => 'success'));
			$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
		} else {
			$this->Session->setFlash(__('The feedback could not be sent. Please, try again.'));
			$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
		}
		exit;
	}
	public function status($id = null) {
		$this->Booking->id = $id;
		if (!$this->Booking->exists()) {
			$this->Session->setFlash(__('Invalid Booking!'));
			$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
		}
		$booked = $this->Booking->find('first', array('fields'=>array('Booking.staff_id', 'Booking.user_id', 'Booking.location', 'Booking.date', 'Booking.start_time', 'Staff.name', 'Staff.staff-uid', 'Staff.email', 'User.name', 'User.emp-uid'), 'conditions'=>array('Booking.id'=>$id)));
		if ($this->Booking->save($this->request->data)) {
			$admin = $this->Setting->read(null,1);
			$mail_To = $admin['Setting']['contact_email'];
			$mail_From = $this->Session->read('Auth.User.email');
			$mail_CC = '';
			$mail_subject = $this->request->data['Booking']['status']." HouseHelp4Hire Booking";
			$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
					 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
					 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
					  <tr>
						<td width='77%' align='left' valign='top' bgcolor=''>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Dear Admin,</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
						  </div>
						  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
							<table width='100%' border='0' cellspacing='5' cellpadding='5'>
							  <tr>
								<td align='left' valign='top'>
								  <p><strong>".$this->request->data['Booking']['status']." details are below...</strong></p>
								  <p>
									 Interviewer:&nbsp;&nbsp;".$booked['User']['name']."<br/>
									 Interviewer's Unique ID:&nbsp;&nbsp;".$booked['User']['emp-uid']."<br/>
									 Interviewee:&nbsp;&nbsp;".$booked['Staff']['name']."<br/>
									 Interviewee's Unique ID:&nbsp;&nbsp;".$booked['Staff']['staff-uid']."<br/>
									 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($booked['Booking']['date']))." ".date("g:i a",strtotime($booked['Booking']['start_time']))."<br/>
									 Location:&nbsp;&nbsp;".$booked['Booking']['location']."<br/>
									</p>
								</td>
							  </tr>
							</table>
						  </div>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
						  </div>
						</td>
					   </tr>
					 </table>
					 <p style='font-size:11px;text-align:center;'>
						<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
						<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
					 </p>
					 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
				   </div>";
			$mail_Body = $content; 
			$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
			$mail_To = $this->Session->read('Auth.User.email');
			$mail_From = $admin['Setting']['noreply_email'];
			$mail_CC = '';
			$mail_subject = $this->request->data['Booking']['status']." HouseHelp4Hire Booking";
			$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
					 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
					 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
					  <tr>
						<td width='77%' align='left' valign='top' bgcolor=''>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Dear ".$this->Session->read('Auth.User.name').",</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
						  </div>
						  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
							<table width='100%' border='0' cellspacing='5' cellpadding='5'>
							  <tr>
								<td align='left' valign='top'>
								  <p><strong>".$this->request->data['Booking']['status']." details are below...</strong></p>
								  <p>
									 Interviewer:&nbsp;&nbsp;".$booked['User']['name']."<br/>
									 Interviewer's Unique ID:&nbsp;&nbsp;".$booked['User']['emp-uid']."<br/>
									 Interviewee:&nbsp;&nbsp;".$booked['Staff']['name']."<br/>
									 Interviewee's Unique ID:&nbsp;&nbsp;".$booked['Staff']['staff-uid']."<br/>
									 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($booked['Booking']['date']))." ".date("g:i a",strtotime($booked['Booking']['start_time']))."<br/>
									 Location:&nbsp;&nbsp;".$booked['Booking']['location']."<br/>
								</td>
							  </tr>
							</table>
						  </div>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
						  </div>
						</td>
					   </tr>
					 </table>
					 <p style='font-size:11px;text-align:center;'>
						<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
						<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
					 </p>
					 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
				   </div>"; 
			$mail_Body = $content; 
			$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
			$mail_To = $booked['Staff']['email'];
			$mail_From = $admin['Setting']['noreply_email'];
			$mail_CC = '';
			$mail_subject = $this->request->data['Booking']['status']." HouseHelp4Hire Booking";
			$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
					 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
					 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
					  <tr>
						<td width='77%' align='left' valign='top' bgcolor=''>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Dear ".$booked['Staff']['name'].",</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
						  </div>
						  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
							<table width='100%' border='0' cellspacing='5' cellpadding='5'>
							  <tr>
								<td align='left' valign='top'>
								  <p><strong>".$this->request->data['Booking']['status']." details are below...</strong></p>
								  <p>
									 Interviewer:&nbsp;&nbsp;".$booked['User']['name']."<br/>
									 Interviewer's Unique ID:&nbsp;&nbsp;".$booked['User']['emp-uid']."<br/>
									 Interviewee:&nbsp;&nbsp;".$booked['Staff']['name']."<br/>
									 Interviewee's Unique ID:&nbsp;&nbsp;".$booked['Staff']['staff-uid']."<br/>
									 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($booked['Booking']['date']))." ".date("g:i a",strtotime($booked['Booking']['start_time']))."<br/>
									 Location:&nbsp;&nbsp;".$booked['Booking']['location']."<br/>
								</td>
							  </tr>
							</table>
						  </div>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
						  </div>
						</td>
					   </tr>
					 </table>
					 <p style='font-size:11px;text-align:center;'>
						<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
						<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
					 </p>
					 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
				   </div>"; 
			$mail_Body = $content; 
			$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
			$this->Session->setFlash(sprintf(__('Thanks! '.$booked['Staff']['name'].' has been '.strtolower($this->request->data['Booking']['status']).'.', true), 'Booking'), 'default', array('class' => 'success'));
			$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
		} else {
			$this->Session->setFlash(__('The booking status could not be changed. Please, try again.'));
			$this->redirect(array('controller'=>'Users', 'action' => 'dashboard'));
		}
		exit;
	}
	public function cancel($id = null) {
		$this->Booking->id = $id;
		if (!$this->Booking->exists()) {
			$this->Session->setFlash(__('Invalid Booking!'));
		}
		$booked = $this->Booking->find('first', array('fields'=>array('Booking.staff_id', 'Booking.user_id', 'Booking.location', 'Booking.date', 'Booking.start_time', 'Staff.name', 'Staff.staff-uid', 'Staff.email', 'User.name', 'User.emp-uid'), 'conditions'=>array('Booking.id'=>$id)));
		if ($this->Booking->save($this->request->data)) {
			$admin = $this->Setting->read(null,1);
			$mail_To = $admin['Setting']['contact_email'];
			$mail_From = $this->Session->read('Auth.User.email');
			$mail_CC = '';
			$mail_subject = $this->request->data['Booking']['status']." HouseHelp4Hire Booking";
			$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
					 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
					 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
					  <tr>
						<td width='77%' align='left' valign='top' bgcolor=''>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Dear Admin,</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
						  </div>
						  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
							<table width='100%' border='0' cellspacing='5' cellpadding='5'>
							  <tr>
								<td align='left' valign='top'>
								  <p><strong>".$this->request->data['Booking']['status']." details are below...</strong></p>
								  <p>
									 Interviewer:&nbsp;&nbsp;".$booked['User']['name']."<br/>
									 Interviewer's Unique ID:&nbsp;&nbsp;".$booked['User']['emp-uid']."<br/>
									 Interviewee:&nbsp;&nbsp;".$booked['Staff']['name']."<br/>
									 Interviewee's Unique ID:&nbsp;&nbsp;".$booked['Staff']['staff-uid']."<br/>
									 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($booked['Booking']['date']))." ".date("g:i a",strtotime($booked['Booking']['start_time']))."<br/>
									 Location:&nbsp;&nbsp;".$booked['Booking']['location']."<br/>
									</p>
								</td>
							  </tr>
							</table>
						  </div>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
						  </div>
						</td>
					   </tr>
					 </table>
					 <p style='font-size:11px;text-align:center;'>
						<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
						<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
					 </p>
					 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
				   </div>";
			$mail_Body = $content; 
			$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
			$mail_To = $this->Session->read('Auth.User.email');
			$mail_From = $admin['Setting']['noreply_email'];
			$mail_CC = '';
			$mail_subject = $this->request->data['Booking']['status']." HouseHelp4Hire Booking";
			$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
					 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
					 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
					  <tr>
						<td width='77%' align='left' valign='top' bgcolor=''>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Dear ".$this->Session->read('Auth.User.name').",</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
						  </div>
						  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
							<table width='100%' border='0' cellspacing='5' cellpadding='5'>
							  <tr>
								<td align='left' valign='top'>
								  <p><strong>".$this->request->data['Booking']['status']." details are below...</strong></p>
								  <p>
									 Interviewer:&nbsp;&nbsp;".$booked['User']['name']."<br/>
									 Interviewer's Unique ID:&nbsp;&nbsp;".$booked['User']['emp-uid']."<br/>
									 Interviewee:&nbsp;&nbsp;".$booked['Staff']['name']."<br/>
									 Interviewee's Unique ID:&nbsp;&nbsp;".$booked['Staff']['staff-uid']."<br/>
									 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($booked['Booking']['date']))." ".date("g:i a",strtotime($booked['Booking']['start_time']))."<br/>
									 Location:&nbsp;&nbsp;".$booked['Booking']['location']."<br/>
								</td>
							  </tr>
							</table>
						  </div>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
						  </div>
						</td>
					   </tr>
					 </table>
					 <p style='font-size:11px;text-align:center;'>
						<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
						<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
					 </p>
					 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
				   </div>"; 
			$mail_Body = $content; 
			$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
			$mail_To = $booked['Staff']['email'];
			$mail_From = $admin['Setting']['noreply_email'];
			$mail_CC = '';
			$mail_subject = $this->request->data['Booking']['status']." HouseHelp4Hire Booking";
			$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
					 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
					 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
					  <tr>
						<td width='77%' align='left' valign='top' bgcolor=''>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Dear ".$booked['Staff']['name'].",</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
						  </div>
						  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
							<table width='100%' border='0' cellspacing='5' cellpadding='5'>
							  <tr>
								<td align='left' valign='top'>
								  <p><strong>".$this->request->data['Booking']['status']." details are below...</strong></p>
								  <p>
									 Interviewer:&nbsp;&nbsp;".$booked['User']['name']."<br/>
									 Interviewer's Unique ID:&nbsp;&nbsp;".$booked['User']['emp-uid']."<br/>
									 Interviewee:&nbsp;&nbsp;".$booked['Staff']['name']."<br/>
									 Interviewee's Unique ID:&nbsp;&nbsp;".$booked['Staff']['staff-uid']."<br/>
									 Date & Time:&nbsp;&nbsp;".date("F j, Y", strtotime($booked['Booking']['date']))." ".date("g:i a",strtotime($booked['Booking']['start_time']))."<br/>
									 Location:&nbsp;&nbsp;".$booked['Booking']['location']."<br/>
								</td>
							  </tr>
							</table>
						  </div>
						  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
						  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
						  </div>
						</td>
					   </tr>
					 </table>
					 <p style='font-size:11px;text-align:center;'>
						<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
						<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
					 </p>
					 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
				   </div>"; 
			$mail_Body = $content; 
			$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
			$this->Session->setFlash(sprintf(__('Thanks! '.$booked['Staff']['name'].' has been '.strtolower($this->request->data['Booking']['status']).'.', true), 'Booking'), 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The booking status could not be cancelled. Please, try again.'));
		}
		$this->redirect(DOMAIN_NAME_PATH.$this->request->data['Booking']['friendly_url']);
		exit;
	}

	public function admin_edit($id = null) {
		$this->Booking->id = $id;
		if (!$this->Booking->exists()) {
			throw new NotFoundException(__('Invalid Booking!'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$check_schedule = $this->Booking->find('first', array('conditions' => array('Booking.staff_id' =>$this->request->data['Booking']['staff_id'] ,'Booking.date' =>$this->request->data['Booking']['date'],'Booking.id !=' =>$this->request->data['Booking']['id'])));
			if($check_schedule)
			{
				$this->Session->setFlash(__('booking failed.This Stuff has been booked already for this date.'));
				$this->redirect(array('action' => 'edit',$id));
			}
			$check_schedule = $this->Booking->find('first', array('conditions' => array('Booking.staff_id' => $this->request->data['Booking']['staff_id'], 'Booking.date' => $this->request->data['Booking']['date'], 'Booking.id !=' => $this->request->data['Booking']['id'])));
			if($check_schedule)
			{
				$this->Session->setFlash(__('booking failed.This Stuff has been booked already for this date.'));
				$this->redirect(array('action' => 'edit',$id));
			}
			$check_selected = $this->Booking->find('first', array('conditions' => array('Booking.staff_id' => $this->request->data['Booking']['staff_id'], 'Booking.status' => 'Selected', 'Booking.id !=' => $this->request->data['Booking']['id'])));
			if($check_selected){
				$this->Session->setFlash(__('Failed to update. This Stuff has been already selected by another employer.'));
				$this->redirect(array('action' => 'edit', $id));
			}
			$check_hired = $this->Booking->find('first', array('conditions' => array('Booking.staff_id' => $this->request->data['Booking']['staff_id'], 'Booking.status' => 'Hired', 'Booking.id !=' => $this->request->data['Booking']['id'])));
			if($check_hired){
				$this->Session->setFlash(__('Failed to update. This Stuff has been already selected by another employer.'));
				$this->redirect(array('action' => 'edit', $id));
			}
			if ($this->Booking->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The booking has been updated successfully!', true), 'User'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The booking could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Booking->read(null, $id);
		}
		$staffs = $this->Booking->Staff->find('list',array('conditions'=>array('Staff.status'=>1)));
		$users = $this->Booking->User->find('list',array('conditions'=>array('User.status'=>1)));
		//$staffs = $this->Booking->find('list',array('conditions'=>array('Booking.status'=>'Pending')));
		$this->set(compact('staffs', 'users', 'statusArr'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Booking->id = $id;
		if (!$this->Booking->exists()) {
			throw new NotFoundException(__('Invalid Booking!'));
		}
		if ($this->Booking->delete()) {
			$this->Session->setFlash(__('Booking deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Booking was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Booking->id = $id;
		if (!$this->Booking->exists()) {
			throw new NotFoundException(__('Invalid Booking!'));
		}
		$check_schedule = $this->Booking->find('first', array('conditions' => array('Booking.id' =>$id)));
		$exist_data = $this->User->find('first', array('fields'=>array('User.id','User.interview_booked'),'conditions' => array('User.id' =>$check_schedule['Booking']['user_id'] )));
		if ($this->Booking->delete()) {
			$this->User->id = $exist_data['User']['id'];
			$totalbooked=$exist_data['User']['interview_booked']-1;
			$this->User->saveField('interview_booked', $totalbooked);
			$this->Session->setFlash(sprintf(__('The booking has been deleted successfully!', true), 'User'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Booking was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
