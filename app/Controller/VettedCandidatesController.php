<?php
App::uses('AppController', 'Controller');
/**
 * VettedCandidates Controller
 *
 * @property VettedCandidate $VettedCandidate
 */
class VettedCandidatesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->VettedCandidate->recursive = 0;
		$this->set('vettedCandidates', $this->paginate());
	}

	public function vet_index() {
		$this->VettedCandidate->recursive = 0;
		$this->paginate = array(
			'order' => array('id' => 'desc'),
			'conditions' => array('vetting_company_id'=>$this->Session->read('vet_com_id'), 'payment_status'=>'1')
		);
		$this->set('vettedCandidates', $this->paginate());
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->VettedCandidate->id = $id;
		if (!$this->VettedCandidate->exists()) {
			throw new NotFoundException(__('Invalid vetted candidate'));
		}
		$this->set('vettedCandidate', $this->VettedCandidate->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->VettedCandidate->create();
			if ($this->VettedCandidate->save($this->request->data)) {
				$this->Session->setFlash(__('The vetted candidate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetted candidate could not be saved. Please, try again.'));
			}
		}
		$vettingCategories = $this->VettedCandidate->VettingCategory->find('list');
		$vettingCompanies = $this->VettedCandidate->VettingCompany->find('list');
		$staffs = $this->VettedCandidate->Staff->find('list');
		$this->set(compact('vettingCategories', 'vettingCompanies', 'staffs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->VettedCandidate->id = $id;
		if (!$this->VettedCandidate->exists()) {
			throw new NotFoundException(__('Invalid vetted candidate'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VettedCandidate->save($this->request->data)) {
				$this->Session->setFlash(__('The vetted candidate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetted candidate could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VettedCandidate->read(null, $id);
		}
		$vettingCategories = $this->VettedCandidate->VettingCategory->find('list');
		$vettingCompanies = $this->VettedCandidate->VettingCompany->find('list');
		$staffs = $this->VettedCandidate->Staff->find('list');
		$this->set(compact('vettingCategories', 'vettingCompanies', 'staffs'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->VettedCandidate->id = $id;
		if (!$this->VettedCandidate->exists()) {
			throw new NotFoundException(__('Invalid vetted candidate'));
		}
		if ($this->VettedCandidate->delete()) {
			$this->Session->setFlash(__('Vetted candidate deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vetted candidate was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VettedCandidate->recursive = 0;
		$this->paginate=array('conditions'=>array('payment_status'=>'1'), 'order'=>array('id'=>'desc'));
		$this->set('vettedCandidates', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->VettedCandidate->id = $id;
		if (!$this->VettedCandidate->exists()) {
			throw new NotFoundException(__('Invalid vetted candidate'));
		}
		$this->set('vettedCandidate', $this->VettedCandidate->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->VettedCandidate->create();
			if ($this->VettedCandidate->save($this->request->data)) {
				$this->Session->setFlash(__('The vetted candidate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetted candidate could not be saved. Please, try again.'));
			}
		}
		$vettingCategories = $this->VettedCandidate->VettingCategory->find('list');
		$vettingCompanies = $this->VettedCandidate->VettingCompany->find('list');
		$staffs = $this->VettedCandidate->Staff->find('list');
		$this->set(compact('vettingCategories', 'vettingCompanies', 'staffs'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->VettedCandidate->id = $id;
		if (!$this->VettedCandidate->exists()) {
			throw new NotFoundException(__('Invalid vetted candidate'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VettedCandidate->save($this->request->data)) {
				$this->Session->setFlash(__('The vetted candidate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetted candidate could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VettedCandidate->read(null, $id);
		}
		$vettingCategories = $this->VettedCandidate->VettingCategory->find('list');
		$vettingCompanies = $this->VettedCandidate->VettingCompany->find('list');
		$staffs = $this->VettedCandidate->Staff->find('list');
		$this->set(compact('vettingCategories', 'vettingCompanies', 'staffs'));
	}

	public function vet_edit($id = null) {
		$this->VettedCandidate->id = $id;
		if (!$this->VettedCandidate->exists()) {
			throw new NotFoundException(__('Invalid vetted candidate'));
		}
		if ($this->request->is('post') || $this->request->is('put')){
			if ($this->VettedCandidate->save($this->request->data)){
				if($this->request->data['VettedCandidate']['status'] =='0'){
					$status = '<button style="color:#fff; padding: 10px; background:red;">Pending</button>';
				}elseif($this->request->data['VettedCandidate']['status'] =='1'){
					$status = '<button style="color:#fff; padding: 10px; background:green;">Approved</button>';
				}elseif($this->request->data['VettedCandidate']['status'] =='2'){
					$status = '<button style="color:#fff; padding: 10px; background:red;">Declined</button>';
				}
				$staffDetails = $this->VettedCandidate->Staff->find('first', array('conditions' => array('staff.id'=>$this->request->data['VettedCandidate']['staff_id'])));
				$this->loadModel('Setting');
					$this->Setting->recursive = 0;
					$settingMain = $this->Setting->read(null, 1);
					/*for mail*/
						$mail_To= $staffDetails['Staff']['email'];
						$mail_From = $settingMain['Setting']['noreply_email'];
						$mail_CC = '';
						$mail_subject="Verification Update";
						$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
										 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$settingMain['Setting']['site_logo']."' alt='HH4H'></p>
										 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
										  <tr>
											<td width='77%' align='left' valign='top' bgcolor=''>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Dear Admin</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
											  </div>
											  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
												<table width='100%' border='0' cellspacing='5' cellpadding='5'>
												  <tr>
													<td align='left' valign='top'>
													  <p><strong>Hello ".$staffDetails['Staff']['name'].",</strong></p>
													  <p>Your request for verification has been review and below is the status of your verification.</p>
													  <p>
													   Status : ".$status."<br/>
													   Comments : ".$this->request->data['VettedCandidate']['comments']."<br/>
													  </p>
													</td>
												  </tr>
												</table>
											  </div>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
											  </div>
											</td>
										   </tr>
										 </table>
										 <p style='font-size:11px;text-align:center;'>
											<a href='".$settingMain['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
											<a href='".$settingMain['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
										 </p>
										 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
									   </div>"; 
					$mail_Body = $content;
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
				
				$this->Session->setFlash(__('The vetted candidate has been updated'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vetted candidate could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VettedCandidate->read(null, $id);
		}
		$vettingCategories = $this->VettedCandidate->VettingCategory->find('list');
		$vettingCompanies = $this->VettedCandidate->VettingCompany->find('list');
		$staffs = $this->VettedCandidate->Staff->find('list');
		$this->set(compact('vettingCategories', 'vettingCompanies', 'staffs'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->VettedCandidate->id = $id;
		if (!$this->VettedCandidate->exists()) {
			throw new NotFoundException(__('Invalid vetted candidate'));
		}
		if ($this->VettedCandidate->delete()) {
			$this->Session->setFlash(__('Vetted candidate deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vetted candidate was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
