<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 */
class ProductsController extends AppController {
	var $uses = array('Product', 'Category');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Product->recursive = 0;
		$this->set('products', $this->paginate());
	}
	public function admin_index() {
		$this->Product->recursive = 0;
		$this->set('products', $this->paginate());
		$categories = $this->Product->Category->find('list', array('conditions'=>array('Category.parent !='=>0)));
		$this->set(compact('categories'));
	}
	public function admin_view($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$product = $this->Product->read(null, $id);
		$categories = $this->Product->Category->find('list', array('conditions'=>array('Category.parent !='=>0)));
		$this->set(compact('categories', 'product'));
	}

	public function view($id = null) {
		$this->Product->id = $id;
		if (!$id) {
			$this->Session->setFlash(__('Invalid product.', true));
			$this->redirect(array('controller' => 'users','action' => 'index'));
		}
		if (!$this->Product->exists()) {
			$this->Session->setFlash(__('Invalid product'));
			$this->redirect(array('controller' => 'users','action' => 'index'));
		}
		$product = $this->Product->read(null, $id);
		$this->set('product', $product);
		$this->Category->recursive = 0;
		$parentcategory= $this->Category->read(null, $product['Category']['parent']);
		$this->Category->recursive = 1;
		$allcategory=$this->Category->find('all', array('conditions'=>array('Category.parent'=>$product['Category']['parent'],'Category.status'=>'Y')));
		$this->set(compact('category','allcategory','parentcategory'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
			}
		}
		$categories = $this->Product->Category->find('list');
		$subcategories = $this->Product->Subcategory->find('list');
		$users = $this->Product->User->find('list');
		$this->set(compact('categories', 'subcategories', 'users'));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$fileerr=true;
			if($this->request->data['Product']['image'])
			{
				if(is_uploaded_file($this->request->data['Product']['image']['tmp_name'])) 
				{
					$p_name = $this->request->data['Product']['image']['name'];
					foreach($this->notAllowedWord AS $arrNAWord)
					{
						$p_name = trim(strtolower(str_replace($arrNAWord, '', $p_name)));
					}
					$p_name = str_replace(' ', '_', $p_name);
					$extention = strrchr($p_name,".");
					$fileName = rand().'_'.$p_name;
					$filePath = BANNER_IMAGES;
					if(in_array($extention, $this->validImageFormats))
					{
						if(move_uploaded_file($this->request->data['Product']['image']['tmp_name'], $filePath.$fileName)){
							$this->request->data['Product']['image'] = $fileName;
						} else {
							$this->Session->setFlash('Product uploading failed. Please try again.');
							$fileerr=false;
						}
					} else {
						$this->Session->setFlash('Please upload images only having .jpg, .gif, .png, .jpeg extensions.');
						$fileerr=false;
					}
				}
				else
				{
					unset($this->request->data['Product']['image']);
				}
			}
			else
			{
				unset($this->request->data['Product']['image']);
			}
			if($fileerr)
			{
				$this->Product->create();
				if ($this->Product->save($this->data)) {
					$this->Session->setFlash(sprintf(__('The Product has been saved', true)), 'default', array('class'=>'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Product could not be saved. Please, try again.', true));
				}
			}
		}
		$categories = $this->Product->Category->find('list', array('conditions'=>array('Category.parent !='=>0)));
		$this->set(compact('categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('Product has been added successfully.', true), 'Product'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Product->read(null, $id);
		}
		$categories = $this->Product->Category->find('list');
		$subcategories = $this->Product->Subcategory->find('list');
		$users = $this->Product->User->find('list');
		$this->set(compact('categories', 'subcategories', 'users'));
	}

	public function admin_edit($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			$this->Session->setFlash(__('Invalid product'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			$fileerr=true;
			$prevdata = $this->Product->read(null, $id);
			if($this->request->data['Product']['image'])
			{
				if(is_uploaded_file($this->request->data['Product']['image']['tmp_name'])) 
				{
					$p_name = $this->request->data['Product']['image']['name'];
					foreach($this->notAllowedWord AS $arrNAWord)
					{
						$p_name = trim(strtolower(str_replace($arrNAWord, '', $p_name)));
					}
					$p_name = str_replace(' ', '_', $p_name);
					$extention = strrchr($p_name,".");
					$fileName = rand().'_'.$p_name;
					$filePath = BANNER_IMAGES;
					if(in_array($extention, $this->validImageFormats))
					{
						if(move_uploaded_file($this->request->data['Product']['image']['tmp_name'], $filePath.$fileName)){
							$this->request->data['Product']['image'] = $fileName;
						} else {
							$this->Session->setFlash('Product uploading failed. Please try again.');
							$fileerr=false;
						}
					} else {
						$this->Session->setFlash('Please upload images only having .jpg, .gif, .png, .jpeg extensions.');
						$fileerr=false;
					}
				}
				else
				{
					unset($this->request->data['Product']['image']);
				}
			}
			else
			{
				unset($this->request->data['Product']['image']);
			}
			if($fileerr)
			{
				if ($this->Product->save($this->request->data)) {
					if($this->request->data['Product']['image'])
					{
						if($prevdata['Product']['image'])
						{
							unlink(BANNER_IMAGES.$prevdata['Product']['image']);
						}
					}
					$this->Session->setFlash(sprintf(__('The Product has been saved', true)), 'default', array('class'=>'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Product could not be saved. Please, try again.', true));
				}
			}
		} 
		else
		{
			$this->request->data = $this->Product->read(null, $id);
		}
		$categories = $this->Product->Category->find('list', array('conditions'=>array('Category.parent !='=>0)));
		$this->set(compact('categories'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			$this->Session->setFlash(__('Invalid product'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Product->delete()) {
			$this->Session->setFlash(__('Product deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Product was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function admin_delete($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$product = $this->Product->read(null, $id);
		if ($this->Product->delete()) {
			if($product['Product']['image'])
			{
				unlink(BANNER_IMAGES.$product['Product']['image']);
			}
			$this->Session->setFlash(sprintf(__('Product deleted successfully.', true), 'Product'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Product was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


	function admin_status($id = null) {
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid product.', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->Product->recursive = 0;
			$status = $this->Product->find('first', array('conditions'=>array('Product.id'=>$id)));
			$this->Product->id=$id;
			if(isset($status['Product']['status']) && $status['Product']['status'] == '1') 
			{
				$this->Product->saveField('status','0');
				$this->Session->setFlash(sprintf(__('Product deactivated successfully.', true), 'Product'), 'default', array('class' => 'success'));
			}
			else
			{
				$this->Product->saveField('status','1');
				$this->Session->setFlash(sprintf(__('Product activated successfully.', true), 'Product'), 'default', array('class' => 'success'));
			}
			$this->redirect(array('action' => 'index'));
		}
	}

	function admin_change_size() {
		$catId = $_POST['category'];
		$sizeIdString = $_POST['sizeIdString'];
		if($sizeIdString == null || $sizeIdString ==''){
			$sizeIdArr = array();
		}else{
			$sizeIdArr = explode(',', $sizeIdString);
		}
		$this->Size->recursive = 0;
		$sizesFetch = $this->Size->find('all', array('conditions'=>array('Size.category_id'=>$catId), 'order'=>array('Size.value'=>'ASC')));
		$sizeListArr = array();
		if(count($sizesFetch) > 0){
			foreach($sizesFetch as $sizeList){
				$temp = null;
				$fstf = false;
				$sndf = false;
				if($sizeList['Size']['name'] != null){
					$temp .= $sizeList['Size']['name'];
					$fstf = true;
				}
				if($sizeList['Size']['value'] != null){
					if($temp == null){
						$temp .= $sizeList['Size']['value'];
						$fstf = true;
					} else if($fstf){
						$temp .= '('.$sizeList['Size']['value'];
						$sndf = true;
					}
				}
				if($sizeList['Size']['unit'] != null){
					if($sndf){
						$temp .= '-'.$sizeList['Size']['unit'];
					} else if($fstf){
						$temp .= '('.$sizeList['Size']['unit'];
						$sndf = true;
					} else {
						$temp .= $sizeList['Size']['unit'];
					}
				}
				if($sndf){
					$temp .= ')';
				}
				//$sizeListArr[$sizeList['Size']['id']] = $sizeList['Size']['name'].' ('.$sizeList['Size']['value'].'-'.$sizeList['Size']['unit'].')';
				$sizeListArr[$sizeList['Size']['id']] = $temp;
			}
		}
		if(isset($sizeListArr) && (count($sizeListArr) > 0)){
			echo '<label for="ProductSizeId" style="color:black;font-size: 90%;">Size</label>';
			foreach($sizeListArr as $key=>$val){
				if(count($sizeIdArr) > 0){
					foreach($sizeIdArr as $id){
						if($id == $key){
							$checked = 'checked';
							break;
						}
						else{
							$checked = '';
						}
					}
				}
				else{
					$checked = '';
				}
				echo '<input id="" type="checkbox" name="data[Product][size]['.$key.']" value="'.$key.'" style="margin:0 6px 2px 0;" id="ProductSizeId" '.$checked.'> '.$val.'<br/>';
			}
		}
		exit;
	}
	function admin_send($id = null){
		$this->layout = false;
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			$this->Session->setFlash(__('Invalid product.'));
			$this->redirect(array('action' => 'index'));
		}
		//$this->set('product', $this->Product->read(null, $id));
		if(!empty($this->request->data)){
			if($this->request->data['send']['mode'] == 1){
				$userEmailArr = array();
				if(isset($this->request->data['send']['file']['name']) && $this->request->data['send']['file']['name'] != null){
					if($this->request->data['send']['file']['type'] == 'application/csv'){
						if (($csvFile = fopen($this->request->data['send']['file']['tmp_name'], "r")) !== FALSE) {
							$valArr = array();
							while (($data = fgetcsv($csvFile, 1000, ",")) !== FALSE) {
								$num = count($data);
								$valArr[] = $data[0];
							}
							if(count($valArr)>0){
								foreach($valArr as $val){
									$userEmailArr[] = $val;
								}
							} else {
								$this->Session->setFlash(__('Upload csv file containing no value in its first coloumn (coloumn A).', true));
							}
							fclose($csvFile);
						}
					} else {
						$this->Session->setFlash(__('Please upload a csv file.', true));
					}
				} else {
					$this->Session->setFlash(__('Please upload a csv file.', true));
				}
				$userEmail = array_unique($userEmailArr);
				$userArray = $this->Email->find('all', array('fields'=>'Email.email', 'conditions'=>array('Email.email' => $userEmail), 'order'=>array('Email.id'=>'desc')));
			}
			if($this->request->data['send']['mode'] == 2){
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'conditions'=>array('User.newslater_subscripyion' => 'Y'), 'order'=>array('User.id'=>'desc')));
			}
			if($this->request->data['send']['mode'] == 3){
				$idArr = $this->request->data['send']['user_id'];
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'conditions'=>array('User.id' => $idArr), 'order'=>array('User.id'=>'desc')));
			}
			if($this->request->data['send']['mode'] == 4){
				$numberOfUser = $this->request->data['send']['number_of_user'];
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'limit'=>$numberOfUser, 'order'=>'rand()'));
			}
			if($this->request->data['send']['mode'] == 5){
				$userArray = $this->User->find('all', array('fields'=>'User.email, User.first_name, User.last_name', 'order'=>array('User.id'=>'desc')));
			}
			$id = $this->request->data['send']['product_id'];
			$product = $this->Product->read('Product.id, Product.sale_price, Product.name, Product.information', $id);
			if(count($product['ProductPhoto']) > 0){
				$flag = true;
				foreach($product['ProductPhoto'] as $productPhoto){
					if($productPhoto['cover_photo'] == 'Y'){
						$flag = false;
						$productImage = PRODUCT_IMAGES_URL.$productPhoto['name'];;
					}
				}
				if($flag == true){
					$productImage = PRODUCT_IMAGES_URL.$product['ProductPhoto'][0]['name'];
				}
			}
			else{
				$productImage = PRODUCT_IMAGES_URL.'noimgavail.gif';
			}
			$siteSetting = $this->viewVars['siteSetting'];
			$this->Mailer->recursive = 0;
			$mailer = $this->Mailer->find('first', array('conditions'=>array('Mailer.id'=>2)));
			$html = $mailer['Mailer']['html_content'];
			$logoPath = DOMAIN_NAME_PATH.'img/'.$siteSetting['Setting']['site_logo'];
			$subject = 'Sixsnowflakes Product: '.$product['Product']['name'];
			$productDescription = $product['Product']['information'];
			$productName = $product['Product']['name'];
			$productPrice = $product['Product']['sale_price'];
			$productId = $product['Product']['id'];
			$productUrl = DOMAIN_NAME_PATH.'Products/view/'.$productId;
			$html = str_replace("##logo_path##", $logoPath, $html);
			$html = str_replace("##product_name##", $subject, $html);
			$html = str_replace("##product_description##", $productDescription, $html);
			$html = str_replace("##product_price##", $productPrice, $html);
			$html = str_replace("##product_url##", $productUrl, $html);
			$html = str_replace("##product_image##", $productImage, $html);
			$mail_From = $siteSetting['Setting']['noreply_email'];
			$mail_CC = null;
			if(count($userArray) > 0){
				$mail_From = $siteSetting['Setting']['noreply_email'];
				$mail_CC = null;
				foreach($userArray as $user){
					if($user['User']['email']){
						$mail_To = $user['User']['email'];
						if($user['User']['first_name'] != '' && $user['User']['last_name'] != ''){
							$userName = $user['User']['first_name'].' '.$user['User']['last_name'];
						} else {
							$userName = $user['User']['email'];
						}
					} else {
						$userName = $user['Email']['email'];
						$mail_To = $user['Email']['email'];
					}
					$mail_body = str_replace("##user##", $userName, $html);
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $subject, $mail_body);
					$this->Session->setFlash(sprintf(__('Product newsletter has been sent Successfully.', true), 'User'), 'default', array('class' => 'success'));
					echo "<script>
						parent.jQuery.colorbox.close();
					</script>";
				}
			} else {
				$this->Session->setFlash(__('Product newsletter sending failed.'));
				echo "<script>
						parent.jQuery.colorbox.close();
					</script>";
				//window.top.location.href = '".DOMAIN_NAME_PATH."admin/Products/';
			}
		}
		$users = $this->User->find('list', array('fields'=>'User.email', 'order'=>array('User.id'=>'desc')));
		$this->set(compact('users'));
	}
}
