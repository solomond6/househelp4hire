<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 */
class CategoriesController extends AppController {
	var $name = 'Categories';
	var $uses = array('Category', 'HomeBanner', 'ShopHomeContent', 'Product');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
	}
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin_default';
		$this->Category->recursive = 0;
		$this->set('categories', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = 'admin_default';
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			$this->Session->setFlash(__('Invalid category'));
		}
		$this->set('category', $this->Category->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin_default';
		if ($this->request->is('post')) {$friendlyurl = $this->request->data['Category']['name'];
			foreach($this->notAllowedWord AS $arrNAWord)
			{
				$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
			}
			$friendlyurl = str_replace(' ', '-', $friendlyurl);
			$chk = $this->Category->find('first', array('conditions' => array('Category.friendly_url' => $friendlyurl)));
			$this->request->data['Category']['friendly_url'] = $friendlyurl;
			$duplicate = false;
			if(!empty($chk)){
				$this->request->data['Category']['friendly_url'] = '';
				$duplicate = true;
			}
			if($this->request->data['Category']['image']['name'] != null && $this->request->data['Category']['thumb_image']['name'] != null){
				$random = rand();
				$primaryName = str_replace(' ', '_', $this->request->data['Category']['image']['name']);
				$extention = strrchr($primaryName,".");
				$fileName = $random.'_'.$primaryName;
				$random2 = rand();
				$primaryName2 = str_replace(' ', '_', $this->request->data['Category']['thumb_image']['name']);
				$extention2 = strrchr($primaryName2,".");
				$fileName2 = $random2.'_'.$primaryName2;
				if(in_array($extention, $this->validImageFormats) && in_array($extention2, $this->validImageFormats)){
					move_uploaded_file($this->request->data['Category']['image']['tmp_name'], CATEGORY_IMAGES.$fileName);
					unset($this->request->data['Category']['image']);
					$this->request->data['Category']['image'] = $fileName;
					move_uploaded_file($this->request->data['Category']['thumb_image']['tmp_name'], CATEGORY_IMAGES.$fileName2);
					unset($this->request->data['Category']['thumb_image']);
					$this->request->data['Category']['thumb_image'] = $fileName2;
					$this->Category->create();
					if ($this->Category->save($this->request->data)) {
						if($duplicate){
							$friendlyurl .= '-'.$this->Category->id;
							$this->Category->savefield('friendly_url', $friendlyurl);
						}
						$this->Session->setFlash(sprintf(__('Category added successfully!', true), 'Category'), 'default', array('class' => 'success'));
						$this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('Category could not be saved. Please, try again.'));
					}
				}
				else{
					$this->Session->setFlash(__('Please, upload gif, jpeg, jpg, png images only.'));
				}
			}
			else{
				$this->Session->setFlash(__('Please, upload atleast one image for this category.'));
			}
		}
		$this->set('options', $this->Category->generateTreeList(null, null, null, '---'));
        //pr ($this->request->data); die;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = 'admin_default';
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			$this->Session->setFlash(sprintf(__('Invalid Category.', true)));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$friendlyurl = $this->request->data['Category']['name'];
			foreach($this->notAllowedWord AS $arrNAWord)
			{
				$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
			}
			$friendlyurl = str_replace(' ', '-', $friendlyurl);
			$chk = $this->Category->find('first', array('conditions' => array('Category.friendly_url' => $friendlyurl, 'Category.id !='=>$id)));
			$this->request->data['Category']['friendly_url'] = $friendlyurl;
			if(!empty($chk)){
				$this->request->data['Category']['friendly_url'] = $friendlyurl.'-'.$id;
			} else {
				$this->request->data['Category']['friendly_url'] = $friendlyurl;
			}
			if($this->request->data['Category']['image']['name'] != null){
				$random = rand();
				$primaryName = str_replace(' ', '_', $this->request->data['Category']['image']['name']);
				$extention = strrchr($primaryName,".");
				$fileName = $random.'_'.$primaryName;
				if(in_array($extention, $this->validImageFormats)){
					unlink(CATEGORY_IMAGES.$this->request->data['Category']['previmage']);
					move_uploaded_file($this->request->data['Category']['image']['tmp_name'], CATEGORY_IMAGES.$fileName);
					unset($this->request->data['Category']['image']);
					$this->request->data['Category']['image'] = $fileName;
				}
				else{
					$this->Session->setFlash(__('Please, upload gif, jpeg, jpg, png images only.'));
				}
			}
			else{
				$this->request->data['Category']['image'] = $this->request->data['Category']['previmage'];
			}
			unset($this->request->data['Category']['previmage']);
			if($this->request->data['Category']['thumb_image']['name'] != null){
				$random = rand();
				$primaryName = str_replace(' ', '_', $this->request->data['Category']['thumb_image']['name']);
				$extention = strrchr($primaryName,".");
				$fileName = $random.'_'.$primaryName;
				if(in_array($extention, $this->validImageFormats)){
					unlink(CATEGORY_IMAGES.$this->request->data['Category']['prevthumbimage']);
					move_uploaded_file($this->request->data['Category']['thumb_image']['tmp_name'], CATEGORY_IMAGES.$fileName);
					unset($this->request->data['Category']['thumb_image']);
					$this->request->data['Category']['thumb_image'] = $fileName;
				}
				else{
					$this->Session->setFlash(__('Please, upload gif, jpeg, jpg, png images only.'));
				}
			}
			else{
				$this->request->data['Category']['thumb_image'] = $this->request->data['Category']['prevthumbimage'];
			}
			unset($this->request->data['Category']['prevthumbimage']);
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('Category added successfully!', true), 'Category'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Category could not be saved. Please, try again.'));
			}
		} else {
		}
		$this->request->data = $this->Category->read(null, $id);
		$this->request->data['Category']['previmage'] = $this->request->data['Category']['image'];
		$this->request->data['Category']['prevthumbimage'] = $this->request->data['Category']['thumb_image'];
		$this->set('options', $this->Category->generateTreeList(null, null, null, '---'));
        //pr ($this->request->data); die;
	}

	public function admin_status($id = null){
		$this->layout = 'admin_default';
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid Category.', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->Category->recursive = 0;
			$this->Category->id=$id;
			$allChildrens = $this->Category->children();
			$status = $this->Category->find('first', array('conditions'=>array('Category.id'=>$id)));
			if(isset($status['Category']['status']) && $status['Category']['status'] == 'Y') 
			{
				$this->Category->saveField('status','N');
				$this->Session->setFlash(sprintf(__('Category deactivated successfully.', true), 'Category'), 'default', array('class' => 'success'));
				if(count($allChildrens) > 0){
					foreach($allChildrens as $allChildren){
						$this->Category->id=$allChildren['Category']['id'];
						$this->Category->saveField('status','N');
					}
				}
			}
			else
			{
				$this->Category->saveField('status','Y');
				$this->Session->setFlash(sprintf(__('Category activated successfully.', true), 'Category'), 'default', array('class' => 'success'));
				if(count($allChildrens) > 0){
					foreach($allChildrens as $allChildren){
						$this->Category->id=$allChildren['Category']['id'];
						$this->Category->saveField('status','Y');
					}
				}
			}
			$this->redirect(array('action' => 'index'));
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			$this->redirect(array('action' => 'index'));
			$this->Session->setFlash(sprintf(__('Invalid Category.', true)));
		}
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			$this->redirect(array('action' => 'index'));
			$this->Session->setFlash(sprintf(__('Invalid Category.', true)));
		}
		$allChildrens = $this->Category->children();
		if(count($allChildrens) > 0){
			foreach($allChildrens as $allChildren){
				unlink(CATEGORY_IMAGES.$allChildren['Category']['image']);
				unlink(CATEGORY_IMAGES.$allChildren['Category']['thumb_image']);
			}
		}
		$this->request->data = $this->Category->read(null, $id);
		unlink(CATEGORY_IMAGES.$this->request->data['Category']['image']);
		unlink(CATEGORY_IMAGES.$this->request->data['Category']['thumb_image']);
		if ($this->Category->delete()) {
			$this->Session->setFlash(sprintf(__('Category deleted successfully.', true), 'Category'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Category was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
