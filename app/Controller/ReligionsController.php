<?php
App::uses('AppController', 'Controller');
/**
 * Religions Controller
 *
 * @property Religion $Religion
 */
class ReligionsController extends AppController {
	public $uses = array('Religion', 'Page');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Religion->recursive = 0;
		$this->set('Religions', $this->paginate());
	}
	public function admin_index() {
		$this->Religion->recursive = 0;
		$this->set('Religions', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid Religion'));
		}
		$this->set('Religiondetails', $this->Religion->read(null, $id));
		$this->Religion->recursive = 0;
		$this->set('Religions', $this->paginate());
		$this->set(compact('id'));
	}
	public function admin_view($id = null) {
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid Religion'));
		}
		$this->set('Religion', $this->Religion->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Religion->create();
			if ($this->Religion->save($this->request->data)) {
				$this->Session->setFlash(__('The Religion has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Religion could not be saved. Please, try again.'));
			}
		}
	}
	public function admin_add() {
		if ($this->request->is('post')) {
			//pr($this->request->data);die;
			$this->Religion->create();
			if ($this->Religion->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The Religion has been saved successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Religion could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid Religion'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Religion->save($this->request->data)) {
				$this->Session->setFlash(__('The Religion has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Religion could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Religion->read(null, $id);
		}
	}
	public function admin_edit($id = null) {
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid Religion'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Religion->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The Religion has been updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Religion could not be updated. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Religion->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid Religion'));
		}
		if ($this->Religion->delete()) {
			$this->Session->setFlash(__('Religion deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Religion was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid Religion'));
		}
		if ($this->Religion->delete()) {
				$this->Session->setFlash(sprintf(__('The Religion has been deleted successfully!', true), 'Page'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Religion was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
