<?php
require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'flutterwave' . DS . 'vendor' . DS . 'autoload.php');
use Flutterwave\Card;
use Flutterwave\Flutterwave;
use Flutterwave\AuthModel;
use Flutterwave\Currencies;
use Flutterwave\Countries;
use Flutterwave\FlutterEncrypt;
use Flutterwave\Bvn;
class UsersController extends AppController {

	public $name = 'Users';
	public $uses = array('User', 'Setting', 'Category', 'Page', 'Service', 'Contact', 'Booking', 'Email', 'Payment', 'Staff','Agent','Location','Locality','Education', 'StaffPreferedLocation', 'StaffSpokenLanguage', 'StaffPreferedEducation', 'StaffPreferedLocality','StaffCategory','VettingCategory','VettingCompany','Rating','RatingConfig','Viewcontacts');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(
            'index',
            'login',
			'register',
			'forgetPassword',
			'resetPassword',
			'verification',
			'locationChecking',
			'numbercaptcha',
			'addagents',
			'admin_addvets',
			'addcandidate',
			'vettinCompanies',
			'vetPayment',
			'validateOtp',
			'home',
			'bvnVet',
			'validateOtpBvn'
            );
	}
	/*public function beforeRender() {
		parent::beforeRender();
	}*/

	function numbercaptcha() {
		//Set first and second num rand(min,max)
		$firstnum = rand(5,8);
		$secondnum = rand(1,4);
		$coinflip = rand(1,2) % 2; //Picks a random equation type
		if($coinflip == 0) {
		$math = $firstnum + $secondnum;
		$operators = array("+","Added To","Plus");
		$operatorschoice = rand(1,3) % 3;
		} else {
		$math = $firstnum - $secondnum;
		$operators = array("-","Minus");
		$operatorschoice = rand(1,2) % 2;
		}
		//echo $firstnum . " " . $operators[$operatorschoice] . " " . $secondnum . " = <input type=\"text\" name=\"number\" maxlength=\"2\" size=\"5\">";
		//return $math;
		$return = array('firstnum' => $firstnum, 'operators' => $operators[$operatorschoice], 'secondnum' => $secondnum, 'result' => $math);
		return $return;
	}

	public function setMenuSession(){
		if(isset($_POST['linkId']) && $_POST['linkId']!=null){
			$this->Session->write('setMenuSession', $_POST['linkId']);
		}
		exit;
	}

	public function index() {
		if(!empty($this->request->data)){
			if($this->request->data['Contact']['captcha'] != $this->Session->read('captcha.result')){
				$this->Session->setFlash(sprintf(__('Invalid Captcha code! please try again!', true)));
				$this->Session->write('setMenuSession', 'ContactEmail');
			} else {
			   $this->request->data["Contact"]["date"]=date('Y-m-d H:i:s');
				$this->Contact->create();
				if ($this->Contact->save($this->request->data)) {
					$this->loadModel('Setting');
					$this->Setting->recursive = 0;
					$settingMain = $this->Setting->read(null, 1);
					/*for mail*/
						$mail_To= $settingMain['Setting']['contact_email'];
						$mail_From = $settingMain['Setting']['noreply_email'];
						$mail_CC = '';
						$mail_subject="Somebody Contact You";
						$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
										 <p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$settingMain['Setting']['site_logo']."' alt='HH4H'></p>
										 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
										  <tr>
											<td width='77%' align='left' valign='top' bgcolor=''>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Dear Admin</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
											  </div>
											  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
												<table width='100%' border='0' cellspacing='5' cellpadding='5'>
												  <tr>
													<td align='left' valign='top'>
													  <p><strong>Details are below...</strong></p>
													  <p>
													   Name : ".$this->request->data["Contact"]["name"]."<br/>
													   Email : ".$this->request->data["Contact"]["email"]."<br/>
													   Phone : ".$this->request->data["Contact"]["phone"]."<br/>
													   About ".$this->request->data["Contact"]["name"]." : ".$this->request->data["Contact"]["about_user"]."<br/>
													   Message : ".$this->request->data["Contact"]["message"]."<br/>
													  </p>
													</td>
												  </tr>
												</table>
											  </div>
											  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
											  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
											  </div>
											</td>
										   </tr>
										 </table>
										 <p style='font-size:11px;text-align:center;'>
											<a href='".$settingMain['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
											<a href='".$settingMain['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
										 </p>
										 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
									   </div>"; 
					$mail_Body = $content;
					$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
					/*for mail*/
					$this->Session->setFlash(sprintf(__("Thanks! We'll be in touch soon!", true), 'User'), 'default', array('class' => 'success', 'contact'));
					$this->Session->write('setMenuSession', 'contactTabLink');
					$this->redirect(array('controller'=>'Users', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('Message could not be sent. Please, try again.'));
				}
		   }
		}
		if($this->Session->check('setMenuSession')){
			$secondTabMenu = $this->Session->read('setMenuSession');
			$this->set('secondTabMenu', $secondTabMenu);
			$this->Session->delete('setMenuSession');
		}
		$this->Category->recursive = 0;
		$categories = $this->Category->find('all', array('conditions'=>array('Category.parent_id'=>null, 'Category.status'=>'Y')));
		$this->set('categories', $categories);
		$this->Service->recursive = 0;
		$services = $this->Service->find('all', array('conditions'=>array('Service.status'=>'Y'), 'limit'=>4));
		$this->set('services', $services);
		$this->Page->recursive = 0;
		$this->set('page', $this->Page->read(null, 1));
		$captcha = $this->numbercaptcha();
		$this->Session->write('captcha', $captcha);
		$this->set(compact('captcha'));
	}

	public function home(){

	}

	function locationChecking(){
		if($_POST['location_id'] != null){
			$locations = $this->User->Location->find('first', array('conditions'=>array('Location.id'=>$_POST['location_id'])));
			if($locations['Location']['service_availability'] == 'Yes'){
				//echo '<p style="color:green;">Our services are currently available in '.$locations['Location']['name'].'. Please continue to sign up as that will help us reach out to you when you introduce our services in your state.</p>';
			} else {
				echo '<p style="color:red;border: 3px solid #FFA422;padding: 5px;">Please note our services are currently only available in LAGOS, but please complete your registration and we will notify you once we commence in your area.</p>';
			}
		}
		exit;
	}
	
	function dashboard(){
		$this->User->recursive = 2;
		$profileInfo = $this->User->read(null, $this->Session->read('Auth.User.id'));
		$newsletter = $this->Email->find('list', array('fields'=>array('Email.email'), 'conditions'=>array('Email.email'=>$this->Session->read('Auth.User.email'))));
		$countries = $this->User->Country->find('list', array('order'=>array('Country.name'=>'ASC')));
		$this->set(compact('profileInfo', 'newsletter', 'countries'));
		
		$siteSetting = $this->viewVars['siteSetting'];
		if($siteSetting['Setting']['booking_module']==1){
			$this->Booking->recursive = 2;
			$this->Booking->unbindModel(array('belongsTo' => array('User')));
			$this->Booking->Staff->unbindModel(array('hasMany' => array('Booking'), 'belongsTo' => array('Religion', 'Language', 'Origin','Country')));
			$this->Booking->belongsTo['Staff']['fields'] = array('Staff.id','Staff.name','Staff.category_id','Staff.friendly_url');
			$this->Booking->Staff->belongsTo['Category']['fields'] = array('Category.name');
			$activeBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$this->Session->read('Auth.User.id'), 'Booking.status'=>'Pending'), 'order'=>array('Booking.date'=>'DESC')));
			$selectedBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$this->Session->read('Auth.User.id'), 'Booking.status'=>'Selected'), 'order'=>array('Booking.date'=>'DESC')));
			$pastBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$this->Session->read('Auth.User.id'), 'Booking.status !='=>array('Pending', 'Selected')), 'order'=>array('Booking.date'=>'DESC')));
			$this->set(compact('activeBookings', 'pastBookings', 'selectedBookings'));
		}
		
		$this->Payment->recursive = 0;
		$this->Payment->unbindModel(array('hasMany' => array(), 'belongsTo' => array('User')));
		$payments = $this->Payment->find('all', array('conditions'=>array('Payment.user_id'=>$this->Session->read('Auth.User.id')), 'order'=>array('Payment.id'=>'DESC')));
		$this->set('payments', $payments);

		$user_id = $this->Session->read('Auth.User.id');
		$shortlisted = $this->Viewcontacts->query("select id from hh_viewcontacts where user_id = '$user_id' and shortlisted='1'");

		// var_dump($shortlisted);exit;
		$contacted = $this->Payment->find('all', array('fields'=>'Payment.viewed_profile_contacts', 'conditions'=>array('Payment.user_id'=>$this->Session->read('Auth.User.id'))));
		// $info = $payment['Payment']['quantity_viewed']==0 ? 'No profile contact has been viewed by you!' : 'You already viewed '.$payment['Payment']['quantity_viewed'].' Profiles which are listed below...';
		
		// $staffs = array();
		foreach($contacted as $contact){
            $contactedprofileArr = explode(',', $contact['Payment']['viewed_profile_contacts']);
            $this->Staff->recursive = 0;
            $contactedStaffs = $this->Staff->find('all',array('fields'=>'Staff.name, Staff.friendly_url, Staff.image1, Category.name_singular', 'conditions'=>array('Staff.id'=>$contactedprofileArr))); 
        }

        $shortlistedStaffs = array();
        foreach($shortlisted as $shortlist){
            //$shortlistedprofileArr = explode(',', $shortlist['Payment']['shortlisted_profile_contacts']);
            $this->Staff->recursive = 0;
            $shortlistedStaffs[] = $this->Staff->find('all',array('fields'=>'Staff.name, Staff.friendly_url, Staff.image1, Category.name_singular', 'conditions'=>array('Staff.id'=>$shortlist['hh_viewcontacts']['id'])));
            
        }
        
		$ratings = $this->Rating->find('all', array('conditions'=>array('Rating.user_id'=>$this->Session->read('Auth.User.id'))));
		$this->set('ratings', $ratings);
		$this->set('shortlistedStaffs', $shortlistedStaffs);
		$this->set('contactedStaffs', $contactedStaffs);
		$ratingConfigs = $this->RatingConfig->find('all');
		$this->set('ratingConfigs', $ratingConfigs);
	}

	function accesslist($paymentId){
		$this->Payment->recursive = 0;
		$payment = $this->Payment->find('first', array('fields'=>'Payment.quantity_viewed, Payment.viewed_profile_contacts', 'conditions'=>array('Payment.id'=>$paymentId)));
		$info = $payment['Payment']['quantity_viewed']==0 ? 'No profile contact has been viewed by you!' : 'You already viewed '.$payment['Payment']['quantity_viewed'].' Profiles which are listed below...';
		$staffs = array();
		if($payment['Payment']['viewed_profile_contacts'] != null){
			$profileArr = explode(',', $payment['Payment']['viewed_profile_contacts']);
			$this->Staff->recursive = 0;
			$staffs = $this->Staff->find('all',array('fields'=>'Staff.name, Staff.friendly_url, Staff.image1, Category.name_singular', 'conditions'=>array('Staff.id'=>$profileArr)));
		}
		$html = '<div class="row-fluid">';
		$html.= '<p>'.$info.'</p>';
		if(count($staffs) > 0){
			$html.= '<ul class="thumbnails example-sites">';
			$i = 0;
			foreach($staffs as $staff){
				$i++;
				$html.= '<li class="span2" style="display: inline-block;">
				  <a target="_blank" href="'.DOMAIN_NAME_PATH.$staff['Staff']['friendly_url'].'" class="thumbnail prf-dtl" title="'.$staff['Category']['name_singular'].' '.$staff['Staff']['name'].'">
					<div class="ch-item ch-img-1">
						<div class="ch-info">
							<img src="'.PAGE_IMAGES_URL.$staff['Staff']['image1'].'" alt="" style="height:95px;width:80px" />
						</div>
					</div>
				  </a>
				</li>';
				if($i==6){
					$i = 0;
					$html.= '</ul><ul class="thumbnails example-sites">';
				}
			}
			$html.= '</ul>';
		}
		$html.= '</div>';
		echo $html;
		exit;
	}

	function shortlist($paymentId){
		$this->Payment->recursive = 0;
		$payment = $this->Payment->find('first', array('fields'=>'Payment.shortlisted_profile_contacts', 'conditions'=>array('Payment.id'=>$paymentId)));
		// $info = $payment['Payment']['quantity_viewed']==0 ? 'No profile contact has been viewed by you!' : 'You already viewed '.$payment['Payment']['quantity_viewed'].' Profiles which are listed below...';
		$staffs = array();
		if($payment['Payment']['shortlisted_profile_contacts'] != null){
			$profileArr = explode(',', $payment['Payment']['shortlisted_profile_contacts']);
			$this->Staff->recursive = 0;
			$staffs = $this->Staff->find('all',array('fields'=>'Staff.name, Staff.friendly_url, Staff.image1, Category.name_singular', 'conditions'=>array('Staff.id'=>$profileArr)));
		}
		$html = '<div class="row-fluid">';
		//$html.= '<p>'.$info.'</p>';
		if(count($staffs) > 0){
			$html.= '<ul class="thumbnails example-sites">';
			$i = 0;
			foreach($staffs as $staff){
				$i++;
				$html.= '<li class="span2" style="display: inline-block;">
				  <a target="_blank" href="'.DOMAIN_NAME_PATH.$staff['Staff']['friendly_url'].'" class="thumbnail prf-dtl" title="'.$staff['Category']['name_singular'].' '.$staff['Staff']['name'].'">
					<div class="ch-item ch-img-1">
						<div class="ch-info">
							<img src="'.PAGE_IMAGES_URL.$staff['Staff']['image1'].'" alt="" style="height:95px;width:80px" />
						</div>
					</div>
				  </a>
				</li>';
				if($i==6){
					$i = 0;
					$html.= '</ul><ul class="thumbnails example-sites">';
				}
			}
			$html.= '</ul>';
		}
		$html.= '</div>';
		echo $html;
		exit;
	}
	
	public function register() {
		if ($this->Session->read('Auth.User.id')) { 
			$this->redirect(array('controller' => 'Users','action' => 'index'));
		}
		if(!empty($this->request->data))
		{
			if($this->request->data['User']['email'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter email.', true)));
			} else if($error = $this->emailCheck($this->request->data['User']['email'])){
				$this->Session->setFlash($error);
			} else if($this->request->data['User']['password'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter password.', true)));
			} else if($this->request->data['User']['confirm_password'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter confirm password.', true)));
			} else if($this->request->data['User']['password'] != $this->request->data['User']['confirm_password']){
				$this->Session->setFlash(sprintf(__('Retype your confirm password.', true)));				
			} else {
				$exist_data = $this->User->find('list', array('conditions' => array('User.email' => $this->request->data['User']['email'])));
				if(empty($exist_data)){
					$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
					$temp_pass = $this->request->data['User']['confirm_password'];
					//unset($this->request->data['User']['confirm_password']);
					$password = $this->request->data['User']['password'];
					$this->request->data['User']['password'] = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['password']);
					$this->request->data['User']['registration_date'] = date('Y-m-d');
					$this->request->data['User']['activation_code'] = $this->create_coupon();
					$this->request->data['User']['is_verified'] = 0;
					$this->request->data['User']['status'] = 1;
					$this->request->data['User']['user_type'] = 2;
					$this->User->create();
					if($this->User->save($this->request->data))
					{
						$uId = 'EM'.$this->User->id;
						$idMinLentgth = 5;
						$idLength = strlen($this->User->id);
						$lengtDiff = $idMinLentgth-$idLength;
						if($lengtDiff > 0){
							$midStr = '';
							for($iter = 0; $iter < $lengtDiff; $iter++){
								$midStr.='0';
							}
							$uId = 'EM'.$midStr.$this->User->id;
						}
						$this->User->savefield('emp-uid', $uId);
						/*$this->Session->setFlash(sprintf(__('You have been registered successfully!', true), 'User'), 'default', array('class' => 'success'));
						$this->request->data['User']['password'] = $this->request->data['User']['confirm_password'];
						if($this->Auth->login()){
							$this->redirect($this->Auth->redirect()); 
						}*/
						//$mail_info = $this->EmailTemplate->find('first', array('fields' => array('EmailTemplate.subject', 'EmailTemplate.template'), 'conditions' => array('EmailTemplate.id' => 1)));
						$admin = $this->Setting->read(null,'1');
						//if($mail_info)
						//{
							$linkAct = DOMAIN_NAME_PATH.'Users/verification/'.$this->request->data['User']['activation_code'];
							$mail_To= $this->data['User']['email'];
							$mail_From = $admin['Setting']['noreply_email'];
							$mail_CC = '';
							$mail_subject="HouseHelp4Hire Registration";
							$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									<p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;'>
										  <span style='font-size:12px'>Hi ".$this->data['User']['name'].",</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p><strong>The following are your account details:</strong></p>
												  <p>
													 Login email:&nbsp;&nbsp;".$this->data['User']['email']."<br/>
													 Password:&nbsp;&nbsp;".$temp_pass."<br/>
													 Please click the link <a href='".$linkAct."'> The No.1 Source for Domestic Staff</a> to activate your account.
													 </p>
													 <p>If you're having any technical issues, please contact our support team (".trim($admin['Setting']['support_email']).")</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
							$mail_Body = $content;
							
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
						//}
						$this->Session->setFlash(sprintf(__('A verification link has been sent to your email. Please check your email to activate your account.', true), 'User'), 'default', array('class' => 'success'));
						$this->redirect(array('controller' => 'users','action' => 'login'));
					}
				} else {
					$this->Session->setFlash(sprintf(__('This email has already been registered!', true)));
				}
			}
		}
		//$countries = $this->User->Country->find('list');
		//$this->set(compact('countries'));
	}

	public function verification($code = null){
		if($this->Session->check('Auth.User.id')) {
			$this->redirect(array('controller' => 'Users','action' => 'dashboard'));
		}
		if($code){
			$this->User->recursive = 0;
			$exist_data = $this->User->find('first', array('conditions' => array('User.activation_code' => $code)));
			if(!empty($exist_data)){
				$this->User->id = $exist_data['User']['id'];
				$this->User->saveField('is_verified', 1);
				$this->User->saveField('status', 1);
				$this->User->saveField('activation_code', null);

				$this->Session->write('Auth.User.id', $exist_data['User']['id']);
				$this->Session->write('Auth.User.user_type', $exist_data['User']['user_type']);
				$this->Session->write('Auth.User.first_name', $exist_data['User']['first_name']);
				$this->Session->write('Auth.User.last_name', $exist_data['User']['last_name']);
				$this->Session->write('Auth.User.email', $exist_data['User']['email']);
				$this->Session->write('Auth.User.phone', $exist_data['User']['phone']);
				$this->Session->write('Auth.User.emp-uid', $exist_data['User']['emp-uid']);
				$this->Session->write('Auth.User.registration_date', $exist_data['User']['registration_date']);
				$this->Session->write('Auth.User.name', $exist_data['User']['name']);
				$this->Session->write('Auth.User.address', $exist_data['User']['address']);
				$this->Session->write('Auth.User.city', $exist_data['User']['city']);
				$this->Session->write('Auth.User.state', $exist_data['User']['state']);
				$this->Session->write('Auth.User.postal_code', $exist_data['User']['postal_code']);
				$this->Session->write('Auth.User.country_id', $exist_data['User']['country_id']);
				$this->Session->write('Auth.User.location_id', $exist_data['User']['location_id']);
				$this->Session->write('Auth.User.is_verified', 1);
				$this->Session->write('Auth.User.status', 1);

				$this->Session->setFlash(sprintf(__('Your profile has successfully been activated!', true), 'User'), 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash(sprintf(__('Invlid verification link or the link is expired!', true)));
			}
		}else {
			$this->Session->setFlash(sprintf(__('Invlid verification link!', true)));
		}
		$this->redirect('/');
	}

	public function forgetPassword() {
		if($this->Session->check('Auth.User.id')) {
			$this->redirect('dashboard');
		}
		$verified = false;
		//Configure::load('Recaptcha.key');
	    if (isset($this->request->data['User'])) {
	    	$this->User->set($this->request->data);
				$sqlCondition = array('conditions' => array('User.email' => $this->request->data['User']['email']));
				if($reslutData = $this->User->find('first',$sqlCondition)) {
						$unique_key = substr(md5(rand(0, 1000000)), 0, 20);
						$this->User->id = $reslutData['User']['id'];
						if($this->User->save(array('uid_string'=>$unique_key))){
							$ChangePassLink = DOMAIN_NAME_PATH.'users/resetPassword/'.$unique_key;
							$admin = $this->Setting->read(null,'1');
							$mail_To = $this->request->data['User']['email'];
							$mail_From = $admin['Setting']['noreply_email'];
							$mail_subject ='HouseHelp4Hire - Forgot Password';
							
							$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									<p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Dear ".$reslutData['User']['name'].",</span><br/><span style='font-size:15px;font-weight:bold;'>".$mail_subject."</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p>Please follow the link below to change your pasword:<br></p>
													<p><a href='".$ChangePassLink."'>Click Here To Change Your Password</a></p>
													<p>We look forward to a successful and profitable working relationship.</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
							
							$mail_Body = $content; 
							
							$this->Send_HTML_Mail($mail_To, $mail_From,'', $mail_subject, $mail_Body);
							$verified = true;
							// Captcha validation passed
							$this->Session->setFlash(sprintf(__('An Email containing the link to change password has been sent to your email address.', true)), 'default', array('class' => 'success'));
						} else {
							//$this->Session->setFlash('CAPTCHA validation failed, please try again.');
						}
				} else {
					$this->Session->setFlash(sprintf(__('Sorry this email does not exist', true)));
				}
	    }
		$validationStatus = '';
		$this->set(compact('validationStatus', 'verified'));		
    }

 //    function addagents() {
	// 	// if($this->Session->read('Auth.User.user_type')!=1 && $this->Session->read('Auth.User.user_type')!=3){
	// 	// 	$this->Session->setFlash(__('You are not allowed to access this', true));
	// 	// 	$this->redirect(array('action'=>'profile'));
	// 	// }
	// 	var_dump($this->request->data);exit;
	// 	if (!empty($this->request->data)) {
			
	// 		$dup_chk = $this->Agent->find('count', array('conditions' => array('Agent.phone' => $this->data['Agent']['phone']), 'limit' => 1));
	// 		if($dup_chk > 0){
	// 			$this->Session->setFlash(sprintf(__('This phone is not available.', true)));
	// 		} else {
	// 			$this->request->data['Agent']['name'] = $this->request->data['Agent']['first_name'].' '.$this->request->data['User']['last_name'];
	// 			$this->request->data['Agent']['user_type'] = 4;
	// 			$this->request->data['Agent']['is_verified'] = 0;
	// 			$this->request->data['Agent']['status'] = 0;
	// 			$this->request->data['Agent']['is_admin'] = 0;
	// 			$this->request->data['Agent']['registration_date'] = date('Y-m-d');
				
	// 			//$pwd=$this->request->data['User']['password'];
	// 			//$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').$this->request->data['User']['password']);
	// 			//$this->request->data['User']['acces']=implode($this->request->data['User']['access'],',');
				
					
	// 			if(isset($this->request->data['Agent']['image']['name']) && $this->request->data['Agent']['image']['name'] != '')
	// 			{
	// 				if($this->request->data['Agent']['image']['type'] == 'image/png' || $this->request->data['Agent']['image']['type'] == 'image/PNG' || $this->request->data['Agent']['image']['type'] == 'image/jpg' || $this->request->data['Agent']['image']['type'] == 'image/JPG' || $this->request->data['Agent']['image']['type'] == 'image/jpeg' || $this->request->data['Agent']['image']['type'] == 'image/JPEG')
	// 				{
	// 					$filename=rand(). $this->request->data['Agent']['image']['name'];

	// 					move_uploaded_file($this->request->data['Agent']['image']['tmp_name'], PAGE_IMAGES.$filename);
	// 					$this->request->data['Agent']['image'] = $filename;
	// 				}
	// 				else
	// 				{
	// 					$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
	// 				}
	// 			}
	// 			if(is_array($this->request->data['Agent']['image']))
	// 			{
	// 				$this->request->data['Agent']['image']='';
	// 			}
	// 			$this->Agent->create();
	// 			if ($this->Agent->save($this->data)) {
	// 				$uId = 'EM'.$this->Agent->id;
	// 				$idMinLentgth = 5;
	// 				$idLength = strlen($this->Agent->id);
	// 				$lengtDiff = $idMinLentgth-$idLength;
	// 				if($lengtDiff > 0){
	// 					$midStr = '';
	// 					for($iter = 0; $iter < $lengtDiff; $iter++){
	// 						$midStr.='0';
	// 					}
	// 					$uId = 'EM'.$midStr.$this->Agent->id;
	// 				}
	// 				$this->Agent->savefield('emp-uid', $uId);
	// 				$admin = $this->Setting->read(null,'1');
					
	// 				$this->Session->setFlash(sprintf(__('Agent has been added successfully.', true), 'Agent'), 'default', array('class' => 'success'));
	// 				$this->redirect(array('action' => 'agents'));
	// 			} else {
	// 				$this->Session->setFlash(__('Agent could not be saved. Please, try again.', true));
	// 			}
	// 		}
	// 	}
	// 	$locations = $this->User->Location->find('list');
	// 	$this->set(compact('locations'));
	// }
    public function addagents() {
		if ($this->Session->read('Auth.User.id')) { 
			$this->redirect(array('controller' => 'Users','action' => 'index'));
		}
		if(!empty($this->request->data))
		{
			if($this->request->data['User']['email'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter email.', true)));
			} else if($error = $this->emailCheck($this->request->data['User']['email'])){
				$this->Session->setFlash($error);
			} else if($this->request->data['User']['password'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter password.', true)));
			} else if($this->request->data['User']['confirm_password'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter confirm password.', true)));
			} else if($this->request->data['User']['password'] != $this->request->data['User']['confirm_password']){
				$this->Session->setFlash(sprintf(__('Retype your confirm password.', true)));				
			} else {
				$exist_data = $this->User->find('list', array('conditions' => array('User.email' => $this->request->data['User']['email'])));
				if(empty($exist_data)){
					$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
					$temp_pass = $this->request->data['User']['confirm_password'];
					//unset($this->request->data['User']['confirm_password']);
					$password = $this->request->data['User']['password'];
					$this->request->data['User']['password'] = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['password']);
					$this->request->data['User']['registration_date'] = date('Y-m-d');
					$this->request->data['User']['activation_code'] = $this->create_coupon();
					$this->request->data['User']['is_verified'] = 0;
					$this->request->data['User']['status'] = 0;
					$this->request->data['User']['user_type'] = 4;
					$this->User->create();
					if($this->User->save($this->request->data))
					{
						$uId = 'AG'.$this->User->id;
						$idMinLentgth = 5;
						$idLength = strlen($this->User->id);
						$lengtDiff = $idMinLentgth-$idLength;
						if($lengtDiff > 0){
							$midStr = '';
							for($iter = 0; $iter < $lengtDiff; $iter++){
								$midStr.='0';
							}
							$uId = 'AG'.$midStr.$this->User->id;
						}
						$this->User->savefield('emp-uid', $uId);
						/*$this->Session->setFlash(sprintf(__('You have been registered successfully!', true), 'User'), 'default', array('class' => 'success'));
						$this->request->data['User']['password'] = $this->request->data['User']['confirm_password'];
						if($this->Auth->login()){
							$this->redirect($this->Auth->redirect()); 
						}*/
						//$mail_info = $this->EmailTemplate->find('first', array('fields' => array('EmailTemplate.subject', 'EmailTemplate.template'), 'conditions' => array('EmailTemplate.id' => 1)));
						$admin = $this->Setting->read(null,'1');
						//if($mail_info)
						//{
							$linkAct = DOMAIN_NAME_PATH.'Users/verification/'.$this->request->data['User']['activation_code'];
							$mail_To= $this->data['User']['email'];
							$mail_From = $admin['Setting']['noreply_email'];
							$mail_CC = '';
							$mail_subject="HouseHelp4Hire Registration";
							$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									<p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;'>
										  <span style='font-size:12px'>Hi ".$this->data['User']['name'].",</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p><strong>The following are your agency account details:</strong></p>
												  <p>
													 Login email:&nbsp;&nbsp;".$this->data['User']['email']."<br/>
													 Password:&nbsp;&nbsp;".$temp_pass."<br/>
													 Please click the link <a href='".$linkAct."'> The No.1 Source for Domestic Staff</a> to activate your account.
													 </p>
													 <p>If you're having any technical issues, please contact our support team (".trim($admin['Setting']['support_email']).")</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
							$mail_Body = $content;
							
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
						//}
						$this->Session->setFlash(sprintf(__('A verification link has been sent to your email. Please check your email to activate your agency account.', true), 'User'), 'default', array('class' => 'success'));
						$this->redirect(array('controller' => 'users','action' => 'login', 'prefix'=>'agent'));
					}
				} else {
					$this->Session->setFlash(sprintf(__('This email has already been registered!', true)));
				}
			}
		}
		//$countries = $this->User->Country->find('list');
		//$this->set(compact('countries'));
	}

	public function admin_addvets() {
		$this->layout = 'admin_default';
		// if ($this->Session->read('Auth.User.id')) { 
		// 	$this->redirect(array('controller' => 'Users','action' => 'index'));
		// }

		if(!empty($this->request->data))
		{
			if($this->request->data['User']['email'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter email.', true)));
			} else if($error = $this->emailCheck($this->request->data['User']['email'])){
				$this->Session->setFlash($error);				
			} else {
				$exist_data = $this->User->find('list', array('conditions' => array('User.email' => $this->request->data['User']['email'])));
				if(empty($exist_data)){
					$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
					$temp_pass = 'password';
					//unset($this->request->data['User']['confirm_password']);
					$password = 'password';
					$this->request->data['User']['password'] = Security::hash(Configure::read('Security.salt') . $password);
					$this->request->data['User']['registration_date'] = date('Y-m-d');
					$this->request->data['User']['activation_code'] = $this->create_coupon();
					$this->request->data['User']['is_verified'] = 1;
					$this->request->data['User']['status'] = 0;
					$this->request->data['User']['user_type'] = 6;
					$this->request->data['User']['address'] = $this->request->data['User']['address'];
					$this->User->create();
					if($this->User->save($this->request->data)){
						$uId = 'VT'.$this->User->id;
						$idMinLentgth = 5;
						$idLength = strlen($this->User->id);
						$lengtDiff = $idMinLentgth-$idLength;
						if($lengtDiff > 0){
							$midStr = '';
							for($iter = 0; $iter < $lengtDiff; $iter++){
								$midStr.='0';
							}
							$uId = 'VT'.$midStr.$this->User->id;
						}
						$this->User->savefield('emp-uid', $uId);
						$vid = $this->User->id;
						$newVet['VettingCompany']['user_id'] = $vid;
						$newVet['VettingCompany']['name'] = $this->request->data['User']['company_name'];
						$newVet['VettingCompany']['price'] = $this->request->data['User']['price'];
						$newVet['VettingCompany']['email'] = $this->request->data['User']['company_email'];
						$newVet['VettingCompany']['phone_no'] = $this->request->data['User']['user_mobile'];
						$newVet['VettingCompany']['vetting_category_id'] = $this->request->data['User']['vetting_category_id'];
						$newVet['VettingCompany']['address'] = $this->request->data['User']['address'];
						$this->VettingCompany->create();
						$this->VettingCompany->save($newVet);
						$admin = $this->Setting->read(null,'1');
						//if($mail_info)
						//{
							$linkAct = DOMAIN_NAME_PATH.'Users/verification/'.$this->request->data['User']['activation_code'];
							$mail_To= $this->data['User']['email'];
							$mail_From = $admin['Setting']['noreply_email'];
							$mail_CC = '';
							$mail_subject="HouseHelp4Hire Registration";
							$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									<p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;'>
										  <span style='font-size:12px'>Hi ".$this->data['User']['name'].",</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p><strong>The following are your agency account details:</strong></p>
												  <p>
													 Login email:&nbsp;&nbsp;".$this->data['User']['email']."<br/>
													 Password:&nbsp;&nbsp;".$temp_pass."<br/>
													 Please click the link <a href='".$linkAct."'> The No.1 Source for Domestic Staff</a> to activate your account.
													 </p>
													 <p>If you're having any technical issues, please contact our support team (".trim($admin['Setting']['support_email']).")</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
							$mail_Body = $content;
							
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
						//}
						$this->Session->setFlash(sprintf(__('A verification link has been sent to the email provided. Please check your email to activate account.', true), 'User'), 'default', array('class' => 'success'));
						$this->redirect(array('controller' => 'vettingCompanies','action' => 'index', 'prefix'=>'admin'));
					}
				} else {
					$this->Session->setFlash(sprintf(__('This email has already been registered!', true)));
				}
			}
		}
		$locations = $this->User->Location->find('list');
		$vettingCategories = $this->VettingCategory->find('list');
		$this->set(compact('locations', 'vettingCategories'));
		//$countries = $this->User->Country->find('list');
		//$this->set(compact('countries'));
	}

	public function addcandidate() {
		if ($this->Session->read('Auth.User.id')) { 
			$this->redirect(array('controller' => 'Users','action' => 'index'));
		}
		if(!empty($this->request->data))
		{
			if($this->request->data['User']['email'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter email.', true)));
			} else if($error = $this->emailCheck($this->request->data['User']['email'])){
				$this->Session->setFlash($error);
			} else if($this->request->data['User']['password'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter password.', true)));
			} else if($this->request->data['User']['confirm_password'] == ''){
				$this->Session->setFlash(sprintf(__('Please enter confirm password.', true)));
			} else if($this->request->data['User']['password'] != $this->request->data['User']['confirm_password']){
				$this->Session->setFlash(sprintf(__('Retype your confirm password.', true)));				
			} else {
				$exist_data = $this->User->find('list', array('conditions' => array('User.email' => $this->request->data['User']['email'])));
				if(empty($exist_data)){
					$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
					$temp_pass = $this->request->data['User']['confirm_password'];
					//unset($this->request->data['User']['confirm_password']);
					$password = $this->request->data['User']['password'];
					$this->request->data['User']['password'] = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['password']);
					$this->request->data['User']['registration_date'] = date('Y-m-d');
					$this->request->data['User']['activation_code'] = $this->create_coupon();
					$this->request->data['User']['is_verified'] = 0;
					$this->request->data['User']['status'] = 0;
					$this->request->data['User']['user_type'] = 5;
					$this->User->create();
					if($this->User->save($this->request->data))
					{
						$uId = 'JS'.$this->User->id;
						$idMinLentgth = 5;
						$idLength = strlen($this->User->id);
						$lengtDiff = $idMinLentgth-$idLength;
						if($lengtDiff > 0){
							$midStr = '';
							for($iter = 0; $iter < $lengtDiff; $iter++){
								$midStr.='0';
							}
							$uId = 'JS'.$midStr.$this->User->id;
						}
						$this->User->savefield('emp-uid', $uId);
						$uid = $this->User->id;
						$newStaff['Staff']['user_id'] = $uid;
						$newStaff['Staff']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
						$newStaff['Staff']['status'] = 0;
						$newStaff['Staff']['accommodation'] = 0;
						$newStaff['Staff']['email'] = $this->request->data['User']['email'];
						$this->Staff->create();
						$this->Staff->save($newStaff);
						/*$this->Session->setFlash(sprintf(__('You have been registered successfully!', true), 'User'), 'default', array('class' => 'success'));
						$this->request->data['User']['password'] = $this->request->data['User']['confirm_password'];
						if($this->Auth->login()){
							$this->redirect($this->Auth->redirect()); 
						}*/
						//$mail_info = $this->EmailTemplate->find('first', array('fields' => array('EmailTemplate.subject', 'EmailTemplate.template'), 'conditions' => array('EmailTemplate.id' => 1)));
						$admin = $this->Setting->read(null,'1');
						//if($mail_info)
						//{
							$linkAct = DOMAIN_NAME_PATH.'Users/verification/'.$this->request->data['User']['activation_code'];
							$mail_To= $this->data['User']['email'];
							$mail_From = $admin['Setting']['noreply_email'];
							$mail_CC = '';
							$mail_subject="HouseHelp4Hire Registration";
							$content = "<div style='width:700px; font-family:Arial, Helvetica, sans-serif;'>
									<p align='center'><img src='".DOMAIN_NAME_PATH.'img/site_logo/'.$admin['Setting']['site_logo']."' alt='HH4H'></p>
									 <table width='100%' border='0' cellspacing='10' cellpadding='10'>
									  <tr>
										<td width='77%' align='left' valign='top' bgcolor=''>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;'>
										  <span style='font-size:12px'>Hi ".$this->data['User']['name'].",</span>
										  </div>
										  <div style='border:1px solid #b8b8b8; border-radius:10px;	-moz-border-radius:10px; padding:10px; margin:0 0 10px 0; width:635px;	background:#ffffff;'>
											<table width='100%' border='0' cellspacing='5' cellpadding='5'>
											  <tr>
												<td align='left' valign='top'>
												  <p><strong>The following are your agency account details:</strong></p>
												  <p>
													 Login email:&nbsp;&nbsp;".$this->data['User']['email']."<br/>
													 Password:&nbsp;&nbsp;".$temp_pass."<br/>
													 Please click the link <a href='".$linkAct."'> The No.1 Source for Domestic Staff</a> to activate your account.
													 </p>
													 <p>If you're having any technical issues, please contact our support team (".trim($admin['Setting']['support_email']).")</p>
												</td>
											  </tr>
											</table>
										  </div>
										  <div style='width:97%; height:auto; border:1px solid #F4760F; -moz-border:10px; border-radius:10px; background:#F4760F; margin:0 0 10px 0; padding:10px; color:#ffffff;' >
										  <span style='font-size:12px'>Regards,</span><br/><span style='font-size:15px;font-weight:bold;'>HouseHelp4Hire</span>
										  </div>
										</td>
									   </tr>
									 </table>
									 <p style='font-size:11px;text-align:center;'>
										<a href='".$admin['Setting']['facebook_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/f.png' alt='facebook' height='48' /></a>
										<a href='".$admin['Setting']['twitter_link']."' target='_blank'><img style='padding:0 10px 0 0' src='".DOMAIN_NAME_PATH."img/t.png' alt='twitter' height='48' /></a>
									 </p>
									 <p style='font-size:11px;text-align:center;'>Copyright 2014 4Hire All Rights Reserved</p>
								   </div>"; 
							$mail_Body = $content;
							
							$this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
						//}
						$this->Session->setFlash(sprintf(__('A verification link has been sent to your email. Please check your email to activate your agency account.', true), 'User'), 'default', array('class' => 'success'));
						$this->redirect(array('controller' => 'users','action' => 'login', 'prefix'=>'candidate'));
					}
				} else {
					$this->Session->setFlash(sprintf(__('This email has already been registered!', true)));
				}
			}
		}
		//$countries = $this->User->Country->find('list');
		//$this->set(compact('countries'));
	}
	public function resetPassword($tempString = null) {
		if($this->Session->check('Auth.User.id')) {
			$this->redirect('dashboard');
		}
		$verified = false;
		if (isset($this->request->data['User'])) {
			$this->User->set($this->request->data);
			if($this->request->data['User']['tempString'] != '') {
				if($this->request->data['User']['password'] == ''){
					$this->Session->setFlash(sprintf(__('Please enter password.', true)));
				} else if($this->request->data['User']['re_password'] == ''){
					$this->Session->setFlash(sprintf(__('Please enter confirm password.', true)));
				} else if($this->request->data['User']['password'] != $this->request->data['User']['re_password']){
					$this->Session->setFlash(sprintf(__('Retype your confirm password.', true)));
				} else {
					$encoded_pass = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['password']);
					$userId = $this->User->field('id', array('uid_string'=>$this->request->data['User']['tempString']));
					if(!$userId){
						$this->Session->setFlash('Youre validation key is invalid. Please check your maiil again.');
						$this->redirect('index');
					}else{
						$this->User->id = $userId;
						$this->User->saveField('password', $encoded_pass);
						$this->User->saveField('uid_string', null);
						//$this->User->save(array('uid_string' => ''));
						$verified = true;
						$this->Session->setFlash(sprintf(__('You password has been changed successfully. Login with your account.', true)), 'default', array('class' => 'success'));
						$this->redirect('index');
					}
				}
			} else {
				$this->Session->setFlash('Youre validation key is invalid. Please check your maiil again.');
			}
		} else if (!empty($tempString)) {
			$this->request->data['User']['tempString'] = $tempString;
		} else {
			$this->redirect('index');
		}
		$this->set(compact('verified'));
    }
	
	function logout(){
		$this->Session->delete('Auth');
		//$this->Session->delete('NL_PopUp');
		$this->redirect($this->Auth->logout());
	}

	private function postlogin(){
		switch ($this->Auth->user('status')) {
            #Active
            case true:
	            $admin = false;
	            $agent = false;
	            $jobseeker = false;
	            $vet = false;
	            
	            switch ($this->Auth->user('user_type')) {
	                case '4':
	                    $agent = true;
	                    break;
	                case '3':
		                $admin = true;
		                break;
		            case '1':
		                $admin = true;
		                break;
		            case '5':
		                $jobseeker = true;
		                break;
		            case '6':
		                $vet = true;
		                break;
	            }

	            $this->Session->write('Auth.User.admin', $admin);
	            $this->Session->write('Auth.User.agent', $agent);
	            $this->Session->write('Auth.User.jobseeker', $jobseeker);
	            $this->Session->write('Auth.User.vet', $vet);

	            #Fetch gravatar image
            	$email = $this->Auth->user('email');

                //$gravatar = $this->Gravatar->getAvatar($email, 50);
                //$this->Session->write('Auth.User.gravatar', $gravatar);

                #Update user information with gravatar image
                $this->User->id = $this->Auth->user('id');
				//$this->User->saveField('image', $gravatar);

				$this->Auth->loginRedirect = array(
		        	'admin' => $admin,
		        	'agent' => $agent,
		        	'jobseeker' => $jobseeker,
		        	'vet' => $vet,
					'controller' => 'users',
					'action' => 'dashboard',
				);

                $this->redirect($this->Auth->redirect());
                break;
            #In-Active
            case false:
            	$this->Session->setFlash('You account is inactive! <br>Please contact the administrator at to resolve this issue');
                $this->redirect($this->Auth->logout());
                break;
        }
	}

	// function login(){
	// 	if ($this->Session->read('Auth.User.id')) { 
	// 		$this->redirect(array('controller' => 'Users','action' => 'dashboard'));
	// 	}
	// 	if ($this->request->is('post')) {
	// 		if ($this->Auth->login()) {
	// 			if(isset($this->request->data['User']['flag']) && $this->request->data['User']['flag'] != '0'){
	// 				$this->redirect(DOMAIN_NAME_PATH.$this->request->data['User']['flag']);
	// 			} else {
	// 				if($this->referer() == DOMAIN_NAME_PATH.'Users/login'){
	// 					$this->redirect($this->Auth->redirect());
	// 				} else {
	// 					$this->redirect($this->referer());
	// 				}
	// 			}
	//         }
	//     }
	// 	if(!empty($this->request->data)){
 //            $this->Session->setFlash(__('Invalid username or password!'));
 //        }
	// 	$this->render('register');
	// }
	public function login(){
		if ($this->Session->read('Auth.User.id')) { 
			$this->redirect(array('controller' => 'Users','action' => 'dashboard'));
		}
		if ($this->request->is('post')) {
            if ($this->Auth->login()) {
            	$this->postLogin();
            } else {
                $this->Session->setFlash('Invalid email or password, try again.');
            }
        }else{
            if ($this->Auth->loggedIn()) {
                $this->redirect($this->Auth->redirectUrl());
                $this->Session->setFlash('Login Successfully');
            }
        }
        $this->render('register_old');
	}
	
	function changeProfileInfo(){
		$this->layout = false;
		$flag = 1;
		$id = $this->Session->read('Auth.User.id');
		if(!empty($this->request->data)){
			if(isset($this->request->data['User']['first_name']) && isset($this->request->data['User']['last_name'])){
				$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
			}
			if(!empty($this->request->data['User']['email'])){
				$emailChk = $this->User->find('list', array('conditions'=>array('User.email'=>$this->request->data['User']['email'], 'User.id !='=>$id)));
				if(count($emailChk) > 0){
					unset($this->request->data['User']['email']);
					$flag = 2;
				}
			}
			$this->User->id=$id;
			$uId = 'EM'.$id;
			$idMinLentgth = 5;
			$idLength = strlen($id);
			$lengtDiff = $idMinLentgth-$idLength;
			if($lengtDiff > 0){
				$midStr = '';
				for($iter = 0; $iter < $lengtDiff; $iter++){
					$midStr.='0';
				}
				$uId = 'EM'.$midStr.$id;
			}
			$this->request->data['User']['emp-uid'] = $uId;
			if ($this->User->save($this->request->data)) {
				if($flag == 1){
					//$this->Session->setFlash(sprintf(__('Profile Information updated successfully.', true), 'User'), 'default', array('class' => 'success'));
					$this->Session->setFlash('Profile Information updated successfully.', 'default', array('class' => 'success'));
				}
				else if($flag == 2){
					$this->Session->setFlash('Email Address already exists. But other informatin updated successfully.', 'default', array('class' => 'error'));
				}
				$this->Session->write('Auth.User.name', $this->request->data['User']['name']);
			} else {
				$this->Session->setFlash(__('Profile Information could not be updated. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
		$this->redirect(array('action' => 'dashboard'));
	}

	function jobseeker_changeProfileInfo(){
		$this->layout = false;
		$flag = 1;
		$id = $this->Session->read('Auth.User.id');
		if(!empty($this->request->data)){
			if(isset($this->request->data['User']['first_name']) && isset($this->request->data['User']['last_name'])){
				$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
			}
			if(!empty($this->request->data['User']['email'])){
				$emailChk = $this->User->find('list', array('conditions'=>array('User.email'=>$this->request->data['User']['email'], 'User.id !='=>$id)));
				if(count($emailChk) > 0){
					unset($this->request->data['User']['email']);
					$flag = 2;
				}
			}
			$this->User->id=$id;
			$uId = 'JS'.$id;
			$idMinLentgth = 5;
			$idLength = strlen($id);
			$lengtDiff = $idMinLentgth-$idLength;
			if($lengtDiff > 0){
				$midStr = '';
				for($iter = 0; $iter < $lengtDiff; $iter++){
					$midStr.='0';
				}
				$uId = 'JS'.$midStr.$id;
			}
			$this->request->data['User']['emp-uid'] = $uId;
			if ($this->User->save($this->request->data)) {
				if($flag == 1){
					//$this->Session->setFlash(sprintf(__('Profile Information updated successfully.', true), 'User'), 'default', array('class' => 'success'));
					$this->Session->setFlash('Profile Information updated successfully.', 'default', array('class' => 'success'));
				}
				else if($flag == 2){
					$this->Session->setFlash('Email Address already exists. But other informatin updated successfully.', 'default', array('class' => 'error'));
				}
				$this->Session->write('Auth.User.name', $this->request->data['User']['name']);
			} else {
				$this->Session->setFlash(__('Profile Information could not be updated. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
		$this->redirect(array('action' => 'dashboard'));
	}

	function changePassword() {
		$this->layout = false;
		$this->User->recursive = 0;
		$account = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
		$this->set(compact('account'));
		if (!empty($this->request->data)) {
			if($this->request->data['User']['old_password'] != ""){
				$this->request->data['User']['old_password'] = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['old_password']);
				if($this->request->data['User']['old_password'] == $account['User']['password']){
					$this->request->data['User']['password'] = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['password']);
					$this->User->id = $this->Session->read('Auth.User.id');
					$this->User->saveField('password', $this->request->data['User']['password']);
					$this->Session->setFlash(sprintf(__('Password has been changed successfully.', true)), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'dashboard'));
				}
				else{
					$this->Session->setFlash(__('Invalid old password.', true));
					$this->redirect(array('action' => 'dashboard'));
				}
			}
		}
	}

	function jobseeker_changePassword() {
		$this->layout = false;
		$this->User->recursive = 0;
		$account = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
		$this->set(compact('account'));
		if (!empty($this->request->data)) {
			if($this->request->data['User']['old_password'] != ""){
				$this->request->data['User']['old_password'] = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['old_password']);
				if($this->request->data['User']['old_password'] == $account['User']['password']){
					$this->request->data['User']['password'] = Security::hash(Configure::read('Security.salt') . $this->request->data['User']['password']);
					$this->User->id = $this->Session->read('Auth.User.id');
					$this->User->saveField('password', $this->request->data['User']['password']);
					$this->Session->setFlash(sprintf(__('Password has been changed successfully.', true)), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'dashboard'));
				}
				else{
					$this->Session->setFlash(__('Invalid old password.', true));
					$this->redirect(array('action' => 'dashboard'));
				}
			}
		}
	}

	function chek_old_password(){
		if(isset($_POST['password']) && $_POST['password']!=null){
			$password1 = Security::hash(Configure::read('Security.salt') . $_POST['password']);
			$account = $this->User->find('first',array('fields'=>'User.password','conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
			$password2 = $account['User']['password'];
			if($password1 == $password2){
				echo '<p style="color:#468847;" id="passOldMatching">Old Password Matched!</p>';
			} else {
				echo '<p style="color:#B94A48;" id="passOldMatching">Old Password Missmatched!</p>';
			}
		} 
		exit;
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user.', true));
			$this->redirect(array('action' => 'index'));
		}
		$user=$this->User->read(null, $id);
		$this->Booking->recursive = 2;
		$this->Booking->unbindModel(array('belongsTo' => array('User')));
		$this->Booking->Staff->unbindModel(array('hasMany' => array('Booking'), 'belongsTo' => array('Religion', 'Language', 'Origin','Country')));
		$this->Booking->belongsTo['Staff']['fields'] = array('Staff.id','Staff.name','Staff.category_id');
		$this->Booking->Staff->belongsTo['Category']['fields'] = array('Category.name');
		$activeBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$id, 'Booking.status'=>'Pending'), 'order'=>array('Booking.date'=>'DESC')));
		$pastBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$id, 'Booking.status !='=>'Pending'), 'order'=>array('Booking.date'=>'DESC')));
		$this->set(compact('activeBookings', 'pastBookings'));
		if($user['User']['user_type']!=2)
		{
			$this->Session->setFlash(__('Invalid user.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}
	
	function admin_index($search = null){	
		$uid = $this->Session->read('Auth.User.id');
		$conditions = array('User.id !='=>$uid, 'User.user_type !='=>1);		
		if(isset($this->request->data['User']['key']) && $this->request->data['User']['key'] != null) {
			$conditions = array('User.id !='=>$uid, 'User.user_type !='=>1, 'or'=>array('User.email LIKE'=>'%'.trim($this->request->data['User']['key']).'%', 'User.emp-uid LIKE'=>'%'.trim($this->request->data['User']['key']).'%', 'User.name LIKE'=>'%'.trim($this->request->data['User']['key']).'%'));
		}
		$this->User->recursive=1;
		$orderby = array('User.id'=>'desc');
        $this->paginate=array('conditions'=>$conditions,'order'=>$orderby);
        
		//$this->paginate = array('order'=>$orderby);
		
		$this->set('users', $this->paginate());		
		$user_types = $this->user_types;
		$this->set(compact('user_types'));
	}

	function admin_indexsubadmin($search = null) {
		if($this->Session->read('Auth.User.user_type')!=1){
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		$uid = $this->Session->read('Auth.User.id');
		$WHERE = 'User.id != "'.$uid.'" AND User.user_type ="3"';	
		$this->User->recursive=1;
		$this->strCondition=$WHERE;		
        $this->paginate=array('conditions'=>array($this->strCondition), 'order' => array('User.id' => 'DESC'));
		$this->set('users', $this->paginate());
		$user_types = $this->user_types;
		$this->set(compact('user_types'));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$dup_chk = $this->User->find('count', array('conditions' => array('User.email' => $this->data['User']['email']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This email is not available.', true)));
			} else {
				$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
				$this->request->data['User']['user_type'] = 2;
				$this->request->data['User']['is_verified'] = 1;
				$this->request->data['User']['status'] = 1;
				$this->request->data['User']['registration_date'] = date('Y-m-d');
				$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').'12345');
				$this->User->create();
				if ($this->User->save($this->data)) {
					$uId = 'EM'.$this->User->id;
					$idMinLentgth = 5;
					$idLength = strlen($this->User->id);
					$lengtDiff = $idMinLentgth-$idLength;
					if($lengtDiff > 0){
						$midStr = '';
						for($iter = 0; $iter < $lengtDiff; $iter++){
							$midStr.='0';
						}
						$uId = 'EM'.$midStr.$this->User->id;
					}
					$this->User->savefield('emp-uid', $uId);
					$this->Session->setFlash(sprintf(__('User has been added successfully.', true), 'User'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('User could not be saved. Please, try again.', true));
				}
			}
		}
		$locations = $this->User->Location->find('list');
		$this->set(compact('locations'));
	}

	function admin_addsubadmin() {
		if($this->Session->read('Auth.User.user_type')!=1)
		{
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		if (!empty($this->data)) {
			$dup_chk = $this->User->find('count', array('conditions' => array('User.email' => $this->data['User']['email']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This email is not available.', true)));
			} else {
				$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
				$this->request->data['User']['user_type'] = 3;
				$this->request->data['User']['is_verified'] = 1;
				$this->request->data['User']['status'] = 1;
				$this->request->data['User']['is_admin'] = 1;
				$this->request->data['User']['registration_date'] = date('Y-m-d');
				$pwd=$this->request->data['User']['password'];
				$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').$this->request->data['User']['password']);
				$this->request->data['User']['acces']=implode($this->request->data['User']['access'],',');
				$this->User->create();
				if ($this->User->save($this->data)) {
					$uId = 'EM'.$this->User->id;
					$idMinLentgth = 5;
					$idLength = strlen($this->User->id);
					$lengtDiff = $idMinLentgth-$idLength;
					if($lengtDiff > 0){
						$midStr = '';
						for($iter = 0; $iter < $lengtDiff; $iter++){
							$midStr.='0';
						}
						$uId = 'EM'.$midStr.$this->User->id;
					}
					$this->User->savefield('emp-uid', $uId);
					$admin = $this->Setting->read(null,'1');
					$content='<div style="width:628px;">
									  <div class="clear"></div>
										<p style="font-family:Arial, Helvetica, sans-serif; font-size:16px;">Dear,'.$this->data['User']['name'].'<br/>
					You have been invited to be the user admin<br/>Your Login Detail:<br/><table><tr><td>User Name:</td><td>"'.$this->data['User']['email'].'"</td></tr><tr><td>Password:</td><td>"'.$pwd.'"</td></tr></table>Please login into the admin <a href="http://househelp4hire.com/admin/Users/login" target="_blank">Click</a> </p>
					<div>Thank You,<br/>
					'.$admin['Setting']['site_name'].'<br/>
					</div></div>';
						$mail_CC="";
						$mail_Body =$content;
						$mail_To=$this->data['User']['email'];	
						$mail_From = $admin['Setting']['noreply_email'];
						$mail_subject="You have been added as Admin By ".$admin['Setting']['site_name']." Admin";
						$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
					 $this->Send_HTML_Mail($mail_To, $mail_From, $mail_CC, $mail_subject, $mail_Body);
					$this->Session->setFlash(sprintf(__('Admin has been added successfully.', true), 'User'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'indexsubadmin'));
				} else {
					$this->Session->setFlash(__('Admin could not be saved. Please, try again.', true));
				}
			}
		}
		$locations = $this->User->Location->find('list');
		$this->set(compact('locations'));
	}

	function admin_edit($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user.', true));
			$this->redirect(array('action' => 'index'));
		}
		$user=$this->User->read(null, $id);
		if($user['User']['user_type']!=2)
		{
			$this->Session->setFlash(__('Invalid user.', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$dup_chk = $this->User->find('count', array('conditions' => array('User.email' => $this->data['User']['email'],'User.id !='=>$this->request->data['User']['id']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This email is not available.', true)));
			} else {
				$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
				if (!empty($this->request->data['User']['password']))
				{
					$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').$this->request->data['User']['password']);
				}
				else
				{
					unset($this->request->data['User']['password']);
				}
				$uId = 'EM'.$this->request->data['User']['id'];
				$idMinLentgth = 5;
				$idLength = strlen($this->request->data['User']['id']);
				$lengtDiff = $idMinLentgth-$idLength;
				if($lengtDiff > 0){
					$midStr = '';
					for($iter = 0; $iter < $lengtDiff; $iter++){
						$midStr.='0';
					}
					$uId = 'EM'.$midStr.$this->request->data['User']['id'];
				}
				$this->request->data['User']['emp-uid'] = $uId;
				if ($this->User->save($this->data)) {
					$this->Session->setFlash(sprintf(__('User has been updated successfully.', true), 'User'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('User could not be updated. Please, try again.', true));
				}
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		$locations = $this->User->Location->find('list');
		$this->set(compact('locations'));
	}

	function admin_subadminedit($id=null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid admin.', true));
			$this->redirect(array('action' => 'indexsubadmin'));
		}
		if($this->Session->read('Auth.User.user_type')!=1)
		{
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		if(!empty($this->data))
		{
			//$this->User->recursive = 0;
			$dup_chk = $this->User->find('count', array('conditions' => array('User.email' => $this->data['User']['email'],'User.id !='=>$this->request->data['User']['id']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This email is not available.', true)));
			} else {
			
				$this->request->data['User']['acces']=implode($this->request->data['User']['access'],',');
				if (!empty($this->request->data['User']['password']))
				{
					$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').$this->request->data['User']['password']);
				}
				else
				{
					unset($this->request->data['User']['password']);
				}
				$this->request->data['User']['name'] = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
				$this->request->data['User']['emp-uid'] = 'EM'.$this->request->data['User']['id'];
				if ($this->User->save($this->data)) {
				$this->Session->setFlash(sprintf(__('Admin has been updated successfully.', true), 'User'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'indexsubadmin'));
				} else {
					$this->Session->setFlash(__('Admin could not be updated. Please, try again.', true));
				}
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
			
		}
		$user=$this->User->read(null, $id);
		if($user['User']['user_type']!=3)
		{
			$this->Session->setFlash(__('Invalid admin.', true));
			$this->redirect(array('action' => 'indexsubadmin'));
		}
		$s=explode(',',$this->data['User']['acces']); 
		$locations = $this->User->Location->find('list');
		$this->set(compact('s', 'locations'));	
	}

    function admin_profile(){
		$this->User->recursive=0;
		$user=$this->User->read(null,$this->Session->read('Auth.User.id'));
		if(isset($this->request->data) && !empty($this->request->data))
		{
			$flag=false;
			$chk_email=$this->User->find('first', array('conditions'=>array('User.email'=>$this->request->data['User']['email'],'User.id !='=>$this->request->data['User']['id'])));
			if(!empty($chk_email) && $chk_email['User']['id']!='1')
			{
				$flag=true;
			}
			if (!empty($this->request->data['User']['oldPassword']) || !empty($this->request->data['User']['password']) || !empty($this->request->data['User']['confirmPassword']))
			{
				if(Security::hash(Configure::read('Security.salt').$this->request->data['User']['oldPassword'])!=$user['User']['password'])
				{
					$this->Session->setFlash(sprintf(__('Old password did not match with the provided password', true)));
				}
				else if(empty($this->request->data['User']['newPassword']))
				{
					$this->Session->setFlash(sprintf(__('Please enter the new password', true)));
				}
				else if($this->request->data['User']['newPassword']!=$this->request->data['User']['confirmPassword'])
				{
					$this->Session->setFlash(sprintf(__('Password is not confirmed correctly', true)));
				}
				else if(!$flag)
				{
					$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').$this->request->data['User']['newPassword']);
					$this->User->saveField('email',$this->request->data['User']['email']);
					$this->User->saveField('password',$this->request->data['User']['password']);
					$this->Session->setFlash(__('Your account information has been updated', true), 'default', array('class' => 'success'));
					$this->redirect(array('action'=>'profile'));
				}
				else if($flag)
				{
					$this->Session->setFlash(sprintf(__('An user with the same email already exists.', true)));
				}
					
			}
			else if(!$flag)
			{
				$this->User->saveField('email',$this->request->data['User']['email']);
				$this->Session->setFlash(__('Your account information has been updated', true), 'default', array('class' => 'success'));
				$this->redirect(array('action'=>'profile'));
			}
			else if($flag)
			{
				$this->Session->setFlash(sprintf(__('An user with the same email already exists.', true)));
			}
			
		}
		else
		{
			$this->request->data=$this->User->read(null,$this->Session->read('Auth.User.id'));
			$this->set(compact('user'));
		}
	}

    function admin_disapprove($id = null){
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid user', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$user=$this->User->read(null, $id);
			if($user['User']['user_type']!=2)
			{
				$this->Session->setFlash(__('Invalid user.', true));
				$this->redirect(array('action' => 'index'));
			}
			$this->User->id=$id;
			$this->User->saveField('status',0);			
			$this->Session->setFlash(sprintf(__('User has been deactivated successfully!', true), 'user'), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'users', 'action' => 'index'));
		}
	}

	function admin_approve($id = null){
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid user', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$user=$this->User->read(null, $id);
			if($user['User']['user_type']!=2)
			{
				$this->Session->setFlash(__('Invalid user.', true));
				$this->redirect(array('action' => 'index'));
			}
			$this->User->recursive = 0;
			$data = $this->User->read(null, $id);
			$this->User->id=$id;
			$this->User->saveField('status',1);	
			/*$mail_info = $this->EmailTemplate->find('first', array('fields' => array('EmailTemplate.subject', 'EmailTemplate.template'), 'conditions' => array('EmailTemplate.id' => 3)));
			$admin = $this->Setting->read(null,'1');
			if($mail_info)
			{
				$LOGIN_LINK = '<a href ="'.DOMAIN_NAME_PATH.'pro/users/login">Click Here</a>';
				$CONTACTUS_LINK = '<a href ="'.DOMAIN_NAME_PATH.'ContactUs">contact us</a>';
				$mail_To = $data['User']['email'];
				$mail_From = $admin['Setting']['contact_email'];
				$mail_subject = $mail_info['EmailTemplate']['subject'];
				$mail_Body = str_replace("[MEMBER]", $data['User']['first_name'], $mail_info['EmailTemplate']['template']);
				$mail_Body= str_replace("[EMAIL]", $data['User']['email'], $mail_Body);
				$mail_Body= str_replace("[LOGIN_LINK]", $LOGIN_LINK, $mail_Body);
				$mail_Body= str_replace("[CONTACTUS_LINK]", $CONTACTUS_LINK, $mail_Body);
				$this->Send_HTML_Mail($mail_To, $mail_From,'', $mail_subject, nl2Br($mail_Body));
			}*/			
			$this->Session->setFlash(sprintf(__('User has been activated successfully!', true), 'user'), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'users', 'action' => 'index'));
		}
	}

    function admin_delete($id = null) {
		$uid = $this->Session->read('Auth.User.id');
		if (!$id || $id == $uid || $id == '1') {
			$this->Session->setFlash(__('Invalid id for user', true));
			$this->redirect(array('action'=>'index'));
		}
		$user=$this->User->read(null, $id);
		if($user['User']['user_type']!=2)
		{
			$this->Session->setFlash(__('Invalid user.', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted successfully', true), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	function admin_sendtomembers() {
		//echo "Still Working Now..................";
		if (!empty($this->request->data)) 
		{
			//pr($this->request->data);
			//die("ANB");
			if($this->request->data['User']['template_list'] == '') {
				$this->Session->setFlash(sprintf(__('Please select template.', true)));
				$this->redirect(array('action' => 'sendtomembers'));
			}
			if($this->request->data['User']['mode'] == 'send_mail') {
				if(empty($this->request->data['User']['ids'])){
					$this->Session->setFlash(sprintf(__('Please select atleast one subscriber.', true)));
					$this->redirect(array('action' => 'sendtomembers'));
				} 
				$this->User->recursive = 0;
				$subscribers = $this->User->find('all', array('conditions' => array('User.id' => $this->request->data['User']['ids'])));
			} else {
				$this->User->recursive = 0;
				$subscribers = $this->User->find('all', array('conditions' => array('User.user_type > ' => '1', 'User.active' => '1')));
			}
			if(!empty($subscribers)) {
				$this->Setting->recursive = 0;
				$admin = $this->Setting->read(null,'1');	
				$this->NewsletterTemplate->recursive = 0;
				$newsletter = $this->NewsletterTemplate->read(null,$this->request->data['User']['template_list']);	
				$mail_From = $admin['Setting']['contact_email'];
				$mail_subject = $newsletter['NewsletterTemplate']['subject_line'];
				foreach($subscribers as $subscriber) {
					$mail_Body = $newsletter['NewsletterTemplate']['template_body'];
					$mail_Body = str_replace("[MEMBER]", $subscriber['User']['first_name'], $mail_Body);
					$mail_To = $subscriber['User']['email'];							
					$this->Send_HTML_Mail($mail_To, $mail_From,'', $mail_subject, nl2Br($mail_Body));
				}
			}
			$this->Session->setFlash(sprintf(__('Newsletter has been sent successfully.', true)), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'sendtomembers'));				
		}
		$this->set("title_for_layout", "Administration Panel :: List Of Users");
		$this->NewsletterTemplate->recursive = 0;
		$template_list = $this->NewsletterTemplate->find('list', array('conditions' => array('NewsletterTemplate.is_active' => 'Y'), 'fields' => array('NewsletterTemplate.id', 'NewsletterTemplate.subject_line')));
		$this->User->recursive = 0;
		$this->paginate = array('conditions' => array('User.user_type >' => '1'));
		$users = $this->paginate();
		$user_types = $this->user_types;
		$this->set(compact('users', 'template_list', 'user_types'));
	}

	function admin_suspend($id = null){
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid admin', true)));
			$this->redirect(array('action' => 'indexsubadmin'));
		}
		else
		{
			$user=$this->User->read(null, $id);
			if($user['User']['user_type']!=3)
			{
				$this->Session->setFlash(__('Invalid admin.', true));
				$this->redirect(array('action' => 'indexsubadmin'));
			}
			$this->User->id=$id;
			$this->User->saveField('status',0);
			$this->Session->setFlash(sprintf(__('Admin suspended successfully!', true), 'user'), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'users', 'action' => 'indexsubadmin'));
		}
	}

	function admin_activate($id = null){
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid Admin', true)));
			$this->redirect(array('action' => 'indexsubadmin'));
		}
		else
		{
			$user=$this->User->read(null, $id);
			if($user['User']['user_type']!=3)
			{
				$this->Session->setFlash(__('Invalid admin.', true));
				$this->redirect(array('action' => 'indexsubadmin'));
			}
			$this->User->updateAll(array('User.status' => 1), array('User.id' => $id));
			$this->Session->setFlash(sprintf(__('Admin activated successfully!', true), 'user'), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'users', 'action' => 'indexsubadmin'));
		}
	}

    function admin_subadmindelete($id = null) {
		if (!$id || $id == '1') {
			$this->Session->setFlash(__('Invalid id for admin', true));
			$this->redirect(array('action'=>'indexsubadmin'));
		}
		//$prop=$this->User->find('list', array('conditions'=>array('Property.user_id'=>$id), 'fields' => 'Property.id'));
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('Admin deleted successfully', true), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'indexsubadmin'));
		}
		$this->Session->setFlash(__('Admin was not deleted', true));
		$this->redirect(array('action' => 'indexsubadmin'));
	}

	function admin_login(){
		//echo Security::hash(Configure::read('Security.salt') . 'admin123');
		if ($this->Auth->login()) {
            $this->redirect($this->Auth->redirect());
        } 
		if(!empty($this->request->data)){
            $this->Session->setFlash(__('Invalid username or password, try again'));
        }
	}

	function admin_logout(){
		$this->Session->delete('Auth.id');
		$this->redirect($this->Auth->logout());
	}

	function vet_logout(){
		$this->Session->delete('Auth.id');
		$this->redirect($this->Auth->logout());
	}
	
	public function agent_dashboard(){
		// $this->layout = 'default';
		// var_dump('hgjgjjg');exit;
	}

	public function vet_dashboard(){
		// $this->layout = 'default';
		// var_dump('hgjgjjg');exit;
		$vet_id = $this->Session->read('Auth.User.id');
		$query = $this->VettingCompany->query("SELECT * FROM hh_vetting_companies WHERE user_id = '$vet_id'");
		// var_dump($query);exit;
		$vet_com_id = $query[0]['hh_vetting_companies']['id'];
		$this->Session->write('vet_com_id', $vet_com_id);

		$this->redirect(array('controller'=>'Vetted_candidates','action' => 'index'));
	}

	public function agent_profile(){

	}

	public function admin_dashboard() {

	}

	function admin_agents($search = null) {
		if($this->Session->read('Auth.User.user_type')!=1 && $this->Session->read('Auth.User.user_type')!=3){
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		$uid = $this->Session->read('Auth.User.id');
		
		$this->Agent->recursive=1;
		
        $agents = $this->paginate('Agent');
		$this->set('users', $agents);
		$user_types = $this->user_types;
		$this->set(compact('user_types'));
	}

	function admin_addagents() {
		if($this->Session->read('Auth.User.user_type')!=1 && $this->Session->read('Auth.User.user_type')!=3){
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		if (!empty($this->data)) {
			$dup_chk = $this->Agent->find('count', array('conditions' => array('Agent.phone' => $this->data['Agent']['phone']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This phone is not available.', true)));
			} else {
				$this->request->data['Agent']['name'] = $this->request->data['Agent']['first_name'].' '.$this->request->data['User']['last_name'];
				$this->request->data['Agent']['user_type'] = 4;
				$this->request->data['Agent']['is_verified'] = 1;
				$this->request->data['Agent']['status'] = 1;
				$this->request->data['Agent']['is_admin'] = 0;
				$this->request->data['Agent']['registration_date'] = date('Y-m-d');
				
				//$pwd=$this->request->data['User']['password'];
				//$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').$this->request->data['User']['password']);
				//$this->request->data['User']['acces']=implode($this->request->data['User']['access'],',');
				
					
				if(isset($this->request->data['Agent']['image']['name']) && $this->request->data['Agent']['image']['name'] != '')
				{
					if($this->request->data['Agent']['image']['type'] == 'image/png' || $this->request->data['Agent']['image']['type'] == 'image/PNG' || $this->request->data['Agent']['image']['type'] == 'image/jpg' || $this->request->data['Agent']['image']['type'] == 'image/JPG' || $this->request->data['Agent']['image']['type'] == 'image/jpeg' || $this->request->data['Agent']['image']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Agent']['image']['name'];

						move_uploaded_file($this->request->data['Agent']['image']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Agent']['image'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				if(is_array($this->request->data['Agent']['image']))
				{
					$this->request->data['Agent']['image']='';
				}
				$this->Agent->create();
				if ($this->Agent->save($this->data)) {
					$uId = 'EM'.$this->Agent->id;
					$idMinLentgth = 5;
					$idLength = strlen($this->Agent->id);
					$lengtDiff = $idMinLentgth-$idLength;
					if($lengtDiff > 0){
						$midStr = '';
						for($iter = 0; $iter < $lengtDiff; $iter++){
							$midStr.='0';
						}
						$uId = 'EM'.$midStr.$this->Agent->id;
					}
					$this->Agent->savefield('emp-uid', $uId);
					$admin = $this->Setting->read(null,'1');
					
					$this->Session->setFlash(sprintf(__('Agent has been added successfully.', true), 'Agent'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'agents'));
				} else {
					$this->Session->setFlash(__('Agent could not be saved. Please, try again.', true));
				}
			}
		}
		$locations = $this->User->Location->find('list');
		$this->set(compact('locations'));
	}

	function admin_editagents($id=null){
		if (!$id) {
			$this->Session->setFlash(__('Invalid admin.', true));
			$this->redirect(array('action' => 'indexsubadmin'));
		}
		if($this->Session->read('Auth.User.user_type')!=1 && $this->Session->read('Auth.User.user_type')!=3){
			$this->Session->setFlash(__('You are not allowed to access this', true));
			$this->redirect(array('action'=>'profile'));
		}
		if(!empty($this->data))
		{
			//$this->User->recursive = 0;
			$dup_chk = $this->Agent->find('count', array('conditions' => array('Agent.phone' => $this->data['Agent']['phone'],'Agent.id !='=>$this->request->data['Agent']['id']), 'limit' => 1));
			if($dup_chk > 0){
				$this->Session->setFlash(sprintf(__('This phone is not available.', true)));
			} else {
				/*
				$this->request->data['User']['acces']=implode($this->request->data['User']['access'],',');
				
				if (!empty($this->request->data['User']['password']))
				{
					$this->request->data['User']['password']=Security::hash(Configure::read('Security.salt').$this->request->data['User']['password']);
				}
				else
				{
					unset($this->request->data['User']['password']);
				}
				*/
				$this->request->data['Agent']['name'] = $this->request->data['Agent']['first_name'].' '.$this->request->data['Agent']['last_name'];
				$this->request->data['Agent']['emp-uid'] = 'EM'.$this->request->data['Agent']['id'];
				if(isset($this->request->data['Agent']['image']['name']) && $this->request->data['Agent']['image']['name'] != '')
				{
					if($this->request->data['Agent']['image']['type'] == 'image/png' || $this->request->data['Agent']['image']['type'] == 'image/PNG' || $this->request->data['Agent']['image']['type'] == 'image/jpg' || $this->request->data['Agent']['image']['type'] == 'image/JPG' || $this->request->data['Agent']['image']['type'] == 'image/jpeg' || $this->request->data['Agent']['image']['type'] == 'image/JPEG')
					{
						$filename=rand(). $this->request->data['Agent']['image']['name'];

						move_uploaded_file($this->request->data['Agent']['image']['tmp_name'], PAGE_IMAGES.$filename);
						$this->request->data['Agent']['image'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
					}
				}
				if(is_array($this->request->data['Agent']['image']))
				{
					$this->request->data['Agent']['image']=$this->request->data['Agent']['image_name'];
				}
				if ($this->Agent->save($this->data)) {
				$this->Session->setFlash(sprintf(__('Agent has been updated successfully.', true), 'Agent'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'agents'));
				} else {
					$this->Session->setFlash(__('Agent could not be updated. Please, try again.', true));
				}
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Agent->read(null, $id);
			
		}
		$user=$this->Agent->read(null, $id);
		
		//$s=explode(',',$this->data['User']['acces']); 
		$locations = $this->User->Location->find('list');
		$this->set(compact('s', 'locations'));	
		$this->set('user',$user);
	}

	function admin_agentdelete($id = null) {
		if (!$id || $id == '1') {
			$this->Session->setFlash(__('Invalid id for admin', true));
			$this->redirect(array('action'=>'indexsubadmin'));
		}
		
		if ($this->Agent->delete($id)) {
			$this->Session->setFlash(__('Agent deleted successfully', true), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'agents'));
		}
		$this->Session->setFlash(__('Agent was not deleted', true));
		$this->redirect(array('action' => 'agents'));
	}

	public function vettinCompanies($id=null){
		$query = $this->User->query("SELECT * FROM hh_vetting_companies WHERE vetting_category_id = '$id'");
		echo json_encode($query);
		exit;
	}

	public function vetPayment(){

		$staff_id = $this->Session->read('Auth.User.id');
		$jobseeker_id = $this->Session->read('jobseeker_id');
		$comId = $this->request->data['comId'];
		$catComId = $this->request->data['catComId'];
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);

		$card = [
		  "card_no" => $this->request->data['card_no'],
		  "cvv" => $this->request->data['cvv'],
		  "expiry_month" => $this->request->data['expiry_month'],
		  "expiry_year" => $this->request->data['expiry_year'],
		  "pin" =>$this->request->data['pin'],
		  "card_type" => "" //optional parameter. only needed if card was issued by diamond card
		];
		$custId = "76464"; //your users customer id
		$currency = Currencies::NAIRA; //currency to charge the card
		$authModel = AuthModel::PIN; //can be BVN, NOAUTH, PIN, etc
		$narration = "Payment for Vetting";
		$responseUrl = ""; //callback url
		$country = Countries::NIGERIA;
		$amount =$this->request->data['amount'];
		$response = Card::charge($card, $amount, $custId, $currency, $country, $authModel, $narration, $responseUrl);
		$data = $response->getResponseData();
		$ref = $data['data']['transactionreference'];
		$this->User->query("insert into hh_vetted_candidates (vetting_category_id, vetting_company_id,staff_id,payment_ref) VALUES ( '$catComId','$comId', '$jobseeker_id','$ref' )");
		echo json_encode($response->getResponseData());
		exit;
	}

	public function validateOtp(){
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
		$ref = $this->request->data['ref'];
		$otp = $this->request->data['otp'];
		$cardType = "";
		$result = Card::validate($ref, $otp, $cardType = "");
		if($result->isSuccessfulResponse()){
			$this->User->query("update hh_vetted_candidates set payment_status = 1 where payment_ref = '$ref'");
		  	echo "Successful";
		  	exit;
		}else{
			echo 'Failed';
			exit;
		}
	}

	public function bvnVet(){
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
		$bvn = $this->request->data['bvn'];
		$result = Bvn::verify($bvn, Flutterwave::SMS);
		if ($result->isSuccessfulResponse()) {
		  echo json_encode($result->responseData);
		  exit;
		} else {
		  echo 'Failed';
		  exit;
		}
	}

	public function validateOtpBvn(){
		$merchantKey = "tk_fLRnODPQzv"; //merchant key on flutterwave dev portal
		$apiKey = "tk_QoGhkH36uy8rEIW9LtZN"; //merchant api key on flutterwave dev portal
		$env = "staging"; //can be staging or production
		Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
		$ref = $this->request->data['ref'];
		$otp = $this->request->data['otp'];
		$bvn = $this->request->data['bvn'];
		$result = Bvn::validate($bvn, $otp, $ref);
		if($result->isSuccessfulResponse()){
		  	echo json_encode($result->responseData);
		  	exit;
		}else{
			echo 'Failed';
			exit;
		}
	}

	public function jobseeker_dashboard(){
		$this->User->recursive = 2;
		$profileInfo = $this->User->read(null, $this->Session->read('Auth.User.id'));
		$staff_id = $this->Session->read('Auth.User.id');
		$query = $this->Staff->query("SELECT * FROM hh_staffs WHERE user_id = '$staff_id'");
		// var_dump($query);exit;
		$jobseeker_id = $query[0]['hh_staffs']['id'];
		$this->Session->write('jobseeker_id', $jobseeker_id);
		$maximumPoints  = 100;
		$point = 0;
		if($query[0]['hh_staffs']['email'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['age'] != '0')
		$point+=4;
		if($query[0]['hh_staffs']['sex'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['country_id'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['profile_description'] != '')
			$point+=8;
		if($query[0]['hh_staffs']['mini_biography'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['experience'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['work_experience'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['marital_status'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['number_of_children'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['origin_id'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['category_id'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['english_speaking'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['language_id'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['religion_id'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['base_salary'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['accommodation'] != '0')
			$point+=4;
		if($query[0]['hh_staffs']['contact_number'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['mobile_number'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['image1'] != '')
			$point+=8;
		if($query[0]['hh_staffs']['image2'] != '')
			$point+=8;
		if($query[0]['hh_staffs']['hobbies'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['bvn'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['identification'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['nationality'] != '')
			$point+=4;
		if($query[0]['hh_staffs']['immigration_status'] != '')
			$point+=0;
		$percentage = ($point*$maximumPoints)/100;
		$this->set('percentage', $percentage);
		$this->set('jobseeker_id', $jobseeker_id);
		$newsletter = $this->Email->find('list', array('fields'=>array('Email.email'), 'conditions'=>array('Email.email'=>$this->Session->read('Auth.User.email'))));
		$countries = $this->User->Country->find('list', array('order'=>array('Country.name'=>'ASC')));
		$this->set(compact('profileInfo', 'newsletter', 'countries'));
		$vetstatus = $this->User->query("select a.id, a.name, b.status from hh_vetting_categories a left join hh_vetted_candidates b on a.id = b.vetting_category_id where b.staff_id ='$jobseeker_id' and b.payment_status = 1");
		$vetCategories = $this->VettingCategory->query("select * from hh_vetting_categories");
		$this->set('vetCategories', $vetCategories);
		$this->set('vetstatus', $vetstatus);
		$siteSetting = $this->viewVars['siteSetting'];
		if($siteSetting['Setting']['booking_module']==1){
			$this->Booking->recursive = 2;
			$this->Booking->unbindModel(array('belongsTo' => array('User')));
			$this->Booking->Staff->unbindModel(array('hasMany' => array('Booking'), 'belongsTo' => array('Religion', 'Language', 'Origin','Country')));
			$this->Booking->belongsTo['Staff']['fields'] = array('Staff.id','Staff.name','Staff.category_id','Staff.friendly_url');
			$this->Booking->Staff->belongsTo['Category']['fields'] = array('Category.name');
			$activeBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$this->Session->read('Auth.User.id'), 'Booking.status'=>'Pending'), 'order'=>array('Booking.date'=>'DESC')));
			$selectedBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$this->Session->read('Auth.User.id'), 'Booking.status'=>'Selected'), 'order'=>array('Booking.date'=>'DESC')));
			$pastBookings = $this->Booking->find('all', array('conditions'=>array('Booking.user_id'=>$this->Session->read('Auth.User.id'), 'Booking.status !='=>array('Pending', 'Selected')), 'order'=>array('Booking.date'=>'DESC')));
			$this->set(compact('activeBookings', 'pastBookings', 'selectedBookings'));
		}
		
		$this->Payment->recursive = 0;
		$this->Payment->unbindModel(array('hasMany' => array(), 'belongsTo' => array('User')));
		$payments = $this->Payment->find('all', array('conditions'=>array('Payment.user_id'=>$this->Session->read('Auth.User.id')), 'order'=>array('Payment.id'=>'DESC')));
		$this->set('payments', $payments);
	}

	public function jobseeker_profile($id = null) {
		$this->Staff->id = $id;
		if (!$this->Staff->exists()) {
			$this->Session->setFlash(sprintf(__('Invalid staff', true)));
		}
		$staffs=$this->Staff->read(null, $id);
		$oldpic1=$staffs['Staff']['image1'];
		$oldpic2=$staffs['Staff']['image2'];
		$oldvideo=$staffs['Staff']['video'];
		$oldidentification=$staffs['Staff']['identification'];
		if ($this->request->is('post') || $this->request->is('put')){
			// var_dump($this->request->data);exit;
			$exist_data = $this->Staff->find('list', array('conditions' => array('Staff.contact_number' => $this->request->data['Staff']['contact_number'], 'Staff.id !=' => $id)));
			if(empty($exist_data)){
				$CategoryCode = $this->Category->field('Category_code', array('Category.id'=>$this->request->data['Staff']['category_id']));
				$friendlyurl = $this->request->data['Staff']['name'];
				foreach($this->notAllowedWord AS $arrNAWord)
				{
					$friendlyurl = trim(strtolower(str_replace($arrNAWord, '-', $friendlyurl)));
				}
				$friendlyurl = str_replace(' ', '-', $friendlyurl);
				$chk = $this->Staff->find('first', array('conditions' => array('Staff.friendly_url' => $friendlyurl, 'Staff.id !='=>$id)));
				$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				if(!empty($chk)){
					$this->request->data['Staff']['friendly_url'] = $friendlyurl.'-'.$id;
				} else {
					$this->request->data['Staff']['friendly_url'] = $friendlyurl;
				}
				if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != '')
				{
					if($this->request->data['Staff']['image1']['type'] == 'image/png' || $this->request->data['Staff']['image1']['type'] == 'image/PNG' || $this->request->data['Staff']['image1']['type'] == 'image/jpg' || $this->request->data['Staff']['image1']['type'] == 'image/JPG' || $this->request->data['Staff']['image1']['type'] == 'image/jpeg' || $this->request->data['Staff']['image1']['type'] == 'image/JPEG')
					{
						$filename=rand().trim(addslashes($this->request->data['Staff']['image']['name']));
						$filename = str_replace(' ', '_', $filename);
						if(move_uploaded_file($this->request->data['Staff']['image1']['tmp_name'], PAGE_IMAGES.$filename)){};
						$this->request->data['Staff']['image1'] = $filename;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
						$this->redirect(array('action' => 'dashboard'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image1']);
				}
				if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != '')
				{
					if($this->request->data['Staff']['image2']['type'] == 'image/png' || $this->request->data['Staff']['image2']['type'] == 'image/PNG' || $this->request->data['Staff']['image2']['type'] == 'image/jpg' || $this->request->data['Staff']['image2']['type'] == 'image/JPG' || $this->request->data['Staff']['image2']['type'] == 'image/jpeg' || $this->request->data['Staff']['image2']['type'] == 'image/JPEG')
					{
						$filename2=rand().trim(addslashes($this->request->data['Staff']['image2']['name']));
						$filename2 = str_replace(' ', '_', $filename2);
						move_uploaded_file($this->request->data['Staff']['image2']['tmp_name'], PAGE_IMAGES.$filename2);
						$this->request->data['Staff']['image2'] = $filename2;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
						$this->redirect(array('action' => 'dashboard'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['image2']);
				}

				if(isset($this->request->data['Staff']['video']['name']) && $this->request->data['Staff']['video']['name'] != '')
				{
					if($this->request->data['Staff']['video']['type'] == 'video/mp4' || $this->request->data['Staff']['video']['type'] == 'video/mpeg' || $this->request->data['Staff']['video']['type'] == 'video/3gp' || $this->request->data['Staff']['video']['size'] >= 10485760)
					{
						$filename3 = substr($this->request->data['Staff']['video']['name'], 0, 10);
						$filename3=rand().trim(addslashes($filename3));
						$filename3 = str_replace(' ', '_', $filename3);
						move_uploaded_file($this->request->data['Staff']['video']['tmp_name'], UPLOADED_VIDEO.$filename3);
						$this->request->data['Staff']['video'] = $filename3;
					}
					else
					{
						$this->Session->setFlash(__('Invalid video type.Please upload mp4/mpeg file'));
						$this->redirect(array('action' => 'dashboard'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['video']);
				}

				if(isset($this->request->data['Staff']['identification']['name']) && $this->request->data['Staff']['identification']['name'] != '')
				{
					if($this->request->data['Staff']['identification']['type'] == 'image/png' || $this->request->data['Staff']['identification']['type'] == 'image/PNG' || $this->request->data['Staff']['identification']['type'] == 'image/jpg' || $this->request->data['Staff']['identification']['type'] == 'image/JPG' || $this->request->data['Staff']['identification']['type'] == 'image/jpeg' || $this->request->data['Staff']['identification']['type'] == 'image/JPEG')
					{
						$filename4=rand().trim(addslashes($this->request->data['Staff']['identification']['name']));
						$filename4 = str_replace(' ', '_', $filename4);
						move_uploaded_file($this->request->data['Staff']['identification']['tmp_name'], PAGE_IMAGES.$filename4);
						$this->request->data['Staff']['identification'] = $filename4;
					}
					else
					{
						$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
						$this->redirect(array('action' => 'dashboard'));
					}
				}
				else
				{
					unset($this->request->data['Staff']['identification']);
				}
				
				$preferedLocationArr = $this->request->data['Staff']['prefered_location'];
				unset($this->request->data['Staff']['prefered_location']);
				$spokenLanguageArr = $this->request->data['Staff']['spoken_languages'];
				unset($this->request->data['Staff']['spoken_languages']);
				$preferedLocalityArr = $this->request->data['Staff']['prefered_locality'];
				unset($this->request->data['Staff']['prefered_locality']);
				$preferedEducationArr = $this->request->data['Staff']['prefered_education'];
				unset($this->request->data['Staff']['prefered_education']);
				
				$uId = $CategoryCode.$this->request->data['Staff']['id'];
				$idMinLentgth = 5;
				$idLength = strlen($this->request->data['Staff']['id']);
				$lengtDiff = $idMinLentgth-$idLength;
				if($lengtDiff > 0){
					$midStr = '';
					for($iter = 0; $iter < $lengtDiff; $iter++){
						$midStr.='0';
					}
					$uId = $CategoryCode.$midStr.$this->request->data['Staff']['id'];
				}
				$staffCategoryArr = $this->request->data['Staff']['category_id'];
				unset($this->request->data['Staff']['category_id']);
				
				$this->request->data['Staff']['staff-uid'] = $uId;
				if($this->request->data['Staff']['hired']=="1")
				{
					$this->request->data['Staff']['hired_date']=date('Y-m-d',strtotime('+3 days'));
				}
					
				//pr($this->request->data);die;
				if ($this->Staff->save($this->request->data)) {
					if(isset($this->request->data['Staff']['image1']['name']) && $this->request->data['Staff']['image1']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic1);
					}
					if(isset($this->request->data['Staff']['image2']['name']) && $this->request->data['Staff']['image2']['name'] != ''){
						unlink(PAGE_IMAGES.$oldpic2);
					}
					if(isset($this->request->data['Staff']['identification']['name']) && $this->request->data['Staff']['identification']['name'] != ''){
						unlink(PAGE_IMAGES.$oldidentification);
					}
					if(isset($this->request->data['Staff']['video']['name']) && $this->request->data['Staff']['video']['name'] != ''){
						unlink(UPLOADED_VIDEO.$oldvideo);
					}
					if($preferedLocationArr != ''){
						if(count($preferedLocationArr)>0){
							$checking = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
							if(count($checking)>0){
								$this->StaffPreferedLocation->delete($checking);
							}
							$locationRow['StaffPreferedLocation']['staff_id'] = $id;
							foreach($preferedLocationArr as $preferedLocation){
								$locationRow['StaffPreferedLocation']['location_id'] = $preferedLocation;
								$this->StaffPreferedLocation->create();
								$this->StaffPreferedLocation->save($locationRow);
							}
						}
					}
					if($preferedLocalityArr != ''){
						if(count($preferedLocalityArr)>0){
							$checking1 = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
							if(count($checking1)>0){
								$this->StaffPreferedLocality->delete($checking1);
							}
							$localityRow['StaffPreferedLocality']['staff_id'] = $id;
							foreach($preferedLocalityArr as $preferedLocality){
								$localityRow['StaffPreferedLocality']['locality_id'] = $preferedLocality;
								$this->StaffPreferedLocality->create();
								$this->StaffPreferedLocality->save($localityRow);
							}
						}
					}
					if($spokenLanguageArr != ''){
						if(count($spokenLanguageArr)>0){
							$checking2 = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
							if(count($checking2)>0){
								$this->StaffSpokenLanguage->delete($checking2);
							}
							$languageRow['StaffSpokenLanguage']['staff_id'] = $id;
							foreach($spokenLanguageArr as $spokenLanguage){
								$languageRow['StaffSpokenLanguage']['language_id'] = $spokenLanguage;
								$this->StaffSpokenLanguage->create();
								$this->StaffSpokenLanguage->save($languageRow);
							}
						}
					}
					if($preferedEducationArr != ''){
						if(count($preferedEducationArr)>0){
							$checking3 = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
							if(count($checking3)>0){
								$this->StaffPreferedEducation->delete($checking3);
							}
							$educationRow['StaffPreferedEducation']['staff_id'] = $id;
							foreach($preferedEducationArr as $education){
								$educationRow['StaffPreferedEducation']['education_id'] = $education;
								$this->StaffPreferedEducation->create();
								$this->StaffPreferedEducation->save($educationRow);
							}
						}
					}
					if($staffCategoryArr != ''){
						if(count($staffCategoryArr)>0){
							$checking4 = $this->StaffCategory->find('list', array('fields'=>'StaffCategory.id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
							if(count($checking4)>0){
								$this->StaffCategory->delete($checking4);
							}
							$categoryRow['StaffCategory']['staff_id'] = $id;
							foreach($staffCategoryArr as $category){
								$categoryRow['StaffCategory']['category_id'] = $category;
								$this->StaffCategory->create();
								$this->StaffCategory->save($categoryRow);
							}
						}
					}
					$this->Session->setFlash(sprintf(__('The staff has been updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'dashboard'));
				} else {
					$this->Session->setFlash(__('The staff could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('This phone has already been registered.', true)));
			}
		} else {
			$this->request->data = $this->Staff->read(null, $id);
		}
		$countries = $this->Staff->Country->find('list');
		$origins = $this->Staff->Origin->find('list');
		$categories = $this->Staff->Category->find('list');
		$languages = $this->Staff->Language->find('list');
		$locations = $this->Location->find('list', array('conditions'=>array('Location.service_availability'=>'Yes')));
		$localities = $this->Locality->find('list');
		$religions = $this->Staff->Religion->find('list');
		$educations = $this->Education->find('list');
		$ploc=array();
		$locDisp = $this->StaffPreferedLocation->find('list', array('fields'=>'StaffPreferedLocation.location_id', 'conditions'=>array('StaffPreferedLocation.staff_id'=>$id)));
		if(count($locDisp)>0)
		{
			$ploc = array_values($locDisp);
		}
		$ploca=array();
		$locaDisp = $this->StaffPreferedLocality->find('list', array('fields'=>'StaffPreferedLocality.locality_id', 'conditions'=>array('StaffPreferedLocality.staff_id'=>$id)));
		if(count($locaDisp)>0)
		{
			$ploca = array_values($locaDisp);
		}
		$pLang=array();
		$langDisp = $this->StaffSpokenLanguage->find('list', array('fields'=>'StaffSpokenLanguage.language_id', 'conditions'=>array('StaffSpokenLanguage.staff_id'=>$id)));
		if(count($langDisp)>0)
		{
			$pLang = array_values($langDisp);
		}
		$pEdu=array();
		$eduDisp = $this->StaffPreferedEducation->find('list', array('fields'=>'StaffPreferedEducation.education_id', 'conditions'=>array('StaffPreferedEducation.staff_id'=>$id)));
		if(count($eduDisp)>0)
		{
			$pEdu = array_values($eduDisp);
		}
		
		$pCat=array();
		$catDisp = $this->StaffCategory->find('list', array('fields'=>'StaffCategory.category_id', 'conditions'=>array('StaffCategory.staff_id'=>$id)));
		if(count($catDisp)>0)
		{
			$pCat = array_values($catDisp);
		}
		
		$agents_list=array();
		$agents=$this->Agent->find('list', array('fields'=>'Agent.name', 'order'=>array('Agent.id'=>'ASC')));
		
		$plans_list["0"]="Direct";
		foreach($agents as $row=>$value) {
    		$plans_list["{$row}"] = "{$value}";
        }
  		$this->set('plans_list', $plans_list);
  		
  		$immigration_list[""] = "Kind Select Your Status ";
  		$immigration_list["Valid Resident Card"] = "Valid Resident Card";
  		$immigration_list["Resident Card Required"] = "Resident Card Required";
  		$this->set('immigration_list', $immigration_list);
  		
  		$nationality_list["Benin"] = "Benin";
  		$nationality_list["Burkina Faso"] = "Burkina Faso";
  		
  		$nationality_list["Cape Verde"] = "Cape Verde";
  		$nationality_list["The Gambia"] = "The Gambia";
  		
  		$nationality_list["Ghana"] = "Ghana";
  		$nationality_list["Guinea"] = "Guinea";
  		$nationality_list["Guinea-Bissau"] = "Guinea-Bissau";
  		$nationality_list["Ivory Coast"] = "Ivory Coast";
  		$nationality_list["Liberia"] = "Liberia";
  		$nationality_list["Mali"] = "Mali";
  		$nationality_list["Mali Federation"] = "Mali Federation";
  		$nationality_list["Mauritania"] = "Mauritania";
  		$nationality_list["Niger"] = "Niger";
  		$nationality_list["Nigeria"] = "Nigeria";
  		$nationality_list["Senegal"] = "Senegal";
  		$nationality_list["Sierra Leone"] = "Sierra Leone";
  		$nationality_list["Togo"] = "Togo";
  		
  		$this->set('nationality_list', $nationality_list);
		$this->set(compact('countries', 'origins', 'categories', 'languages','locations','localities','ploc','ploca','pLang','pEdu','staffs','religions','educations','pCat'));
	}
	public function admin_import_export($pass = null) {
		$model = $this->modelClass;
		$subquery=new stdClass();
		$subquery->type="expression";
		$subquery->value="id IN (select distinct user_id from hh_payments)";
		$options['conditions'][]=$subquery;
		$this->backup($model,$options);
		$this->set('modelName', $model);
	}
	
}
?>