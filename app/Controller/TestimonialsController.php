<?php
App::uses('AppController', 'Controller');
/**
 * Testimonials Controller
 *
 * @property Testimonial $Testimonial
 */
class TestimonialsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Testimonial->recursive = 0;
		$this->set('testimonials', $this->paginate());
	}
	public function admin_index() {
		$this->Testimonial->recursive = 0;
		$this->set('testimonials', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Testimonial->id = $id;
		if (!$this->Testimonial->exists()) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		$this->set('testimonial', $this->Testimonial->read(null, $id));
	}
	public function admin_view($id = null) {
		$this->Testimonial->id = $id;
		if (!$this->Testimonial->exists()) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		$this->set('testimonial', $this->Testimonial->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Testimonial->create();
			if ($this->Testimonial->save($this->request->data)) {
				$this->Session->setFlash(__('The testimonial has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'));
			}
		}
	}
	public function admin_add() {
		if ($this->request->is('post')) {
			//pr($this->request->data);die;
			if(isset($this->request->data['Testimonial']['image']['name']) && $this->request->data['Testimonial']['image']['name'] != ''){
				if($this->request->data['Testimonial']['image']['type'] == 'image/png' || $this->request->data['Testimonial']['image']['type'] == 'image/PNG' || $this->request->data['Testimonial']['image']['type'] == 'image/jpg' || $this->request->data['Testimonial']['image']['type'] == 'image/JPG' || $this->request->data['Testimonial']['image']['type'] == 'image/jpeg' || $this->request->data['Testimonial']['image']['type'] == 'image/JPEG'){
					move_uploaded_file($this->request->data['Testimonial']['image']['tmp_name'], PAGE_IMAGES.$this->request->data['Testimonial']['image']['name']);
					$this->request->data['Testimonial']['image'] = $this->request->data['Testimonial']['image']['name'];
					$this->request->data['Testimonial']['date'] = date('Y-m-d');
					$this->Testimonial->create();
					if ($this->Testimonial->save($this->request->data)) {
						$this->Session->setFlash(sprintf(__('The testimonial added successfully!', true), 'Page'), 'default', array('class' => 'success'));
						$this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'));
					}
				}
				else
				{
					$this->Session->setFlash(__('Invalid file type.Please upload jpeg/png file'));
				}
			}
			else{
				$this->Session->setFlash(__('The testimonial could not be saved. Please upload image.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Testimonial->id = $id;
		if (!$this->Testimonial->exists()) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Testimonial->save($this->request->data)) {
				$this->Session->setFlash(__('The testimonial has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Testimonial->read(null, $id);
		}
	}
	public function admin_edit($id = null) {
		$this->Testimonial->id = $id;
		if (!$this->Testimonial->exists()) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$account = $this->Testimonial->find('first',array('conditions'=>array('Testimonial.id'=>$id)));
			//pr($account);die;
			if(isset($this->request->data['Testimonial']['image']['name']) && $this->request->data['Testimonial']['image']['name'] != ''){
				if($this->request->data['Testimonial']['image']['type'] == 'image/png' || $this->request->data['Testimonial']['image']['type'] == 'image/PNG' || $this->request->data['Testimonial']['image']['type'] == 'image/jpg' || $this->request->data['Testimonial']['image']['type'] == 'image/JPG' || $this->request->data['Testimonial']['image']['type'] == 'image/jpeg' || $this->request->data['Testimonial']['image']['type'] == 'image/JPEG'){
					if($account['Testimonial']['image'] != ''){
						unlink(PAGE_IMAGES.$account['Testimonial']['image']);
						//echo 'yes';die;
					}
					$filename=rand().basename($this->request->data['Testimonial']['image']['name']);
					move_uploaded_file($this->request->data['Testimonial']['image']['tmp_name'], PAGE_IMAGES.$filename);
					$this->request->data['Testimonial']['image'] =$filename;
				}
				else
				{
					$this->request->data['Testimonial']['image'] = $account['Testimonial']['image'];
					$fileTypeError = true;
				}
			}
			else{
				$this->request->data['Testimonial']['image'] = $account['Testimonial']['image'];
			}
			if ($this->Testimonial->save($this->request->data)) {
				if(isset($fileTypeError) && $fileTypeError == true){
					$this->Session->setFlash(__('Please upload png or jpg image.', true));
				}
				else{
					$this->Session->setFlash(sprintf(__('Testimonial updated successfully!', true), 'Testimonial'), 'default', array('class' => 'success'));
				}
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Testimonial->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Testimonial->id = $id;
		if (!$this->Testimonial->exists()) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		if ($this->Testimonial->delete()) {
			$this->Session->setFlash(__('Testimonial deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Testimonial was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Testimonial->id = $id;
		if (!$this->Testimonial->exists()) {
			throw new NotFoundException(__('Invalid testimonial'));
		}
		$account = $this->Testimonial->find('first',array('conditions'=>array('Testimonial.id'=>$id)));
		if ($this->Testimonial->delete()) {
			if($account['Testimonial']['image'] != ''){
				unlink(PAGE_IMAGES.$account['Testimonial']['image']);
				//echo 'yes';die;
			}
			$this->Session->setFlash(sprintf(__('Testimonial deleted successfully!', true), 'Testimonial'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Testimonial was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
