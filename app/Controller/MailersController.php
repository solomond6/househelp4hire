<?php
App::uses('AppController', 'Controller');
/**
 * Emails Controller
 *
 * @property Email $Email
 */
class MailersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Mailer->recursive = 0;
		$this->set('mailers', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Mailer->id = $id;
		if (!$this->Mailer->exists()) {
			//throw new NotFoundException(__('Invalid Coupon'));
			$this->Session->setFlash(__('Invalid Mailer.'));
		}
		$this->set('mailer', $this->Mailer->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$exist_data = $this->Mailer->find('list', array('conditions' => array('Mailer.name' => $this->request->data['Mailer']['name'])));
			if(empty($exist_data))
			{
				$this->Mailer->create();
				if ($this->Mailer->save($this->request->data)) {
					$this->Session->setFlash(sprintf(__('Mailer Added Successfully.', true), 'Mailer'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Mailer could not be saved. Please, try again.'));
				}
			}
			else {
				$this->Session->setFlash(sprintf(__('Mailer already exists.', true)));
			}	
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Mailer->id = $id;
		if (!$this->Mailer->exists()) {
			$this->Session->setFlash(__('Invalid Mailer.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$exist_data = $this->Mailer->find('list', array('conditions' => array('Mailer.name' => $this->request->data['Mailer']['name'], 'Mailer.id !=' => $this->request->data['Mailer']['id'])));
			if(empty($exist_data))
			{
				if ($this->Mailer->save($this->request->data)) {
					$this->Session->setFlash(sprintf(__('Mailer updated Successfully.', true), 'Mailer'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Mailer could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(sprintf(__('Mailer already exists.', true)));
			}
		}
		else {
			$this->request->data = $this->Mailer->read(null, $id);
		}
	}
	/*function admin_status($id = null)
	{
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid Coupon.', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->Coupon->recursive = 0;
			$status = $this->Coupon->find('first', array('conditions'=>array('Coupon.id'=>$id)));
			$this->Coupon->id=$id;
			if(isset($status['Coupon']['status']) && $status['Coupon']['status'] == 'Y') 
			{
				$this->Coupon->saveField('status','N');
				$this->Session->setFlash(sprintf(__('Coupon deactivated successfully.', true), 'Coupon'), 'default', array('class' => 'success'));
			}
			else
			{
				$this->Coupon->saveField('status','Y');
				$this->Session->setFlash(sprintf(__('Coupon activated successfully.', true), 'Coupon'), 'default', array('class' => 'success'));
			}
			$this->redirect(array('action' => 'index'));
		}
	}*/
/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			$this->Session->setFlash(__('Invalid Mailer.'));
		}
		$this->Mailer->id = $id;
		if (!$this->Mailer->exists()) {
			$this->Session->setFlash(__('Invalid Mailer.'));
		}
		if ($this->Mailer->delete()) {
			$this->Session->setFlash(sprintf(__('Mailer deleted Successfully.', true), 'Mailer'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Mailer was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
