<?php
class MembersController extends AppController {

	var $name = 'Members';
	var $uses = array('Member', 'Country', 'Newslater', 'PaymentOption', 'ResumeSearchSubscriber', 'FeaturedProfileSubscriber', 'FeaturedCompanySubscriber', 'Setting');
	/*var $components = array('Linkedin.Linkedin' => array(
        'key' => 'xa3t0nkd8qfc',
        'secret' => 'LicZCQhgKjgqoTFx',
    ));*/

	function index() {
		$this->Member->recursive = 0;
		$this->paginate = array(
        'limit' => 25,
        'order' => array(
            'Member.ship-to_state' => 'asc'
        ));
		$this->set('members', $this->paginate());
	}
	function admin_index() {
		$this->Member->recursive = 0;
		$this->set('members', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid User.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('member', $this->Member->read(null, $id));
	}
	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid User.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('member', $this->Member->read(null, $id));
	}
	
	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid User.', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Member->save($this->data)) {
				$this->Session->setFlash(__('The User has been saved.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
		$industries = $this->Member->Industry->find('list');
		$countries = $this->Member->Country->find('list');
		$this->set(compact('industries', 'countries'));
	}

	function admin_add() {
		if (!empty($this->data)) {
				$this->Member->create();
				if ($this->Member->save($this->data)) {
					$this->Session->setFlash(sprintf(__('Outlet has been saved successfully.', true), 'Member'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Outlet could not be saved. Please, try again.', true));
				}
			} 
		}

	function admin_status($id = null)
	{
		if (!$id) 
		{
			$this->Session->setFlash(sprintf(__('Invalid User.', true)));
			$this->redirect(array('action' => 'index'));
		}
		else
		{
			$this->Member->recursive = 0;
			$status = $this->Member->find('first', array('conditions'=>array('Member.id'=>$id)));
			$this->Member->id=$id;
			if(isset($status['Member']['status']) && $status['Member']['status'] == 'Y') 
			{
				$this->Member->saveField('status','N');
				$this->Session->setFlash(sprintf(__('User deactivated successfully.', true), 'Member'), 'default', array('class' => 'success'));
			}
			else
			{
				$this->Member->saveField('status','Y');
				$this->Session->setFlash(sprintf(__('User activated successfully.', true), 'Member'), 'default', array('class' => 'success'));
			}
			$this->redirect(array('action' => 'index'));
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Outlet.', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Member->save($this->data)) {
					$this->Session->setFlash(sprintf(__('Outlet has been saved successfully.', true), 'Member'), 'default', array('class' => 'success'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Outlet could not be saved. Please, try again.', true));
				}
		}
		if (empty($this->data)) {
			$this->data = $this->Member->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for User.', true));
			$this->redirect(array('action'=>'index'));
		}
		$account = $this->Member->read(null, $id);
		if ($this->Member->delete($id)) {
			$this->Session->setFlash(__('User has been deleted successfully.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted.', true));
		$this->redirect(array('action' => 'index'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Outlet.', true));
			$this->redirect(array('action'=>'index'));
		}
		$member = $this->Member->read(null, $id);
		if ($this->Member->delete($id)) {
			$this->Session->setFlash(sprintf(__('Outlet has been deleted successfully.', true), 'Member'), 'default', array('class' => 'success'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Outlet was not deleted.', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>