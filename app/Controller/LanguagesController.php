<?php
App::uses('AppController', 'Controller');
/**
 * Languages Controller
 *
 * @property Language $Language
 */
class LanguagesController extends AppController {
	public $uses = array('Language', 'Page');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Language->recursive = 0;
		$this->set('Languages', $this->paginate());
		$this->loadmodel('Page');
		$this->Page->recursive = 0;
		$Languagepage=$this->Page->find('first',array('conditions'=>array('Page.id' => 8)));
		$this->set('Languagepage',$Languagepage);
	}
	public function admin_index() {
		$this->Language->recursive = 0;
		$this->set('Languages', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Language->id = $id;
		if (!$this->Language->exists()) {
			throw new NotFoundException(__('Invalid Language'));
		}
		$this->set('Languagedetails', $this->Language->read(null, $id));
		$this->Language->recursive = 0;
		$this->set('Languages', $this->paginate());
		$this->set(compact('id'));
	}
	public function admin_view($id = null) {
		$this->Language->id = $id;
		if (!$this->Language->exists()) {
			throw new NotFoundException(__('Invalid Language'));
		}
		$this->set('Language', $this->Language->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Language->create();
			if ($this->Language->save($this->request->data)) {
				$this->Session->setFlash(__('The Language has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Language could not be saved. Please, try again.'));
			}
		}
	}
	public function admin_add() {
		if ($this->request->is('post')) {
			//pr($this->request->data);die;
			$this->Language->create();
			if ($this->Language->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The Language has been saved successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Language could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Language->id = $id;
		if (!$this->Language->exists()) {
			throw new NotFoundException(__('Invalid Language'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Language->save($this->request->data)) {
				$this->Session->setFlash(__('The Language has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Language could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Language->read(null, $id);
		}
	}
	public function admin_edit($id = null) {
		$this->Language->id = $id;
		if (!$this->Language->exists()) {
			throw new NotFoundException(__('Invalid Language'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Language->save($this->request->data)) {
				$this->Session->setFlash(sprintf(__('The Language has been updated successfully!', true), 'Page'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Language could not be updated. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Language->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Language->id = $id;
		if (!$this->Language->exists()) {
			throw new NotFoundException(__('Invalid Language'));
		}
		if ($this->Language->delete()) {
			$this->Session->setFlash(__('Language deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Language was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Language->id = $id;
		if (!$this->Language->exists()) {
			throw new NotFoundException(__('Invalid Language'));
		}
		if ($this->Language->delete()) {
				$this->Session->setFlash(sprintf(__('The Language has been deleted successfully!', true), 'Page'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Language was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_import_export($pass = null) {
		$model = $this->modelClass;
		$this->backup($model);
		$this->set('modelName', $model);
	}
}
