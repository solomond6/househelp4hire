<?php
App::uses('VettingCategory', 'Model');

/**
 * VettingCategory Test Case
 *
 */
class VettingCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vetting_category',
		'app.vetting_company',
		'app.vetting_categories'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VettingCategory = ClassRegistry::init('VettingCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VettingCategory);

		parent::tearDown();
	}

}
