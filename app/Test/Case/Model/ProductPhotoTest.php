<?php
App::uses('ProductPhoto', 'Model');

/**
 * ProductPhoto Test Case
 *
 */
class ProductPhotoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_photo',
		'app.product',
		'app.category',
		'app.subcategory',
		'app.user',
		'app.country',
		'app.order_detail',
		'app.order'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductPhoto = ClassRegistry::init('ProductPhoto');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductPhoto);

		parent::tearDown();
	}

}
