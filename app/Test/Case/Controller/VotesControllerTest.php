<?php
App::uses('VotesController', 'Controller');

/**
 * VotesController Test Case
 *
 */
class VotesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vote',
		'app.group',
		'app.collection',
		'app.run',
		'app.user',
		'app.country',
		'app.product',
		'app.category',
		'app.subcategory',
		'app.product_photo',
		'app.order_detail',
		'app.order'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
